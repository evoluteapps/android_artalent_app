package com.artalent.ui.adapter;

import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.ViewPager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextSwitcher;
import android.widget.TextView;

import com.artalent.ArtalentApplication;
import com.artalent.R;
import com.artalent.Utils;
import com.artalent.ui.activity.MainActivity;
import com.artalent.ui.activity.OtherUserProfileActivity;
import com.artalent.ui.model.AdvertiseObj;
import com.artalent.ui.model.PostObj;
import com.artalent.ui.model.UserObj;
import com.artalent.ui.utils.CircleTransformation;
import com.artalent.ui.utils.Constants;
import com.artalent.ui.pageindicator.PageIndicator;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Akshay.
 */
public class HomePostAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = "HomePostAdapter";
    private static int FEED_POST = 1;
    private static int ADVERTISE_POST = 2;
    private List<Object> feedItems;
    private RequestOptions options;
    private Context context;
    private OnFeedItemClickListener onFeedItemClickListener;

    public HomePostAdapter(Context context, List<Object> postObjs) {
        this.context = context;
        this.feedItems = postObjs;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.interest_categories)
                .error(R.drawable.interest_categories);
        if (viewType == FEED_POST) {
            View view = LayoutInflater.from(context).inflate(R.layout.item_feed, parent, false);
            HomeFeedViewHolder cellFeedViewHolder = new HomeFeedViewHolder(view, context);
            final GestureDetector gd = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener(){

                @Override
                public boolean onDoubleTap(MotionEvent e) {
                    onFeedItemClickListener.onItemDoubleClick(cellFeedViewHolder.btnLike, cellFeedViewHolder.getAdapterPosition(),cellFeedViewHolder.tsLikesCounter );
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    super.onLongPress(e);
                }

                @Override
                public boolean onDoubleTapEvent(MotionEvent e) {
                    return true;
                }

                @Override
                public boolean onDown(MotionEvent e) {
                    return true;
                }
            });

            cellFeedViewHolder.vImageRoot.setOnTouchListener((v, event) -> gd.onTouchEvent(event));
            setupClickableViews(view, cellFeedViewHolder);
            return cellFeedViewHolder;
        } else if (viewType == ADVERTISE_POST) {
            View view = LayoutInflater.from(context).inflate(R.layout.item_advertise, parent, false);
            ActiveAdvertiseViewHolder cellFeedViewHolder = new ActiveAdvertiseViewHolder(view);
            return cellFeedViewHolder;
        }

        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        if (viewHolder instanceof HomeFeedViewHolder) {
            ((HomeFeedViewHolder) viewHolder).bindView((PostObj) feedItems.get(position));
        } else {
            ((ActiveAdvertiseViewHolder) viewHolder).bindView((AdvertiseObj) feedItems.get(position));
        }
    }

    /*
    advert
     */
    class ActiveAdvertiseViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.advertise_banner)
        ImageView advertiseBanner;

        @BindView(R.id.adv_description)
        TextView advertiseDesc;

        @BindView(R.id.adv_company_name)
        TextView advCompanyName;

        @BindView(R.id.btnCompanyURL)
        Button btnCompanyWebsite;

        @BindView(R.id.ivCompanyLogo)
        ImageView advCompanyLogo;

        AdvertiseObj mAdvertiseObj;

        ActiveAdvertiseViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bindView(AdvertiseObj advertiseObj) {
            this.mAdvertiseObj = advertiseObj;
            int height = ArtalentApplication.deviceWidth * 9 / 16;

            Log.d(TAG, "bindView: AdvertiseObj -> " + advertiseObj.toString());

            if (mAdvertiseObj != null && !TextUtils.isEmpty(mAdvertiseObj.getImage())) {
                Picasso.get()
                        .load(mAdvertiseObj.getImage())
                        .resize(ArtalentApplication.deviceWidth, height)
                        .placeholder(R.drawable.feed_placeholder)
                        .centerCrop()
                        .into(advertiseBanner);
            } else {
                Picasso.get()
                        .load(R.drawable.feed_placeholder)
                        .resize(ArtalentApplication.deviceWidth, height)
                        .placeholder(R.drawable.feed_placeholder)
                        .centerCrop()
                        .into(advertiseBanner);
            }
            if(mAdvertiseObj != null && !TextUtils.isEmpty(mAdvertiseObj.getImage())){
                Glide.with(context).load(mAdvertiseObj.getImage()).apply(options).into(advCompanyLogo);
            }
            if (mAdvertiseObj != null && !TextUtils.isEmpty(mAdvertiseObj.getDescription())) {
                advertiseDesc.setText(mAdvertiseObj.getDescription());
            } else {
                advertiseDesc.setText("");
            }
            if (mAdvertiseObj != null && !TextUtils.isEmpty(mAdvertiseObj.getCompanyName())) {
                advCompanyName.setText(mAdvertiseObj.getCompanyName());
            } else {
                advCompanyName.setText("");
            }



        }
    }


    private void setupClickableViews(final View view, final HomeFeedViewHolder cellFeedViewHolder) {
        cellFeedViewHolder.btnComments.setOnClickListener(v -> onFeedItemClickListener.onCommentsClick(view, cellFeedViewHolder.getAdapterPosition()));
        cellFeedViewHolder.ivDelete.setOnClickListener(v -> onFeedItemClickListener.onDeleteClick((PostObj) v.getTag(), cellFeedViewHolder.getAdapterPosition()));

        cellFeedViewHolder.pager.setOnClickListener(v -> {
            int adapterPosition = cellFeedViewHolder.getAdapterPosition();
            ((PostObj) feedItems.get(adapterPosition)).getLikeCount();
            if (context instanceof MainActivity) {
                ((MainActivity) context).showLikedSnackbar();
            }
        });

        cellFeedViewHolder.btnLike.setOnClickListener(v -> {
            int adapterPosition = cellFeedViewHolder.getAdapterPosition();
            onFeedItemClickListener.onLikeClick(cellFeedViewHolder.btnLike, adapterPosition, cellFeedViewHolder.tsLikesCounter);
        });

        cellFeedViewHolder.btnMore.setOnClickListener(v -> {
            PostObj feedItem = (PostObj) v.getTag();
            onFeedItemClickListener.onDownloadImage(feedItem);
        });

        cellFeedViewHolder.ivUserProfile.setOnClickListener(v -> onFeedItemClickListener.onProfileClick(view));
    }


    @Override
    public int getItemViewType(int position) {
        if (feedItems.get(position) instanceof PostObj) {
            return FEED_POST;
        } else {
            return ADVERTISE_POST;
        }
    }

    @Override
    public int getItemCount() {
        return feedItems.size();
    }


    public void setOnFeedItemClickListener(OnFeedItemClickListener onFeedItemClickListener) {
        this.onFeedItemClickListener = onFeedItemClickListener;
    }

    static class HomeFeedViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.pager)
        ViewPager pager;
        @BindView(R.id.pagerPageIndicator)
        PageIndicator pageIndicator;
        @BindView(R.id.ivFeedBottom)
        TextView ivFeedBottom;
        @BindView(R.id.user_name_ll)
        RelativeLayout userNameLayout;
        @BindView(R.id.btnComments)
        ImageButton btnComments;
        @BindView(R.id.btnLike)
        ImageButton btnLike;
        @BindView(R.id.btnMore)
        ImageButton btnMore;
        @BindView(R.id.vBgLike)
        View vBgLike;
        @BindView(R.id.ivLike)
        ImageView ivLike;
        @BindView(R.id.tsLikesCounter)
        TextSwitcher tsLikesCounter;
        @BindView(R.id.ivUserProfile)
        ImageView ivUserProfile;
        @BindView(R.id.vImageRoot)
        FrameLayout vImageRoot;

        @BindView(R.id.tvUserName)
        TextView tvUserName;

        @BindView(R.id.ivDelete)
        ImageView ivDelete;


        MyPagerAdapter myPagerAdapter;
        PostObj feedItem;
        private Context context;

        HomeFeedViewHolder(View view, Context context) {
            super(view);
            ButterKnife.bind(this, view);
            this.context = context;
        }

        void bindView(PostObj feedItem) {
            this.feedItem = feedItem;
            int adapterPosition = getAdapterPosition();
            // ivFeedCenter.setImageResource(adapterPosition % 2 == 0 ? R.drawable.img3 : R.drawable.img4);
            if (feedItem != null && feedItem.getMediaUrls() != null && feedItem.getMediaUrls().size() > 0) {
                vImageRoot.setVisibility(View.VISIBLE);
                Picasso picasso = Picasso.get();
                myPagerAdapter = new MyPagerAdapter(picasso, feedItem.getMediaUrls());
                pager.setAdapter(myPagerAdapter);
                Log.d("PageIndicator", "bindView: pageIndicator Attached to pager");
                pageIndicator.attachTo(pager);
                userNameLayout.setOnClickListener(view -> {
                    UserObj userObj1 = feedItem.getUserInfo();
                    if (userObj1 != null) {
                        Intent intent = new Intent(context, OtherUserProfileActivity.class);
                        intent.putExtra(Constants.DATA, userObj1);
                        context.startActivity(intent);
                    }
                });
                ivUserProfile.setOnClickListener(view -> {
                    UserObj userObj1 = feedItem.getUserInfo();
                    if (userObj1 != null) {
                        Intent intent = new Intent(context, OtherUserProfileActivity.class);
                        intent.putExtra(Constants.DATA, userObj1);
                        context.startActivity(intent);
                    }
                });

                if (feedItem.getMediaUrls().size() > 1) {
                    pageIndicator.setVisibility(View.VISIBLE);
                } else {
                    pageIndicator.setVisibility(View.GONE);
                }
            } else {
                vImageRoot.setVisibility(View.GONE);
            }

            ivDelete.setVisibility(View.GONE);
            ivDelete.setTag(feedItem);
            if (feedItem.getUserInfo().getId() == ArtalentApplication.prefManager.getUserObj().getId()) {
                ivDelete.setVisibility(View.VISIBLE);
            }

            Log.d(TAG, "bindView - HomePostAdapter: FeedItem.getDescription -> " + feedItem.getDescription());

            if (feedItem != null) {
                if (feedItem.getDescription().equals("-")) {
                    ivFeedBottom.setVisibility(View.GONE);
                } else {
                    ivFeedBottom.setText(feedItem.getDescription());
                }
            }


            if (feedItem != null && feedItem.getUserInfo() != null) {
                tvUserName.setText(Utils.getFullName(feedItem.getUserInfo()));
            } else {
                tvUserName.setText("");
            }
            btnMore.setTag(feedItem);
            if (feedItem != null && feedItem.getUserInfo() != null && !TextUtils.isEmpty(feedItem.getUserInfo().getProfileImage())) {

                Picasso.get()
                        .load(feedItem.getUserInfo().getProfileImage())
                        .placeholder(R.drawable.user_outline)
                        .resize(itemView.getContext().getResources().getDimensionPixelSize(R.dimen.dimen_90), itemView.getContext().getResources().getDimensionPixelSize(R.dimen.dimen_90))
                        .centerCrop()
                        .transform(new CircleTransformation())
                        .into(ivUserProfile);
            } else {
                Picasso.get()
                        .load(R.drawable.man)
                        .placeholder(R.drawable.user_outline)
                        .resize(itemView.getContext().getResources().getDimensionPixelSize(R.dimen.dimen_90), itemView.getContext().getResources().getDimensionPixelSize(R.dimen.dimen_90))
                        .centerCrop()
                        .transform(new CircleTransformation())
                        .into(ivUserProfile);
            }

            // ivFeedBottom.setImageResource(adapterPosition % 2 == 0 ? R.drawable.img_feed_bottom_1 : R.drawable.img_feed_bottom_2);
            btnLike.setImageResource(feedItem.getIsLiked().equals("0") ? R.drawable.ic_favourite : R.drawable.ic_heart_red);
            tsLikesCounter.setCurrentText(vImageRoot.getResources().getQuantityString(
                    R.plurals.likes_count, feedItem.getLikeCount(), feedItem.getLikeCount()
            ));
        }
    }

    public interface OnFeedItemClickListener {
        void onCommentsClick(View v, int position);

        void onMoreClick(View v, int position);

        void onProfileClick(View v);

        void onDeleteClick(PostObj postObj, int position);

        void onItemDoubleClick(ImageButton v, int position, TextSwitcher tsLikesCounter);

        void onLikeClick(ImageButton btnLike, int position, TextSwitcher tsLikesCounter);

        void onDownloadImage(PostObj postObj);
    }


//here yourView is the View on which you want to set the double tap action
}
