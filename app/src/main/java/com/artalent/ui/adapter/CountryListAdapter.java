package com.artalent.ui.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.artalent.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Mahajan on 16-09-2019.
 */

public class CountryListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<String> countryNameList;
    private Context context;

    public CountryListAdapter(Context context, List<String> countryNameList){
        this.context = context;
        this.countryNameList = countryNameList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).
                inflate(R.layout.item_country, viewGroup, false);
        return new CountryHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        CountryHolder holder = (CountryHolder) viewHolder;
        String countryName = countryNameList.get(i);
        holder.tvCountryName.setText(countryName);
        if(countryName.equalsIgnoreCase("India")){
            holder.ivSelection.setVisibility(View.VISIBLE);
            holder.ivCountryFlag.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.india_flag));
        } else if(countryName.equalsIgnoreCase("Indonesia")){
            holder.ivCountryFlag.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.indonesia_flag));
        }else if(countryName.equalsIgnoreCase("Malaysia")){
            holder.ivCountryFlag.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.malaysia_flag));
        }else if(countryName.equalsIgnoreCase("Singapore")){
            holder.ivCountryFlag.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.singapore_flag));
        }else if(countryName.equalsIgnoreCase("Sri Lanka")){
            holder.ivCountryFlag.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.srilanka_flag));
        }else if(countryName.equalsIgnoreCase("Thailand")){
            holder.ivCountryFlag.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.thailand_flag));
        }
    }

    @Override
    public int getItemCount() {
        return countryNameList.size();
    }


    class CountryHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.country_flag)
        ImageView ivCountryFlag;
        @BindView(R.id.selection)
        ImageView ivSelection;
        @BindView(R.id.name)
        TextView tvCountryName;
        public CountryHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }

    }


}
