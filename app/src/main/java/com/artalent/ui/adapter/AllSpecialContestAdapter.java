package com.artalent.ui.adapter;

import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.artalent.R;
import com.artalent.Utils;
import com.artalent.ui.activity.SpecialContestDetailsActivity;
import com.artalent.ui.activity.SpecialContestRegisterActivity;
import com.artalent.ui.model.SpecialContestObj;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AllSpecialContestAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<SpecialContestObj> mPostObjs;
    public View.OnClickListener mOnClickListener;

    public AllSpecialContestAdapter(List<SpecialContestObj> postObjs, View.OnClickListener onClickListener) {
        this.mPostObjs = postObjs;
        this.mOnClickListener = onClickListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).
                inflate(R.layout.item_special_contest_banner_list, viewGroup, false);
        return new ActiveAdvertiseViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        ((ActiveAdvertiseViewHolder) viewHolder).bindView(mPostObjs.get(position), mOnClickListener);

    }

    @Override
    public int getItemCount() {
        return mPostObjs.size();
    }

    static class ActiveAdvertiseViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.btnRegister)
        Button btnRegister;

        @BindView(R.id.image)
        AppCompatImageView image;

        @BindView(R.id.tvEndDate)
        TextView tvEndDate;

        @BindView(R.id.special_contest_title)
        TextView special_contest_title;

        @BindView(R.id.tvWinnerPrice)
        TextView tvWinnerPrice;

        @BindView(R.id.tvContestRegCount)
        TextView tvContestRegCount;


        SpecialContestObj mSpecialContestObj;

        ActiveAdvertiseViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bindView(SpecialContestObj advertiseObj, View.OnClickListener clickListener) {
            this.mSpecialContestObj = advertiseObj;
            btnRegister.setTag(mSpecialContestObj);
            image.setTag(mSpecialContestObj);

            if (mSpecialContestObj != null && mSpecialContestObj.getTittle() != null && !TextUtils.isEmpty(mSpecialContestObj.getTittle())) {
                special_contest_title.setText(mSpecialContestObj.getTittle());
            } else {
                special_contest_title.setText("");
            }

            btnRegister.setVisibility(View.VISIBLE);
            if (mSpecialContestObj != null && !TextUtils.isEmpty(mSpecialContestObj.getRegFee())) {
                try {
                    double regFee = Double.parseDouble((mSpecialContestObj.getRegFee()));
                    btnRegister.setText("₹ " + regFee);
                } catch (NumberFormatException e) {
                    btnRegister.setText("Free");
                    e.printStackTrace();
                }
            } else {
                btnRegister.setText("Free");
            }


            if (mSpecialContestObj != null && mSpecialContestObj.getRegEndDate() != null && !TextUtils.isEmpty(mSpecialContestObj.getRegEndDate())) {
                String sCurrentDate = Utils.getCurrentDate();
                String sContestRegEndDate = mSpecialContestObj.getRegEndDate();
                if (Utils.getCurrentDiffDate(sCurrentDate, sContestRegEndDate) >= 0) {
                    tvEndDate.setText("Registration Ends On " + mSpecialContestObj.getRegEndDate());
                    ;
                } else {
                    if (mSpecialContestObj != null && mSpecialContestObj.getEndDate() != null && !TextUtils.isEmpty(mSpecialContestObj.getEndDate())) {
                        tvEndDate.setText("Contest Ends On " + mSpecialContestObj.getEndDate());
                    }
                    btnRegister.setVisibility(View.GONE);
                }

                //-------------------


            } else {
                btnRegister.setVisibility(View.GONE);
                if (mSpecialContestObj != null && mSpecialContestObj.getEndDate() != null && !TextUtils.isEmpty(mSpecialContestObj.getEndDate())) {
                    tvEndDate.setText("Contest Ends On " + mSpecialContestObj.getEndDate());
                }
            }
            if (mSpecialContestObj.getIsRegistered() != 0) {
                btnRegister.setVisibility(View.GONE);
            }

            Picasso.get().load(mSpecialContestObj.getImage())
                    .placeholder(R.drawable.chat_background)
                    .into(image);

            double price = 0.0;
            if (mSpecialContestObj != null && mSpecialContestObj.getWinnerContestWinnerList() != null && mSpecialContestObj.getWinnerContestWinnerList().size() > 0) {
                price = Utils.getSpecialContestPrice(mSpecialContestObj.getWinnerContestWinnerList());
                tvWinnerPrice.setText("Winner Prize ₹ " + price);
            }
            if (price > 0) {
                tvWinnerPrice.setVisibility(View.VISIBLE);
            } else {
                tvWinnerPrice.setVisibility(View.GONE);
            }

            if (mSpecialContestObj != null && mSpecialContestObj.getMaxRegCount() > 0) {
                tvContestRegCount.setText(mSpecialContestObj.getRegisteredCount() + "/" + mSpecialContestObj.getMaxRegCount() + " Talent");
            } else {
                tvContestRegCount.setText("");
            }

            image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SpecialContestObj itemContest = (SpecialContestObj) v.getTag();
                    if (itemContest.getIsRegistered() == 0) {

                        if (itemContest.getRegEndDate() != null && !TextUtils.isEmpty(itemContest.getRegEndDate())) {
                            String sCurrentDate = Utils.getCurrentDate();
                            String sContestRegEndDate = itemContest.getRegEndDate();
                            if (Utils.getCurrentDiffDate(sCurrentDate, sContestRegEndDate) >= 0) {
                                Intent intent = new Intent(v.getContext(), SpecialContestRegisterActivity.class);
                                intent.putExtra("contest_item", itemContest);
                                v.getContext().startActivity(intent);
                            }
                        }


                    } else {
                        Intent intent = new Intent(v.getContext(), SpecialContestDetailsActivity.class);
                        intent.putExtra("contest_item", itemContest);
                        v.getContext().startActivity(intent);
                    }
                }
            });
            btnRegister.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SpecialContestObj itemContest = (SpecialContestObj) v.getTag();
                    if (itemContest.getIsRegistered() == 0) {

                        if (itemContest.getRegEndDate() != null && !TextUtils.isEmpty(itemContest.getRegEndDate())) {
                            String sCurrentDate = Utils.getCurrentDate();
                            String sContestRegEndDate = itemContest.getRegEndDate();
                            if (Utils.getCurrentDiffDate(sCurrentDate, sContestRegEndDate) >= 0) {
                                Intent intent = new Intent(v.getContext(), SpecialContestRegisterActivity.class);
                                intent.putExtra("contest_item", itemContest);
                                v.getContext().startActivity(intent);
                            }
                        }


                    } else {
                        Intent intent = new Intent(v.getContext(), SpecialContestDetailsActivity.class);
                        intent.putExtra("contest_item", itemContest);
                        v.getContext().startActivity(intent);
                    }
                }
            });


        }
    }
}
