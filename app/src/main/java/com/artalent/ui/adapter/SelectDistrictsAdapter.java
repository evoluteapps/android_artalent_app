package com.artalent.ui.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.artalent.R;
import com.artalent.ui.model.DistrictsObj;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Mahajan on 16-09-2019.
 */

public class SelectDistrictsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<DistrictsObj> countryNameList;
    private View.OnClickListener onClickListener;
    private Context context;

    public SelectDistrictsAdapter(Context context, List<DistrictsObj> countryNameList, View.OnClickListener onClickListener1) {
        this.onClickListener = onClickListener1;
        this.countryNameList = countryNameList;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).
                inflate(R.layout.item_country, viewGroup, false);
        return new CountryHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        CountryHolder holder = (CountryHolder) viewHolder;
        DistrictsObj stateObj = countryNameList.get(i);
        holder.llMain.setOnClickListener(onClickListener);
        holder.llMain.setTag(stateObj);
        if (stateObj != null && stateObj.getDistrict() != null && !TextUtils.isEmpty(stateObj.getDistrict())) {
            holder.tvCountryName.setText(stateObj.getDistrict());
            String countryName = stateObj.getDistrict();
            if (stateObj.isCheck()) {
                holder.ivSelection.setVisibility(View.VISIBLE);
            } else {
                holder.ivSelection.setVisibility(View.GONE);
            }
            if (countryName.equalsIgnoreCase("India")) {

                holder.ivCountryFlag.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.india_flag));
            } else if (countryName.equalsIgnoreCase("USA")) {
                holder.ivCountryFlag.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.indonesia_flag));
            } else if (countryName.equalsIgnoreCase("UK")) {
                holder.ivCountryFlag.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.malaysia_flag));
            } else {
                holder.ivCountryFlag.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.india_flag));
            }
        }

    }

    @Override
    public int getItemCount() {
        return countryNameList.size();
    }


    class CountryHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.country_flag)
        ImageView ivCountryFlag;
        @BindView(R.id.selection)
        ImageView ivSelection;
        @BindView(R.id.name)
        TextView tvCountryName;

        @BindView(R.id.llMain)
        LinearLayout llMain;


        public CountryHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }

    }


}
