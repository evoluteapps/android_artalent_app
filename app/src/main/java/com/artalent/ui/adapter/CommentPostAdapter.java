package com.artalent.ui.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.artalent.ArtalentApplication;
import com.artalent.R;
import com.artalent.ui.model.CommentObj;
import com.artalent.ui.model.UserObj;
import com.artalent.ui.utils.CircleTransformation;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CommentPostAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<CommentObj> userObjList;
private View.OnClickListener mOnClickListener;

    public CommentPostAdapter(List<CommentObj> userObjList, Context context,View.OnClickListener onClickListener) {
        this.context = context;
        this.userObjList = userObjList;
        this.mOnClickListener=onClickListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).
                inflate(R.layout.item_comment_post, viewGroup, false);
        return new SearchUserHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        SearchUserHolder holder = (SearchUserHolder) viewHolder;
        CommentObj commentObj = userObjList.get(position);
        UserObj userObj = commentObj.getUserInfoPojo2();
        holder.ivDelete.setTag(position);
        holder.ivDelete.setOnClickListener(mOnClickListener);
        holder.ivDelete.setVisibility(View.GONE);
        if (userObj.getId() == ArtalentApplication.prefManager.getUserObj().getId()){
            holder.ivDelete.setVisibility(View.VISIBLE);
        }
        if (userObj != null) {
            String userName = userObj.getUserName();
            String firstName = userObj.getFirstName();
            String lastName = userObj.getLastName();

            if (!TextUtils.isEmpty(userName))
                holder.userName.setText(userName);
            if (!TextUtils.isEmpty(firstName) && !TextUtils.isEmpty(lastName))
                holder.userFullName.setText(firstName + " " + lastName);

            if (!TextUtils.isEmpty(userObj.getProfileImage())) {
                Picasso.get()
                        .load(userObj.getProfileImage())
                        .placeholder(R.drawable.user_outline)
                        .resize(context.getResources().getDimensionPixelSize(R.dimen.dimen_90), context.getResources().getDimensionPixelSize(R.dimen.dimen_90))
                        .centerCrop()
                        .transform(new CircleTransformation())
                        .into(holder.ivUserProfileThunbnail);
            } else {
                Picasso.get()
                        .load(R.drawable.man)
                        .placeholder(R.drawable.user_outline)
                        .resize(context.getResources().getDimensionPixelSize(R.dimen.dimen_90), context.getResources().getDimensionPixelSize(R.dimen.dimen_90))
                        .centerCrop()
                        .transform(new CircleTransformation())
                        .into(holder.ivUserProfileThunbnail);
            }


        }

        if (!TextUtils.isEmpty(commentObj.getComment())){
            holder.tvDes.setText(commentObj.getComment());
        }else {
            holder.tvDes.setText("");
        }

    }

    @Override
    public int getItemCount() {
        return userObjList.size();
    }

    public class SearchUserHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.user_profile)
        ImageView ivUserProfileThunbnail;

        @BindView(R.id.user_name)
        TextView userName;

        @BindView(R.id.user_full_name)
        TextView userFullName;

        @BindView(R.id.tvDes)
        TextView tvDes;

        @BindView(R.id.ivDelete)
        ImageView ivDelete;



        public SearchUserHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }

    }
}
