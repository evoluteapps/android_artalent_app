package com.artalent.ui.adapter;

import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.artalent.R;
import com.artalent.ui.model.room.ContestVideoDraft;
import com.bumptech.glide.Glide;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class DraftAdapter extends ListAdapter<ContestVideoDraft, DraftAdapter.DraftViewHolder> {

    DraftClickInterface draftClickInterface;

    public DraftAdapter(@NonNull @NotNull DiffUtil.ItemCallback<ContestVideoDraft> diffCallback, DraftClickInterface draftClickInterface) {
        super(diffCallback);
        this.draftClickInterface = draftClickInterface;
    }

    @NonNull
    @NotNull
    @Override
    public DraftViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        return new DraftViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_draft, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull DraftViewHolder holder, int position) {
        ContestVideoDraft draft = getItem(position);
        holder.bind(draft);
    }

    public class DraftViewHolder extends RecyclerView.ViewHolder {

        TextView titleTxt, noteTxt, dateTxt;
        ImageView videoThumbnail;
        ConstraintLayout parent;


        public DraftViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            titleTxt = itemView.findViewById(R.id.title_txt);
            noteTxt = itemView.findViewById(R.id.note_txt);
            dateTxt = itemView.findViewById(R.id.date_txt);
            videoThumbnail = itemView.findViewById(R.id.video_thumbnail_img);
            parent = itemView.findViewById(R.id.draft_parent);
        }

        public void bind(ContestVideoDraft draft) {
            titleTxt.setText(draft.getTitle());
            noteTxt.setText(draft.getNote());

            DateFormat dateFormat = new SimpleDateFormat("dd-mm-yyyy hh:mm:ss");
            String strDate = dateFormat.format(draft.getCreatedDate());
            dateTxt.setText(strDate);

            Glide.with(itemView.getContext())
                    .load(Uri.fromFile(new File(draft.getStoragePath())))
                    .into(videoThumbnail);

            parent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    draftClickInterface.onClick(draft);
                }
            });
        }
    }

    public interface DraftClickInterface {
        public void onClick(ContestVideoDraft contestVideoDraft);
    }
}