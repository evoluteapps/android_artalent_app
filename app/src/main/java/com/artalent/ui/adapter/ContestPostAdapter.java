package com.artalent.ui.adapter;

import android.content.Context;

import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextSwitcher;
import android.widget.TextView;

import com.artalent.ArtalentApplication;
import com.artalent.R;
import com.artalent.Utils;
import com.artalent.ui.activity.MainActivity;
import com.artalent.ui.model.PostContestObj;
import com.artalent.ui.utils.CircleTransformation;
import com.artalent.ui.view.LoadingFeedItemView;
import com.artalent.ui.pageindicator.PageIndicator;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by froger_mcs on 05.11.14.
 */
public class ContestPostAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static final String ACTION_LIKE_BUTTON_CLICKED = "action_like_button_button";
    public static final String ACTION_LIKE_IMAGE_CLICKED = "action_like_image_button";

    public static final int VIEW_TYPE_DEFAULT = 1;
    public static final int VIEW_TYPE_LOADER = 2;

    private List<PostContestObj> feedItems;
    private Context context;
    private OnFeedItemClickListener onFeedItemClickListener;

    private boolean showLoadingView = false;

    public ContestPostAdapter(Context context, List<PostContestObj> postObjs) {
        this.context = context;
        this.feedItems = postObjs;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_DEFAULT) {
            View view = LayoutInflater.from(context).inflate(R.layout.item_contest_feed, parent, false);
            CellFeedViewHolder cellFeedViewHolder = new CellFeedViewHolder(view);
            setupClickableViews(view, cellFeedViewHolder);
            return cellFeedViewHolder;
        } else if (viewType == VIEW_TYPE_LOADER) {
            LoadingFeedItemView view = new LoadingFeedItemView(context);
            view.setLayoutParams(new LinearLayoutCompat.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT)
            );
            return new LoadingCellFeedViewHolder(view);
        }

        return null;
    }

    private void setupClickableViews(final View view, final CellFeedViewHolder cellFeedViewHolder) {


        cellFeedViewHolder.pager.setOnClickListener(v -> {
            int adapterPosition = cellFeedViewHolder.getAdapterPosition();
            feedItems.get(adapterPosition).getVoteCount();
            notifyItemChanged(adapterPosition, ACTION_LIKE_IMAGE_CLICKED);
            if (context instanceof MainActivity) {
                ((MainActivity) context).showLikedSnackbar();
            }
        });
        cellFeedViewHolder.btnLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int adapterPosition = cellFeedViewHolder.getAdapterPosition();
                onFeedItemClickListener.onLikeClick(cellFeedViewHolder.btnLike, adapterPosition, cellFeedViewHolder.tsLikesCounter);
            }
        });
        cellFeedViewHolder.ivUserProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onFeedItemClickListener.onProfileClick(view);
            }
        });
        cellFeedViewHolder.btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onFeedItemClickListener.onRegisterClick();
            }
        });
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        ((CellFeedViewHolder) viewHolder).bindView(feedItems.get(position));

        if (getItemViewType(position) == VIEW_TYPE_LOADER) {
            bindLoadingFeedItem((LoadingCellFeedViewHolder) viewHolder);
        }
    }

    private void bindLoadingFeedItem(final LoadingCellFeedViewHolder holder) {
        holder.loadingFeedItemView.setOnLoadingFinishedListener(new LoadingFeedItemView.OnLoadingFinishedListener() {
            @Override
            public void onLoadingFinished() {
                showLoadingView = false;
                notifyItemChanged(0);
            }
        });
        holder.loadingFeedItemView.startLoading();
    }

    @Override
    public int getItemViewType(int position) {
        if (showLoadingView && position == 0) {
            return VIEW_TYPE_LOADER;
        } else {
            return VIEW_TYPE_DEFAULT;
        }
    }

    @Override
    public int getItemCount() {
        return feedItems.size();
    }


    public void setOnFeedItemClickListener(OnFeedItemClickListener onFeedItemClickListener) {
        this.onFeedItemClickListener = onFeedItemClickListener;
    }

    public void showLoadingView() {
        showLoadingView = true;
        notifyItemChanged(0);
    }

    public static class CellFeedViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.pager)
        ViewPager pager;
        @BindView(R.id.pagerPageIndicator)
        PageIndicator pageIndicator;
        @BindView(R.id.ivFeedBottom)
        TextView ivFeedBottom;
        @BindView(R.id.btnLike)
        ImageView btnLike;

        @BindView(R.id.btnRegister)
        Button btnRegister;

        @BindView(R.id.vBgLike)
        View vBgLike;
        @BindView(R.id.ivLike)
        ImageView ivLike;
        @BindView(R.id.tsLikesCounter)
        TextSwitcher tsLikesCounter;
        @BindView(R.id.ivUserProfile)
        ImageView ivUserProfile;
        @BindView(R.id.vImageRoot)
        FrameLayout vImageRoot;

        @BindView(R.id.tvUserName)
        TextView tvUserName;

        MyPagerAdapter myPagerAdapter;
        PostContestObj feedItem;

        public CellFeedViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bindView(PostContestObj feedItem) {
            this.feedItem = feedItem;
            int adapterPosition = getAdapterPosition();
            // ivFeedCenter.setImageResource(adapterPosition % 2 == 0 ? R.drawable.img3 : R.drawable.img4);
            if (feedItem != null && feedItem.getMediaUrls() != null && feedItem.getMediaUrls().size() > 0) {
                vImageRoot.setVisibility(View.VISIBLE);
                Picasso picasso = Picasso.get();
                myPagerAdapter = new MyPagerAdapter(picasso, feedItem.getMediaUrls());
                pager.setAdapter(myPagerAdapter);
                pageIndicator.attachTo(pager);

            } else {
                vImageRoot.setVisibility(View.GONE);
            }

            if (feedItem != null && !TextUtils.isEmpty(feedItem.getDescription())) {
                ivFeedBottom.setText(feedItem.getDescription());
            } else {
                ivFeedBottom.setText("");
            }

            if (feedItem != null && feedItem.getUserInfo() != null) {
                tvUserName.setText(Utils.getFullName(feedItem.getUserInfo()));
            } else {
                tvUserName.setText("");
            }

            if (feedItem != null && feedItem.getUserInfo() != null && feedItem.getUserInfo().getId() > 0 && ArtalentApplication.prefManager.getUserObj().getId() == feedItem.getUserInfo().getId()) {
                btnRegister.setVisibility(View.GONE);
            } else {
                btnRegister.setVisibility(View.VISIBLE);
            }


            if (feedItem != null && feedItem.getUserInfo() != null && !TextUtils.isEmpty(feedItem.getUserInfo().getProfileImage())) {
                Picasso.get()
                        .load(feedItem.getUserInfo().getProfileImage())
                        .placeholder(R.drawable.user_outline)
                        .resize(itemView.getContext().getResources().getDimensionPixelSize(R.dimen.dimen_90), itemView.getContext().getResources().getDimensionPixelSize(R.dimen.dimen_90))
                        .centerCrop()
                        .transform(new CircleTransformation())
                        .into(ivUserProfile);
            } else {
                Picasso.get()
                        .load(R.drawable.man)
                        .placeholder(R.drawable.user_outline)
                        .resize(itemView.getContext().getResources().getDimensionPixelSize(R.dimen.dimen_90), itemView.getContext().getResources().getDimensionPixelSize(R.dimen.dimen_90))
                        .centerCrop()
                        .transform(new CircleTransformation())
                        .into(ivUserProfile);
            }


            // ivFeedBottom.setImageResource(adapterPosition % 2 == 0 ? R.drawable.img_feed_bottom_1 : R.drawable.img_feed_bottom_2);
            btnLike.setImageResource(feedItem.getIsVoted().equals("0") ? R.drawable.ic_voted_grey_outline : R.drawable.voted_colored);
            tsLikesCounter.setCurrentText(vImageRoot.getResources().getQuantityString(
                    R.plurals.votes_count, feedItem.getVoteCount(), feedItem.getVoteCount()
            ));
            if (feedItem != null && feedItem.getUserInfo() != null && feedItem.getUserInfo().getId() > 0 && feedItem.getUserInfo().getId() == ArtalentApplication.prefManager.getUserObj().getId()) {
                btnLike.setVisibility(View.GONE);
            } else {
                btnLike.setVisibility(View.VISIBLE);
            }
        }

        public PostContestObj getFeedItem() {
            return feedItem;
        }
    }

    public static class LoadingCellFeedViewHolder extends CellFeedViewHolder {

        LoadingFeedItemView loadingFeedItemView;

        public LoadingCellFeedViewHolder(LoadingFeedItemView view) {
            super(view);
            this.loadingFeedItemView = view;
        }

        @Override
        public void bindView(PostContestObj feedItem) {
            super.bindView(feedItem);
        }
    }

    public static class FeedItem {
        public int likesCount;
        public boolean isLiked;

        public FeedItem(int likesCount, boolean isLiked) {
            this.likesCount = likesCount;
            this.isLiked = isLiked;
        }
    }

    public interface OnFeedItemClickListener {
        void onCommentsClick(View v, int position);

        void onRegisterClick();

        void onProfileClick(View v);

        void onLikeClick(ImageView btnLike, int position, TextSwitcher tsLikesCounter);
    }
}
