package com.artalent.ui.adapter;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.artalent.ArtalentApplication;
import com.artalent.R;
import com.artalent.ui.model.SpecialContestObj;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SpecialContestAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<SpecialContestObj> mPostObjs;
    public View.OnClickListener mOnClickListener;

    public SpecialContestAdapter(List<SpecialContestObj> postObjs, View.OnClickListener onClickListener) {
        this.mPostObjs = postObjs;
        this.mOnClickListener = onClickListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).
                inflate(R.layout.item_special_contest, viewGroup, false);
        return new ActiveAdvertiseViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        ((ActiveAdvertiseViewHolder) viewHolder).bindView(mPostObjs.get(position), mOnClickListener);

    }

    @Override
    public int getItemCount() {
        return mPostObjs.size();
    }

    static class ActiveAdvertiseViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.advertise_banner)
        ImageView advertiseBanner;

        @BindView(R.id.adv_description)
        TextView advertiseDesc;

        @BindView(R.id.adv_company_name)
        TextView advCompanyName;

        @BindView(R.id.interest_category)
        TextView tvInterestCategory;

        @BindView(R.id.btnDelete)
        ImageView btnDelete;

        @BindView(R.id.btnEditContest)
        ImageView btnEditContest;


        SpecialContestObj mSpecialContestObj;

        ActiveAdvertiseViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bindView(SpecialContestObj advertiseObj, View.OnClickListener clickListener) {
            this.mSpecialContestObj = advertiseObj;
            int height = ArtalentApplication.deviceWidth * 9 / 16;
            if (mSpecialContestObj != null && !TextUtils.isEmpty(mSpecialContestObj.getImage())) {
                Picasso.get()
                        .load(mSpecialContestObj.getImage())
//                        .resize(ArtalentApplication.deviceWidth, height)
                        .placeholder(R.drawable.feed_placeholder)
//                        .centerCrop()
                        .into(advertiseBanner);
            } else {
                Picasso.get()
                        .load(R.drawable.feed_placeholder)
                        .resize(ArtalentApplication.deviceWidth, height)
                        .placeholder(R.drawable.feed_placeholder)
                        .centerCrop()
                        .into(advertiseBanner);
            }
            if (mSpecialContestObj != null && !TextUtils.isEmpty(mSpecialContestObj.getDescription())) {
                advertiseDesc.setText(mSpecialContestObj.getDescription());
            } else {
                advertiseDesc.setText("");
            }

            if (mSpecialContestObj != null && !TextUtils.isEmpty(mSpecialContestObj.getTittle())) {
                advCompanyName.setText(mSpecialContestObj.getTittle());
            } else {
                advCompanyName.setText("");
            }

            if (mSpecialContestObj != null && !TextUtils.isEmpty(mSpecialContestObj.getInterestObj().getInterest())) {
                tvInterestCategory.setText(mSpecialContestObj.getInterestObj().getInterest());
            } else {
                tvInterestCategory.setText("");
            }
            btnDelete.setTag(mSpecialContestObj);
            btnDelete.setOnClickListener(clickListener);

            btnEditContest.setTag(mSpecialContestObj);
            btnEditContest.setOnClickListener(clickListener);


        }
    }
}
