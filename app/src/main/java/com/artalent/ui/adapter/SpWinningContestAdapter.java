package com.artalent.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.artalent.R;
import com.artalent.Utils;
import com.artalent.ui.activity.OtherUserProfileActivity;
import com.artalent.ui.model.SpecialContestObj;
import com.artalent.ui.utils.Constants;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SpWinningContestAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = "SpWinningContestAdapter";
    private List<SpecialContestObj> contestList;
    private RequestOptions options;
    private Context context;

    public SpWinningContestAdapter(List<SpecialContestObj> contestList, Context context) {
        //this.contestHistoryObjs = interestNameList;
        this.contestList = contestList;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.person_placeholder)
                .error(R.drawable.person_placeholder);
        View v = LayoutInflater.from(viewGroup.getContext()).
                inflate(R.layout.item_winning_contest, viewGroup, false);
        return new WinningContetViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {

        WinningContetViewHolder holder = (WinningContetViewHolder) viewHolder;
        SpecialContestObj contestObj = contestList.get(i);
        holder.cardView.setTag(contestObj);
        Log.d(TAG, "onBindViewHolder: SpecialContestObj -> "
                + contestList.get(i));
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SpecialContestObj contestObjView = (SpecialContestObj) v.getTag();
                /*if (contestObjView != null && contestObjView.getWinner1() != null && contestObjView.getWinner1().getId() > 0) {
                    Intent intent = new Intent(context, OtherUserProfileActivity.class);
                    intent.putExtra(Constants.DATA, contestObjView.getWinner1());
                    context.startActivity(intent);
                }*/

                if (contestObjView != null && contestObjView.getAdminContestResultWinners().get(i).getWinnerUser() != null) {
                    Intent intent = new Intent(context, OtherUserProfileActivity.class);
                    intent.putExtra(Constants.DATA, contestObjView.getAdminContestResultWinners().get(i).getWinnerUser());
                    context.startActivity(intent);
                }
            }
        });
        try {
            holder.tvContestTitle.setText("" + contestObj.getTittle());
        } catch (Exception e) {
            holder.tvContestTitle.setText("");
            e.printStackTrace();
        }

        holder.tvContestWinnerName.setText("");
        holder.ivUserIcon.setImageResource(R.drawable.person_placeholder);
        if (contestObj != null && contestObj.getAdminContestResultWinners() != null && contestObj.getAdminContestResultWinners().get(0).getWinnerUser().getId() > 0) {

            if (contestObj.getAdminContestResultWinners().get(0).getWinnerUser().getProfileImage() != null && !TextUtils.isEmpty(contestObj.getAdminContestResultWinners().get(0).getWinnerUser().getProfileImage())) {
                /*Glide.with(context)
                        .load(contestObj.getWinner1().getProfileImage()).apply(new RequestOptions()
                        .centerCrop()
                        .placeholder(R.drawable.interest_categories)
                        .transform(new RoundedCorners(120))
                        .error(R.drawable.interest_categories)).into(holder.ivUserIcon);*/
                Utils.setImageRounded(context, contestObj.getAdminContestResultWinners().get(0).getWinnerUser().getProfileImage(), holder.ivUserIcon);
            }


            try {
                holder.tvContestWinnerName.setText(contestObj.getAdminContestResultWinners().get(0).getWinnerUser().getFirstName() + " ");
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                holder.tvContestWinnerName.append(contestObj.getAdminContestResultWinners().get(0).getWinnerUser().getLastName());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public int getItemCount() {
        return contestList.size();
    }

    public class WinningContetViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.contest_title)
        TextView tvContestTitle;

        @BindView(R.id.cardView)
        CardView cardView;


        @BindView(R.id.contest_winner_name)
        TextView tvContestWinnerName;

        @BindView(R.id.user_icon)
        ImageView ivUserIcon;

        WinningContetViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
