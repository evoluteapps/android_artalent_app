package com.artalent.ui.adapter;

import android.app.AlertDialog;
import android.content.Context;

import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.artalent.ArtalentApplication;
import com.artalent.R;
import com.artalent.Utils;
import com.artalent.ui.fragment.ProfileFragment;
import com.artalent.ui.model.BaseResponse;
import com.artalent.ui.model.PostObj;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Rohit mahajan on 18/05/16.
 */
public class MyPostAdapter extends RecyclerView.Adapter<MyPostAdapter.MyViewHolder> {

    private Context mContext;
    private List<PostObj> albumList;
    private OnProfileDataChangedListener onProfileDataChangedListener;

    private static final String TAG = "MyPostAdapter";

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, count;
        public ImageView thumbnail, overflow;

        public MyViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.title);
            count = view.findViewById(R.id.count);
            thumbnail = view.findViewById(R.id.thumbnail);
            overflow = view.findViewById(R.id.overflow);
        }
    }

    View.OnClickListener mOnClickListener;

    public MyPostAdapter(Context mContext, List<PostObj> albumList, View.OnClickListener onClickListener) {
        this.mContext = mContext;
        this.albumList = albumList;
        this.mOnClickListener = onClickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_my_post, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        PostObj album = albumList.get(position);
        //holder.title.setText(album.getName());
        //holder.count.setText(album.getNumOfSongs() + " songs");

        // loading album cover using Glide library
        Log.d(TAG, "onBindViewHolder: PostObje albun : MYPOSTADAPTER -" + position + ": " + album.toString());
        if (album != null && album.getMediaUrls() != null && album.getMediaUrls().size() > 0) {
//            Picasso.get().load(album.getMediaUrls().get(0)).resize(150, 150).placeholder(R.drawable.feed_placeholder).into(holder.thumbnail);
            Glide
                    .with(mContext)
                    .load(album.getMediaUrls().get(0))
                    .override(250)
                    .fitCenter()
                    .into(holder.thumbnail);
        } else {
            Picasso.get().load(R.drawable.feed_placeholder).into(holder.thumbnail);
        }
        holder.thumbnail.setTag(album);

        holder.thumbnail.setOnClickListener(mOnClickListener);

        if (album.getUserInfo().getId() == ArtalentApplication.prefManager.getUserObj().getId()) {
            holder.thumbnail.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    selectedPostObj = (PostObj) v.getTag();
                    showPopupMenu(holder.thumbnail);
                    return true;
                }
            });
        }
    }

    private PostObj selectedPostObj = null;

    /**
     * Showing popup menu when tapping on 3 dots
     */
    private void showPopupMenu(View view) {
        // inflate menu
        PopupMenu popup = new PopupMenu(mContext, view);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.menu_post_delete, popup.getMenu());
        popup.setOnMenuItemClickListener(new MyMenuItemClickListener());
        popup.show();
    }

    /**
     * Click listener for popup menu items
     */
    class MyMenuItemClickListener implements PopupMenu.OnMenuItemClickListener {

        public MyMenuItemClickListener() {
        }

        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            switch (menuItem.getItemId()) {
                /*case R.id.action_add_favourite:
                    Toast.makeText(mContext, "Add to favourite", Toast.LENGTH_SHORT).show();
                    return true;*/
                case R.id.action_play_next:
                    openDelete(selectedPostObj);
                    return true;
                default:
            }
            return false;
        }
    }

    @Override
    public int getItemCount() {
        Log.d(TAG, "getItemCount: - " + albumList.size());
        return albumList.size();
    }

    public void openDelete(PostObj postObj) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
        alertDialogBuilder.setMessage("Are you sure, You want to delete post?");
        alertDialogBuilder.setPositiveButton("yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        arg0.dismiss();
                        deletePost(postObj);
                    }
                });

        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void deletePost(PostObj postObj) {
        if (Utils.isInternetAvailable(mContext)) {
            ArtalentApplication.apiService.deletePost(ArtalentApplication.prefManager.getUserObj().getId(), postObj.getPostId())
                    .enqueue(new Callback<BaseResponse>() {
                        @Override
                        public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                            if (response.body() != null) {
                                onProfileDataChangedListener.onProfileDataChanged();
                                int po = -1;
                                for (int i = 0; i < albumList.size(); i++) {
                                    if (albumList.get(i).getPostId() == selectedPostObj.getPostId()) {
                                        po = i;
                                    }
                                }
                                if (po != -1) {
                                    albumList.remove(po);
                                    selectedPostObj = null;
                                    notifyDataSetChanged();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<BaseResponse> call, Throwable t) {

                        }
                    });
        }
    }

    public interface OnProfileDataChangedListener {
        void onProfileDataChanged();
    }

    public void setOnProfileDataChangedListener(OnProfileDataChangedListener onProfileDataChangedListener) {
        this.onProfileDataChangedListener = onProfileDataChangedListener;
    }
}
