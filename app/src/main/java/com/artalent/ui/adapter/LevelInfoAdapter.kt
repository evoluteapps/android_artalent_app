package com.artalent.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.artalent.R
import com.artalent.ui.model.LevelInfoModel

class LevelInfoAdapter(private val levelList: List<LevelInfoModel>) :
    RecyclerView.Adapter<LevelInfoAdapter.MyViewHolder>() {

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        lateinit var levelTxt: TextView
        lateinit var priceTxt: TextView
        lateinit var rangeTxt: TextView

        fun bind(levelInfoModel: LevelInfoModel) {
            levelTxt = itemView.findViewById(R.id.level_txt)
            priceTxt = itemView.findViewById(R.id.price_txt)
            rangeTxt = itemView.findViewById(R.id.range_txt)

            levelTxt.text = levelInfoModel.level
            priceTxt.text = levelInfoModel.price
            rangeTxt.text = levelInfoModel.range
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_level_info, parent, false)
        )
    }

    override fun getItemCount(): Int = levelList.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(levelList[position])
    }
}