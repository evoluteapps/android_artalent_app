package com.artalent.ui.adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.artalent.R;
import com.otaliastudios.cameraview.filter.Filters;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VideoFilterAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Filters[] mAllFilters;
    private View.OnClickListener mOnClickListener;

    public VideoFilterAdapter(Filters[] allFilters, View.OnClickListener onClickListener) {
        this.mAllFilters = allFilters;
        this.mOnClickListener = onClickListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).
                inflate(R.layout.item_video_filter, viewGroup, false);
        return new WinningContetViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        WinningContetViewHolder holder = (WinningContetViewHolder) viewHolder;
        Filters filter = mAllFilters[i];
        holder.tvFilter.setText(((filter.toString()).replaceAll("_", " ")).toLowerCase());
        holder.tvFilter.setTag(filter);
        holder.tvFilter.setOnClickListener(mOnClickListener);
    }

    @Override
    public int getItemCount() {
        return mAllFilters.length;
    }

    class WinningContetViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvFilter)
        TextView tvFilter;


        WinningContetViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
