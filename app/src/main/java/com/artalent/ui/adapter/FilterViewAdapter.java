package com.artalent.ui.adapter;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.artalent.R;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import ja.burhanrashid52.photoeditor.PhotoEditor;
import ja.burhanrashid52.photoeditor.PhotoEditorView;
import ja.burhanrashid52.photoeditor.PhotoFilter;


public class FilterViewAdapter extends RecyclerView.Adapter<FilterViewAdapter.ViewHolder> {

    private static final String TAG = "FilterViewAdapter";
    private FilterListener mFilterListener;
    private List<Pair<String, PhotoFilter>> mPairList = new ArrayList<>();
    private Bitmap currentBitmap;
    //    private ArrayList<Bitmap> currentBitmap;
    private PhotoEditor mPhotoEditor = null;

    public FilterViewAdapter(FilterListener filterListener) {
        Log.d(TAG, "FilterViewAdapter: constructor called");
        mFilterListener = filterListener;
        setupFilters();
    }

    public void getBitmap(Bitmap bitmap) {
        Log.d(TAG, "getBitmap: " + bitmap.toString());
        currentBitmap = bitmap;
    }

//    public void getBitmapList(ArrayList<Bitmap> bitmap) {
//        Log.d(TAG, "getBitmap: " + bitmap.toString());
//        currentBitmap = bitmap;
//    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Log.d(TAG, "onCreateViewHolder: called");
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_filter_view, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Log.d(TAG, "onBindViewHolder: called");
        Pair<String, PhotoFilter> filterPair = mPairList.get(position);
//        Bitmap fromAsset = getBitmapFromAsset(holder.itemView.getContext(), filterPair.first);
//        holder.mImageFilterView.setImageBitmap(fromAsset);
        mPhotoEditor = null;
        mPhotoEditor = new PhotoEditor.Builder(holder.itemView.getContext(), holder.mImageFilterView).build();
        holder.mImageFilterView.getSource().setImageBitmap(currentBitmap);
        mPhotoEditor.setFilterEffect(filterPair.second);

//        holder.mImageFilterView.getSource().setImageBitmap(currentBitmap.get(position));

        holder.mTxtFilterName.setText(filterPair.second.name().replace("_", " "));
    }

    @Override
    public int getItemCount() {
        Log.d(TAG, "getItemCount: called");
        return mPairList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        //        ImageView mImageFilterView;
        PhotoEditorView mImageFilterView;
        TextView mTxtFilterName;

        ViewHolder(View itemView) {
            super(itemView);
            Log.d(TAG, "ViewHolder: constructor called");
            this.setIsRecyclable(false);
            mImageFilterView = itemView.findViewById(R.id.imgFilterView);
            mTxtFilterName = itemView.findViewById(R.id.txtFilterName);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mFilterListener.onFilterSelected(mPairList.get(getLayoutPosition()).second);
                }
            });
        }
    }

    private Bitmap getBitmapFromAsset(Context context, String strName) {
        AssetManager assetManager = context.getAssets();
        InputStream istr = null;
        try {
            istr = assetManager.open(strName);
            return BitmapFactory.decodeStream(istr);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private void setupFilters() {
        mPairList.add(new Pair<>("filters/original.jpg", PhotoFilter.NONE));
        mPairList.add(new Pair<>("filters/auto_fix.png", PhotoFilter.AUTO_FIX));
//        mPairList.add(new Pair<>("filters/brightness.png", PhotoFilter.BRIGHTNESS));
        mPairList.add(new Pair<>("filters/contrast.png", PhotoFilter.CONTRAST));
        mPairList.add(new Pair<>("filters/documentary.png", PhotoFilter.DOCUMENTARY));
//        mPairList.add(new Pair<>("filters/dual_tone.png", PhotoFilter.DUE_TONE));
//        mPairList.add(new Pair<>("filters/fill_light.png", PhotoFilter.FILL_LIGHT));
//        mPairList.add(new Pair<>("filters/fish_eye.png", PhotoFilter.FISH_EYE));
        mPairList.add(new Pair<>("filters/grain.png", PhotoFilter.GRAIN));
        mPairList.add(new Pair<>("filters/gray_scale.png", PhotoFilter.GRAY_SCALE));
//        mPairList.add(new Pair<>("filters/lomish.png", PhotoFilter.LOMISH));
//        mPairList.add(new Pair<>("filters/negative.png", PhotoFilter.NEGATIVE));
//        mPairList.add(new Pair<>("filters/posterize.png", PhotoFilter.POSTERIZE));
        mPairList.add(new Pair<>("filters/saturate.png", PhotoFilter.SATURATE));
//        mPairList.add(new Pair<>("filters/sepia.png", PhotoFilter.SEPIA));
//        mPairList.add(new Pair<>("filters/sharpen.png", PhotoFilter.SHARPEN));
        mPairList.add(new Pair<>("filters/temprature.png", PhotoFilter.TEMPERATURE));
//        mPairList.add(new Pair<>("filters/tint.png", PhotoFilter.TINT));
        mPairList.add(new Pair<>("filters/vignette.png", PhotoFilter.VIGNETTE));
        mPairList.add(new Pair<>("filters/cross_process.png", PhotoFilter.CROSS_PROCESS));
//        mPairList.add(new Pair<>("filters/b_n_w.png", PhotoFilter.BLACK_WHITE));
//        mPairList.add(new Pair<>("filters/flip_horizental.png", PhotoFilter.FLIP_HORIZONTAL));
//        mPairList.add(new Pair<>("filters/flip_vertical.png", PhotoFilter.FLIP_VERTICAL));
//        mPairList.add(new Pair<>("filters/rotate.png", PhotoFilter.ROTATE));
    }
}
