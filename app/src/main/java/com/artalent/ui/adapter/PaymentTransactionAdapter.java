package com.artalent.ui.adapter;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.artalent.DateFormatter;
import com.artalent.R;
import com.artalent.ui.model.WalletTransactionListObj;
import com.artalent.ui.utils.AmountType;
import com.artalent.ui.utils.TransactionType;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PaymentTransactionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<WalletTransactionListObj> transactionListObjs;
    private boolean isShowAll;
    private Context context;

    public PaymentTransactionAdapter(List<WalletTransactionListObj> transactionListObjs, boolean isShowAll, Context context) {
        this.transactionListObjs = transactionListObjs;
        this.isShowAll = isShowAll;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_transaction, parent, false);

        return new PaymentTransactionViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        PaymentTransactionViewHolder holder = (PaymentTransactionViewHolder) viewHolder;
        WalletTransactionListObj walletTransactionListObj = transactionListObjs.get(i);
        if (walletTransactionListObj != null) {

            if (!TextUtils.isEmpty(walletTransactionListObj.getCreatedAt())) {
                holder.tvDate.setText(DateFormatter.INSTANCE.getFormattedByString("yyyy-MM-dd HH:mm:ss", "MMMM dd, yyyy", walletTransactionListObj.getCreatedAt()));
            } else {
                holder.tvDate.setText("");
            }
            if (!TextUtils.isEmpty(walletTransactionListObj.getDescription())) {
                holder.descriptionTitle.setText(walletTransactionListObj.getDescription());
            } else {
                holder.descriptionTitle.setText("");
            }
            holder.walletAmount.setText(context.getResources().getString(R.string.rupee) + " " + walletTransactionListObj.getAmount());
            if (walletTransactionListObj.getAmountType().equalsIgnoreCase(String.valueOf(AmountType.REFERRAL))) {

                if (walletTransactionListObj.getTransactionType().equalsIgnoreCase(String.valueOf(TransactionType.CREDIT))) {
                    holder.walletRewardType.setText("Refer & Earn Bonus");
                    holder.walletAmount.setTextColor(context.getResources().getColor(R.color.green));
                    holder.walletRewardType.setTextColor(context.getResources().getColor(R.color.green));
                } else {
                    holder.walletRewardType.setText("Debit From Refer");
                    holder.walletAmount.setTextColor(context.getResources().getColor(R.color.red));
                    holder.walletRewardType.setTextColor(context.getResources().getColor(R.color.red));
                }
            } else if (walletTransactionListObj.getAmountType().equalsIgnoreCase(String.valueOf(AmountType.WALLET))) {
                if (walletTransactionListObj.getTransactionType().equalsIgnoreCase(String.valueOf(TransactionType.CREDIT))) {
                    holder.walletAmount.setTextColor(context.getResources().getColor(R.color.green));
                    holder.walletRewardType.setTextColor(context.getResources().getColor(R.color.green));
                    holder.walletRewardType.setText("Credit");
                } else {
                    holder.walletAmount.setTextColor(context.getResources().getColor(R.color.red));
                    holder.walletRewardType.setTextColor(context.getResources().getColor(R.color.red));
                    holder.walletRewardType.setText("Debit From Wallet");
                }
            }
        }
    }

    @Override
    public int getItemCount() {
        if (isShowAll) {
            return transactionListObjs.size();
        } else {
            if (transactionListObjs.size() > 3) {
                return 3;
            } else {
                return transactionListObjs.size();
            }
        }
    }

    class PaymentTransactionViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.expiry_date)
        TextView walletExpiryDate;

        @BindView(R.id.amount)
        TextView walletAmount;

        @BindView(R.id.reward_type)
        TextView walletRewardType;

        @BindView(R.id.description_title)
        TextView descriptionTitle;

        @BindView(R.id.tvDate)
        TextView tvDate;


        PaymentTransactionViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
