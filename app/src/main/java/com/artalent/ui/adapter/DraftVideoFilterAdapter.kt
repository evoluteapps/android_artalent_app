package com.artalent.ui.adapter

import android.media.Image
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.artalent.R
import com.otaliastudios.cameraview.filter.Filters

class DraftVideoFilterAdapter(val filterList: Array<Filters>) :
    RecyclerView.Adapter<DraftVideoFilterAdapter.MyViewHolder>() {

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val filterNameTxt: TextView = itemView.findViewById<TextView>(R.id.filter_name_txt)
        val filterPreviewImg: ImageView = itemView.findViewById<ImageView>(R.id.filter_preview_img)

        fun bind(filter: Filters) {
            filterNameTxt.text = filter.name
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_draft_video_filter, parent, false)
        )
    }

    override fun getItemCount(): Int = filterList.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val filter = filterList[position]
        holder.bind(filter)
    }
}