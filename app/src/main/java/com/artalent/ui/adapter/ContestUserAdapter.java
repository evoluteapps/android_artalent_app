package com.artalent.ui.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.artalent.R;
import com.artalent.ui.model.ContestRegistrationObj;
import com.artalent.ui.model.InterestObj;
import com.artalent.ui.model.UserObj;
import com.artalent.ui.utils.Constants;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ContestUserAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<ContestRegistrationObj> contestRegistrationObjList;
    private Activity activity;

    public ContestUserAdapter(List<ContestRegistrationObj> contestRegistrationObjList, Activity activity) {
        this.contestRegistrationObjList = contestRegistrationObjList;
        this.activity = activity;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).
                inflate(R.layout.item_contest_registered_user, viewGroup, false);
        return new ContestUserViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        ContestUserViewHolder holder = (ContestUserViewHolder) viewHolder;
        ContestRegistrationObj contestRegistrationObj = contestRegistrationObjList.get(position);
        if (contestRegistrationObj != null) {

            UserObj userObj = contestRegistrationObj.getUser();
            String userName = userObj.getUserName();
            String firstName = userObj.getFirstName();
            String lastName = userObj.getLastName();

            if (!TextUtils.isEmpty(userName))
                holder.userName.setText(userName);
            if (!TextUtils.isEmpty(firstName) && !TextUtils.isEmpty(lastName))
                holder.userFullName.setText(firstName + " " + lastName);

            List<InterestObj> interestObjList = userObj.getInterest();
            InterestListAdapter interestListAdapter = new InterestListAdapter(interestObjList, Constants.INTEREST_SEARCH_LIST_NAME, activity);
            @SuppressLint("WrongConstant") LinearLayoutManager childLayoutManager = new LinearLayoutManager(holder.recyclerView.getContext(), LinearLayout.HORIZONTAL, false);
            holder.recyclerView.setLayoutManager(childLayoutManager);
            holder.recyclerView.setAdapter(interestListAdapter);
        }
    }

    @Override
    public int getItemCount() {
        return contestRegistrationObjList.size();
    }

    public class ContestUserViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.user_profile)
        ImageView ivUserProfileThunbnail;

        @BindView(R.id.user_name)
        TextView userName;

        @BindView(R.id.user_full_name)
        TextView userFullName;

        @BindView(R.id.recycler_view_interest)
        RecyclerView recyclerView;

        public ContestUserViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
