package com.artalent.ui.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.artalent.R;
import com.artalent.ui.model.InterestObj;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Mahajan on 16-09-2019.
 */

public class FilterInterestAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<InterestObj> interestNameList;
    private View.OnClickListener onClickListener;
    private Context context;

    public FilterInterestAdapter(Context context, List<InterestObj> interestNameList, View.OnClickListener onClickListener1) {
        this.onClickListener = onClickListener1;
        this.interestNameList = interestNameList;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).
                inflate(R.layout.item_filter_interest, viewGroup, false);
        return new CountryHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        CountryHolder holder = (CountryHolder) viewHolder;
        InterestObj interest = interestNameList.get(i);
        holder.llMain.setOnClickListener(onClickListener);
        holder.llMain.setTag(interest);
        
        if (interest != null && interest.getInterest() != null && !TextUtils.isEmpty(interest.getInterest())) {
            holder.tvInterestName.setText(interest.getInterest());
            if (interest.isCheck()) {
                holder.ivSelection.setImageResource(R.drawable.tick_active);
            } else {
                holder.ivSelection.setImageResource(R.drawable.tick);
            }
        }
    }

    @Override
    public int getItemCount() {
        return interestNameList.size();
    }

    class CountryHolder extends RecyclerView.ViewHolder {
        
        @BindView(R.id.selection)
        ImageView ivSelection;
        @BindView(R.id.name)
        TextView tvInterestName;
        @BindView(R.id.llMain)
        LinearLayout llMain;

        public CountryHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
