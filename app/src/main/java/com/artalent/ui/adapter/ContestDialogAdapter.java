package com.artalent.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.artalent.R;
import com.artalent.Utils;
import com.artalent.ui.activity.AddPostActivity;
import com.artalent.ui.activity.MainActivity;
import com.artalent.ui.model.ContestObj;
import com.artalent.ui.model.SpecialContestObj;
import com.artalent.ui.model.room.ContestVideoDraft;
import com.artalent.ui.utils.Constants;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ContestDialogAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "ContestDialogAdapter";
    private Context context;
    private List<Object> contestList;
    private RequestOptions options;

    public ContestDialogAdapter(Context context, List<Object> contestList) {
        this.context = context;
        this.contestList = contestList;
    }

    private ContestVideoDraft contestVideoDraft = null;
    private boolean isFromDraftAdapterToBottomSheet = false;

    public ContestDialogAdapter(Context context, List<Object> contestList, ContestVideoDraft contestVideoDraft, boolean isFromDraftAdapterToBottomSheet) {
        this.context = context;
        this.contestList = contestList;
        this.contestVideoDraft = contestVideoDraft;
        this.isFromDraftAdapterToBottomSheet = isFromDraftAdapterToBottomSheet;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).
                inflate(R.layout.item_contest_bottom_sheet, viewGroup, false);
        options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.interest_categories)
                .error(R.drawable.interest_categories);

        return new ContestDialogViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        ContestDialogViewHolder holder = (ContestDialogViewHolder) viewHolder;
        Object o = contestList.get(i);
        String contestTitle, location;
        if (o instanceof ContestObj) {
            ContestObj contestObj = (ContestObj) o;

            Log.d(TAG, "onBindViewHolder: contest Obj :" + contestObj.toString());

            holder.itemView.setTag(contestObj);
            holder.tvContestTitle.setVisibility(View.VISIBLE);
            holder.tvSpecialContestTitle.setVisibility(View.GONE);
            if (contestObj != null && contestObj.getInterest() != null && contestObj.getInterest().getBaner() != null && !TextUtils.isEmpty(contestObj.getInterest().getBaner())) {
                Utils.setImageRounded(context, contestObj.getInterest().getBaner(), holder.ivContestIcon);
            } else {
                holder.ivContestIcon.setImageResource(R.drawable.singing);
            }
            if (contestObj.getFlag() == 1) {
                location = contestObj.getDistrictEntity().getDistrict();
                contestTitle = contestObj.getInterest().getInterest() + " Contest for " + location;
                holder.tvContestTitle.setText(contestTitle);
                holder.tvContestLocation.setText(contestObj.getDistrictEntity().getDistrict());
            } else if (contestObj.getFlag() == 2) {
                location = contestObj.getStateEntity().getState();
                contestTitle = contestObj.getInterest().getInterest() + " Contest for " + location;
                holder.tvContestTitle.setText(contestTitle);
                holder.tvContestLocation.setText(contestObj.getStateEntity().getState());
            } else {
                location = contestObj.getCountryEntity().getCountryName();
                contestTitle = contestObj.getInterest().getInterest() + " Contest for " + location;
                holder.tvContestTitle.setText(contestTitle);
                holder.tvContestLocation.setText(contestObj.getCountryEntity().getCountryName());
            }
            holder.tvContestDescription.setText("You can post your " + contestObj.getInterest().getInterest() + " talent.");
        } else if (o instanceof SpecialContestObj) {
            Animation animShake = AnimationUtils.loadAnimation(context, R.anim.wobble);
            SpecialContestObj specialContestObj = (SpecialContestObj) o;

            if (specialContestObj != null && specialContestObj.getInterestObj() != null && specialContestObj.getInterestObj().getBaner() != null && !TextUtils.isEmpty(specialContestObj.getInterestObj().getBaner())) {
                Glide.with(context)
                        .load(specialContestObj.getInterestObj().getBaner()).apply(options).into(holder.ivContestIcon);
            } else {
                holder.ivContestIcon.setImageResource(R.drawable.singing);
            }
            holder.itemView.startAnimation(animShake);
            holder.tvContestTitle.setVisibility(View.GONE);
            holder.tvSpecialContestTitle.setVisibility(View.VISIBLE);
            holder.tvSpecialContestTitle.setText(specialContestObj.getTittle());
            holder.tvContestDescription.setText(specialContestObj.getDescription());
            holder.tvContestLocation.setText(specialContestObj.getCountry().getCountryName());
            holder.itemView.setTag(specialContestObj);
        }

        if (isFromDraftAdapterToBottomSheet) {
            Log.d("ContestDialogAdapter", "onBindViewHolder: draftobj-> " + contestVideoDraft.toString());
            Log.d("ContestDialogAdapter", "onBindViewHolder: isFromDraftAdapterToBottomSheet-> " + isFromDraftAdapterToBottomSheet);
        }

        holder.itemView.setOnClickListener(v -> {
            if (context instanceof MainActivity) {
                if (((MainActivity) context).bottomSheetFragment != null) {
                    ((MainActivity) context).bottomSheetFragment.dismiss();
                }
            }
            Intent intent = new Intent(context, AddPostActivity.class);

            if (isFromDraftAdapterToBottomSheet){
                intent.putExtra("DraftObject", contestVideoDraft);
            }
            if (v.getTag() instanceof SpecialContestObj) {
                intent.putExtra("contestObj", (SpecialContestObj) v.getTag());
            } else if (v.getTag() instanceof ContestObj) {
                intent.putExtra("contestObj", (ContestObj) v.getTag());
            }
            intent.putExtra(Constants.CONTEST_POST, true);
            context.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return contestList.size();
    }

    static class ContestDialogViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.contest_location)
        TextView tvContestLocation;
        @BindView(R.id.contest_icon)
        ImageView ivContestIcon;
        @BindView(R.id.contest_title)
        TextView tvContestTitle;
        @BindView(R.id.contest_description)
        TextView tvContestDescription;

        @BindView(R.id.special_contest_title)
        TextView tvSpecialContestTitle;

        ContestDialogViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
