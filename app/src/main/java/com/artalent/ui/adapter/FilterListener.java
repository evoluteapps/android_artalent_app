package com.artalent.ui.adapter;

import ja.burhanrashid52.photoeditor.PhotoFilter;

public interface FilterListener {
    void onFilterSelected(PhotoFilter photoFilter);
}
