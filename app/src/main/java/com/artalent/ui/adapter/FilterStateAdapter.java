package com.artalent.ui.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.artalent.R;
import com.artalent.ui.model.StateObj;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Mahajan on 16-09-2019.
 */

public class FilterStateAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<StateObj> countryNameList = new ArrayList<>();
    private View.OnClickListener onClickListener;
    private Context context;
    private int fromData;

    public FilterStateAdapter(Context context, List<StateObj> countryNameList, View.OnClickListener onClickListener1, int fromData) {
        this.onClickListener = onClickListener1;
        this.countryNameList = countryNameList;
        this.context = context;
        this.fromData = fromData;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v;
        if (fromData == 1) {
            v = LayoutInflater.from(viewGroup.getContext()).
                    inflate(R.layout.item_swipable_recyclerview, viewGroup, false);
            return new ManageCountryHolder(v);
        } else {
            v = LayoutInflater.from(viewGroup.getContext()).
                    inflate(R.layout.item_filter_interest, viewGroup, false);
            return new CountryHolder(v);
        }
    }

    public void setTaskList(List<StateObj> countryNameList) {
        this.countryNameList = countryNameList;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        if (fromData == 0) {
            CountryHolder holder = (CountryHolder) viewHolder;
            StateObj stateObj = countryNameList.get(i);
            holder.llMain.setOnClickListener(onClickListener);
            holder.llMain.setTag(stateObj);
            if (stateObj != null && stateObj.getState() != null && !TextUtils.isEmpty(stateObj.getState())) {
                holder.tvCountryName.setText(stateObj.getState());
                if (stateObj.isCheck()) {
                    holder.ivSelection.setImageResource(R.drawable.tick_active);
                } else {
                    holder.ivSelection.setImageResource(R.drawable.tick);
                }
            }
        } else {
            ManageCountryHolder holder = (ManageCountryHolder) viewHolder;
            holder.llMain.setOnClickListener(onClickListener);
            StateObj stateObj = countryNameList.get(i);
            holder.llMain.setTag(stateObj);
            if (stateObj != null && stateObj.getState() != null && !TextUtils.isEmpty(stateObj.getState())) {
                holder.tvCountryName.setText(stateObj.getState());
            }

        }

    }

    @Override
    public int getItemCount() {
        return countryNameList.size();
    }


    class ManageCountryHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.name)
        TextView tvCountryName;
        @BindView(R.id.llMain)
        RelativeLayout llMain;
        @BindView(R.id.img_edit)
        ImageView imgEdit;
        @BindView(R.id.img_delete)
        ImageView imgDelete;

        ManageCountryHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


    class CountryHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.selection)
        ImageView ivSelection;
        @BindView(R.id.name)
        TextView tvCountryName;

        @BindView(R.id.llMain)
        LinearLayout llMain;


        public CountryHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }

    }


}
