package com.artalent.ui.adapter;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.artalent.R;
import com.artalent.Utils;
import com.artalent.ui.activity.OtherUserProfileActivity;
import com.artalent.ui.model.ContestHistoryObj;
import com.artalent.ui.utils.Constants;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LastWeekWinnerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<ContestHistoryObj> contestList;
    private RequestOptions options;
    private Context context;

    public LastWeekWinnerAdapter(List<ContestHistoryObj> contestList, Context context) {
        this.contestList = contestList;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.person_placeholder)
                .error(R.drawable.person_placeholder);
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_winner, parent, false);

        return new LastWeekWinnerViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        LastWeekWinnerViewHolder holder = (LastWeekWinnerViewHolder) viewHolder;
        ContestHistoryObj contestObj = contestList.get(i);
        holder.cardView.setTag(contestObj);
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContestHistoryObj contestObjView = (ContestHistoryObj) v.getTag();
                if (contestObjView != null && contestObjView.getWinner1() != null && contestObjView.getWinner1().getId() > 0) {
                    Intent intent = new Intent(context, OtherUserProfileActivity.class);
                    intent.putExtra(Constants.DATA, contestObjView.getWinner1());
                    context.startActivity(intent);
                }

            }
        });
        try {
            holder.contestName.setText("" + contestObj.getInterest().getInterest());
        } catch (Exception e) {
            holder.contestName.setText("");
            e.printStackTrace();
        }
        if (contestObj != null) {
            try {
                if (contestObj.getGeoFlag() == 1) {
                    holder.contestName.append(" Contest for " + contestObj.getDistrict().getDistrict());
                } else if (contestObj.getGeoFlag() == 2) {
                    holder.contestName.append(" Contest for " + contestObj.getState().getState());
                } else {
                    holder.contestName.append(" Contest for " + contestObj.getCountry().getCountryName());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        //winner1VoteCount
        holder.voteCount.setText("Best vote count - " + contestObj.getWinner1VoteCount());
        holder.userName.setText("");
        holder.ivUserIcon.setImageResource(R.drawable.person_placeholder);
        if (contestObj != null && contestObj.getWinner1() != null && contestObj.getWinner1().getId() > 0) {

            if (contestObj.getWinner1().getProfileImage() != null && !TextUtils.isEmpty(contestObj.getWinner1().getProfileImage())) {
                /*Glide.with(context)
                        .load(contestObj.getWinner1().getProfileImage()).apply(options).into(holder.ivUserIcon);*/
                Utils.setImageRounded(context, contestObj.getWinner1().getProfileImage(), holder.ivUserIcon);
            }


            try {
                holder.userName.setText(contestObj.getWinner1().getFirstName() + " ");
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                holder.userName.append(contestObj.getWinner1().getLastName());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public int getItemCount() {
        return contestList.size();
    }

    public class LastWeekWinnerViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.contest_icon)
        ImageView ivUserIcon;

        @BindView(R.id.user_name)
        TextView userName;

        @BindView(R.id.contest_name)
        TextView contestName;

        @BindView(R.id.cardView)
        CardView cardView;

        @BindView(R.id.vote_count)
        TextView voteCount;

        @BindView(R.id.check_talent_post)
        TextView checkTalentPost;

        @BindView(R.id.winner_rank)
        TextView winnerRank;


        LastWeekWinnerViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
