package com.artalent.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.artalent.R;
import com.artalent.Utils;
import com.artalent.ui.activity.OtherUserProfileActivity;
import com.artalent.ui.model.ContestHistoryObj;
import com.artalent.ui.utils.Constants;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WinningContestListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<ContestHistoryObj> contestList;
    private RequestOptions options;
    private Context context;

    public WinningContestListAdapter(List<ContestHistoryObj> contestList, Context context) {
        //this.contestHistoryObjs = interestNameList;
        this.contestList = contestList;
        this.context = context;

    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.person_placeholder)
                .error(R.drawable.person_placeholder);
        View v = LayoutInflater.from(viewGroup.getContext()).
                inflate(R.layout.item_winning_contest, viewGroup, false);
        return new WinningContetViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {

        WinningContetViewHolder holder = (WinningContetViewHolder) viewHolder;
        ContestHistoryObj contestObj = contestList.get(i);
        holder.cardView.setTag(contestObj);
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContestHistoryObj contestObjView = (ContestHistoryObj) v.getTag();
                if (contestObjView != null && contestObjView.getWinner1() != null && contestObjView.getWinner1().getId() > 0) {
                    Intent intent = new Intent(context, OtherUserProfileActivity.class);
                    intent.putExtra(Constants.DATA, contestObjView.getWinner1());
                    context.startActivity(intent);
                }

            }
        });
        try {
            if (contestObj.getInterest() != null)
                holder.tvContestTitle.setText("" + contestObj.getInterest().getInterest());
        } catch (Exception e) {
            holder.tvContestTitle.setText("");
            e.printStackTrace();
        }
        if (contestObj != null) {
            try {
                if (contestObj.getGeoFlag() == 1) {
                    holder.tvContestTitle.append(" Contest for " + contestObj.getDistrict().getDistrict());
                } else if (contestObj.getGeoFlag() == 2) {
                    holder.tvContestTitle.append(" Contest for " + contestObj.getState().getState());
                } else {
                    holder.tvContestTitle.append(" Contest for " + contestObj.getCountry().getCountryName());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        holder.tvContestWinnerName.setText("");
        holder.ivUserIcon.setImageResource(R.drawable.person_placeholder);
        if (contestObj != null && contestObj.getWinner1() != null && contestObj.getWinner1().getId() > 0) {

            if (contestObj.getWinner1().getProfileImage() != null && !TextUtils.isEmpty(contestObj.getWinner1().getProfileImage())) {
                /*Glide.with(context)
                        .load(contestObj.getWinner1().getProfileImage()).apply(new RequestOptions()
                        .centerCrop()
                        .placeholder(R.drawable.interest_categories)
                        .transform(new RoundedCorners(120))
                        .error(R.drawable.interest_categories)).into(holder.ivUserIcon);*/
                Utils.setImageRounded(context, contestObj.getWinner1().getProfileImage(), holder.ivUserIcon);
            }


            try {
                holder.tvContestWinnerName.setText(contestObj.getWinner1().getFirstName() + " ");
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                holder.tvContestWinnerName.append(contestObj.getWinner1().getLastName());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public int getItemCount() {
        return contestList.size();
    }

    public class WinningContetViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.contest_title)
        TextView tvContestTitle;

        @BindView(R.id.cardView)
        CardView cardView;

        @BindView(R.id.contest_winner_name)
        TextView tvContestWinnerName;

        @BindView(R.id.user_icon)
        ImageView ivUserIcon;

        WinningContetViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
