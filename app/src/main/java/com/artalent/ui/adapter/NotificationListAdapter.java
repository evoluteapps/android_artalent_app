package com.artalent.ui.adapter;

import android.app.Activity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.artalent.DateFormatter;
import com.artalent.R;
import com.artalent.ui.model.NotificationDataObj;
import com.artalent.ui.utils.CircleTransformation;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NotificationListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Activity activity;
    private List<NotificationDataObj> userObjList;
    private View.OnClickListener onClickListener;

    public NotificationListAdapter(List<NotificationDataObj> userObjList, Activity activity, View.OnClickListener onClickListener1) {
        this.activity = activity;
        this.userObjList = userObjList;
        this.onClickListener = onClickListener1;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).
                inflate(R.layout.item_votest_user_detail, viewGroup, false);
        return new SearchUserHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        SearchUserHolder holder = (SearchUserHolder) viewHolder;
        NotificationDataObj postContestObj = userObjList.get(position);

        holder.btnFollow.setVisibility(View.GONE);
        holder.btnFollow.setTag(position);
        holder.btnFollow.setOnClickListener(onClickListener);

        if (postContestObj != null && postContestObj.getCreatedAt() != null && !TextUtils.isEmpty(postContestObj.getCreatedAt())) {
            holder.userTalent.setText(DateFormatter.INSTANCE.getFormattedByString("yyyy-MM-dd HH:mm:ss", "MMMM dd, hh:mm a", postContestObj.getCreatedAt()));

        } else {
            holder.userTalent.setText("");
        }


        if (!TextUtils.isEmpty(postContestObj.getImage())) {
            Picasso.get()
                    .load(postContestObj.getImage())
                    .placeholder(R.drawable.user_outline)
                    .resize(activity.getResources().getDimensionPixelSize(R.dimen.dimen_90), activity.getResources().getDimensionPixelSize(R.dimen.dimen_90))
                    .centerCrop()
                    .transform(new CircleTransformation())
                    .into(holder.ivUserProfileThunbnail);
        } else {
            Picasso.get()
                    .load(R.drawable.man)
                    .placeholder(R.drawable.user_outline)
                    .resize(activity.getResources().getDimensionPixelSize(R.dimen.dimen_90), activity.getResources().getDimensionPixelSize(R.dimen.dimen_90))
                    .centerCrop()
                    .transform(new CircleTransformation())
                    .into(holder.ivUserProfileThunbnail);
        }


        if (!TextUtils.isEmpty(postContestObj.getTitle())) {
            holder.userName.setText(postContestObj.getTitle());
        } else {
            holder.userName.setText("");
        }
        holder.userName.setVisibility(View.GONE);

        if (!TextUtils.isEmpty(postContestObj.getDescription())) {
            holder.userFullName.setText(postContestObj.getDescription());
        } else {
            holder.userFullName.setText("");
        }


        holder.recyclerView.setVisibility(View.GONE);

    }

    @Override
    public int getItemCount() {
        return userObjList.size();
    }

    /*public void updateList(List<UserObj> userObjList) {
        this.userObjList = userObjList;
        notifyDataSetChanged();
    }*/

    public class SearchUserHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.user_profile)
        ImageView ivUserProfileThunbnail;

        @BindView(R.id.btnFollow)
        Button btnFollow;

        @BindView(R.id.rlTop)
        RelativeLayout rlTop;


        @BindView(R.id.user_name)
        TextView userName;

        @BindView(R.id.user_full_name)
        TextView userFullName;

        @BindView(R.id.user_talent)
        TextView userTalent;


        @BindView(R.id.recycler_view_interest)
        RecyclerView recyclerView;

        public SearchUserHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }

    }
}
