package com.artalent.ui.adapter;

import android.content.Context;

import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.RecyclerView;

import android.net.Uri;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.artalent.R;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Rohit mahajan on 18/05/16.
 */
public class SelectedPostAdapter extends RecyclerView.Adapter<SelectedPostAdapter.MyViewHolder> {

    private Context mContext;
    private ArrayList<String> list;
    private View.OnClickListener mOnClickListener;

    private boolean isFromBottomSheetWithDraft = false;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, count;
        ImageView ivClose, thumbnail, overflow;

        MyViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.title);
            count = view.findViewById(R.id.count);
            thumbnail = view.findViewById(R.id.thumbnail);
            ivClose = view.findViewById(R.id.ivClose);
            overflow = view.findViewById(R.id.overflow);
        }
    }


    public SelectedPostAdapter(Context mContext, ArrayList<String> list, View.OnClickListener onClickListener) {
        this.mContext = mContext;
        this.list = list;
        this.mOnClickListener = onClickListener;
    }
    public SelectedPostAdapter(Context mContext, ArrayList<String> list, View.OnClickListener onClickListener,boolean isFromBottomSheetWithDraft) {
        this.mContext = mContext;
        this.list = list;
        this.mOnClickListener = onClickListener;
        this.isFromBottomSheetWithDraft = isFromBottomSheetWithDraft;
    }

    @NotNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_selected_post, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        File file = new File(list.get(position));

        if (isFromBottomSheetWithDraft) {
            Glide.with(holder.itemView.getContext())
                    .load(Uri.fromFile(file))
                    .into(holder.thumbnail);
        } else {
            Picasso.get()
                    .load(file)
                    .into(holder.thumbnail);
        }

        // loading album cover using Glide library
        // Picasso.with(mContext).load(album.getThumbnail()).into(holder.thumbnail);

        holder.overflow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPopupMenu(holder.overflow);
            }
        });
        holder.ivClose.setTag(position);
        holder.ivClose.setOnClickListener(mOnClickListener);
    }

    /**
     * Showing popup menu when tapping on 3 dots
     */
    private void showPopupMenu(View view) {
        // inflate menu
        PopupMenu popup = new PopupMenu(mContext, view);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.menu_album, popup.getMenu());
        popup.setOnMenuItemClickListener(new MyMenuItemClickListener());
        popup.show();
    }

    /**
     * Click listener for popup menu items
     */
    class MyMenuItemClickListener implements PopupMenu.OnMenuItemClickListener {

        MyMenuItemClickListener() {
        }

        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            switch (menuItem.getItemId()) {
                case R.id.action_add_favourite:
                    Toast.makeText(mContext, "Add to favourite", Toast.LENGTH_SHORT).show();
                    return true;
                case R.id.action_play_next:
                    Toast.makeText(mContext, "Play next", Toast.LENGTH_SHORT).show();
                    return true;
                default:
            }
            return false;
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
