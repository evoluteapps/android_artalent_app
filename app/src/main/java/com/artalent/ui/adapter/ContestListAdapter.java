package com.artalent.ui.adapter;

import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.artalent.ArtalentApplication;
import com.artalent.R;
import com.artalent.Utils;
import com.artalent.ui.activity.ContestPostActivity;
import com.artalent.ui.activity.ContestRegisterUpdateActivity;
import com.artalent.ui.activity.ContestUserActivity;
import com.artalent.ui.model.ContestObj;
import com.artalent.ui.model.ContestRegistrationObj;
import com.artalent.ui.model.InterestObj;
import com.artalent.ui.model.UserObj;
import com.artalent.ui.utils.Constants;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ContestListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<ContestObj> contestList;
    private RequestOptions options;

    public ContestListAdapter(Context context, List<ContestObj> contestList) {
        this.context = context;
        this.contestList = contestList;

    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).
                inflate(R.layout.item_contest, viewGroup, false);
        options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.interest_categories)
                .error(R.drawable.interest_categories);
        return new ContestListViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        ContestListViewHolder holder = (ContestListViewHolder) viewHolder;
        ContestObj contestObj = contestList.get(i);
        String contestTitle = "";
        String location = "";
        String category = "";

        if (contestObj != null && contestObj.getInterest() != null && !TextUtils.isEmpty(contestObj.getInterest().getInterest())) {
            category = contestObj.getInterest().getInterest();
        } else {
            category = "";
        }
        if (contestObj != null) {
            /*Glide.with(context)
                    .load(contestObj.getInterest().getBaner()).apply(options).into(holder.ivContestIcon);*/
            Utils.setImageRounded(context, contestObj.getInterest().getBaner(), holder.ivContestIcon);
            if (contestObj.getFlag() == 1) {
                location = contestObj.getDistrictEntity().getDistrict();
                contestTitle = category + " " + "contest for " + location;
                holder.tvContestTitle.setText(contestTitle);
                holder.tvContestLocation.setText(contestObj.getDistrictEntity().getDistrict());
            } else if (contestObj.getFlag() == 2) {
                location = contestObj.getStateEntity().getState();
                contestTitle = category + " " + "contest for " + location;
                holder.tvContestTitle.setText(contestTitle);
                holder.tvContestLocation.setText(contestObj.getStateEntity().getState());
            } else {
                location = contestObj.getCountryEntity().getCountryName();
                contestTitle = category + " " + "contest for " + location;
                holder.tvContestTitle.setText(contestTitle);
                holder.tvContestLocation.setText(contestObj.getCountryEntity().getCountryName());
            }
        }
        if (contestObj != null && contestObj.getRegistrationDetails() != null && contestObj.getRegistrationDetails().size() > 0) {

            holder.rlContestMember.setOnClickListener(v -> {
                Intent intent = new Intent(context, ContestUserActivity.class);
                intent.putExtra("contestObj", contestObj);
                context.startActivity(intent);
            });

            String memberCount;
            if (contestObj.getRegistrationDetails().size() == 1) {
                memberCount = contestObj.getRegistrationDetails().size() + "  Member";
                holder.tvContestMember.setText(memberCount);
            } else {
                memberCount = contestObj.getRegistrationDetails().size() + "  Members";
                holder.tvContestMember.setText(memberCount);
            }
            boolean isAvailable = false;
            for (ContestRegistrationObj contestRegistrationObj : contestObj.getRegistrationDetails()) {
                if (ArtalentApplication.prefManager.getUserObj() != null && contestRegistrationObj != null && contestRegistrationObj.getUser() != null) {
                    UserObj userObj = ArtalentApplication.prefManager.getUserObj();
                    if (contestRegistrationObj.getUser().getId() == userObj.getId()) {
                        isAvailable = true;
                    }
                }
            }
            if (!isAvailable) {
                holder.btnContestRegister.setVisibility(View.VISIBLE);
            } else {
                holder.btnContestRegister.setVisibility(View.GONE);
            }

        } else {
            holder.tvContestMember.setText(context.getResources().getString(R.string.zero_member));
            holder.btnContestRegister.setVisibility(View.VISIBLE);
        }

        holder.itemView.setTag(contestObj);

        holder.itemView.setOnClickListener(v -> {
            ContestObj contestObj1 = (ContestObj) v.getTag();
            if (contestObj1 != null) {
                Intent intent = new Intent(context, ContestPostActivity.class);
                intent.putExtra(Constants.DATA, contestObj1);
                context.startActivity(intent);
            }


        });

        holder.btnContestRegister.setTag(contestObj.getInterest());
        holder.btnContestRegister.setOnClickListener(v -> {
            InterestObj interestObj = (InterestObj) v.getTag();
            if (interestObj != null) {
                Intent intent = new Intent(context, ContestRegisterUpdateActivity.class);
                intent.putExtra(Constants.INTEREST_OBJ, interestObj);
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return contestList.size();
    }

    class ContestListViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.contest_location)
        TextView tvContestLocation;
        @BindView(R.id.register_contest_btn)
        Button btnContestRegister;
        @BindView(R.id.contest_icon)
        ImageView ivContestIcon;
        @BindView(R.id.contest_title)
        TextView tvContestTitle;
        @BindView(R.id.contest_description)
        TextView tvContestDescription;
        @BindView(R.id.contest_member_layout)
        RelativeLayout rlContestMember;
        @BindView(R.id.tv_contest_member)
        TextView tvContestMember;

        ContestListViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
