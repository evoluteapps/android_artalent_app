package com.artalent.ui.adapter

import android.app.Activity
import android.content.Intent
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import com.artalent.R
import com.artalent.Utils
import com.artalent.ui.activity.SpecialContestDetailsActivity
import com.artalent.ui.activity.SpecialContestRegisterActivity
import com.artalent.ui.model.SpecialContestObj
import com.squareup.picasso.Picasso

class PagerSpecialContestAdapter(
    private val picasso: Picasso,
    private val items: List<SpecialContestObj>,
    private val context: Activity
) : androidx.viewpager.widget.PagerAdapter() {

    override fun getCount() = items.size

    override fun isViewFromObject(view: View, `object`: Any): Boolean = view == `object`

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val view = LayoutInflater
            .from(container.context)
            .inflate(R.layout.item_special_contest_banner, container, false)

        val item = items[position]
        val btnRegister: Button = view.findViewById(R.id.btnRegister)
        btnRegister.setTag(item)
        if (item != null && !TextUtils.isEmpty(item.regFee)) {
            try {
                var regFee = (item.regFee).toDouble()
                btnRegister.text = "₹ " + regFee
            } catch (e: Exception) {
                btnRegister.text = "Free"
            }
        } else {
            btnRegister.text = "Free"
        }
        val image: AppCompatImageView = view.findViewById(R.id.image)
        image.setTag(item)
        val tvEndDate: TextView = view.findViewById(R.id.tvEndDate)
        val special_contest_title: TextView = view.findViewById(R.id.special_contest_title)
        val tvWinnerPrice: TextView = view.findViewById(R.id.tvWinnerPrice)
        val tvContestRegCount: TextView = view.findViewById(R.id.tvContestRegCount)

        if (item != null && item.tittle != null && !TextUtils.isEmpty(item.tittle)) {
            special_contest_title.text = item.tittle
        } else {
            special_contest_title.text = ""
        }
        var price = 0.0
        if (item != null && item.winnerContestWinnerList != null && item.winnerContestWinnerList.size > 0) {
            price = Utils.getSpecialContestPrice(item.winnerContestWinnerList)
            tvWinnerPrice.text = "Winner Prize ₹ " + price
        }
        if (price > 0) {
            tvWinnerPrice.visibility = View.VISIBLE
        } else {
            tvWinnerPrice.visibility = View.GONE
        }

        if (item != null && item.maxRegCount != null && item.maxRegCount > 0) {
            tvContestRegCount.text = "" + item.registeredCount + "/" + item.maxRegCount + " Talent"
        } else {
            tvContestRegCount.text = "";
        }



        btnRegister.setVisibility(View.VISIBLE)
        if (item != null && item.regEndDate != null && !TextUtils.isEmpty(item.regEndDate)) {
            var sCurrentDate = Utils.getCurrentDate();
            var sContestRegEndDate = item.regEndDate
            if (Utils.getCurrentDiffDate(sCurrentDate, sContestRegEndDate) >= 0) {
                tvEndDate.text = "Registration Ends On " + item.regEndDate
            } else {
                if (item != null && item.endDate != null && !TextUtils.isEmpty(item.endDate)) {
                    tvEndDate.text = "Contest Ends On " + item.endDate
                }
                btnRegister.setVisibility(View.GONE)
            }

            //-------------------


        } else {
            btnRegister.setVisibility(View.GONE)
            if (item != null && item.endDate != null && !TextUtils.isEmpty(item.endDate)) {
                tvEndDate.text = "Contest Ends On " + item.endDate
            }
        }
        if (item.isRegistered != 0) {
            btnRegister.setVisibility(View.GONE)
        }
        btnRegister.setOnClickListener {
            val itemContest = it.tag as SpecialContestObj
            if (itemContest.isRegistered == 0) {
                if (itemContest?.regEndDate != null && !TextUtils.isEmpty(itemContest.regEndDate)) {
                    var sCurrentDate = Utils.getCurrentDate();
                    var sContestRegEndDate = item.regEndDate
                    if (Utils.getCurrentDiffDate(sCurrentDate, sContestRegEndDate) >= 0) {
                        val intent = Intent(context, SpecialContestRegisterActivity::class.java)
                        intent.putExtra("contest_item", itemContest)
                        context.startActivity(intent)
                    }
                }


            } else {
                val intent = Intent(context, SpecialContestDetailsActivity::class.java)
                intent.putExtra("contest_item", itemContest)
                context.startActivity(intent)
            }
        }
        image.setOnClickListener {
            val itemContest = it.tag as SpecialContestObj
            if (itemContest.isRegistered == 0) {
                if (itemContest?.regEndDate != null && !TextUtils.isEmpty(itemContest.regEndDate)) {
                    var sCurrentDate = Utils.getCurrentDate();
                    var sContestRegEndDate = item.regEndDate
                    if (Utils.getCurrentDiffDate(sCurrentDate, sContestRegEndDate) >= 0) {
                        val intent = Intent(context, SpecialContestRegisterActivity::class.java)
                        intent.putExtra("contest_item", itemContest)
                        context.startActivity(intent)
                    }
                }
            } else {
                val intent = Intent(context, SpecialContestDetailsActivity::class.java)
                intent.putExtra("contest_item", itemContest)
                context.startActivity(intent)
            }
        }
        picasso.load(item.image)
            .into(image)

        /*picasso.load(R.drawable.banner_des)
                .placeholder(R.drawable.chat_background)
                .into(image)*/

        container.addView(view)
        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, view: Any) {
        container.removeView(view as View)
    }
}