package com.artalent.ui.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.artalent.R;
import com.artalent.ui.model.InterestObj;
import com.artalent.ui.view.RoundedImageView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Mahajan on 16-09-2019.
 */

public class SelectInterestAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<InterestObj> interestNameList;
    private View.OnClickListener onClickListener;
    private Context context;
    private RequestOptions options;


    public SelectInterestAdapter(Context context, List<InterestObj> interestNameList, View.OnClickListener onClickListener1) {
        this.onClickListener = onClickListener1;
        this.interestNameList = interestNameList;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).
                inflate(R.layout.item_interest, viewGroup, false);
        options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.interest_categories)
                .error(R.drawable.interest_categories);
        return new InterestHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        InterestHolder holder = (InterestHolder) viewHolder;
        InterestObj interest = interestNameList.get(i);
        holder.llMain.setOnClickListener(onClickListener);
        holder.llMain.setTag(interest);
        
        if (interest != null && interest.getInterest() != null && !TextUtils.isEmpty(interest.getInterest())) {

            Glide.with(context)
                    .load(interest.getBaner()).apply(options).into(holder.roundedImageView);
            holder.tvInterestName.setText(interest.getInterest());
            if (interest.isCheck()) {
                holder.ivSelection.setVisibility(View.VISIBLE);
            } else {
                holder.ivSelection.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public int getItemCount() {
        return interestNameList.size();
    }

    class InterestHolder extends RecyclerView.ViewHolder {
        
        @BindView(R.id.selection)
        ImageView ivSelection;
        @BindView(R.id.category_image)
        RoundedImageView roundedImageView;
        @BindView(R.id.name)
        TextView tvInterestName;
        @BindView(R.id.llMain)
        LinearLayout llMain;

        InterestHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
