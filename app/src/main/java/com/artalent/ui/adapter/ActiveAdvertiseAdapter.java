package com.artalent.ui.adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.artalent.ArtalentApplication;
import com.artalent.R;
import com.artalent.ui.model.AdvertiseObj;
import com.artalent.ui.view.RoundedImageView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class ActiveAdvertiseAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<AdvertiseObj> mPostObjs;
    private View.OnClickListener mOnClickListener;
    private Context context;
    private RequestOptions options;

    public ActiveAdvertiseAdapter(List<AdvertiseObj> postObjs, View.OnClickListener onClickListener, Context context) {
        this.mPostObjs = postObjs;
        this.context = context;
        this.mOnClickListener = onClickListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).
                inflate(R.layout.item_advertise, viewGroup, false);
        options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.interest_categories)
                .error(R.drawable.interest_categories);
        return new ActiveAdvertiseViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        AdvertiseObj advertiseObj = mPostObjs.get(position);
        ((ActiveAdvertiseViewHolder) viewHolder).bindView(mPostObjs.get(position));
        ((ActiveAdvertiseViewHolder) viewHolder).ivDelete.setTag(position);
        ((ActiveAdvertiseViewHolder) viewHolder).btnCompanyWebsite.setTag(position);
        ((ActiveAdvertiseViewHolder) viewHolder).advCompanyName.setText(advertiseObj.getCompanyName());
        Glide.with(context).load(advertiseObj.getImage()).apply(options).into(((ActiveAdvertiseViewHolder) viewHolder).advCompanyLogo);
        ((ActiveAdvertiseViewHolder) viewHolder).ivDelete.setOnClickListener(mOnClickListener);
        ((ActiveAdvertiseViewHolder) viewHolder).btnCompanyWebsite.setOnClickListener(mOnClickListener);
    }

    @Override
    public int getItemCount() {
        return mPostObjs.size();
    }

    static class ActiveAdvertiseViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.advertise_banner)
        ImageView advertiseBanner;

        @BindView(R.id.adv_description)
        TextView advertiseDesc;

        @BindView(R.id.adv_company_name)
        TextView advCompanyName;

        @BindView(R.id.btnCompanyURL)
        Button btnCompanyWebsite;

        @BindView(R.id.ivDelete)
        ImageView ivDelete;


        @BindView(R.id.ivCompanyLogo)
        CircleImageView advCompanyLogo;

        AdvertiseObj mAdvertiseObj;

        ActiveAdvertiseViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bindView(AdvertiseObj advertiseObj) {
            this.mAdvertiseObj = advertiseObj;
            int height = ArtalentApplication.deviceWidth * 9 / 16;
            if (mAdvertiseObj != null && !TextUtils.isEmpty(mAdvertiseObj.getImage())) {
                Picasso.get()
                        .load(mAdvertiseObj.getImage())
                        .resize(ArtalentApplication.deviceWidth, height)
                        .placeholder(R.drawable.feed_placeholder)
                        .centerCrop()
                        .into(advertiseBanner);
            } else {
                Picasso.get()
                        .load(R.drawable.feed_placeholder)
                        .resize(ArtalentApplication.deviceWidth, height)
                        .placeholder(R.drawable.feed_placeholder)
                        .centerCrop()
                        .into(advertiseBanner);
            }
            if (mAdvertiseObj != null && !TextUtils.isEmpty(mAdvertiseObj.getDescription())) {
                advertiseDesc.setText(mAdvertiseObj.getDescription());
            } else {
                advertiseDesc.setText("");
            }

        }
    }
}
