package com.artalent.ui.adapter

import android.content.Intent
import androidx.viewpager.widget.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import com.artalent.R
import com.squareup.picasso.Picasso

class MyPagerAdapter(private val picasso: Picasso,
                     private val items: List<String>) : androidx.viewpager.widget.PagerAdapter() {

    override fun getCount() = items.size

    override fun isViewFromObject(view: View, `object`: Any): Boolean = view == `object`

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val view = LayoutInflater
                .from(container.context)
                .inflate(R.layout.item_card, container, false)

        val item = items[position]
        val image: ImageView = view.findViewById(R.id.image)
        picasso.load(item)
                .placeholder(R.drawable.chat_background)
                .fit()
                .centerCrop()
                .into(image)
        container.addView(view)
        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, view: Any) {
        container.removeView(view as View)
    }
}