package com.artalent.ui.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.artalent.R;
import com.artalent.ui.model.InterestObj;
import com.artalent.ui.model.PostContestObj;
import com.artalent.ui.model.UserObj;
import com.artalent.ui.utils.CircleTransformation;
import com.artalent.ui.utils.Constants;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VotedUserAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Activity activity;
    private List<PostContestObj> userObjList;
    private View.OnClickListener onClickListener;

    public VotedUserAdapter(List<PostContestObj> userObjList, Activity activity, View.OnClickListener onClickListener1) {
        this.activity = activity;
        this.userObjList = userObjList;
        this.onClickListener = onClickListener1;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).
                inflate(R.layout.item_votest_user_detail, viewGroup, false);
        return new SearchUserHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        SearchUserHolder holder = (SearchUserHolder) viewHolder;
        PostContestObj postContestObj = userObjList.get(position);
        UserObj userObj = postContestObj.getUserInfo();
        if (userObj != null && userObj.getIsFollowed() == 1) {
            holder.btnFollow.setVisibility(View.VISIBLE);
            holder.btnFollow.setText(activity.getResources().getString(R.string.unfollow));
        } else {
            holder.btnFollow.setVisibility(View.VISIBLE);
            holder.btnFollow.setText(activity.getResources().getString(R.string.follow));
        }
        holder.btnFollow.setVisibility(View.GONE);
        holder.btnFollow.setTag(position);
        holder.btnFollow.setOnClickListener(onClickListener);


        holder.rlTop.setTag(userObj);
        holder.rlTop.setOnClickListener(onClickListener);


        if (postContestObj != null && postContestObj.getInterest() != null && !TextUtils.isEmpty(postContestObj.getInterest().getInterest())) {
            holder.userTalent.setText("I have voted for " + postContestObj.getInterest().getInterest() + " Contest");
        } else {
            holder.userTalent.setText("");
        }

        if (userObj != null) {

            if (!TextUtils.isEmpty(userObj.getProfileImage())) {
                Picasso.get()
                        .load(userObj.getProfileImage())
                        .placeholder(R.drawable.user_outline)
                        .resize(activity.getResources().getDimensionPixelSize(R.dimen.dimen_90), activity.getResources().getDimensionPixelSize(R.dimen.dimen_90))
                        .centerCrop()
                        .transform(new CircleTransformation())
                        .into(holder.ivUserProfileThunbnail);
            } else {
                Picasso.get()
                        .load(R.drawable.man)
                        .placeholder(R.drawable.user_outline)
                        .resize(activity.getResources().getDimensionPixelSize(R.dimen.dimen_90), activity.getResources().getDimensionPixelSize(R.dimen.dimen_90))
                        .centerCrop()
                        .transform(new CircleTransformation())
                        .into(holder.ivUserProfileThunbnail);
            }


            String userName = userObj.getUserName();
            String firstName = userObj.getFirstName();
            String lastName = userObj.getLastName();

            if (!TextUtils.isEmpty(userName)) {
                holder.userName.setText(userName);
            } else {
                holder.userName.setText("");
            }

            if (!TextUtils.isEmpty(firstName) && !TextUtils.isEmpty(lastName)) {
                holder.userFullName.setText(firstName + " " + lastName);
            } else {
                holder.userFullName.setText("");
            }


            List<InterestObj> interestObjList = userObj.getInterest();
            InterestListAdapter interestListAdapter = new InterestListAdapter(interestObjList, Constants.INTEREST_SEARCH_LIST_NAME, activity);
            @SuppressLint("WrongConstant") LinearLayoutManager childLayoutManager = new LinearLayoutManager(holder.recyclerView.getContext(), LinearLayout.HORIZONTAL, false);
            holder.recyclerView.setLayoutManager(childLayoutManager);
            holder.recyclerView.setAdapter(interestListAdapter);
        }
    }

    @Override
    public int getItemCount() {
        return userObjList.size();
    }

    /*public void updateList(List<UserObj> userObjList) {
        this.userObjList = userObjList;
        notifyDataSetChanged();
    }*/

    public class SearchUserHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.user_profile)
        ImageView ivUserProfileThunbnail;

        @BindView(R.id.btnFollow)
        Button btnFollow;

        @BindView(R.id.rlTop)
        RelativeLayout rlTop;


        @BindView(R.id.user_name)
        TextView userName;

        @BindView(R.id.user_full_name)
        TextView userFullName;

        @BindView(R.id.user_talent)
        TextView userTalent;


        @BindView(R.id.recycler_view_interest)
        RecyclerView recyclerView;

        public SearchUserHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }

    }
}
