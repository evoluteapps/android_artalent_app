package com.artalent.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.artalent.R;
import com.artalent.Utils;
import com.artalent.ui.activity.OtherUserProfileActivity;
import com.artalent.ui.model.SpecialContestObj;
import com.artalent.ui.utils.Constants;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SPLastWeekWinnerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<SpecialContestObj> contestList;
    private RequestOptions options;
    private Context context;

    public SPLastWeekWinnerAdapter(List<SpecialContestObj> contestList, Context context) {
        this.contestList = contestList;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.person_placeholder)
                .error(R.drawable.person_placeholder);
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_winner, parent, false);

        return new LastWeekWinnerViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        LastWeekWinnerViewHolder holder = (LastWeekWinnerViewHolder) viewHolder;
        SpecialContestObj contestObj = contestList.get(i);
        holder.cardView.setTag(contestObj);
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SpecialContestObj contestObjView = (SpecialContestObj) v.getTag();
                if (contestObjView != null && contestObjView.getAdminContestResultWinners().get(0).getWinnerUser() != null && contestObjView.getAdminContestResultWinners().get(0).getWinnerUser().getId() > 0) {
                    Intent intent = new Intent(context, OtherUserProfileActivity.class);
                    intent.putExtra(Constants.DATA, contestObjView.getAdminContestResultWinners().get(0).getWinnerUser());
                    context.startActivity(intent);
                }

            }
        });
        try {
            holder.contestName.setText("" + contestObj.getTittle());
        } catch (Exception e) {
            holder.contestName.setText("");
            e.printStackTrace();
        }


        //winner1VoteCount
        holder.voteCount.setText("Best vote count - " + contestObj.getAdminContestResultWinners().get(0).getVoteCount());
        holder.userName.setText("");
        holder.ivUserIcon.setImageResource(R.drawable.person_placeholder);
        if (contestObj != null && contestObj.getAdminContestResultWinners().get(0).getWinnerUser() != null && contestObj.getAdminContestResultWinners().get(0).getWinnerUser().getId() > 0) {

            if (contestObj.getAdminContestResultWinners().get(0).getWinnerUser().getProfileImage() != null && !TextUtils.isEmpty(contestObj.getAdminContestResultWinners().get(0).getWinnerUser().getProfileImage())) {
                /*Glide.with(context)
                        .load(contestObj.getWinner1().getProfileImage()).apply(options).into(holder.ivUserIcon);*/
                Utils.setImageRounded(context, contestObj.getAdminContestResultWinners().get(0).getWinnerUser().getProfileImage(), holder.ivUserIcon);
            }


            try {
                holder.userName.setText(contestObj.getAdminContestResultWinners().get(0).getWinnerUser().getFirstName() + " ");
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                holder.userName.append(contestObj.getAdminContestResultWinners().get(0).getWinnerUser().getLastName());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public int getItemCount() {
        return contestList.size();
    }

    public class LastWeekWinnerViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.contest_icon)
        ImageView ivUserIcon;

        @BindView(R.id.user_name)
        TextView userName;

        @BindView(R.id.contest_name)
        TextView contestName;

        @BindView(R.id.cardView)
        CardView cardView;

        @BindView(R.id.vote_count)
        TextView voteCount;

        @BindView(R.id.check_talent_post)
        TextView checkTalentPost;

        @BindView(R.id.winner_rank)
        TextView winnerRank;


        LastWeekWinnerViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
