package com.artalent.ui.adapter;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.artalent.R;
import com.artalent.ui.activity.ContestRegisterUpdateActivity;
import com.artalent.ui.model.InterestObj;
import com.artalent.ui.utils.Constants;
import com.artalent.ui.view.RoundedImageView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class InterestUserListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<InterestObj> interestNameList;
    private String layoutName;
    private Activity context;
    private RequestOptions options;

    public InterestUserListAdapter(List<InterestObj> interestNameList, String layoutName, Activity context) {
        this.interestNameList = interestNameList;
        this.layoutName = layoutName;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v;
        options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.interest_categories)
                .error(R.drawable.interest_categories);
        switch (layoutName) {
            case Constants.INTEREST_LIST_NAME:

            case Constants.INTEREST_SHOW_ALL_LIST_NAME:
                v = LayoutInflater.from(viewGroup.getContext()).
                        inflate(R.layout.item_interest, viewGroup, false);
                return new InterestUserListAdapter.InterestHolder(v);

            case Constants.INTEREST_CONTEST_LIST_NAME:
                v = LayoutInflater.from(viewGroup.getContext()).
                        inflate(R.layout.item_category, viewGroup, false);
                return new InterestUserListAdapter.CategoryHolder(v);
            default:
                v = LayoutInflater.from(viewGroup.getContext()).
                        inflate(R.layout.item_interest_search, viewGroup, false);
                return new InterestUserListAdapter.SearchInterestHolder(v);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        InterestObj interestObj = interestNameList.get(i);
        String interestName = interestObj.getInterest();
        if (layoutName.equalsIgnoreCase(Constants.INTEREST_LIST_NAME)) {
            InterestHolder holder = (InterestHolder) viewHolder;
            holder.tvInterestName.setText(interestName);
        } else if (layoutName.equalsIgnoreCase(Constants.INTEREST_CONTEST_LIST_NAME)) {
            CategoryHolder categoryHolder = (CategoryHolder) viewHolder;
            categoryHolder.itemView.setTag(interestObj);
            categoryHolder.itemView.setOnClickListener(v -> {
                InterestObj interestObjTag = (InterestObj) v.getTag();
                if (interestObjTag != null) {
                    Intent intent = new Intent(context, ContestRegisterUpdateActivity.class);
                    intent.putExtra(Constants.INTEREST_OBJ, interestObjTag);
                    context.startActivityForResult(intent, Constants.REFRESH_CONTEST_LIST);
                }
            });
            categoryHolder.tvCategoryName.setText(interestName);
            Glide.with(context)
                    .load(interestObj.getIcon()).apply(new RequestOptions()
                    .centerCrop()
                    .placeholder(R.drawable.interest_categories)
                    .transform(new RoundedCorners(100))
                    .error(R.drawable.interest_categories)).into(categoryHolder.ivCategoryImage);
//            categoryHolder.ivCategoryImage.setImageDrawable(context.getResources().getDrawable(R.drawable.singing_icon));
        } else if (layoutName.equalsIgnoreCase(Constants.INTEREST_SHOW_ALL_LIST_NAME)) {
            InterestHolder holder = (InterestHolder) viewHolder;
            holder.itemView.setTag(interestObj);
            holder.ivSelection.setVisibility(View.GONE);
            holder.tvInterestName.setText(interestName);
            Log.d("TAG", i + interestObj.getIcon());
            Glide.with(context).load(interestObj.getIcon()).apply(options).into(holder.ivCategoryImage);
            Log.d("TAG", "after Glide instance called: ");


        } else {
            SearchInterestHolder searchInterestHolder = (SearchInterestHolder) viewHolder;
            searchInterestHolder.categoryName.setText(interestName);
            searchInterestHolder.categoryImage.setImageDrawable(context.getResources().getDrawable(R.drawable.singing_icon));
        }

    }

    @Override
    public int getItemCount() {
        return interestNameList.size();
    }

    class CategoryHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.category_image)
        ImageView ivCategoryImage;

        @BindView(R.id.category_name)
        TextView tvCategoryName;

        CategoryHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    class SearchInterestHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.category_image)
        ImageView categoryImage;

        @BindView(R.id.category_name)
        TextView categoryName;

        SearchInterestHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    class InterestHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.selection)
        ImageView ivSelection;
        @BindView(R.id.name)
        TextView tvInterestName;
        @BindView(R.id.category_image)
        RoundedImageView ivCategoryImage;
        @BindView(R.id.llMain)
        LinearLayout linearMain;

        InterestHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }

    }

}
