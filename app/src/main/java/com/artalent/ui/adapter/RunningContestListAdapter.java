package com.artalent.ui.adapter;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.artalent.R;
import com.artalent.ui.activity.ContestPostActivity;
import com.artalent.ui.model.ContestObj;
import com.artalent.ui.utils.Constants;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RunningContestListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<ContestObj> contestList;
    private RequestOptions options;
    private Context context;

    public RunningContestListAdapter(List<ContestObj> contestList, Context context) {
        this.contestList = contestList;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).
                inflate(R.layout.item_running_contest_home, viewGroup, false);
        options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.interest_categories)
                .error(R.drawable.interest_categories);
        return new RunningContestViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        RunningContestViewHolder holder = (RunningContestViewHolder) viewHolder;
        ContestObj contestObj = contestList.get(i);
        if (contestObj != null) {
            if (contestObj.getFlag() == 1) {
                holder.tvContestTitle.setText("Contest for " + contestObj.getDistrictEntity().getDistrict());
                holder.tvContestLocation.setText(contestObj.getDistrictEntity().getDistrict());
            } else if (contestObj.getFlag() == 2) {
                holder.tvContestTitle.setText("Contest for " + contestObj.getStateEntity().getState());
                holder.tvContestLocation.setText(contestObj.getStateEntity().getState());
            } else {
                holder.tvContestTitle.setText("Contest for " + contestObj.getCountryEntity().getCountryName());
                holder.tvContestLocation.setText(contestObj.getCountryEntity().getCountryName());
            }
        }
        holder.tvContestTitle.setVisibility(View.INVISIBLE);
        if (contestObj != null && contestObj.getInterest() != null && !TextUtils.isEmpty(contestObj.getInterest().getInterest())) {
            Glide.with(context)
                    .load(contestObj.getInterest().getBaner()).apply(options).into(holder.ivContestCategoryImage);
            holder.tvContestCategoryName.setText(contestObj.getInterest().getInterest()+" Contest");
        } else {
            holder.tvContestCategoryName.setText("");
        }

        holder.itemView.setTag(contestObj);

        holder.itemView.setOnClickListener(v -> {
            ContestObj contestObj1 = (ContestObj) v.getTag();
            if (contestObj1 != null) {
                Intent intent = new Intent(v.getContext(), ContestPostActivity.class);
                intent.putExtra(Constants.DATA, contestObj1);
                v.getContext().startActivity(intent);
            }


        });
    }

    @Override
    public int getItemCount() {
        return contestList.size();
    }

    static class RunningContestViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.contest_title)
        TextView tvContestTitle;

        @BindView(R.id.contest_category_name)
        TextView tvContestCategoryName;

        @BindView(R.id.contest_category_image)
        ImageView ivContestCategoryImage;

        @BindView(R.id.contest_location)
        TextView tvContestLocation;

        RunningContestViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
