package com.artalent.ui.adapter;

import android.app.Activity;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.artalent.Data;
import com.artalent.R;
import com.artalent.Utils;
import com.artalent.ui.model.AudioObj;
import com.example.jean.jcplayer.model.JcAudio;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MusicListAdapterAdmin extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Activity context;

    private List<JcAudio> stringList;
    private List<AudioObj> audioObjList;
    private static OnItemClickListener mListener;
    private static final String TAG = MusicListAdapterAdmin.class.getSimpleName();
    private SparseArray<Float> progressMap = new SparseArray<>();

    private int checkedPosition = 0;

    public MusicListAdapterAdmin(Activity context, List<JcAudio> stringList, List<AudioObj> audioObjList) {
        this.context = context;
        this.stringList = stringList;
        this.audioObjList = audioObjList;
        setHasStableIds(true);
    }

    public void setAudio(ArrayList<AudioObj> audioObjList) {
        this.audioObjList = new ArrayList<>();
        this.audioObjList = audioObjList;
        notifyDataSetChanged();
    }

    public AudioObj getSelected() {
        if (checkedPosition != -1) {
            return audioObjList.get(checkedPosition);
        }
        return null;
    }

    public interface OnItemClickListener {
        void onItemClick(int position);

        void onDownloadImage(AudioObj audioObj);

        void onDownloadedBtnClicked();

        void onDeleteClick(AudioObj postObj, int position);

        void onSongItemDeleteClicked(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_music, parent, false);
        return new MusicListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        MusicListViewHolder musicListViewHolder = (MusicListViewHolder) holder;
        AudioObj stateObj = audioObjList.get(position);
        ((MusicListViewHolder) holder).rlName.setTag(stateObj);
        ((MusicListViewHolder) holder).ivDelete.setTag(stateObj);
        ((MusicListViewHolder) holder).selected.setVisibility(View.GONE);
        ((MusicListViewHolder) holder).btnDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AudioObj feedItem = (AudioObj) v.getTag();
                mListener.onDownloadImage(feedItem);

            }
        });

        ((MusicListViewHolder) holder).ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AudioObj feedItem = (AudioObj) v.getTag();
                mListener.onDeleteClick(feedItem, position);

            }
        });

        ((MusicListViewHolder) holder).imgDownloaded.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onDownloadedBtnClicked();
            }
        });
        String filePath = Data.getSaveDir() + "ARTalentAudio/" + Data.getNameFromUrl(stateObj.getAudioUrl());
        File file = new File(filePath);
        if (checkedPosition == -1) {
            ((MusicListViewHolder) holder).selected.setChecked(false);
        } else {
            if (checkedPosition == holder.getAdapterPosition()) {
                ((MusicListViewHolder) holder).selected.setChecked(true);
            } else {
                ((MusicListViewHolder) holder).selected.setChecked(false);
            }
        }

        ((MusicListViewHolder) holder).selected.setOnClickListener(view -> {
            if (file.exists()) {
                ((MusicListViewHolder) holder).selected.setChecked(true);
            } else {
                Utils.showToast(context, "Please first download your selected Music/Song");
                ((MusicListViewHolder) holder).selected.setChecked(false);
            }
            if (checkedPosition != holder.getAdapterPosition()) {
                notifyItemChanged(checkedPosition);
                checkedPosition = holder.getAdapterPosition();
            }
        });
        applyProgressPercentage((MusicListViewHolder) holder, progressMap.get(position, 0.0f));
        ((MusicListViewHolder) holder).btnDownload.setTag(stateObj);

        if (stateObj != null) {

            if (file.exists()) {
                ((MusicListViewHolder) holder).imgDownloaded.setVisibility(View.VISIBLE);
                ((MusicListViewHolder) holder).btnDownload.setVisibility(View.GONE);
            } else {
                ((MusicListViewHolder) holder).imgDownloaded.setVisibility(View.GONE);
                ((MusicListViewHolder) holder).btnDownload.setVisibility(View.VISIBLE);
            }
            if (!TextUtils.isEmpty(stateObj.getSongName())) {
                String songName = stateObj.getSongName();
                String songInitial = String.valueOf(songName.charAt(0));
                musicListViewHolder.songName.setText(stateObj.getSongName());
                musicListViewHolder.textInitial.setText(songInitial.toUpperCase());
            } else {
                musicListViewHolder.songName.setText("");
                musicListViewHolder.textInitial.setText("");
            }
            if (!TextUtils.isEmpty(stateObj.getType())) {
                musicListViewHolder.songType.setText(stateObj.getType());
            } else {
                musicListViewHolder.songType.setText("");
            }
            if (!TextUtils.isEmpty(stateObj.getDuration())) {
                musicListViewHolder.songDuration.setText(stateObj.getDuration() + " Sec");
            } else {
                musicListViewHolder.songDuration.setText("");
            }
        }
    }

    private void applyProgressPercentage(MusicListViewHolder holder, float percentage) {
        Log.d(TAG, "applyProgressPercentage() with percentage = " + percentage);
        LinearLayout.LayoutParams progress = (LinearLayout.LayoutParams) holder.viewProgress.getLayoutParams();
        LinearLayout.LayoutParams antiProgress = (LinearLayout.LayoutParams) holder.viewAntiProgress.getLayoutParams();

        progress.weight = percentage;
        holder.viewProgress.setLayoutParams(progress);

        antiProgress.weight = 1.0f - percentage;
        holder.viewAntiProgress.setLayoutParams(antiProgress);
    }

    public void updateProgress(JcAudio jcAudio, float progress) {
        int position = stringList.indexOf(jcAudio);
        Log.d(TAG, "Progress = " + progress);
        progressMap.put(position, progress);
        if (progressMap.size() > 1) {
            for (int i = 0; i < progressMap.size(); i++) {
                if (progressMap.keyAt(i) != position) {
                    Log.d(TAG, "KeyAt(" + i + ") = " + progressMap.keyAt(i));
                    notifyItemChanged(progressMap.keyAt(i));
                    progressMap.delete(progressMap.keyAt(i));
                }
            }
        }
        notifyItemChanged(position);
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return audioObjList.size();
    }

    public class MusicListViewHolder extends RecyclerView.ViewHolder {

        boolean isClicked = false;
        @BindView(R.id.text_initial)
        TextView textInitial;
        @BindView(R.id.img_downloaded)
        ImageButton imgDownloaded;
        @BindView(R.id.card_parent)
        CardView cardView;
        @BindView(R.id.selected)
        CheckBox selected;
        @BindView(R.id.song_name)
        TextView songName;
        @BindView(R.id.song_duration)
        TextView songDuration;
        @BindView(R.id.song_progress_view)
        View viewProgress;
        @BindView(R.id.song_anti_progress_view)
        View viewAntiProgress;
        @BindView(R.id.play_pause_button)
        ImageButton playPauseAudioBtn;
        @BindView(R.id.song_type)
        TextView songType;
        @BindView(R.id.btnDownload)
        ImageButton btnDownload;

        @BindView(R.id.rlName)
        RelativeLayout rlName;

        @BindView(R.id.ivDelete)
        ImageView ivDelete;


        MusicListViewHolder(@NonNull View view) {
            super(view);
            ButterKnife.bind(this, view);

            rlName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Triggers click upwards to the adapter on click
                    if (isClicked) {
                        isClicked = false;
                        playPauseAudioBtn.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_play_circle_filled_black_24dp));
                    } else {
                        isClicked = true;
                        playPauseAudioBtn.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_pause_circle_filled_black_24dp));
                    }
                    if (mListener != null) mListener.onItemClick(getAdapterPosition());
                }
            });


        }
    }
}
