package com.artalent.ui.adapter;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.artalent.ArtalentApplication;
import com.artalent.R;
import com.artalent.ui.model.StoryObj;
import com.artalent.ui.model.UserObj;
import com.artalent.ui.utils.CircleTransformation;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private String TAG = "StoryAdapter";
    private List<StoryObj> mStoryObjs;
    private View.OnClickListener mOnClickListener;

    public StoryAdapter(List<StoryObj> storyObjs, View.OnClickListener onClickListener) {
        this.mStoryObjs = storyObjs;
        this.mOnClickListener = onClickListener;
        Log.d(TAG, "StoryAdapter: " + storyObjs.toString());
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).
                inflate(R.layout.item_story, viewGroup, false);
        return new WinningContetViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        WinningContetViewHolder holder = (WinningContetViewHolder) viewHolder;

        StoryObj storyObj = mStoryObjs.get(i);

        if (storyObj.getUserId() == 0) {
            holder.cardMainView.setVisibility(View.GONE);
            holder.cartFirst.setVisibility(View.VISIBLE);
            holder.cartFirst.setOnClickListener(mOnClickListener);
            UserObj userObj = ArtalentApplication.prefManager.getUserObj();
            if (userObj != null && !TextUtils.isEmpty(userObj.getProfileImage())) {
                Picasso.get()
                        .load(userObj.getProfileImage())
                        .placeholder(R.drawable.man)
                        .into(holder.ivProfileUserRo);
            } else {
                Picasso.get()
                        .load(R.drawable.man)
                        .placeholder(R.drawable.man)
                        .into(holder.ivProfileUserRo);
            }

        } else {
            holder.cardMainView.setVisibility(View.VISIBLE);
            holder.cartFirst.setVisibility(View.GONE);

            holder.cardMainView.setTag(i);
            holder.cardMainView.setOnClickListener(mOnClickListener);
        /*ArrayList<StoryModel> uris = new ArrayList<>();
        for (StoryImagesObj storyImagesObj : storyObj.getStories()) {
            uris.add(new StoryModel(storyImagesObj.getStoryUrl(), storyImagesObj.getDesc(), "" + storyImagesObj.getStoryId()));
        }*/
            UserObj userObj = storyObj.getOwner();
            //holder.storyView.setImageUris(uris);
            holder.tvUserName.setText(userObj.getUserName());
            if (storyObj != null && storyObj.getStories() != null && storyObj.getStories().size() > 0) {
                Picasso.get()
                        .load(storyObj.getStories().get(0).getStoryUrl())
                        .placeholder(R.drawable.feed_placeholder)
                        .into(holder.ivStory);
            } else {
                Picasso.get()
                        .load(R.drawable.feed_placeholder)
                        .placeholder(R.drawable.feed_placeholder)
                        .transform(new CircleTransformation())
                        .into(holder.ivStory);
            }

//            if (userObj != null && !TextUtils.isEmpty(userObj.getProfileImage())) {
//                Picasso.get()
//                        .load(userObj.getProfileImage())
//                        .placeholder(R.drawable.man)
//                        .resize(150, 150)
//                        .transform(new CircleTransformation())
//                        .into(holder.ivPersionStory);
//            } else {
//                Picasso.get()
//                        .load(R.drawable.man)
//                        .placeholder(R.drawable.man)
//                        .transform(new CircleTransformation())
//                        .into(holder.ivPersionStory);
//            }
        }
    }


    @Override
    public int getItemCount() {
        return mStoryObjs.size();
    }


    public class WinningContetViewHolder extends RecyclerView.ViewHolder {

        //@BindView(R.id.storyView)
        //StoryView storyView;


        @BindView(R.id.cardMainView)
        CardView cardMainView;

        @BindView(R.id.cartFirst)
        CardView cartFirst;

        @BindView(R.id.ivProfileUserRo)
        ImageView ivProfileUserRo;

        @BindView(R.id.tvUserName)
        TextView tvUserName;

        @BindView(R.id.ivStory)
        ImageView ivStory;

        @BindView(R.id.ivPersionStory)
        ImageView ivPersionStory;


        WinningContetViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
