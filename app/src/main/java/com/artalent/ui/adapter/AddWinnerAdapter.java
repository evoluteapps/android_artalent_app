package com.artalent.ui.adapter;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.RecyclerView;

import com.artalent.R;
import com.artalent.ui.model.ContestWinnerObj;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

/**
 * Created by Rohit mahajan on 18/05/16.
 */
public class AddWinnerAdapter extends RecyclerView.Adapter<AddWinnerAdapter.MyViewHolder> {

    private Context mContext;
    private ArrayList<ContestWinnerObj> list;
    private View.OnClickListener mOnClickListener;
    private OnEditTextChanged onEditTextChanged;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        //public TextView title, count;
        //ImageView ivClose, thumbnail, overflow;
        ImageView ivDelete;
        EditText edtWinnerTitle, edtPrice, edtRegUpto;


        MyViewHolder(View view) {
            super(view);
            ivDelete = view.findViewById(R.id.ivDelete);
            edtWinnerTitle = view.findViewById(R.id.edtWinnerTitle);
            edtPrice = view.findViewById(R.id.edtPrice);
            edtRegUpto = view.findViewById(R.id.edtRegUpto);
            /*title = view.findViewById(R.id.title);
            count = view.findViewById(R.id.count);
            thumbnail = view.findViewById(R.id.thumbnail);
            ivClose = view.findViewById(R.id.ivClose);
            overflow = view.findViewById(R.id.overflow);*/
        }
    }


    public AddWinnerAdapter(Context mContext, ArrayList<ContestWinnerObj> list, View.OnClickListener onClickListener, OnEditTextChanged onEditTextChanged) {
        this.mContext = mContext;
        this.list = list;
        this.mOnClickListener = onClickListener;
        this.onEditTextChanged = onEditTextChanged;
    }

    @NotNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_add_winner, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        if (list.size() == 1) {
            holder.ivDelete.setVisibility(View.GONE);
        } else {
            holder.ivDelete.setVisibility(View.VISIBLE);
        }
        holder.ivDelete.setTag(position);
        holder.ivDelete.setOnClickListener(mOnClickListener);

        holder.edtWinnerTitle.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                onEditTextChanged.onTextChangedTitle(position, charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        holder.edtPrice.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                onEditTextChanged.onTextChangedPrice(position, charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });


        holder.edtRegUpto.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                onEditTextChanged.onTextChangedRegUpto(position, charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });



        /*File file = new File(list.get(position));
        Picasso.get()
                .load(file)
                .into(holder.thumbnail);

        // loading album cover using Glide library
        // Picasso.with(mContext).load(album.getThumbnail()).into(holder.thumbnail);

        holder.overflow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPopupMenu(holder.overflow);
            }
        });
        holder.ivClose.setTag(position);
        holder.ivClose.setOnClickListener(mOnClickListener);*/
    }

    /**
     * Showing popup menu when tapping on 3 dots
     */
    private void showPopupMenu(View view) {
        // inflate menu
        PopupMenu popup = new PopupMenu(mContext, view);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.menu_album, popup.getMenu());
        popup.setOnMenuItemClickListener(new MyMenuItemClickListener());
        popup.show();
    }

    /**
     * Click listener for popup menu items
     */
    class MyMenuItemClickListener implements PopupMenu.OnMenuItemClickListener {

        MyMenuItemClickListener() {
        }

        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            switch (menuItem.getItemId()) {
                case R.id.action_add_favourite:
                    Toast.makeText(mContext, "Add to favourite", Toast.LENGTH_SHORT).show();
                    return true;
                case R.id.action_play_next:
                    Toast.makeText(mContext, "Play next", Toast.LENGTH_SHORT).show();
                    return true;
                default:
            }
            return false;
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface OnEditTextChanged {
        void onTextChangedTitle(int position, String charSeq);

        void onTextChangedPrice(int position, String charSeq);

        void onTextChangedRegUpto(int position, String charSeq);
    }
}
