package com.artalent.ui.razorpay;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.artalent.ArtalentApplication;
import com.artalent.R;
import com.artalent.Utils;
import com.artalent.ui.activity.BaseActivityOther;
import com.artalent.ui.dialog.PaymentDialog;
import com.artalent.ui.model.BaseResponse;
import com.artalent.ui.model.CheckAccountResponse;
import com.artalent.ui.model.FundAccountResponse;
import com.artalent.ui.model.PayoutResponse;
import com.artalent.ui.model.RazorpayContactResponse;
import com.artalent.ui.model.RazorpayErrorResponse;
import com.artalent.ui.model.UserObj;
import com.artalent.ui.model.request.AccountFoundReq;
import com.artalent.ui.model.request.AccountReq;
import com.artalent.ui.model.request.AmountCreditDebitReq;
import com.artalent.ui.model.request.CardFoundReq;
import com.artalent.ui.model.request.CardReq;
import com.artalent.ui.model.request.CheckAccountReq;
import com.artalent.ui.model.request.NotesDataReq;
import com.artalent.ui.model.request.PayOutReq;
import com.artalent.ui.model.request.RazorpayContactReq;
import com.artalent.ui.model.request.VPAFoundContactReq;
import com.artalent.ui.model.request.VPAReq;
import com.artalent.ui.utils.Constants;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import butterknife.BindView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RazorpayTestActivity extends BaseActivityOther {

    @BindView(R.id.response_result)
    TextView tvResponse;

    @BindView(R.id.btn)
    Button btnCreateContact;

    @BindView(R.id.total_balance_amount)
    TextView totalBalanceAmount;

    @BindView(R.id.tvName)
    TextView tvName;

    @BindView(R.id.tvMobile)
    TextView tvMobile;

    @BindView(R.id.tvEmail)
    TextView tvEmail;

    @BindView(R.id.edtEmail)
    TextInputEditText edtEmail;

    @BindView(R.id.edtUPI)
    TextInputEditText edtUPI;

    @BindView(R.id.edtCard)
    TextInputEditText edtCard;

    @BindView(R.id.edtIFSC)
    TextInputEditText edtIFSC;

    @BindView(R.id.edtAccount)
    TextInputEditText edtAccount;

    @BindView(R.id.edtAmount)
    TextInputEditText edtAmount;

    @BindView(R.id.radioGroup)
    RadioGroup radioGroup;

    @BindView(R.id.inputUPI)
    TextInputLayout inputUPI;

    @BindView(R.id.inputCard)
    TextInputLayout inputCard;

    @BindView(R.id.inputIFSC)
    TextInputLayout inputIFSC;

    @BindView(R.id.inputAccount)
    TextInputLayout inputAccount;

    private double dTotalWalletBalance;
    private String sType = Constants.VPA;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_razorpay_test);
        dTotalWalletBalance = getIntent().getDoubleExtra(Constants.DATA, 0.0);
        setData();
        sType = Constants.VPA;
        setViewData();
        edtIFSC.setText("BARB0KHARGO");
        edtAccount.setText("29790100008070");
        radioGroup.setOnCheckedChangeListener((group, checkedId) -> {
            // checkedId is the RadioButton selected
            if (checkedId == R.id.upi) {
                sType = Constants.VPA;
            } else if (checkedId == R.id.card) {
                sType = Constants.CARD;
            } else if (checkedId == R.id.bank_account) {
                sType = Constants.BANK_ACCOUNT;
            }
            setViewData();
        });
        btnCreateContact.setOnClickListener(v -> submitData());


    }

    private void submitData() {
        if (TextUtils.isEmpty(edtAmount.getText().toString())) {
            Utils.showToast(mActivity, getString(R.string.plz_enter_amount));
        } else if (Double.parseDouble(edtAmount.getText().toString()) < 100) {
            Utils.showToast(mActivity, "withdrawal amount at least 100 rupees");
        } else if (TextUtils.isEmpty(edtEmail.getText().toString())) {
            Utils.showToast(mActivity, getString(R.string.plz_enter_email));
        } else {
            if (sType.equalsIgnoreCase(Constants.VPA)) {
                if (TextUtils.isEmpty(edtUPI.getText().toString())) {
                    Utils.showToast(mActivity, getString(R.string.plz_enter_upi));
                } else {
                    callCheckUPIApi();
                    //
                }

            } else if (sType.equalsIgnoreCase(Constants.CARD)) {
                if (TextUtils.isEmpty(edtCard.getText().toString())) {
                    Utils.showToast(mActivity, getString(R.string.plz_enter_card));
                } else {
                    callRazorpayContactApi();
                }

            } else if (sType.equalsIgnoreCase(Constants.BANK_ACCOUNT)) {
                if (TextUtils.isEmpty(edtIFSC.getText().toString())) {
                    Utils.showToast(mActivity, getString(R.string.plz_enter_ifsc));
                } else if (TextUtils.isEmpty(edtAccount.getText().toString())) {
                    Utils.showToast(mActivity, getString(R.string.plz_enter_account_no));
                } else {
                    callRazorpayContactApi();
                }

            }
        }

    }

    private void setViewData() {
        if (sType.equalsIgnoreCase(Constants.VPA)) {
            inputUPI.setVisibility(View.VISIBLE);
            inputCard.setVisibility(View.GONE);
            inputIFSC.setVisibility(View.GONE);
            inputAccount.setVisibility(View.GONE);

        } else if (sType.equalsIgnoreCase(Constants.CARD)) {
            inputUPI.setVisibility(View.GONE);
            inputCard.setVisibility(View.VISIBLE);
            inputIFSC.setVisibility(View.GONE);
            inputAccount.setVisibility(View.GONE);

        } else if (sType.equalsIgnoreCase(Constants.BANK_ACCOUNT)) {
            inputUPI.setVisibility(View.GONE);
            inputCard.setVisibility(View.GONE);
            inputIFSC.setVisibility(View.VISIBLE);
            inputAccount.setVisibility(View.VISIBLE);

        }
    }


    private void setData() {
        tvEmail.setText("");
        totalBalanceAmount.setText(getResources().getString(R.string.rupee) + " " + dTotalWalletBalance);
        UserObj userObj = ArtalentApplication.prefManager.getUserObj();
        if (userObj != null) {
            if (!TextUtils.isEmpty(userObj.getFirstName()) && !TextUtils.isEmpty(userObj.getLastName())) {
                tvName.setText(userObj.getFirstName() + " " + userObj.getLastName());
            } else {
                tvName.setText("");
            }
            if (!TextUtils.isEmpty(userObj.getMobile())) {
                tvMobile.setText(userObj.getMobile());
            } else {
                tvMobile.setText("");
            }
        }
    }

    private void callCheckUPIApi() {
        if (Utils.isInternetAvailable(mActivity)) {
            showProgress();

            CheckAccountReq checkAccountReq = new CheckAccountReq();
            checkAccountReq.setEntity("vpa");
            checkAccountReq.setValue(edtUPI.getText().toString());

            Call<CheckAccountResponse> call = ArtalentApplication.apiRazorpay.validateAccount(Constants.RAZORPAY_TEST_API_KEY, checkAccountReq);
            call.enqueue(new Callback<CheckAccountResponse>() {//1
                @Override
                public void onResponse(Call<CheckAccountResponse> call, Response<CheckAccountResponse> response) {
                    hideProgress();
                    int statusCode = response.code();
                    if (response.body() != null) {
                        CheckAccountResponse razorpayContactResponse = response.body();
                        if (razorpayContactResponse != null && razorpayContactResponse.isSuccess() && !TextUtils.isEmpty(razorpayContactResponse.getVpa())) {
                            callRazorpayContactApi();
                        } else {
                            Utils.showToast(mActivity, (response.message()).replaceAll("VPA", "UPI ID"));
                        }
                    } else {
                        getErrorMessage(response.errorBody());
                        hideProgress();
                    }
                }

                @Override
                public void onFailure(Call<CheckAccountResponse> call, Throwable t) {
                    // Log error here since request failed
                    Utils.showToast(mActivity, t.toString());
                    hideProgress();
                }
            });
        }
    }

    private void callRazorpayContactApi() {
        if (Utils.isInternetAvailable(mActivity)) {
            showProgress();
            Call<RazorpayContactResponse> call = ArtalentApplication.apiRazorpay.createRazorpayContact(fillRazorpayContact());
            call.enqueue(new Callback<RazorpayContactResponse>() {//1
                @Override
                public void onResponse(Call<RazorpayContactResponse> call, Response<RazorpayContactResponse> response) {
                    hideProgress();
                    int statusCode = response.code();
                    if (response.body() != null) {
                        //Utils.showToast(mActivity, "Created Successfully, " + response.message());
                        RazorpayContactResponse razorpayContactResponse = response.body();
                        if (razorpayContactResponse != null && razorpayContactResponse.getId() != null && !TextUtils.isEmpty(razorpayContactResponse.getId())) {
                            if (sType.equalsIgnoreCase(Constants.VPA)) {
                                callRazorpayVPAApi(razorpayContactResponse.getId());
                            } else if (sType.equalsIgnoreCase(Constants.CARD)) {
                                callByCardApi(razorpayContactResponse.getId());
                            } else if (sType.equalsIgnoreCase(Constants.BANK_ACCOUNT)) {
                                callByAccountApi(razorpayContactResponse.getId());
                            } else {
                                Utils.showToast(mActivity, response.message());
                            }
                        } else {
                            Utils.showToast(mActivity, response.message());
                        }
                        /*tvResponse.setText("Name - " + razorpayContactResponse.getName() + "/n Contact - " + razorpayContactResponse.getContact() +
                                "/n Status - " + razorpayContactResponse.getActive());*/
                    } else {
                        getErrorMessage(response.errorBody());
                        hideProgress();
                    }
                }

                @Override
                public void onFailure(Call<RazorpayContactResponse> call, Throwable t) {
                    // Log error here since request failed
                    Utils.showToast(mActivity, t.toString());
                    hideProgress();
                }
            });
        }
    }

    private RazorpayContactReq fillRazorpayContact() {
        RazorpayContactReq razorpayContactReq = new RazorpayContactReq();
        razorpayContactReq.setName(tvName.getText().toString());
        razorpayContactReq.setEmail(edtEmail.getText().toString());
        razorpayContactReq.setContact(tvMobile.getText().toString());
        razorpayContactReq.setType("employee");
        razorpayContactReq.setReference_id("ARTalent new contact");
        return razorpayContactReq;
    }

    /*
    VPA
     */
    private void callRazorpayVPAApi(String id) {
        if (Utils.isInternetAvailable(mActivity)) {
            showProgress();
            Call<FundAccountResponse> call = ArtalentApplication.apiRazorpay.createRazorpayFoundVPA(fillRazorpayVPA(id));
            call.enqueue(new Callback<FundAccountResponse>() {//2
                @Override
                public void onResponse(Call<FundAccountResponse> call, Response<FundAccountResponse> response) {
                    hideProgress();
                    int statusCode = response.code();
                    if (response.body() != null) {
                        FundAccountResponse fundAccountResponse = response.body();
                        if (fundAccountResponse != null && fundAccountResponse.getId() != null && !TextUtils.isEmpty(fundAccountResponse.getId())) {
                            callPayoutsApi(fundAccountResponse.getId());
                        }
                    } else {
                        getErrorMessage(response.errorBody());
                        hideProgress();
                    }
                }

                @Override
                public void onFailure(Call<FundAccountResponse> call, Throwable t) {
                    // Log error here since request failed
                    Utils.showToast(mActivity, t.toString());
                    hideProgress();
                }
            });
        }
    }

    private VPAFoundContactReq fillRazorpayVPA(String id) {
        VPAFoundContactReq razorpayContactReq = new VPAFoundContactReq();
        razorpayContactReq.setAccount_type(sType);
        razorpayContactReq.setContact_id(id);
        VPAReq vpa = new VPAReq();
        vpa.setAddress(edtUPI.getText().toString());
        razorpayContactReq.setVpa(vpa);
        return razorpayContactReq;
    }


    private void callPayoutsApi(String id) {
        if (Utils.isInternetAvailable(mActivity)) {
            showProgress();
            Call<PayoutResponse> call = ArtalentApplication.apiRazorpay.createRazorpayPayouts(fillCheckOut(id));
            call.enqueue(new Callback<PayoutResponse>() {//3
                @Override
                public void onResponse(Call<PayoutResponse> call, Response<PayoutResponse> response) {
                    hideProgress();
                    int statusCode = response.code();
                    if (response.body() != null) {
                        PayoutResponse payoutResponse = response.body();
                        if (payoutResponse != null && payoutResponse.getId() != null && !TextUtils.isEmpty(payoutResponse.getId())) {
                            callDebitAmount();
                        }
                    } else {
                        getErrorMessage(response.errorBody());
                        hideProgress();
                    }
                }

                @Override
                public void onFailure(Call<PayoutResponse> call, Throwable t) {
                    // Log error here since request failed
                    Utils.showToast(mActivity, t.toString());
                    hideProgress();
                }
            });
        }
    }

    private void callDebitAmount() {
        if (Utils.isInternetAvailable(mActivity)) {
            showProgress();
            Call<BaseResponse> call = ArtalentApplication.apiService.amountCreditDebit(ArtalentApplication.prefManager.getUserObj().getId(), fillDataDistrict(

            ));
            call.enqueue(new Callback<BaseResponse>() {
                @Override
                public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                    hideProgress();
                    int statusCode = response.code();
                    String mess = response.body().getMessage();
                    if (statusCode == 200) {
                        String des = "";
                        if (sType.equalsIgnoreCase(Constants.VPA)) {
                            des = String.format(getResources().getString(R.string.payment_des_upi),
                                    "" + edtAmount.getText().toString());
                        } else if (sType.equalsIgnoreCase(Constants.BANK_ACCOUNT)) {
                            des = String.format(getResources().getString(R.string.payment_des),
                                    "" + edtAmount.getText().toString());
                        }

                        PaymentDialog rateDialog = new PaymentDialog(RazorpayTestActivity.this, getResources().getString(R.string.payment_title),
                                des, true);
                        rateDialog.show();
                    } else {
                        //hideProgress();
                        Utils.showToast(mActivity, mess);
                    }
                }

                @Override
                public void onFailure(Call<BaseResponse> call, Throwable t) {
                    // Log error here since request failed
                    hideProgress();
                    Utils.showToast(mActivity, "onFailure : " + t.toString());
                }
            });
        }
    }

    private AmountCreditDebitReq fillDataDistrict() {
        AmountCreditDebitReq districtReq = new AmountCreditDebitReq();
        districtReq.setAmount(Double.parseDouble(edtAmount.getText().toString()));
        districtReq.setDescription("Amount transferred from ARTalent wallet to your added Account.");
        districtReq.setTransactionType("DEBIT");
        return districtReq;
    }

    private PayOutReq fillCheckOut(String id) {

        PayOutReq razorpayContactReq = new PayOutReq();
        razorpayContactReq.setAccount_number(Constants.RAZORPAY_ACCOUNT_NUMBER);
        razorpayContactReq.setFund_account_id(id);

        razorpayContactReq.setAmount(Double.parseDouble(edtAmount.getText().toString()));
        razorpayContactReq.setCurrency(Constants.CURRENCY);

        if (sType.equalsIgnoreCase(Constants.VPA)) {
            razorpayContactReq.setMode("UPI");
        } else if (sType.equalsIgnoreCase(Constants.BANK_ACCOUNT)) {
            razorpayContactReq.setMode("IMPS");
        }
        razorpayContactReq.setPurpose("cashback");

        razorpayContactReq.setQueue_if_low_balance(false);
        razorpayContactReq.setReference_id("Acme Transaction ID 12345");
        razorpayContactReq.setNarration("");


        NotesDataReq vpa = new NotesDataReq();
        vpa.setNotes_key_1("Tea, Earl Grey, Hot");
        vpa.setNotes_key_2("Tea, Earl Grey… decaf.");
        razorpayContactReq.setNotes(vpa);
        return razorpayContactReq;
    }

    /*
   call By CardApi
    */
    private void callByCardApi(String id) {
        if (Utils.isInternetAvailable(mActivity)) {
            showProgress();
            Call<FundAccountResponse> call = ArtalentApplication.apiRazorpay.createRazorpayFoundVPA(fillByCard(id));
            call.enqueue(new Callback<FundAccountResponse>() {//4
                @Override
                public void onResponse(Call<FundAccountResponse> call, Response<FundAccountResponse> response) {
                    hideProgress();
                    int statusCode = response.code();
                    if (response.body() != null) {
                        FundAccountResponse fundAccountResponse = response.body();
                        if (fundAccountResponse != null && fundAccountResponse.getId() != null && !TextUtils.isEmpty(fundAccountResponse.getId())) {
                            //Utils.showToast(RazorpayTestActivity.this,"hop");
                            callPayoutsApi(fundAccountResponse.getId());
                        }
                    } else {
                        getErrorMessage(response.errorBody());

                        hideProgress();
                    }
                }

                @Override
                public void onFailure(Call<FundAccountResponse> call, Throwable t) {
                    // Log error here since request failed
                    Utils.showToast(mActivity, t.toString());
                    hideProgress();
                }
            });
        }
    }

    private void getErrorMessage(ResponseBody errorBody) {
        try {
            Gson gson = new Gson();
            Type type = new TypeToken<RazorpayErrorResponse>() {
            }.getType();
            RazorpayErrorResponse errorResponse = gson.fromJson(errorBody.charStream(), type);
            Utils.showToast(mActivity, errorResponse.getError().getDescription());
        } catch (JsonIOException | JsonSyntaxException e) {
            Utils.showToast(mActivity, e.getMessage());
            e.printStackTrace();
        }
    }

    private CardFoundReq fillByCard(String id) {
        CardFoundReq razorpayContactReq = new CardFoundReq();
        razorpayContactReq.setAccount_type(sType);
        razorpayContactReq.setContact_id(id);
        CardReq cardReq = new CardReq();
        cardReq.setName(tvName.getText().toString());
        cardReq.setNumber(edtCard.getText().toString());
        razorpayContactReq.setCard(cardReq);
        return razorpayContactReq;
    }


    /*
  call By CardApi
   */
    private void callByAccountApi(String id) {
        if (Utils.isInternetAvailable(mActivity)) {
            showProgress();
            Call<FundAccountResponse> call = ArtalentApplication.apiRazorpay.createRazorpayFoundVPA(fillByAccount(id));
            call.enqueue(new Callback<FundAccountResponse>() {//4
                @Override
                public void onResponse(Call<FundAccountResponse> call, Response<FundAccountResponse> response) {
                    hideProgress();
                    int statusCode = response.code();
                    if (response.body() != null) {
                        FundAccountResponse fundAccountResponse = response.body();
                        if (fundAccountResponse != null && fundAccountResponse.getId() != null && !TextUtils.isEmpty(fundAccountResponse.getId())) {
                            callPayoutsApi(fundAccountResponse.getId());
                        }
                    } else {
                        getErrorMessage(response.errorBody());

                        hideProgress();
                    }
                }

                @Override
                public void onFailure(Call<FundAccountResponse> call, Throwable t) {
                    // Log error here since request failed
                    Utils.showToast(mActivity, t.toString());
                    hideProgress();
                }
            });
        }
    }

    private AccountFoundReq fillByAccount(String id) {
        AccountFoundReq razorpayContactReq = new AccountFoundReq();
        razorpayContactReq.setAccount_type(sType);
        razorpayContactReq.setContact_id(id);
        AccountReq cardReq = new AccountReq();
        cardReq.setName(tvName.getText().toString());
        cardReq.setIfsc(edtIFSC.getText().toString());
        cardReq.setAccount_number(edtAccount.getText().toString());
        razorpayContactReq.setBank_account(cardReq);
        return razorpayContactReq;
    }

}
