package com.artalent.ui.roomdb;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.artalent.ui.model.room.ContestVideoDraft;

import java.util.List;

@Dao
public interface ContestVideoDraftDao {

    @Query("SELECT * FROM contestvideodraft")
    LiveData<List<ContestVideoDraft>> getAllContestVideoDraft();

    @Query("SELECT COUNT(*) FROM contestvideodraft")
    LiveData<Integer> getSavedDraftCount();

    @Query("DELETE FROM contestvideodraft")
    void clearAllSavedDraft();

    @Insert
    void insertContestVideoDraft(ContestVideoDraft contestVideoDraft);

    @Update
    void updateContestVideoDraft(ContestVideoDraft contestVideoDraft);

    @Delete
    void deleteContestVideoDraft(ContestVideoDraft contestVideoDraft);

}
