package com.artalent.ui.roomdb;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.artalent.ui.model.room.ContestVideoDraft;

@Database(entities = {ContestVideoDraft.class}, version = 1)
@TypeConverters(TypeConverter.class)
public abstract class AppRoomDatabase extends RoomDatabase {
    private static final String LOG_TAG = AppRoomDatabase.class.getSimpleName();
    private static final Object LOCK = new Object();
    private static final String DATABASE_NAME = "draft_video_db";
    private static AppRoomDatabase sInstance;

    public static AppRoomDatabase getInstance(Context context) {
        if (sInstance == null) {
            synchronized (LOCK) {
                sInstance = Room.databaseBuilder(context.getApplicationContext(),
                        AppRoomDatabase.class, AppRoomDatabase.DATABASE_NAME)
                        .build();
            }
        }
        return sInstance;
    }
    public abstract ContestVideoDraftDao getContestVideoDraftDao();
}