package com.artalent.ui.roomdb.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.artalent.ui.model.room.ContestVideoDraft;
import com.artalent.ui.roomdb.repository.DraftRepository;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class DraftViewModel extends AndroidViewModel {

    private DraftRepository repository;

    private LiveData<List<ContestVideoDraft>> allContestVideoDraft;

    public DraftViewModel(@NonNull @NotNull Application application) {
        super(application);
        repository = new DraftRepository(application);
    }

    public void insert(ContestVideoDraft contestVideoDraft) {
        repository.insertContestVideoDraft(contestVideoDraft);
    }

    public void clearAllSavedDrafts() {
        repository.clearAllSavedDrafts();
    }

    public void delete(ContestVideoDraft contestVideoDraft) {
        repository.deleteContestVideoDraft(contestVideoDraft);
    }

    public LiveData<Integer> getSavedDraftCount() {
        return repository.getSavedDraftCount();
    }

    public LiveData<List<ContestVideoDraft>> getSavedDraft() {
        return repository.getAllContestVideoDraft();
    }
}
