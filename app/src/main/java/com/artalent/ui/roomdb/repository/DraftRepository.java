package com.artalent.ui.roomdb.repository;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.artalent.ui.model.room.ContestVideoDraft;
import com.artalent.ui.roomdb.AppRoomDatabase;
import com.artalent.ui.roomdb.ContestVideoDraftDao;

import java.util.List;

public class DraftRepository {

    private ContestVideoDraftDao dao;
//    private LiveData<List<ContestVideoDraft>> allContestVideoDraft;

    public DraftRepository(Application application) {
        AppRoomDatabase database = AppRoomDatabase.getInstance(application);
        dao = database.getContestVideoDraftDao();
    }

    // All methods

    public void insertContestVideoDraft(ContestVideoDraft contestVideoDraft) {
        new InsertDraftAsyncTask(dao).execute(contestVideoDraft);
    }

    private static class InsertDraftAsyncTask extends AsyncTask<ContestVideoDraft, Void, Void> {
        private ContestVideoDraftDao dao;

        private InsertDraftAsyncTask(ContestVideoDraftDao dao) {
            this.dao = dao;
        }

        @Override
        protected Void doInBackground(ContestVideoDraft... model) {
            // below line is use to insert our modal in dao.
            dao.insertContestVideoDraft(model[0]);
            return null;
        }
    }

    public void clearAllSavedDrafts() {
        new ClearAllDraftAsyncTask(dao).execute();
    }

    private static class ClearAllDraftAsyncTask extends AsyncTask<Void, Void, Void> {
        private ContestVideoDraftDao dao;

        private ClearAllDraftAsyncTask(ContestVideoDraftDao dao) {
            this.dao = dao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            dao.clearAllSavedDraft();
            return null;
        }
    }


    public void deleteContestVideoDraft(ContestVideoDraft contestVideoDraft) {
        new deleteDraftAsyncTask(dao).execute(contestVideoDraft);
    }

    private static class deleteDraftAsyncTask extends AsyncTask<ContestVideoDraft, Void, Void> {
        private ContestVideoDraftDao dao;

        private deleteDraftAsyncTask(ContestVideoDraftDao dao) {
            this.dao = dao;
        }

        @Override
        protected Void doInBackground(ContestVideoDraft... model) {
            // below line is use to insert our modal in dao.
            dao.deleteContestVideoDraft(model[0]);
            return null;
        }
    }


    public LiveData<Integer> getSavedDraftCount() {
        return dao.getSavedDraftCount();
    }

    public LiveData<List<ContestVideoDraft>> getAllContestVideoDraft() {
        return dao.getAllContestVideoDraft();
    }

}
