package com.artalent.ui.camerafilter.utils;


import com.artalent.ui.camerafilter.imageprocessors.Filter;

/**
 * @author Varun on 01/07/15.
 */
public interface ThumbnailCallback {

    void onThumbnailClick(Filter filter);
}
