package com.artalent.ui.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class Country implements Serializable {
    @SerializedName("countryId")
    private int countryId;
    @SerializedName("countryName")
    private String countryName;

    private boolean isCheck;

    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public boolean isCheck() {
        return isCheck;
    }

    public void setCheck(boolean check) {
        isCheck = check;
    }

    @Override
    public String toString() {
        return "Country{" +
                "countryId=" + countryId +
                ", countryName='" + countryName + '\'' +
                ", isCheck=" + isCheck +
                '}';
    }
}
