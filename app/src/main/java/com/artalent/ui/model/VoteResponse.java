package com.artalent.ui.model;

import com.google.gson.annotations.SerializedName;

public class VoteResponse extends BaseResponse {

    @SerializedName("obj")
    private String obj;

    public String getObj() {
        return obj;
    }

    public void setObj(String obj) {
        this.obj = obj;
    }
}
