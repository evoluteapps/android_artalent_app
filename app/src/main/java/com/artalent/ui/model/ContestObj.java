package com.artalent.ui.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;


public class ContestObj implements Serializable {
    @SerializedName("countryEntity")
    private Country countryEntity;

    @SerializedName("stateEntity")
    private StateObj stateEntity;

    @SerializedName("districtEntity")
    private DistrictsObj districtEntity;

    @SerializedName("flag")
    private int flag;

    @SerializedName("registrationDetails")
    private List<ContestRegistrationObj> registrationDetails;

    @SerializedName("interest")
    private InterestObj interest;

    public InterestObj getInterest() {
        return interest;
    }

    public void setInterest(InterestObj interest) {
        this.interest = interest;
    }

    public Country getCountryEntity() {
        return countryEntity;
    }

    public StateObj getStateEntity() {
        return stateEntity;
    }

    public DistrictsObj getDistrictEntity() {
        return districtEntity;
    }

    public int getFlag() {
        return flag;
    }

    public List<ContestRegistrationObj> getRegistrationDetails() {
        return registrationDetails;
    }

    @Override
    public String toString() {
        return "ContestObj{" +
                "countryEntity=" + countryEntity +
                ", stateEntity=" + stateEntity +
                ", districtEntity=" + districtEntity +
                ", flag=" + flag +
                ", registrationDetails=" + registrationDetails +
                ", interest=" + interest +
                '}';
    }
}
