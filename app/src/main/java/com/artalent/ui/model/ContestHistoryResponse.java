package com.artalent.ui.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;


public class ContestHistoryResponse {
    @SerializedName("obj")
    private List<ContestHistoryObj> obj;

    @SerializedName("index")
    private int index;

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public List<ContestHistoryObj> getObj() {
        return obj;
    }

    public void setObj(List<ContestHistoryObj> obj) {
        this.obj = obj;
    }
}
