package com.artalent.ui.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;


public class DistrictsResponse {
    @SerializedName("obj")
    private List<DistrictsObj> obj;

    public List<DistrictsObj> getObj() {
        return obj;
    }

    public void setObj(List<DistrictsObj> obj) {
        this.obj = obj;
    }
}
