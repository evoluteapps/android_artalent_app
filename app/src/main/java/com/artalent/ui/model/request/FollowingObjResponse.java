package com.artalent.ui.model.request;

import com.google.gson.annotations.SerializedName;

public class FollowingObjResponse {
    @SerializedName("obj")
    FollowingListResponse obj;

    @SerializedName("index")
    private int index;

    public FollowingListResponse getObj() {
        return obj;
    }

    public void setObj(FollowingListResponse obj) {
        this.obj = obj;
    }

    public int getIndex() {
        return index;
    }
    public void setIndex(int index) {
        this.index = index;
    }
}
