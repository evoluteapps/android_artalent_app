package com.artalent.ui.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CommentResponse {
    @SerializedName("obj")
    private List<CommentObj> obj;

    @SerializedName("index")
    private int index;

    public List<CommentObj> getObj() {
        return obj;
    }

    public void setObj(List<CommentObj> obj) {
        this.obj = obj;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
