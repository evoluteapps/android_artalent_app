package com.artalent.ui.model.request;

import com.google.gson.annotations.SerializedName;


public class FollowingReq {
    @SerializedName("followingId")
    private int followingId;

    public int getFollowingId() {
        return followingId;
    }

    public void setFollowingId(int followingId) {
        this.followingId = followingId;
    }
}
