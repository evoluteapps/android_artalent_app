package com.artalent.ui.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class FundAccountResponse implements Serializable {

    @SerializedName("id")
    private String id;
    @SerializedName("entity")
    private String entity;
    @SerializedName("contact_id")
    private String contact_id;
    @SerializedName("account_type")
    private String account_type;
    @SerializedName("active")
    private boolean active;
    @SerializedName("created_at")
    private String created_at;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }

    public String getContact_id() {
        return contact_id;
    }

    public void setContact_id(String contact_id) {
        this.contact_id = contact_id;
    }

    public String getAccount_type() {
        return account_type;
    }

    public void setAccount_type(String account_type) {
        this.account_type = account_type;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }
}
