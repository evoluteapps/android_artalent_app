package com.artalent.ui.model.request;

import com.google.gson.annotations.SerializedName;

public class CardFoundReq {

    @SerializedName("account_type")
    private String account_type;

    @SerializedName("contact_id")
    private String contact_id;

    @SerializedName("card")
    private CardReq card;

    public String getAccount_type() {
        return account_type;
    }

    public void setAccount_type(String account_type) {
        this.account_type = account_type;
    }

    public String getContact_id() {
        return contact_id;
    }

    public void setContact_id(String contact_id) {
        this.contact_id = contact_id;
    }

    public CardReq getCard() {
        return card;
    }

    public void setCard(CardReq card) {
        this.card = card;
    }
}
