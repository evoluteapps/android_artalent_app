package com.artalent.ui.model.request;

import com.google.gson.annotations.SerializedName;


public class ContestGetReq {
    @SerializedName("registrationIds")
    private int[] registrationIds;

    public int[] getRegistrationIds() {
        return registrationIds;
    }

    public void setRegistrationIds(int[] registrationIds) {
        this.registrationIds = registrationIds;
    }
}
