package com.artalent.ui.model.request;

import com.google.gson.annotations.SerializedName;

public class StateReq {

    @SerializedName("stateName")
    private String stateName;

    @SerializedName("countryId")
    private int countryId;

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }
}
