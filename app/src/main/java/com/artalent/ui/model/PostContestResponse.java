package com.artalent.ui.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;


public class PostContestResponse {
    @SerializedName("obj")
    private List<PostContestObj> obj;

    @SerializedName("index")
    private int index;

    public List<PostContestObj> getObj() {
        return obj;
    }

    public void setObj(List<PostContestObj> obj) {
        this.obj = obj;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
