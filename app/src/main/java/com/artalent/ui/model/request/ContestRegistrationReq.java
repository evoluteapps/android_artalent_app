package com.artalent.ui.model.request;

import com.google.gson.annotations.SerializedName;


public class ContestRegistrationReq {

    @SerializedName("contestId")
    private String contestId;
    @SerializedName("districtId")
    private String districtId;
    @SerializedName("interestId")
    private String interestId;
    @SerializedName("transactionId")
    private String transactionId;
    @SerializedName("referalDebit")
    private String referalDebit;
    @SerializedName("walletDebit")
    private String walletDebit;
    @SerializedName("registrationAmount")
    private String registrationAmount;

    @SerializedName("countryId")
    private String countryId;

    @SerializedName("stateId")
    private String stateId;

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public String getContestId() {
        return contestId;
    }

    public void setContestId(String contestId) {
        this.contestId = contestId;
    }

    public String getReferalDebit() {
        return referalDebit;
    }

    public String getWalletDebit() {
        return walletDebit;
    }

    public String getRegistrationAmount() {
        return registrationAmount;
    }

    public void setReferalDebit(String referalDebit) {
        this.referalDebit = referalDebit;
    }

    public void setWalletDebit(String walletDebit) {
        this.walletDebit = walletDebit;
    }

    public void setRegistrationAmount(String registrationAmount) {
        this.registrationAmount = registrationAmount;
    }


    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public String getDistrictId() {
        return districtId;
    }

    public void setDistrictId(String districtId) {
        this.districtId = districtId;
    }

    public String getInterestId() {
        return interestId;
    }

    public void setInterestId(String interestId) {
        this.interestId = interestId;
    }
}
