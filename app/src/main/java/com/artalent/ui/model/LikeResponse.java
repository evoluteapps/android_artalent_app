package com.artalent.ui.model;

import com.google.gson.annotations.SerializedName;


public class LikeResponse extends BaseResponse {
    @SerializedName("obj")
    private String obj;

    public String getObj() {
        return obj;
    }

    public void setObj(String obj) {
        this.obj = obj;
    }
}
