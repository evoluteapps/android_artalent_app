package com.artalent.ui.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;


public class ContestResponse implements Serializable {
    @SerializedName("obj")
    private List<ContestObj> obj;

    public List<ContestObj> getObj() {
        return obj;
    }

    public void setObj(List<ContestObj> obj) {
        this.obj = obj;
    }
}
