package com.artalent.ui.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ContestRegistrationObj implements Serializable {
    @SerializedName("registerationId")
    private int registerationId;

    @SerializedName("user")
    private UserObj user;

    public int getRegisterationId() {
        return registerationId;
    }

    public UserObj getUser() {
        return user;
    }
}
