package com.artalent.ui.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SpecialContestWinnerObj implements Serializable {

    @SerializedName("id")
    private int id;

    @SerializedName("winnerTitle")
    private String winnerTitle;

    @SerializedName("adminContestId")
    private String adminContestId;

    @SerializedName("winnerType")
    private String winnerType;

    @SerializedName("price")
    private String price;

    @SerializedName("regCountUpto")
    private int regCountUpto;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getWinnerTitle() {
        return winnerTitle;
    }

    public void setWinnerTitle(String winnerTitle) {
        this.winnerTitle = winnerTitle;
    }

    public String getAdminContestId() {
        return adminContestId;
    }

    public void setAdminContestId(String adminContestId) {
        this.adminContestId = adminContestId;
    }

    public String getWinnerType() {
        return winnerType;
    }

    public void setWinnerType(String winnerType) {
        this.winnerType = winnerType;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public int getRegCountUpto() {
        return regCountUpto;
    }

    public void setRegCountUpto(int regCountUpto) {
        this.regCountUpto = regCountUpto;
    }
}
