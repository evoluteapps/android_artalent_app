package com.artalent.ui.model.room;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Entity
public class ContestVideoDraft implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int id;
    private int userId;
    private String title;
    private String note;
    private String storagePath;
    private boolean status;
    private Date createdDate;
    private Date updatedDate;

    public ContestVideoDraft(int userId, String title, String note, String storagePath, boolean status, Date createdDate, Date updatedDate) {
        this.userId = userId;
        this.title = title;
        this.note = note;
        this.storagePath = storagePath;
        this.status = status;
        this.createdDate = createdDate;
        this.updatedDate = updatedDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getStoragePath() {
        return storagePath;
    }

    public void setStoragePath(String storagePath) {
        this.storagePath = storagePath;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    @Override
    public String toString() {
        return "ContestVideoDraft{" +
                "id=" + id +
                ", userId=" + userId +
                ", title='" + title + '\'' +
                ", note='" + note + '\'' +
                ", storagePath='" + storagePath + '\'' +
                ", status=" + status +
                ", createdDate=" + createdDate +
                ", updatedDate=" + updatedDate +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ContestVideoDraft that = (ContestVideoDraft) o;
        return id == that.id &&
                userId == that.userId &&
                status == that.status &&
                title.equals(that.title) &&
                note.equals(that.note) &&
                storagePath.equals(that.storagePath) &&
                createdDate.equals(that.createdDate) &&
                updatedDate.equals(that.updatedDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, userId, title, note, storagePath, status, createdDate, updatedDate);
    }

    public static DiffUtil.ItemCallback<ContestVideoDraft> itemCallback = new DiffUtil.ItemCallback<ContestVideoDraft>() {
        @Override
        public boolean areItemsTheSame(@NonNull @NotNull ContestVideoDraft oldItem, @NonNull @NotNull ContestVideoDraft newItem) {
            return oldItem.getId() == newItem.getId();
        }

        @Override
        public boolean areContentsTheSame(@NonNull @NotNull ContestVideoDraft oldItem, @NonNull @NotNull ContestVideoDraft newItem) {
            return oldItem.equals(newItem);
        }
    };
}
