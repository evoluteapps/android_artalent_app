package com.artalent.ui.model.request;

import com.google.gson.annotations.SerializedName;

public class CheckAccountReq {

    @SerializedName("entity")
    private String entity;
    @SerializedName("value")
    private String value;

    public String getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
