package com.artalent.ui.model.request;

import com.google.gson.annotations.SerializedName;


public class InterestReqObj {
    @SerializedName("interest")
    private String interest;
    @SerializedName("id")
    private int id;

    public String getInterest() {
        return interest;
    }

    public void setInterest(String interest) {
        this.interest = interest;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
