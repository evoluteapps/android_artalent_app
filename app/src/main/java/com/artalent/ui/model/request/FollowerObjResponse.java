package com.artalent.ui.model.request;

import com.google.gson.annotations.SerializedName;

public class FollowerObjResponse {
    @SerializedName("obj")
    FollowerListResponse obj;

    @SerializedName("index")
    private int index;

    public FollowerListResponse getObj() {
        return obj;
    }

    public void setObj(FollowerListResponse obj) {
        this.obj = obj;
    }

    public int getIndex() {
        return index;
    }
    public void setIndex(int index) {
        this.index = index;
    }
}
