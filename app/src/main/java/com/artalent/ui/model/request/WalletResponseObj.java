package com.artalent.ui.model.request;

import com.artalent.ui.model.WalletTransactionObj;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class WalletResponseObj implements Serializable {

    @SerializedName("obj")
    private WalletTransactionObj obj;

    @SerializedName("index")
    private int index;

    public WalletTransactionObj getObj() {
        return obj;
    }

    public void setObj(WalletTransactionObj obj) {
        this.obj = obj;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
