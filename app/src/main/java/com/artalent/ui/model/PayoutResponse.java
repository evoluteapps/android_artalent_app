package com.artalent.ui.model;

import com.artalent.ui.model.request.NotesDataReq;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PayoutResponse implements Serializable {

    @SerializedName("id")
    private String id;
    @SerializedName("entity")
    private String entity;
    @SerializedName("fund_account_id")
    private String fund_account_id;
    @SerializedName("status")
    private String status;
    @SerializedName("amount")
    private double amount;
    @SerializedName("currency")
    private String currency;

    @SerializedName("notes")
    private NotesDataReq notes;

    @SerializedName("created_at")
    private String created_at;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }

    public String getFund_account_id() {
        return fund_account_id;
    }

    public void setFund_account_id(String fund_account_id) {
        this.fund_account_id = fund_account_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public NotesDataReq getNotes() {
        return notes;
    }

    public void setNotes(NotesDataReq notes) {
        this.notes = notes;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }
}
