package com.artalent.ui.model.request;

import com.google.gson.annotations.SerializedName;

public class DistrictReq {

    @SerializedName("districtName")
    private String districtName;

    @SerializedName("stateId")
    private int stateId;

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public int getStateId() {
        return stateId;
    }

    public void setStateId(int stateId) {
        this.stateId = stateId;
    }
}
