package com.artalent.ui.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class RazorpayErrorResponse implements Serializable {

    @SerializedName("error")
    private ErrorData error;

    public ErrorData getError() {
        return error;
    }

    public void setError(ErrorData error) {
        this.error = error;
    }
}
