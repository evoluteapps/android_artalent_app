package com.artalent.ui.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Rohit Mahajan on 18/05/16.
 */
public class PostContestObj implements Serializable {
    @SerializedName("contestPostId")
    private int contestPostId;

    @SerializedName("description")
    private String description;

    @SerializedName("isVoted")
    private String isVoted;

    @SerializedName("voteCount")
    private int voteCount;

    @SerializedName("userInfo")
    private UserObj userInfo;

    @SerializedName("interest")
    private InterestObj interest;

    @SerializedName("mediaUrls")
    private List<String> mediaUrls;

    @SerializedName("thumbImage")
    private String thumbImage;

    public InterestObj getInterest() {
        return interest;
    }

    public void setInterest(InterestObj interest) {
        this.interest = interest;
    }

    public String getThumbImage() {
        return thumbImage;
    }

    public void setThumbImage(String thumbImage) {
        this.thumbImage = thumbImage;
    }

    public int getContestPostId() {
        return contestPostId;
    }

    public void setContestPostId(int contestPostId) {
        this.contestPostId = contestPostId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIsVoted() {
        return isVoted;
    }

    public void setIsVoted(String isVoted) {
        this.isVoted = isVoted;
    }

    public int getVoteCount() {
        return voteCount;
    }

    public void setVoteCount(int voteCount) {
        this.voteCount = voteCount;
    }

    public UserObj getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserObj userInfo) {
        this.userInfo = userInfo;
    }

    public List<String> getMediaUrls() {
        return mediaUrls;
    }

    public void setMediaUrls(List<String> mediaUrls) {
        this.mediaUrls = mediaUrls;
    }

    @Override
    public String toString() {
        return "PostContestObj{" +
                "contestPostId=" + contestPostId +
                ", description='" + description + '\'' +
                ", isVoted='" + isVoted + '\'' +
                ", voteCount=" + voteCount +
                ", userInfo=" + userInfo +
                ", interest=" + interest +
                ", mediaUrls=" + mediaUrls +
                ", thumbImage='" + thumbImage + '\'' +
                '}';
    }
}
