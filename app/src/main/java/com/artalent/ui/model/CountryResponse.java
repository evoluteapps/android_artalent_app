package com.artalent.ui.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;


public class CountryResponse {
    @SerializedName("obj")
    private List<Country> obj;

    public List<Country> getObj() {
        return obj;
    }

    public void setObj(List<Country> obj) {
        this.obj = obj;
    }
}
