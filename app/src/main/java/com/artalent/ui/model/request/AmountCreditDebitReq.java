package com.artalent.ui.model.request;

import com.google.gson.annotations.SerializedName;

public class AmountCreditDebitReq {

    @SerializedName("description")
    private String description;

    @SerializedName("transactionType")
    private String transactionType;

    @SerializedName("amount")
    private Double amount;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }
}
