package com.artalent.ui.model.request;

import com.google.gson.annotations.SerializedName;

public class AccountReq {

    @SerializedName("name")
    private String name;

    @SerializedName("ifsc")
    private String ifsc;

    @SerializedName("account_number")
    private String account_number;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIfsc() {
        return ifsc;
    }

    public void setIfsc(String ifsc) {
        this.ifsc = ifsc;
    }

    public String getAccount_number() {
        return account_number;
    }

    public void setAccount_number(String account_number) {
        this.account_number = account_number;
    }
}
