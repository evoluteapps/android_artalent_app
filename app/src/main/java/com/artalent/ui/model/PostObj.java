package com.artalent.ui.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Rohit Mahajan on 18/05/16.
 */
public class PostObj implements Serializable {
    @SerializedName("postId")
    private int postId;

    @SerializedName("description")
    private String description;

    @SerializedName("isLiked")
    private String isLiked;

    @SerializedName("likeCount")
    private int likeCount;

    @SerializedName("commentCount")
    private int commentCount;

    @SerializedName("userInfo")
    private UserObj userInfo;

    @SerializedName("mediaUrls")
    private List<String> mediaUrls;

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIsLiked() {
        return isLiked;
    }

    public void setIsLiked(String isLiked) {
        this.isLiked = isLiked;
    }

    public int getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(int likeCount) {
        this.likeCount = likeCount;
    }

    public int getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(int commentCount) {
        this.commentCount = commentCount;
    }

    public UserObj getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserObj userInfo) {
        this.userInfo = userInfo;
    }

    public List<String> getMediaUrls() {
        return mediaUrls;
    }

    public void setMediaUrls(List<String> mediaUrls) {
        this.mediaUrls = mediaUrls;
    }

    @Override
    public String toString() {
        return "PostObj{" +
                "postId=" + postId +
                ", description='" + description + '\'' +
                ", isLiked='" + isLiked + '\'' +
                ", likeCount=" + likeCount +
                ", commentCount=" + commentCount +
                ", userInfo=" + userInfo +
                ", mediaUrls=" + mediaUrls +
                '}';
    }
}
