package com.artalent.ui.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;


public class NotificationListResponse {
    @SerializedName("obj")
    private List<NotificationDataObj> obj;

    @SerializedName("index")
    private int index;

    public List<NotificationDataObj> getObj() {
        return obj;
    }

    public void setObj(List<NotificationDataObj> obj) {
        this.obj = obj;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
