package com.artalent.ui.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;


public class UserObj implements Serializable {
    @SerializedName("id")
    private int id;

    @SerializedName("firstName")
    private String firstName;

    @SerializedName("lastName")
    private String lastName;

    @SerializedName("pincode")
    private String pincode;

    @SerializedName("mobile")
    private String mobile;

    @SerializedName("districtId")
    private int districtId;

    @SerializedName("isVerified")
    private String isVerified;

    @SerializedName("date")
    private String date;

    @SerializedName("profileImage")
    private String profileImage;

    @SerializedName("level")
    private String level;

    @SerializedName("userName")
    private String userName;

    @SerializedName("country")
    private Country country;

    @SerializedName("state")
    private StateObj state;

    @SerializedName("district")
    private DistrictsObj district;

    @SerializedName("interest")
    private List<InterestObj> interest;

    @SerializedName("contestRegisterCount")
    private int contestRegisterCount;

    @SerializedName("followingCount")
    private int followingCount;

    @SerializedName("followerCount")
    private int followerCount;

    @SerializedName("contestPostCount")
    private int contestPostCount;

    @SerializedName("postCount")
    private int postCount;

    @SerializedName("isFollowed")
    private int isFollowed;

    @SerializedName("referalCode")
    private String referalCode;

    @SerializedName("userType")
    private int userType;

    @SerializedName("amount")
    private double amount;

    @SerializedName("walletBalance")
    private double walletBalance;

    @SerializedName("referralBalance")
    private double referralBalance;

    public double getWalletBalance() {
        return walletBalance;
    }

    public double getReferralBalance() {
        return referralBalance;
    }

    public double getAmount() {
        return amount;
    }

    public int getUserType() {
        return userType;
    }

    public String getReferalCode() {
        return referalCode;
    }

    public void setIsFollowed(int isFollowed) {
        this.isFollowed = isFollowed;
    }

    public int getContestRegisterCount() {
        return contestRegisterCount;
    }

    public int getFollowingCount() {
        return followingCount;
    }

    public int getFollowerCount() {
        return followerCount;
    }

    public int getContestPostCount() {
        return contestPostCount;
    }

    public int getPostCount() {
        return postCount;
    }

    public int getIsFollowed() {
        return isFollowed;
    }

    public List<InterestObj> getInterest() {
        return interest;
    }

    public void setInterest(List<InterestObj> interest) {
        this.interest = interest;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public StateObj getState() {
        return state;
    }

    public void setState(StateObj state) {
        this.state = state;
    }

    public DistrictsObj getDistrict() {
        return district;
    }

    public void setDistrict(DistrictsObj district) {
        this.district = district;
    }

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPincode() {
        return pincode;
    }

    public String getMobile() {
        return mobile;
    }

    public int getDistrictId() {
        return districtId;
    }

    public String getIsVerified() {
        return isVerified;
    }

    public String getDate() {
        return date;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public String getLevel() {
        return level;
    }

    public String getUserName() {
        return userName;
    }

    @Override
    public String toString() {
        return "UserObj{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", pincode='" + pincode + '\'' +
                ", mobile='" + mobile + '\'' +
                ", districtId=" + districtId +
                ", isVerified='" + isVerified + '\'' +
                ", date='" + date + '\'' +
                ", profileImage='" + profileImage + '\'' +
                ", level='" + level + '\'' +
                ", userName='" + userName + '\'' +
                ", country=" + country +
                ", state=" + state +
                ", district=" + district +
                ", interest=" + interest +
                ", contestRegisterCount=" + contestRegisterCount +
                ", followingCount=" + followingCount +
                ", followerCount=" + followerCount +
                ", contestPostCount=" + contestPostCount +
                ", postCount=" + postCount +
                ", isFollowed=" + isFollowed +
                ", referalCode='" + referalCode + '\'' +
                ", userType=" + userType +
                ", amount=" + amount +
                ", walletBalance=" + walletBalance +
                ", referralBalance=" + referralBalance +
                '}';
    }
}
