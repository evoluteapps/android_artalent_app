package com.artalent.ui.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;


public class PostResponse {
    @SerializedName("obj")
    private List<PostObj> obj;

    @SerializedName("index")
    private int index;

    public List<PostObj> getObj() {
        return obj;
    }

    public void setObj(List<PostObj> obj) {
        this.obj = obj;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    @Override
    public String toString() {
        return "PostResponse{" +
                "obj=" + obj +
                ", index=" + index +
                '}';
    }
}
