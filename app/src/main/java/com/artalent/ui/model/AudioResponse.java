package com.artalent.ui.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class AudioResponse implements Serializable {

    @SerializedName("obj")
    private List<AudioObj> obj;

    @SerializedName("index")
    private int index;

    public List<AudioObj> getObj() {
        return obj;
    }

    public void setObj(List<AudioObj> obj) {
        this.obj = obj;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    @Override
    public String toString() {
        return "AudioResponse{" +
                "obj=" + obj +
                ", index=" + index +
                '}';
    }
}
