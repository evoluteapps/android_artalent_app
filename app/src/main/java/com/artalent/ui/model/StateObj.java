package com.artalent.ui.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class StateObj implements Serializable {
    @SerializedName("countryId")
    private int countryId;
    @SerializedName("stateId")
    private int stateId;
    @SerializedName("state")
    private String state;

    private boolean isCheck;

    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }

    public int getStateId() {
        return stateId;
    }

    public void setStateId(int stateId) {
        this.stateId = stateId;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public boolean isCheck() {
        return isCheck;
    }

    public void setCheck(boolean check) {
        isCheck = check;
    }

    @Override
    public String toString() {
        return "StateObj{" +
                "countryId=" + countryId +
                ", stateId=" + stateId +
                ", state='" + state + '\'' +
                ", isCheck=" + isCheck +
                '}';
    }
}
