package com.artalent.ui.model.request;

import com.google.gson.annotations.SerializedName;


public class CommentReq {
    @SerializedName("comment")
    private String comment;

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
