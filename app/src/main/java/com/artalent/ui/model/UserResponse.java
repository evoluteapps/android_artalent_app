package com.artalent.ui.model;

import com.google.gson.annotations.SerializedName;


public class UserResponse extends BaseResponse {
    @SerializedName("obj")
    private UserObj obj;

    public UserObj getObj() {
        return obj;
    }
}
