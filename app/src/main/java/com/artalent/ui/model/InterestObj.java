package com.artalent.ui.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class InterestObj implements Serializable {
    @SerializedName("interestMasterId")
    private int interestMasterId;
    @SerializedName("interest")
    private String interest;

    @SerializedName("icon")
    private String icon;

    @SerializedName("baner")
    private String baner;

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getBaner() {
        return baner;
    }

    public void setBaner(String baner) {
        this.baner = baner;
    }

    private boolean isCheck;

    public int getInterestMasterId() {
        return interestMasterId;
    }

    public void setInterestMasterId(int interestMasterId) {
        this.interestMasterId = interestMasterId;
    }

    public String getInterest() {
        return interest;
    }

    public void setInterest(String interest) {
        this.interest = interest;
    }

    public boolean isCheck() {
        return isCheck;
    }

    public void setCheck(boolean check) {
        isCheck = check;
    }

    @Override
    public String toString() {
        return "InterestObj{" +
                "interestMasterId=" + interestMasterId +
                ", interest='" + interest + '\'' +
                ", icon='" + icon + '\'' +
                ", baner='" + baner + '\'' +
                ", isCheck=" + isCheck +
                '}';
    }
}
