package com.artalent.ui.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;


public class SpecialContestWinnerResponse {
    @SerializedName("obj")
    private List<SpecialContestObj> obj;

    @SerializedName("index")
    private int index;

    public List<SpecialContestObj> getObj() {
        return obj;
    }

    public void setObj(List<SpecialContestObj> obj) {
        this.obj = obj;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
