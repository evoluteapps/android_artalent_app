package com.artalent.ui.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Rohit Mahajan on 18/05/16.
 */
public class StoryImagesObj implements Serializable {

    @SerializedName("storyId")
    private int storyId;

    @SerializedName("storyUrl")
    private String storyUrl;

    @SerializedName("desc")
    private String desc;

    public int getStoryId() {
        return storyId;
    }

    public String getStoryUrl() {
        return storyUrl;
    }

    public String getDesc() {
        return desc;
    }

    @Override
    public String toString() {
        return "StoryImagesObj{" +
                "storyId=" + storyId +
                ", storyUrl='" + storyUrl + '\'' +
                ", desc='" + desc + '\'' +
                '}';
    }
}
