package com.artalent.ui.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class SpecialContestResponse implements Serializable {

    @SerializedName("obj")
    private List<SpecialContestObj> obj;

    @SerializedName("index")
    private int index;

    public List<SpecialContestObj> getObj() {
        return obj;
    }

    public int getIndex() {
        return index;
    }
}
