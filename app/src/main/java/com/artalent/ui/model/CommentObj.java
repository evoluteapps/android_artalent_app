package com.artalent.ui.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class CommentObj implements Serializable {


    @SerializedName("postCommentedById")
    private int postCommentedById;

    @SerializedName("postId")
    private int postId;

    @SerializedName("comment")
    private String comment;

    @SerializedName("userInfoPojo2")
    private UserObj userInfoPojo2;

    public int getPostCommentedById() {
        return postCommentedById;
    }

    public void setPostCommentedById(int postCommentedById) {
        this.postCommentedById = postCommentedById;
    }

    public int getPostId() {
        return postId;
    }

    public String getComment() {
        return comment;
    }

    public UserObj getUserInfoPojo2() {
        return userInfoPojo2;
    }
}
