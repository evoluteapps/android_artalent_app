package com.artalent.ui.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AllUserListResponse {
    @SerializedName("obj")
    private List<UserObj> obj;


    @SerializedName("index")
    private int index;

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public List<UserObj> getObj() {
        return obj;
    }

    public void setObj(List<UserObj> obj) {
        this.obj = obj;
    }
}
