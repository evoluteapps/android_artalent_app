package com.artalent.ui.model.request;

import com.artalent.ui.model.UserObj;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FollowingListResponse {
    @SerializedName("following")
    private List<UserObj> following;

    @SerializedName("index")
    private int index;

    public List<UserObj> getFollowing() {
        return following;
    }

    public void setFollowing(List<UserObj> following) {
        this.following = following;
    }

    public int getIndex() {
        return index;
    }
    public void setIndex(int index) {
        this.index = index;
    }
}
