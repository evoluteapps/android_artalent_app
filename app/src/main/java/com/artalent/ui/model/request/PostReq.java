package com.artalent.ui.model.request;

import com.google.gson.annotations.SerializedName;


public class PostReq {
    @SerializedName("postId")
    private int postId;

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }
}
