package com.artalent.ui.model;

import com.google.gson.annotations.SerializedName;


public class OTPResponse extends BaseResponse {
    @SerializedName("userInfo")
    private UserObj userInfo;

    @SerializedName("token")
    private String token;

    public UserObj getUserInfo() {
        return userInfo;
    }

    public String getToken() {
        return token;
    }
}
