package com.artalent.ui.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class ContestHistoryObj implements Serializable {
    @SerializedName("contestHistoryId")
    private int contestHistoryId;

    @SerializedName("registrationIds")
    private String registrationIds;

    @SerializedName("geoFlag")
    private int geoFlag;

    @SerializedName("winner1VoteCount")
    private int winner1VoteCount;

    @SerializedName("winner2VoteCount")
    private int winner2VoteCount;

    @SerializedName("winner3VoteCount")
    private int winner3VoteCount;

    @SerializedName("interest")
    private InterestObj interest;

    @SerializedName("state")
    private StateObj state;

    @SerializedName("district")
    private DistrictsObj district;

    @SerializedName("country")
    private Country country;

    @SerializedName("winner1")
    private UserObj winner1;

    @SerializedName("winner2")
    private UserObj winner2;

    @SerializedName("winner3")
    private UserObj winner3;

    @SerializedName("winner1ContestPost")
    private PostContestObj winner1ContestPost;

    @SerializedName("winner2ContestPost")
    private PostContestObj winner2ContestPost;

    @SerializedName("winner3ContestPost")
    private PostContestObj winner3ContestPost;

    public int getContestHistoryId() {
        return contestHistoryId;
    }

    public String getRegistrationIds() {
        return registrationIds;
    }

    public int getGeoFlag() {
        return geoFlag;
    }

    public int getWinner1VoteCount() {
        return winner1VoteCount;
    }

    public int getWinner2VoteCount() {
        return winner2VoteCount;
    }

    public int getWinner3VoteCount() {
        return winner3VoteCount;
    }

    public InterestObj getInterest() {
        return interest;
    }

    public StateObj getState() {
        return state;
    }

    public DistrictsObj getDistrict() {
        return district;
    }

    public Country getCountry() {
        return country;
    }

    public UserObj getWinner1() {
        return winner1;
    }

    public UserObj getWinner2() {
        return winner2;
    }

    public UserObj getWinner3() {
        return winner3;
    }

    public PostContestObj getWinner1ContestPost() {
        return winner1ContestPost;
    }

    public PostContestObj getWinner2ContestPost() {
        return winner2ContestPost;
    }

    public PostContestObj getWinner3ContestPost() {
        return winner3ContestPost;
    }
}
