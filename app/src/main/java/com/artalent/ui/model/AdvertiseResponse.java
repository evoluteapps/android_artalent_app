package com.artalent.ui.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class AdvertiseResponse implements Serializable {

    @SerializedName("obj")
    private List<AdvertiseObj> obj;

    @SerializedName("index")
    private int index;

    public List<AdvertiseObj> getObj() {
        return obj;
    }

    public int getIndex() {
        return index;
    }
}
