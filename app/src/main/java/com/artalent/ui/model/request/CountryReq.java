package com.artalent.ui.model.request;

import com.google.gson.annotations.SerializedName;

public class CountryReq {

    @SerializedName("countryName")
    private String countryName;

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }
}
