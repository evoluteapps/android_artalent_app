package com.artalent.ui.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class RegisteredContestObj implements Serializable {

    @SerializedName("interest")
    private List<ContestObj> contestObjList;

    public List<ContestObj> getContestObjList() {
        return contestObjList;
    }

    public void setContestObjList(List<ContestObj> contestObjList) {
        this.contestObjList = contestObjList;
    }
}
