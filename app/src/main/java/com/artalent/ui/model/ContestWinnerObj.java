package com.artalent.ui.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ContestWinnerObj implements Serializable {

    @SerializedName("id")
    private int id;

    @SerializedName("winner_title")
    private String winner_title;

    @SerializedName("winner_type")
    private String winner_type;

    @SerializedName("price")
    private String price;

    @SerializedName("reg_count_upto")
    private int reg_count_upto;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getWinner_title() {
        return winner_title;
    }

    public void setWinner_title(String winner_title) {
        this.winner_title = winner_title;
    }

    public String getWinner_type() {
        return winner_type;
    }

    public void setWinner_type(String winner_type) {
        this.winner_type = winner_type;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public int getReg_count_upto() {
        return reg_count_upto;
    }

    public void setReg_count_upto(int reg_count_upto) {
        this.reg_count_upto = reg_count_upto;
    }
}
