package com.artalent.ui.model.request;

import com.artalent.ui.model.UserObj;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FollowerListResponse {
    @SerializedName("followers")
    private List<UserObj> followers;

    @SerializedName("index")
    private int index;

    public List<UserObj> getFollowers() {
        return followers;
    }

    public int getIndex() {
        return index;
    }

    public void setFollowers(List<UserObj> followers) {
        this.followers = followers;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
