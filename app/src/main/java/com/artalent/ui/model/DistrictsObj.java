package com.artalent.ui.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class DistrictsObj implements Serializable {
    @SerializedName("districtId")
    private int districtId;
    @SerializedName("stateId")
    private int stateId;
    @SerializedName("district")
    private String district;

    private boolean isCheck;

    public int getDistrictId() {
        return districtId;
    }

    public void setDistrictId(int districtId) {
        this.districtId = districtId;
    }

    public int getStateId() {
        return stateId;
    }

    public void setStateId(int stateId) {
        this.stateId = stateId;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public boolean isCheck() {
        return isCheck;
    }

    public void setCheck(boolean check) {
        isCheck = check;
    }

    @Override
    public String toString() {
        return "DistrictsObj{" +
                "districtId=" + districtId +
                ", stateId=" + stateId +
                ", district='" + district + '\'' +
                ", isCheck=" + isCheck +
                '}';
    }
}
