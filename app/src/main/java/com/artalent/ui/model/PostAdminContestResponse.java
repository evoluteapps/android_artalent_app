package com.artalent.ui.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;


public class PostAdminContestResponse {
    @SerializedName("obj")
    private List<PostAdminContestObj> obj;

    @SerializedName("index")
    private int index;

    public List<PostAdminContestObj> getObj() {
        return obj;
    }

    public void setObj(List<PostAdminContestObj> obj) {
        this.obj = obj;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    @Override
    public String toString() {
        return "PostAdminContestResponse{" +
                "obj=" + obj +
                ", index=" + index +
                '}';
    }
}
