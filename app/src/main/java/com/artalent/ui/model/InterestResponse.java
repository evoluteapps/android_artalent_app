package com.artalent.ui.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;


public class InterestResponse {
    @SerializedName("obj")
    private List<InterestObj> obj;

    public List<InterestObj> getObj() {
        return obj;
    }

    public void setObj(List<InterestObj> obj) {
        this.obj = obj;
    }
}
