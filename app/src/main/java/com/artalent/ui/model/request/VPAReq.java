package com.artalent.ui.model.request;

import com.google.gson.annotations.SerializedName;

public class VPAReq {

    @SerializedName("address")
    private String address;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
