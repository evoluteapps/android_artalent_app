package com.artalent.ui.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class InterestListObj implements Serializable {
    @SerializedName("interestList")
    List<InterestObj> interestObjList;

    public List<InterestObj> getInterestObjList() {
        return interestObjList;
    }

    public void setInterestObjList(List<InterestObj> interestObjList) {
        this.interestObjList = interestObjList;
    }
}
