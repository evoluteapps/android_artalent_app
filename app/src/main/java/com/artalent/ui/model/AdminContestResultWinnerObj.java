package com.artalent.ui.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class AdminContestResultWinnerObj implements Serializable {

    @SerializedName("winnerResultId")
    private int winnerResultId;

    @SerializedName("postId")
    private int postId;

    @SerializedName("adminContestId")
    private int adminContestId;

    @SerializedName("winnerType")
    private String winnerType;

    @SerializedName("winnerPrice")
    private String winnerPrice;

    @SerializedName("voteCount")
    private int voteCount;

    @SerializedName("winnerUser")
    private UserObj winnerUser;

    public int getWinnerResultId() {
        return winnerResultId;
    }

    public void setWinnerResultId(int winnerResultId) {
        this.winnerResultId = winnerResultId;
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    public int getAdminContestId() {
        return adminContestId;
    }

    public void setAdminContestId(int adminContestId) {
        this.adminContestId = adminContestId;
    }

    public String getWinnerType() {
        return winnerType;
    }

    public void setWinnerType(String winnerType) {
        this.winnerType = winnerType;
    }

    public String getWinnerPrice() {
        return winnerPrice;
    }

    public void setWinnerPrice(String winnerPrice) {
        this.winnerPrice = winnerPrice;
    }

    public int getVoteCount() {
        return voteCount;
    }

    public void setVoteCount(int voteCount) {
        this.voteCount = voteCount;
    }

    public UserObj getWinnerUser() {
        return winnerUser;
    }

    public void setWinnerUser(UserObj winnerUser) {
        this.winnerUser = winnerUser;
    }

    @Override
    public String toString() {
        return "AdminContestResultWinnerObj{" +
                "winnerResultId=" + winnerResultId +
                ", postId=" + postId +
                ", adminContestId=" + adminContestId +
                ", winnerType='" + winnerType + '\'' +
                ", winnerPrice='" + winnerPrice + '\'' +
                ", voteCount=" + voteCount +
                ", winnerUser=" + winnerUser +
                '}';
    }
}
