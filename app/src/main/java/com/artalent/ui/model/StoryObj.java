package com.artalent.ui.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Rohit Mahajan on 18/05/16.
 */
public class StoryObj implements Serializable {
    @SerializedName("stories")
    private List<StoryImagesObj> stories;

    @SerializedName("userId")
    private int userId;

    @SerializedName("owner")
    private UserObj owner;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public UserObj getOwner() {
        return owner;
    }

    public List<StoryImagesObj> getStories() {
        return stories;
    }

    @Override
    public String toString() {
        return "StoryObj{" +
                "stories=" + stories +
                ", userId=" + userId +
                ", owner=" + owner +
                '}';
    }
}
