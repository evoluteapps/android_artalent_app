package com.artalent.ui.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class AudioObj implements Serializable {

    @SerializedName("audioid")
    private int audioid;

    @SerializedName("type")
    private String type;

    @SerializedName("duration")
    private String duration;

    @SerializedName("songName")
    private String songName;

    @SerializedName("uploader")
    private UserObj uploader;

    @SerializedName("audioUrl")
    private String audioUrl;

    public int getAudioid() {
        return audioid;
    }

    public void setAudioid(int audioid) {
        this.audioid = audioid;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getSongName() {
        return songName;
    }

    public void setSongName(String songName) {
        this.songName = songName;
    }

    public String getAudioUrl() {
        return audioUrl;
    }

    public UserObj getUploader() {
        return uploader;
    }

    public void setUploader(UserObj uploader) {
        this.uploader = uploader;
    }

    public void setAudioUrl(String audioUrl) {
        this.audioUrl = audioUrl;
    }

    @Override
    public String toString() {
        return "AudioObj{" +
                "audioid=" + audioid +
                ", type='" + type + '\'' +
                ", duration='" + duration + '\'' +
                ", songName='" + songName + '\'' +
                ", uploader=" + uploader +
                ", audioUrl='" + audioUrl + '\'' +
                '}';
    }
}
