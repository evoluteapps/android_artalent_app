package com.artalent.ui.model.request;

import com.google.gson.annotations.SerializedName;

public class FirebaseToken {

    @SerializedName("firebaseToken")
    private String firebaseToken;

    public String getFirebaseToken() {
        return firebaseToken;
    }

    public void setFirebaseToken(String firebaseToken) {
        this.firebaseToken = firebaseToken;
    }
}
