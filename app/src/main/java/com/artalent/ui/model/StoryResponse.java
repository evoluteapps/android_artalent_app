package com.artalent.ui.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;


public class StoryResponse {
    @SerializedName("obj")
    private List<StoryObj> obj;

    @SerializedName("index")

    private int index;

    public List<StoryObj> getObj() {
        return obj;
    }

    public void setObj(List<StoryObj> obj) {
        this.obj = obj;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    @Override
    public String toString() {
        return "StoryResponse{" +
                "obj=" + obj +
                ", index=" + index +
                '}';
    }
}
