package com.artalent.ui.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class SpecialContestObj implements Serializable {

    @SerializedName("adminContestId")
    private int adminContestId;

    @SerializedName("tittle")
    private String tittle;

    @SerializedName("description")
    private String description;

    @SerializedName("startDate")
    private String startDate;

    @SerializedName("endDate")
    private String endDate;

    @SerializedName("image")
    private String image;

    @SerializedName("interest")
    private InterestObj interestObj;

    @SerializedName("isRegistered")
    private int isRegistered;

    @SerializedName("userContestRegistrationId")
    private int userContestRegistrationId;

    @SerializedName("userId")
    private int userId;

    public int getUserContestRegistrationId() {
        return userContestRegistrationId;
    }

    @SerializedName("country")
    private Country country;

    @SerializedName("state")
    private StateObj state;

    @SerializedName("district")
    private DistrictsObj districts;

    @SerializedName("regEndDate")
    private String regEndDate;

    @SerializedName("maxRegCount")
    private int maxRegCount;

    @SerializedName("registeredCount")
    private int registeredCount;

    @SerializedName("regFee")
    private String regFee;

    @SerializedName("adminContestResultWinners")
    private List<AdminContestResultWinnerObj> adminContestResultWinners;

    @SerializedName("winnerContestWinnerList")
    private List<SpecialContestWinnerObj> winnerContestWinnerList;

    public int getAdminContestId() {
        return adminContestId;
    }

    public void setAdminContestId(int adminContestId) {
        this.adminContestId = adminContestId;
    }

    public String getTittle() {
        return tittle;
    }

    public void setTittle(String tittle) {
        this.tittle = tittle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public InterestObj getInterestObj() {
        return interestObj;
    }

    public void setInterestObj(InterestObj interestObj) {
        this.interestObj = interestObj;
    }

    public int getIsRegistered() {
        return isRegistered;
    }

    public void setIsRegistered(int isRegistered) {
        this.isRegistered = isRegistered;
    }

    public void setUserContestRegistrationId(int userContestRegistrationId) {
        this.userContestRegistrationId = userContestRegistrationId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public StateObj getState() {
        return state;
    }

    public void setState(StateObj state) {
        this.state = state;
    }

    public DistrictsObj getDistricts() {
        return districts;
    }

    public void setDistricts(DistrictsObj districts) {
        this.districts = districts;
    }

    public String getRegEndDate() {
        return regEndDate;
    }

    public void setRegEndDate(String regEndDate) {
        this.regEndDate = regEndDate;
    }

    public int getMaxRegCount() {
        return maxRegCount;
    }

    public void setMaxRegCount(int maxRegCount) {
        this.maxRegCount = maxRegCount;
    }

    public int getRegisteredCount() {
        return registeredCount;
    }

    public void setRegisteredCount(int registeredCount) {
        this.registeredCount = registeredCount;
    }

    public String getRegFee() {
        return regFee;
    }

    public void setRegFee(String regFee) {
        this.regFee = regFee;
    }

    public List<AdminContestResultWinnerObj> getAdminContestResultWinners() {
        return adminContestResultWinners;
    }

    public void setAdminContestResultWinners(List<AdminContestResultWinnerObj> adminContestResultWinners) {
        this.adminContestResultWinners = adminContestResultWinners;
    }

    public List<SpecialContestWinnerObj> getWinnerContestWinnerList() {
        return winnerContestWinnerList;
    }

    public void setWinnerContestWinnerList(List<SpecialContestWinnerObj> winnerContestWinnerList) {
        this.winnerContestWinnerList = winnerContestWinnerList;
    }

    @Override
    public String toString() {
        return "SpecialContestObj{" +
                "adminContestId=" + adminContestId +
                ", tittle='" + tittle + '\'' +
                ", description='" + description + '\'' +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", image='" + image + '\'' +
                ", interestObj=" + interestObj +
                ", isRegistered=" + isRegistered +
                ", userContestRegistrationId=" + userContestRegistrationId +
                ", userId=" + userId +
                ", country=" + country +
                ", state=" + state +
                ", districts=" + districts +
                ", regEndDate='" + regEndDate + '\'' +
                ", maxRegCount=" + maxRegCount +
                ", registeredCount=" + registeredCount +
                ", regFee='" + regFee + '\'' +
                ", adminContestResultWinners=" + adminContestResultWinners +
                ", winnerContestWinnerList=" + winnerContestWinnerList +
                '}';
    }
}
