package com.artalent.ui.model.request;

import com.google.gson.annotations.SerializedName;

public class NotesDataReq {

    @SerializedName("notes_key_1")
    private String notes_key_1;

    @SerializedName("notes_key_2")
    private String notes_key_2;

    public String getNotes_key_1() {
        return notes_key_1;
    }

    public void setNotes_key_1(String notes_key_1) {
        this.notes_key_1 = notes_key_1;
    }

    public String getNotes_key_2() {
        return notes_key_2;
    }

    public void setNotes_key_2(String notes_key_2) {
        this.notes_key_2 = notes_key_2;
    }
}
