package com.artalent.ui.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class AdvertiseObj implements Serializable {

    @SerializedName("id")
    private int id;
    @SerializedName("name")
    private String name;

    @SerializedName("expiryDate")
    private String expiryDate;

    @SerializedName("url")
    private String url;

    @SerializedName("description")
    private String description;

    @SerializedName("image")
    private String image;

    public int getId() {
        return id;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public String getUrl() {
        return url;
    }

    public String getDescription() {
        return description;
    }

    public String getImage() {
        return image;
    }

    @Override
    public String toString() {
        return "AdvertiseObj{" +
                "id=" + id +
                ", expiryDate='" + expiryDate + '\'' +
                ", url='" + url + '\'' +
                ", description='" + description + '\'' +
                ", image='" + image + '\'' +
                '}';
    }

    public String getCompanyName() {
        return name;
    }

    public void setCompanyName(String companyName) {
        this.name = companyName;
    }
}
