package com.artalent.ui.model.request;

import com.google.gson.annotations.SerializedName;

public class PayOutReq {

    @SerializedName("account_number")
    private String account_number;

    @SerializedName("fund_account_id")
    private String fund_account_id;

    @SerializedName("amount")
    private double amount;

    @SerializedName("currency")
    private String currency;

    @SerializedName("mode")
    private String mode;

    @SerializedName("purpose")
    private String purpose;

    @SerializedName("queue_if_low_balance")
    private boolean queue_if_low_balance;

    @SerializedName("reference_id")
    private String reference_id;

    @SerializedName("narration")
    private String narration;

    @SerializedName("notes")
    private NotesDataReq notes;

    public String getAccount_number() {
        return account_number;
    }

    public void setAccount_number(String account_number) {
        this.account_number = account_number;
    }

    public String getFund_account_id() {
        return fund_account_id;
    }

    public void setFund_account_id(String fund_account_id) {
        this.fund_account_id = fund_account_id;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public boolean isQueue_if_low_balance() {
        return queue_if_low_balance;
    }

    public void setQueue_if_low_balance(boolean queue_if_low_balance) {
        this.queue_if_low_balance = queue_if_low_balance;
    }

    public String getReference_id() {
        return reference_id;
    }

    public void setReference_id(String reference_id) {
        this.reference_id = reference_id;
    }

    public String getNarration() {
        return narration;
    }

    public void setNarration(String narration) {
        this.narration = narration;
    }

    public NotesDataReq getNotes() {
        return notes;
    }

    public void setNotes(NotesDataReq notes) {
        this.notes = notes;
    }
}
