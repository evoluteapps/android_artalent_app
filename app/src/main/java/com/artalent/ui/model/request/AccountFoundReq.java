package com.artalent.ui.model.request;

import com.google.gson.annotations.SerializedName;

public class AccountFoundReq {

    @SerializedName("account_type")
    private String account_type;

    @SerializedName("contact_id")
    private String contact_id;

    @SerializedName("bank_account")
    private AccountReq bank_account;

    public String getAccount_type() {
        return account_type;
    }

    public void setAccount_type(String account_type) {
        this.account_type = account_type;
    }

    public String getContact_id() {
        return contact_id;
    }

    public void setContact_id(String contact_id) {
        this.contact_id = contact_id;
    }

    public AccountReq getBank_account() {
        return bank_account;
    }

    public void setBank_account(AccountReq bank_account) {
        this.bank_account = bank_account;
    }
}
