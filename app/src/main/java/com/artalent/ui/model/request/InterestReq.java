package com.artalent.ui.model.request;

import com.google.gson.annotations.SerializedName;

import java.util.List;


public class InterestReq {
    @SerializedName("obj")
    private List<InterestReqObj> obj;

    public List<InterestReqObj> getObj() {
        return obj;
    }

    public void setObj(List<InterestReqObj> obj) {
        this.obj = obj;
    }
}
