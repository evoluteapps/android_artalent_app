package com.artalent.ui.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;


public class StateResponse {
    @SerializedName("obj")
    private List<StateObj> obj;

    public List<StateObj> getObj() {
        return obj;
    }

    public void setObj(List<StateObj> obj) {
        this.obj = obj;
    }
}
