package com.artalent.ui.model

data class LevelInfoModel(
    val level: String,
    val price: String,
    val range: String
)
