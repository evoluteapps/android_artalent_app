package com.artalent.ui.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class FollowerObj implements Serializable {
    @SerializedName("user")
    private UserObj user;

    public UserObj getUser() {
        return user;
    }
}
