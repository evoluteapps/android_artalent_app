package com.artalent.ui.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class WalletTransactionObj implements Serializable {

    @SerializedName("walletBalance")
    private double walletBalance;

    @SerializedName("referralBalance")
    private double referralBalance;

    @SerializedName("transactions")
    private List<WalletTransactionListObj> transactionListObjs;

    public double getWalletBalance() {
        return walletBalance;
    }

    public void setWalletBalance(double walletBalance) {
        this.walletBalance = walletBalance;
    }

    public double getReferralBalance() {
        return referralBalance;
    }

    public void setReferralBalance(double referralBalance) {
        this.referralBalance = referralBalance;
    }

    public List<WalletTransactionListObj> getTransactionListObjs() {
        return transactionListObjs;
    }

    public void setTransactionListObjs(List<WalletTransactionListObj> transactionListObjs) {
        this.transactionListObjs = transactionListObjs;
    }
}
