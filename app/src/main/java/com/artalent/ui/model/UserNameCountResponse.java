package com.artalent.ui.model;

import com.google.gson.annotations.SerializedName;


public class UserNameCountResponse extends BaseResponse {
    @SerializedName("obj")
    private UserNameCountObj obj;

    public UserNameCountObj getObj() {
        return obj;
    }
}
