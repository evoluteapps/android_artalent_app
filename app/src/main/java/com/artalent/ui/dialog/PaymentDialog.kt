package com.artalent.ui.dialog

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.Animatable
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.view.Window
import android.widget.RelativeLayout
import com.artalent.R
import com.artalent.ui.activity.ArtalentWalletActivity
import com.artalent.ui.razorpay.RazorpayTestActivity
import kotlinx.android.synthetic.main.dialog_rate.*


class PaymentDialog(val activity: Activity, val titleText: String, val descriptionText: String, val isOnlySingleBtn: Boolean) : Dialog(activity) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.dialog_pay_done)

        title.setText(titleText)
        answerLabel.setText(descriptionText)
        window?.setLayout(
                RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT
        )
        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val playStoreUrl =
                "https://play.google.com/store/apps/details?id=com.artalent&hl=en_US"
        this@PaymentDialog.setCanceledOnTouchOutside(false)
        if (isOnlySingleBtn) {
            rateButton.visibility = View.GONE
            laterButton.setText("OK")
        } else {
            rateButton.visibility = View.VISIBLE
        }

        rateButton.setOnClickListener {

            val sendIntent = Intent(Intent.ACTION_VIEW, Uri.parse(playStoreUrl))
            sendIntent.setPackage("com.android.vending")
            activity.startActivity(sendIntent)
            this@PaymentDialog.dismiss()
        }
        laterButton.setOnClickListener {
            val intent1 = Intent(activity, ArtalentWalletActivity::class.java)
            activity.startActivity(intent1)
            activity.finish()
            this@PaymentDialog.dismiss()
        }
    }

}
