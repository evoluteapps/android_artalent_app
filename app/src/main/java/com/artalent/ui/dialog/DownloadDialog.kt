package com.artalent.ui.dialog

import android.app.Activity
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Window
import android.widget.RelativeLayout
import com.artalent.R
import com.artalent.ui.model.AudioObj
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.layout_dialog_download.*

class DownloadDialog(val activity: Activity, val audioObj : AudioObj) : Dialog(activity) {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.layout_dialog_download)
        window?.setLayout(
                RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT
        )
        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
//        (rateIcon.drawable as Animatable).start()

        downloadButton.setOnClickListener {
           // val intent_upload = Intent()
           // intent_upload.type = "audio/*"
          //  intent_upload.action = Intent.ACTION_GET_CONTENT
           // activity.startActivityForResult(intent_upload, 1)
            //this@DownloadDialog.dismiss()
            //Snackbar.make(it, "audioObj : "+audioObj.audioUrl, Snackbar.LENGTH_LONG).show()

        }


        cancelButton.setOnClickListener {
            this@DownloadDialog.dismiss()
        }
    }
}
