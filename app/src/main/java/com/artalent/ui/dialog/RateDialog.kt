package com.artalent.ui.dialog

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.Animatable
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.view.Window
import android.widget.RelativeLayout
import com.artalent.R
import kotlinx.android.synthetic.main.dialog_rate.*


class RateDialog(val activity: Activity, private val titleText: String, private val descriptionText: String, private val isOnlySingleBtn: Boolean) : Dialog(activity) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.dialog_rate)
        title.text = titleText
        answerLabel.text = descriptionText
        window?.setLayout(
                RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT
        )
        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        (rateIcon.drawable as Animatable).start()
        val playStoreUrl =
                "https://play.google.com/store/apps/details?id=com.artalent&hl=en_US"

        if (isOnlySingleBtn) {
            rateButton.visibility = View.GONE
            laterButton.setText("OK")
        } else {
            rateButton.visibility = View.VISIBLE
        }

        rateButton.setOnClickListener {

            val sendIntent = Intent(Intent.ACTION_VIEW, Uri.parse(playStoreUrl))
            sendIntent.setPackage("com.android.vending")
            activity.startActivity(sendIntent)
            this@RateDialog.dismiss()
        }
        laterButton.setOnClickListener {
            this@RateDialog.dismiss()
            val temp = rateIcon.drawable as Animatable
            temp.start()
        }
    }

}
