package com.artalent.ui.dialog

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.Animatable
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.Window
import android.widget.RelativeLayout
import com.artalent.R
import com.artalent.ui.activity.UploadAudioActivity
import kotlinx.android.synthetic.main.dialog_rate.*
import androidx.core.app.ActivityCompat.startActivityForResult





class AudioDialog(val activity: Activity) : Dialog(activity) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.dialog_upload_audio)
        window?.setLayout(
            RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT
        )
        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
//        (rateIcon.drawable as Animatable).start()

        rateButton.setOnClickListener {
            val intent_upload = Intent()
            intent_upload.type = "audio/*"
//            intent_upload.type = "audio/mpeg"
            intent_upload.action = Intent.ACTION_GET_CONTENT
            activity.startActivityForResult(intent_upload, 1)

//            val audioIntent =
//                Intent(Intent.ACTION_GET_CONTENT, MediaStore.Audio.Media.EXTERNAL_CONTENT_URI)
//            activity.startActivityForResult(audioIntent, 1);

            this@AudioDialog.dismiss()
        }


        laterButton.setOnClickListener {
            this@AudioDialog.dismiss()
        }
    }
}
