package com.artalent.ui.utils;

import android.net.Uri;
import android.provider.MediaStore;

/**
 * Created by akshay on 06/04/18.
 */

public class Constants {
    public static final int sScrollbarAnimDuration = 300;

    //ARTalent Razorpay Test API Key & Secret
    //public static final String RAZORPAY_TEST_API_KEY = "rzp_test_Se3dQ9YMmwq4yF";
    //public static final String RAZORPAY_TEST_API_SECRET = "plDWebP8TuiafANREGbNBCTv";

    public static final String RAZORPAY_TEST_API_KEY = "rzp_live_cWMSggTmBwH1oI";
    public static final String RAZORPAY_TEST_API_SECRET = "vYqw4KeiYPbbayfXHnVwnuZO";

    //    public static final String RAZORPAY_ACCOUNT_NUMBER = "2323230040373438";
    public static final String RAZORPAY_ACCOUNT_NUMBER = "3434181687461948";
    public static final String CURRENCY = "INR";


    //ARTalent Razorpay Live API Key & Secret
    public static final String RAZORPAY_API_KEY = "rzp_live_Ql67BMSmRCHoGP";
    public static final String RAZORPAY_API_SECRET = "dKp7BSeGJCwcoWpjYjjPSI1o";

    public static final String BACKEND_OTP_VALUE = "1111";
    public static final String POSITION = "position";
    public static final String IS_STORY_DELETED = "is_story_deleted";
    public static final String COUNTRY_LIST_NAME = "country_list_name";
    public static final String CONTEST_POST = "contest_post";
    public static final String SOCIAL_POST = "social_post";
    public static final String DISTRICT_LIST_NAME = "District";
    public static final String STATE_LIST_NAME = "State";
    public static final String INTEREST_LIST_NAME = "Interest";
    public static final String INTEREST_SHOW_ALL_LIST_NAME = "Show_All_Interest";
    public static final String INTEREST_CONTEST_LIST_NAME = "Contest_Interest";
    public static final String INTEREST_SEARCH_LIST_NAME = "Search_Interest";
    public static final String LANGUAGE_LIST_NAME = "Language";
    public static final String CATEGORY_LIST_NAME = "Category";
    public static final String MOBILE_NUMBER = "mobile_number";
    public static final String COUNTRY_OBJ = "country_obj";
    public static final String FROM = "from";

    public static final String INTEREST_OBJ = "interest_obj";
    public static final String DATA = "data_obj";
    public static final String MUSIC_LIBRARY_PATH = "music_library_path";
    public static final String STATE_OBJ = "state_obj";
    public static final String ID = "id";
    public static final int NO_INTERNET = 267;
    public static final int REFRESH_CONTEST_LIST = 202;

    public static final String INTEREST_LIST = "interest_list";
    public static final String USER_NAME = "user_name";
    public static final String PROFILE_URL = "profile_url";
    public static final String PROFILE_ID_FOR_STORY = "profile_id_for_story";

    public static final String DISTRICTS_LIST = "districts_list";

    public static final String ARTALENT_OTP_TEMPLATE_NAME = "ARTalent";
    public static final String ARTALENT_OTP_API_KEY = "134841aa-5d75-11ea-9fa5-0200cd936042";

    public static final String VPA = "vpa";
    public static final String CARD = "card";
    public static final String BANK_ACCOUNT = "bank_account";


    public static String[] PROJECTION = new String[]{
            MediaStore.Images.Media.DATA,
            MediaStore.Images.Media._ID,
            MediaStore.Images.Media.BUCKET_DISPLAY_NAME,
            MediaStore.Images.Media.BUCKET_ID,
            MediaStore.Images.Media.DATE_TAKEN,
            MediaStore.Images.Media.DATE_ADDED,
            MediaStore.Images.Media.DATE_MODIFIED,
    };
    public static Uri URI = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
    public static String ORDERBY = MediaStore.Images.Media.DATE_TAKEN + " DESC";

}
