package com.artalent.ui.utils;

public enum ImageQuality {
    LOW, REGULAR, HIGH
}
