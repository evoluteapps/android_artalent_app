package com.artalent.ui.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.artalent.ui.model.InterestObj;
import com.artalent.ui.model.UserObj;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.util.List;

/**
 * Created by Rohit on 05/05/16.
 */
public class PrefManager {
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private Context _context;

    // shared pref mode
    private int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_NAME = "artalent_app";
    private static final String IS_FIRST_TIME_MUSIC_LIBRARY_LAUNCH = "IsFirstTimeMusicLibraryLaunch";
    private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";
    private static final String AUTHORIZATION_TOKEN = "authorization_token";
    private static final String USER_OBJ = "user_obj";
    private static final String IS_FCM_TOTEN = "is_fcm_toten";

    public PrefManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setFirstTimeLaunch() {
        editor.putBoolean(IS_FIRST_TIME_LAUNCH, false);
        editor.commit();
    }

    public boolean isFirstTimeLaunch() {
        return pref.getBoolean(IS_FIRST_TIME_LAUNCH, true);
    }

    public void setFirstTimeMusicLibraryLaunch() {
        editor.putBoolean(IS_FIRST_TIME_MUSIC_LIBRARY_LAUNCH, false);
        editor.commit();
    }

    public boolean isFirstTimeMusicLibraryLaunch() {
        return pref.getBoolean(IS_FIRST_TIME_MUSIC_LIBRARY_LAUNCH, true);
    }

    public void setInterestList(List<InterestObj> interestList) {

    }

    public void setAuthorizationToken(String authorizationToken) {
        editor.putString(AUTHORIZATION_TOKEN, authorizationToken);
        editor.commit();
    }

    public String getAuthorizationToken() {
        return pref.getString(AUTHORIZATION_TOKEN, "");
    }

    public void setUserObj(UserObj userObj) {
        if (userObj != null) {
            Gson gson = new Gson();
            String json = gson.toJson(userObj);
            editor.putString(USER_OBJ, json);
            editor.commit();
        }
    }

    public void setUserObj() {
        editor.putString(USER_OBJ, "");
        editor.commit();
    }

    public UserObj getUserObj() {
        Gson gson = new Gson();
        String json = pref.getString(USER_OBJ, "");
        try {
            return gson.fromJson(json, UserObj.class);
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void setFCMToken(boolean isSet) {
        editor.putBoolean(IS_FCM_TOTEN, isSet);
        editor.commit();
    }

    public boolean isFCMToken() {
        return pref.getBoolean(IS_FCM_TOTEN, false);
    }

}
