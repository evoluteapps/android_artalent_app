package com.artalent.ui.utils;

public enum TransactionType {
    CREDIT,
    DEBIT
}
