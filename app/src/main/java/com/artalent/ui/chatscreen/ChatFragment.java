package com.artalent.ui.chatscreen;

import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;

import com.artalent.R;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by vihaan on 22/05/17.
 */

public class ChatFragment extends Fragment {

    public static ChatFragment newInstance(Bundle bundle) {
        ChatFragment fragment = new ChatFragment();
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_chat, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
    }

    private void initViews() {
        initMessageBar();
        initRecyclerView();
        showChats();
    }


    private FloatingActionButton mFabButton;
    private EditText mEditText;

    private void initMessageBar() {
        mEditText = (EditText) getView().findViewById(R.id.editText);
        mEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

            }
        });

        mEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (s.length() == 0) {
                    showSendButton();
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 0) {
                    showAudioButton();
                }
            }
        });


        mFabButton = (FloatingActionButton) getView().findViewById(R.id.floatingButton);
        mFabButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String tag = (String) mFabButton.getTag();
                Log.d("fab tag", tag);
                if (tag.equalsIgnoreCase(SEND_IMAGE)) {
                    onSendButtonClicked();
                }

            }
        });
    }

    private RecyclerView mRecyclerView;

    private void initRecyclerView() {
        mRecyclerView = (RecyclerView) getView().findViewById(R.id.chatsRecyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setStackFromEnd(true);
        mRecyclerView.setLayoutManager(linearLayoutManager);
    }


    private static final String SEND_IMAGE = "send_image";

    private void showSendButton() {
        mFabButton.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.input_send));
        mFabButton.setTag(SEND_IMAGE);
    }

    private static final String MIC_IMAGE = "mic_image";

    private void showAudioButton() {
        mFabButton.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.input_mic_white));
        mFabButton.setTag(MIC_IMAGE);
    }

    private void onSendButtonClicked() {
        String message = mEditText.getText().toString();
        mEditText.setText("");
        Log.d("send msg", message);
        writeTextMessage(message);
    }

    private void writeTextMessage(String data) {

     /*   Message message = new Message();

        message.setSenderUid(mAuth.getCurrentUser().getUid());
        message.setReceiverUid(mReceivingUser.getUid());
        message.setType("text");
        message.setData(data);

        String messagesNode = getMessagesNode();
        String messageNode = mDatabase.getReference().child(Database.NODE_MESSAGES).child(messagesNode).push().getKey();
        mDatabase.getReference().child(Database.NODE_MESSAGES).child(messagesNode).child(messageNode).setValue(message);

        FirebaseUser sendingUser = mAuth.getCurrentUser();

        UserChat userChat = new UserChat();
        userChat.setUid(sendingUser.getUid());
        userChat.setLastMessage(data);

        mDatabase.getReference().child(Database.NODE_USER_CHATS).child(mReceivingUser.getUid()).child(sendingUser.getUid()).setValue(userChat);


        userChat.setUid(mReceivingUser.getUid());
        mDatabase.getReference().child(Database.NODE_USER_CHATS).child(sendingUser.getUid()).child(mReceivingUser.getUid()).setValue(userChat);
*/
    }


    private String getMessagesNode() {
        String messageNode = null;
       /* if (mAuth.getCurrentUser() != null) {
            FirebaseUser firebaseUser = mAuth.getCurrentUser();
            String sendingUID = firebaseUser.getUid();
            String receivingUID = mReceivingUser.getUid();
            messageNode = Util.getMessageNode(sendingUID, receivingUID);
        }*/
        return messageNode;
    }

    private ChatAdapter mChatAdapter;

    private void showChats() {
        try {


            List<ChatMessage> chatMessages = getChatMessages();
            mChatAdapter = new ChatAdapter(getActivity(), chatMessages);
            mRecyclerView.setAdapter(mChatAdapter);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private List<ChatMessage> getChatMessages() throws IOException, JSONException {
        List<ChatMessage> albumList = new ArrayList<>();
        ChatMessage chatMessage = new ChatMessage();
        chatMessage.setType(ChatAdapter.MSG_TYPE_TEXT);
        chatMessage.setMessage("Hello Akshay");
        albumList.add(chatMessage);

        chatMessage = new ChatMessage();
        chatMessage.setType(ChatAdapter.MSG_TYPE_TEXT);
        chatMessage.setMessage("Hello Nikhil");
        albumList.add(chatMessage);
     /*   List<ChatMessage> chatMessages = null;

        JSONObject jsonObject;
        String json;

        InputStream is = getActivity().getAssets().open("chatmessages.json");

        int size = is.available();
        byte[] buffer = new byte[size];
        is.read(buffer);
        is.close();
        json = new String(buffer, "UTF-8");

        jsonObject = new JSONObject(json);
        JSONArray jsonArray = (JSONArray) jsonObject.get("1");

        Type listType = new TypeToken<List<ChatMessage>>() {
        }.getType();

        chatMessages = new Gson().fromJson(jsonArray.toString(), listType);*/

        return albumList;

    }
}
