package com.artalent.ui.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.FragmentActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.provider.MediaStore;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextSwitcher;

import com.artalent.ArtalentApplication;
import com.artalent.R;
import com.artalent.Utils;
import com.artalent.ui.activity.AppCameraActivity;
import com.artalent.ui.activity.ImagePickerActivity;
import com.artalent.ui.activity.MainActivity;
import com.artalent.ui.activity.NoInternetActivity;
import com.artalent.ui.activity.PostCommentActivity;
import com.artalent.ui.activity.SelectedImageActivityKotlin;
import com.artalent.ui.adapter.HomePostAdapter;
import com.artalent.ui.adapter.StoryAdapter;
import com.artalent.ui.model.AdvertiseResponse;
import com.artalent.ui.model.BaseResponse;
import com.artalent.ui.model.LikeResponse;
import com.artalent.ui.model.PostObj;
import com.artalent.ui.model.PostResponse;
import com.artalent.ui.model.StoryObj;
import com.artalent.ui.model.StoryResponse;
import com.artalent.ui.utils.Constants;
import com.artalent.ui.utils.ImageQuality;
import com.artalent.ui.utils.Options;
import com.artalent.ui.video.StoryViewActivity;
import com.artalent.ui.view.FeedContextMenu;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.artalent.ui.activity.AppCameraActivity.IMAGE_RESULTS;


public class HomeFragment extends BaseFragment implements HomePostAdapter.OnFeedItemClickListener,
        FeedContextMenu.OnFeedContextMenuItemClickListener, View.OnClickListener {

    private static final String TAG = "HomeFragment";
    private static final int FILE_ATTACHMENT = 100;

    public static boolean doYouWantStoryRefresh = false;

    public HomeFragment() {
        // Required empty public constructor
    }

    @BindView(R.id.rvFeed)
    RecyclerView rvFeed;

    @BindView(R.id.myScroll)
    NestedScrollView scroller;

    @BindView(R.id.pullToRefresh)
    SwipeRefreshLayout pullToRefresh;

    @BindView(R.id.story_recycler_view)
    RecyclerView storyRecyclerView;

    private HomePostAdapter feedAdapter;
    private List<Object> postObjs = new ArrayList<>();
    private List<Object> addvert = new ArrayList<>();
    private int addPosition = 0;
    private int randamNumber = 2;

    private List<StoryObj> storyObjs = new ArrayList<>();
    private int offset = 0;
    private View rootView;
    private Options options;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate: called");
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView: called");
        rootView = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, rootView);
        options = Options.init()
                .setRequestCode(100)
                .setCount(1)
                .setFrontfacing(false)
                .setImageQuality(ImageQuality.LOW)
                .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)
                .setPath("/akshay/new");
        setupFeed();
        initScrollListener();
        callGetStoryApi();
        pullToRefresh.setOnRefreshListener(() -> {
            callGetStoryApi();
            callHomePostApiAdvert();
        });
        return rootView;
    }

    public String saveImageFile(Bitmap bitmap) {
        FileOutputStream out = null;
        String filename = getFilename();
        try {
            out = new FileOutputStream(filename);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
            Log.e("Picasso123", filename);
            //
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return filename;
    }

    private void downLoadData(String url) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            showProgressTitle(rootView);
        }
        String fileName = url.substring(url.lastIndexOf('/') + 1, url.length());
        File extStore = Environment.getExternalStorageDirectory();
        File myFile = new File(extStore.getPath() + fileName, "Artalent");
        if (!myFile.exists()) {
            if (hasStoragePermission(FILE_ATTACHMENT)) {
                Call<ResponseBody> call = ArtalentApplication.apiServiceOTP.fetchCaptcha(url);
                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()) {
                            if (response.body() != null) {
                                Bitmap bmp = BitmapFactory.decodeStream(response.body().byteStream());
                                if (bmp != null) {
                                    saveImageFile(bmp, url);
                                }
                            }
                        }
                        hideProgress();
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        hideProgress();
                        // TODO
                    }
                });
            }
        } else {
            String filename = getFilename(url);
            /*
            Sending post to another app
             */
            sendPostWithDataToAnotherApp(filename);

//            ArrayList<String> list = new ArrayList<>();
//            list.add(filename);
//            Intent resultIntent = new Intent(mActivity, SelectedImageActivityKotlin.class);
//            resultIntent.putStringArrayListExtra(IMAGE_RESULTS, list);
//            startActivityForResult(resultIntent, 100);
        }
    }

    private String saveImageFile(Bitmap bitmap, String url) {
        FileOutputStream out = null;
        String filename = getFilename(url);
        try {
            out = new FileOutputStream(filename);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
//            ArrayList<String> list = new ArrayList<>();
//            list.add(filename);

            /*
            Sending post to another app
             */
            sendPostWithDataToAnotherApp(filename);

//            Intent resultIntent = new Intent(mActivity, SelectedImageActivityKotlin.class);
//            resultIntent.putStringArrayListExtra(IMAGE_RESULTS, list);
//            startActivityForResult(resultIntent, 100);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return filename;
    }


    private void sendPostWithDataToAnotherApp(String filename) {
        Log.d(TAG, "saveImageFile: Clicking on share button - url " + filename);

        String mShare = "Hey! Check out the Artalent app.\n" +
                "\n" +
                "Download the app at :\n" +
                "http://www.fusionik.in";

        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("image/jpeg");
        share.putExtra(Intent.EXTRA_STREAM, Uri.parse(filename));
        share.putExtra(Intent.EXTRA_TEXT, mShare);
        startActivity(Intent.createChooser(share, "Share"));
    }

    private boolean hasStoragePermission(int requestCode) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (mActivity.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, requestCode);
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (requestCode == FILE_ATTACHMENT)
                downLoadData(sDownLoadURL);
        }
    }
    private String getFilename() {
        File file = new File(Environment.getExternalStorageDirectory()
                .getPath(), "Artalent");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/art_"
                + System.currentTimeMillis() + ".jpg");
        return uriSting;
    }

    private String getFilename(String url) {
        File file = new File(Environment.getExternalStorageDirectory()
                .getPath(), "Artalent");
        if (!file.exists()) {
            file.mkdirs();
        }

        String fileName = url.substring(url.lastIndexOf('/') + 1);
        return (file.getAbsolutePath() + fileName);
    }


    public void callGetStoryApi() {
        Log.d(TAG, "callGetStoryApi: called");
        storyObjs.clear();
        StoryObj storyObj = new StoryObj();
        storyObj.setUserId(0);
        storyObjs.add(storyObj);
        if (Utils.isInternetAvailable(mActivity)) {
            Call<StoryResponse> call = ArtalentApplication.apiService.getStory(ArtalentApplication.prefManager.getUserObj().getId());
            call.enqueue(new Callback<StoryResponse>() {
                @Override
                public void onResponse(Call<StoryResponse> call, Response<StoryResponse> response) {
                    if (response.body() != null && response.body().getObj() != null) {
                        if (response.body().getObj().size() > 0) {
                            storyObjs.addAll(response.body().getObj());
                        }
                    }
                    setContestWinningAdapter();
                }

                @Override
                public void onFailure(Call<StoryResponse> call, Throwable t) {
                    loading = true;
                    setContestWinningAdapter();
                }
            });
        } else {
            setContestWinningAdapter();
            pullToRefresh.setRefreshing(false);
        }
    }

    protected void setContestWinningAdapter() {
        StoryAdapter winningContestListAdapter = new StoryAdapter(storyObjs, this);
        storyRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        storyRecyclerView.setAdapter(winningContestListAdapter);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        Log.d(TAG, "setUserVisibleHint: called");
        super.setUserVisibleHint(isVisibleToUser);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Utils.updateToolbar(Objects.requireNonNull(mActivity), R.color.white, false);
            }
        }, 100);

        Log.d(TAG, "setUserVisibleHint: doYouWantStory refresh - " + doYouWantStoryRefresh);
        if (doYouWantStoryRefresh && isVisibleToUser) {
            callGetStoryApi();
            doYouWantStoryRefresh = false;
        }
    }

    private void refresh() {
        randamNumber = 2;
        postObjs.clear();
        offset = 0;
        callHomePostApi();
    }


    @Override
    public void onResume() {
        Log.d(TAG, "onResume: called");
        callHomePostApiAdvert();
        super.onResume();
    }

    @Override
    public void onPause() {
        Log.d(TAG, "onPause: called");
        super.onPause();
    }

    private void callHomePostApiAdvert() {
        if (addvert.size() == 0) {
            if (Utils.isInternetAvailable(mActivity)) {
                Call<AdvertiseResponse> call = ArtalentApplication.apiService.getAllAdvertise(ArtalentApplication.prefManager.getUserObj().getId(), 0);
                call.enqueue(new Callback<AdvertiseResponse>() {
                    @Override
                    public void onResponse(Call<AdvertiseResponse> call, Response<AdvertiseResponse> response) {
                        addvert.clear();
                        if (response.body() != null && response.body().getObj() != null) {
                            offset = response.body().getIndex();
                            if (response.body().getObj().size() > 0) {
                                randamNumber = getRandamNumber(2, 5);
                                Log.e("getRandamNumber", "" + randamNumber);
                                addvert.addAll(response.body().getObj());
                            }
                        }
                        postObjs.clear();
                        offset = 0;
                        callHomePostApi();
                    }

                    @Override
                    public void onFailure(Call<AdvertiseResponse> call, Throwable t) {
                        refresh();
                    }
                });
            }
        } else {
            refresh();
        }


    }

    private int getRandamNumber(int x, int y) {
        return x - ((int) Math.round((Math.random()) * (x - y)));
    }

    private void callHomePostApi() {
        if (Utils.isInternetAvailable(mActivity)) {
            if (offset == 0) {
                if (!pullToRefresh.isRefreshing()) {
                    showProgress(rootView);
                }
            }

            Call<PostResponse> call = ArtalentApplication.apiService.getPost(ArtalentApplication.prefManager.getUserObj().getId(), offset);
            call.enqueue(new Callback<PostResponse>() {
                @Override
                public void onResponse(Call<PostResponse> call, Response<PostResponse> response) {
                    hideProgress();
                    int statusCode = response.code();
                    pullToRefresh.setRefreshing(false);
                    loading = true;
                    if (response.body() != null && response.body().getObj() != null) {
                        offset = response.body().getIndex();
                        if (response.body().getObj().size() > 0) {
                            for (PostObj postObj : response.body().getObj()) {
                                if (postObjs.size() == randamNumber) {
                                    if (addPosition >= addvert.size()) {
                                        addPosition = 0;
                                    }
                                    if (addvert.size() > 0) {
                                        postObjs.add(addvert.get(addPosition));
                                    }
                                    addPosition = addPosition + 1;
                                    randamNumber = getRandamNumber(2, 5);
                                    randamNumber = postObjs.size() + randamNumber;
                                    postObjs.add(postObj);
                                } else {
                                    postObjs.add(postObj);
                                }
                            }
                        }
                        feedAdapter.notifyDataSetChanged();
                    }
                }

                @Override
                public void onFailure(Call<PostResponse> call, Throwable t) {
                    // Log error here since request failed
                    hideProgress();
                    loading = true;
                    Log.e("TAG", t.toString());
                    pullToRefresh.setRefreshing(false);
                }
            });
        } else {
            pullToRefresh.setRefreshing(false);
            Intent intent = new Intent(mActivity, NoInternetActivity.class);
            startActivityForResult(intent, Constants.NO_INTERNET);
        }

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == Constants.NO_INTERNET) {
            if (resultCode == Activity.RESULT_OK) {
//                callHomePostApi();
                callGetStoryApi();
            }
        } else if (requestCode == REQUEST_COUNTRY) {
            if (resultCode == Activity.RESULT_OK) {
                int position = data.getIntExtra(Constants.POSITION, -1);
                boolean isStoryDeleted = data.getBooleanExtra(Constants.IS_STORY_DELETED, false);
                Log.d(TAG, "onActivityResult: [HomeFragment] IS_STORY_DELETED - " + isStoryDeleted);

                if (isStoryDeleted) {
                    Utils.showToast(requireActivity(), "Story deleted successfully");
                    callGetStoryApi();
                    return;
                }
                if (position != -1) {
                    Log.d(TAG, "onActivityResult: aage ki story load kar raha hai");
                    StoryObj storyObj = storyObjs.get(position);
                    if (storyObj != null && storyObj.getStories() != null && storyObj.getStories().size() > 0) {
                        Intent intent = new Intent(getActivity(), StoryViewActivity.class);

                        intent.putExtra(Constants.INTEREST_LIST, (Serializable) storyObj.getStories());
                        intent.putExtra(Constants.USER_NAME, storyObj.getOwner().getUserName());
                        intent.putExtra(Constants.PROFILE_URL, storyObj.getOwner().getProfileImage());
                        intent.putExtra(Constants.PROFILE_ID_FOR_STORY, storyObj.getOwner().getId());

                        int nextPosition = -1;
                        if (storyObjs.size() > (position + 1)) {
                            nextPosition = (position + 1);
                        }
                        intent.putExtra(Constants.POSITION, nextPosition);
                        startActivityForResult(intent, REQUEST_COUNTRY);
                    }
                }
            }
        }else if(requestCode == REQUEST_IMAGE){
            if (resultCode == Activity.RESULT_OK) {
                if (data != null && data.getParcelableExtra("path") != null) {
                    Uri uri = data.getParcelableExtra("path");
                    Bitmap bitmap;
                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), uri);
                        saveImageFile(bitmap);
                        returnValue.add(saveImageFile(bitmap));
                        if (returnValue.size() > 0) {
                            callUserProfileApi();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }
        }
    }
    ArrayList<String> returnValue = new ArrayList<>();
    private void callUserProfileApi() {
        if (Utils.isInternetAvailable(getActivity())) {
            showProgress(rootView);
            File file = new File(returnValue.get(0));
            RequestBody postBody = RequestBody.create(MediaType.parse("image/*"), file);
            MultipartBody.Part postImagesParts = MultipartBody.Part.createFormData("file", file.getName(), postBody);

            RequestBody description = RequestBody.create(MediaType.parse("multipart/form-data"), "Hello, Artalent");
            ArtalentApplication.apiServicePost.addStory(ArtalentApplication.prefManager.getUserObj().getId(), postImagesParts, description)
                    .enqueue(new Callback<BaseResponse>() {
                        @Override
                        public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                            hideProgress();
                            returnValue.clear();
                            int statusCode = response.code();
                            String mess = "";
                            try {
                                mess = response.body().getMessage();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            if (statusCode == 200) {
                                Utils.showToast(getActivity(), mess);
                                    callGetStoryApi();
                            }
                        }

                        @Override
                        public void onFailure(Call<BaseResponse> call, Throwable t) {
                            // Log error here since request failed
                            Log.e("TAGArtalent", t.toString());
                            hideProgress();
                            returnValue.clear();
                        }
                    });
        }

    }


    protected void setupFeed() {
        mLayoutManager = new LinearLayoutManager(mActivity);
        rvFeed.setLayoutManager(mLayoutManager);
        feedAdapter = new HomePostAdapter(mActivity, postObjs);
        feedAdapter.setOnFeedItemClickListener(this);
        rvFeed.setAdapter(feedAdapter);
    }

    private LinearLayoutManager mLayoutManager;
    private boolean loading = true;

    private void initScrollListener() {


        if (scroller != null) {

            scroller.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
                @Override
                public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {

                    if (scrollY > oldScrollY) {
                        Log.i("daw_ss", "Scroll DOWN");
                    }
                    if (scrollY < oldScrollY) {
                        Log.i("daw_ss", "Scroll UP");
                    }

                    if (scrollY == 0) {
                        Log.i("daw_ss", "TOP SCROLL");
                    }

                    if (scrollY == v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight()) {
                        Log.i("daw_ss", "BOTTOM SCROLL");
                        if (loading) {
                            if (offset != -1) {
                                loading = false;
                                Log.e("mLayoutManager", "Last Item Wow !");
                                callHomePostApi();
                            }
                        }
                    }
                }
            });
        }

    }


    @Override
    public void onReportClick(int feedItem) {

    }

    @Override
    public void onSharePhotoClick(int feedItem) {

    }

    @Override
    public void onCopyShareUrlClick(int feedItem) {

    }

    @Override
    public void onCancelClick(int feedItem) {

    }

    @Override
    public void onCommentsClick(View v, int position) {
        PostObj postObj = (PostObj) postObjs.get(position);
        if (postObj != null) {
            PostCommentActivity.startActivity(mActivity, postObj);
        }

    }

    @Override
    public void onMoreClick(View v, int position) {

    }

    @Override
    public void onLikeClick(ImageButton btnLike, int position, TextSwitcher tsLikesCounter) {
        PostObj postObj = (PostObj) postObjs.get(position);
        if (postObj != null && postObj.getPostId() > 0) {
            callLikeApi(btnLike, position, postObj, tsLikesCounter);
        }
    }

    private String sDownLoadURL = "";

    @Override
    public void onDownloadImage(PostObj feedItem) {
        if (feedItem != null && feedItem.getMediaUrls() != null && feedItem.getMediaUrls().size() > 0) {
            sDownLoadURL = feedItem.getMediaUrls().get(0);
            downLoadData(sDownLoadURL);
        }

    }

    @Override
    public void onProfileClick(View v) {

    }

    @Override
    public void onDeleteClick(PostObj postObj, int position) {
        open(postObj, position);
    }

    @Override
    public void onItemDoubleClick(ImageButton v, int position, TextSwitcher tsLikesCounter) {
        PostObj postObj = (PostObj) postObjs.get(position);
        if (postObj != null && postObj.getPostId() > 0) {
            callLikeApi(v, position, postObj, tsLikesCounter);
        }
    }

    public void open(PostObj postObj, int position) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setMessage("Are you sure, You want to delete post?");
        alertDialogBuilder.setPositiveButton("yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        arg0.dismiss();
                        deleteInterest(postObj, position);
                    }
                });

        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void deleteInterest(PostObj postObj, int position) {
        if (Utils.isInternetAvailable(mActivity)) {
            ArtalentApplication.apiService.deletePost(ArtalentApplication.prefManager.getUserObj().getId(), postObj.getPostId())
                    .enqueue(new Callback<BaseResponse>() {
                        @Override
                        public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                            //callInterestApi();
                            if (response.body() != null) {
                                postObjs.remove(position);
                                feedAdapter.notifyDataSetChanged();
                            }
                        }

                        @Override
                        public void onFailure(Call<BaseResponse> call, Throwable t) {

                        }
                    });
        }
    }

    private void callLikeApi(ImageButton btnLike, int position, PostObj postObj, TextSwitcher tsLikesCounter) {
        if (Utils.isInternetAvailable(mActivity)) {
            Call<LikeResponse> call = ArtalentApplication.apiService.callLike(ArtalentApplication.prefManager.getUserObj().getId(), postObj.getPostId());
            call.enqueue(new Callback<LikeResponse>() {
                @Override
                public void onResponse(Call<LikeResponse> call, Response<LikeResponse> response) {
                    int statusCode = response.code();
                    if (response.body() != null) {
                        if (response.body().getObj() != null && !TextUtils.isEmpty(response.body().getObj())) {
                            btnLike.setImageResource(response.body().getObj().equals("0") ? R.drawable.ic_heart_outline_grey : R.drawable.ic_heart_red);
                            postObj.setIsLiked(response.body().getObj());
                            if (response.body().getObj().equals("0")) {
                                postObj.setLikeCount(postObj.getLikeCount() - 1);

                            } else {
                                postObj.setLikeCount(postObj.getLikeCount() + 1);
                            }

                            tsLikesCounter.setCurrentText(getResources().getQuantityString(
                                    R.plurals.likes_count, postObj.getLikeCount(), postObj.getLikeCount()
                            ));

                        }
                    }


                }

                @Override
                public void onFailure(Call<LikeResponse> call, Throwable t) {
                    // Log error here since request failed
                    Log.e("TAG", t.toString());
                }
            });
        }
    }

    public static final int REQUEST_COUNTRY = 200;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cardMainView:
                int i = (int) v.getTag();
                StoryObj storyObj = storyObjs.get(i);
                if (storyObj != null && storyObj.getStories() != null && storyObj.getStories().size() > 0) {
                    Intent intent = new Intent(getActivity(), StoryViewActivity.class);
                    intent.putExtra(Constants.INTEREST_LIST, (Serializable) storyObj.getStories());

                    intent.putExtra(Constants.USER_NAME, storyObj.getOwner().getUserName());
                    intent.putExtra(Constants.PROFILE_URL, storyObj.getOwner().getProfileImage());
                    intent.putExtra(Constants.PROFILE_ID_FOR_STORY, storyObj.getOwner().getId());

                    int nextPosition = -1;
                    if (storyObjs.size() > (i + 1)) {
                        nextPosition = (i + 1);
                    }

                    intent.putExtra(Constants.POSITION, nextPosition);
                    startActivityForResult(intent, REQUEST_COUNTRY);
                }
                break;
            case R.id.cartFirst:
                openCameraGallery();

//                AppCameraActivity.start(mActivity, options, 1);
                break;
        }
    }

    public void openCameraGallery() {

        Dexter.withActivity(getActivity())
                .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            showImagePickerOptions();
                        }

                        if (report.isAnyPermissionPermanentlyDenied()) {
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    private void showSettingsDialog() {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(getContext());
        builder.setTitle(getString(R.string.dialog_permission_title));
        builder.setMessage(getString(R.string.dialog_permission_message));
        builder.setPositiveButton(getString(R.string.go_to_settings), (dialog, which) -> {
            dialog.cancel();
            openSettings();
        });
        builder.setNegativeButton(getString(android.R.string.cancel), (dialog, which) -> dialog.cancel());
        builder.show();

    }

    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getContext().getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }


    private void showImagePickerOptions() {
        ImagePickerActivity.showImagePicker(getContext(), new ImagePickerActivity.PickerOptionListener() {
            @Override
            public void onTakeCameraSelected() {
                launchCameraIntent();
            }

            @Override
            public void onChooseGallerySelected() {
                launchGalleryIntent();
            }

            @Override
            public void onDeleteImage() {

            }

        });
    }

    private void launchCameraIntent() {
        Intent intent = new Intent(getActivity(), ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_IMAGE_CAPTURE);

        // setting aspect ratio
        /*intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 16); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 9);*/

        /*// setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 1000);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 1000);*/

        startActivityForResult(intent, REQUEST_IMAGE);
    }


    private void launchGalleryIntent() {
        Intent intent = new Intent(getActivity(), ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_GALLERY_IMAGE);

  /*      // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 16); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 9);*/
        startActivityForResult(intent, REQUEST_IMAGE);
    }

    public static final int REQUEST_IMAGE = 100;
}
