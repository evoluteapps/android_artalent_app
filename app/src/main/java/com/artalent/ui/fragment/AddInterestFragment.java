package com.artalent.ui.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;

import com.artalent.ArtalentApplication;
import com.artalent.R;
import com.artalent.Utils;
import com.artalent.ui.activity.ImagePickerActivity;
import com.artalent.ui.model.BaseResponse;
import com.artalent.ui.model.InterestObj;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.textfield.TextInputEditText;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.artalent.ui.activity.RegistrationActivity.REQUEST_IMAGE;

public class AddInterestFragment extends BottomSheetDialogFragment {
    private static final String TAG = "AddInterestFragment";

    @BindView(R.id.addInterestBtn)
    ImageButton addInterestBtn;

    @BindView(R.id.et_interest_name)
    TextInputEditText etInterestName;

    @BindView(R.id.textView)
    TextView textView;

    @BindView(R.id.interestImage)
    ImageView ivInterestIcon;

    @BindView(R.id.llInterest)
    LinearLayout llInterest;

    private Activity activity;
    private InterestObj selectedInterest = null;
    private View view;
    private ProgressCallback progressCallback;
    private RequestOptions options;

    public AddInterestFragment(Activity activity, InterestObj interestObj) {
        this.activity = activity;
        this.selectedInterest = interestObj;

        // Required empty public constructor
    }

    public void setOnProgressListener(ProgressCallback progressCallback) {
        this.progressCallback = progressCallback;
    }


    public interface ProgressCallback {
        public void startProgress();

        public void hideCircularProgress();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.activity_add_interest, container, false);
        ButterKnife.bind(this, view);
        if (selectedInterest != null && !TextUtils.isEmpty(selectedInterest.getInterest())) {
            etInterestName.setText(selectedInterest.getInterest());
            textView.setText("Edit Interest");
        }
        if (selectedInterest != null && !TextUtils.isEmpty(selectedInterest.getBaner())) {
            options = new RequestOptions()
                    .centerCrop()
                    .placeholder(R.drawable.interest_categories)
                    .error(R.drawable.interest_categories);
            Glide.with(requireActivity())
                    .load(selectedInterest.getBaner()).apply(options).into(ivInterestIcon);
        }

        return view;
    }


    @OnClick(R.id.addInterestBtn)
    void onAddInterestBtnClick() {
        String interestName = etInterestName.getText().toString().trim();
        if (TextUtils.isEmpty(interestName)) {
            Utils.showToast(activity, "Please enter talent name.");
        } else if (TextUtils.isEmpty(selectedFileName)) {
            if (selectedInterest != null && selectedInterest.getInterestMasterId() > 0) {
                callEditInterestApi(interestName);
            } else {
                Utils.showToast(activity, "Please select talent icon");
            }
        } else {
            if (selectedInterest != null && selectedInterest.getInterestMasterId() > 0) {
                callEditInterestApi(selectedFileName, interestName);
            } else {
                callAddInterestApi(selectedFileName, interestName);
            }
        }
    }


    @OnClick(R.id.llInterest)
    void onllInterestClick() {
        Dexter.withActivity(activity)
                .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) showImagePickerOptions();
                        if (report.isAnyPermissionPermanentlyDenied()) showSettingsDialog();
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }


    private void callAddInterestApi(String getSelectedPath, String interestName) {
        if (Utils.isInternetAvailable(activity)) {
            progressCallback.startProgress();
            File file = new File(getSelectedPath);
            RequestBody postBody = RequestBody.create(MediaType.parse("image/*"), file);
            RequestBody banerBody = RequestBody.create(MediaType.parse("image/*"), "");
            MultipartBody.Part interestIconImagesParts = MultipartBody.Part.createFormData("icon", file.getName(), postBody);
            MultipartBody.Part interestBanerImageParts = MultipartBody.Part.createFormData("baner", "", banerBody);
            RequestBody interestNameBody = RequestBody.create(MediaType.parse("multipart/form-data"), interestName);

            ArtalentApplication.apiServicePost.addInterest(ArtalentApplication.prefManager.getUserObj().getId(), interestIconImagesParts, interestBanerImageParts, interestNameBody)
                    .enqueue(new Callback<BaseResponse>() {
                        @Override
                        public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                            int statusCode = response.code();
                            String mess = response.body().getMessage();
                            if (statusCode == 200) {
                                progressCallback.hideCircularProgress();
                                Utils.showToast(activity, mess);
                                dismiss();
                            } else {
                                progressCallback.hideCircularProgress();
                                Utils.showToast(activity, mess);
                            }
                        }

                        @Override
                        public void onFailure(Call<BaseResponse> call, Throwable t) {
                            // Log error here since request failed
                            progressCallback.hideCircularProgress();
                            Utils.showToast(activity, "onFailure : " + t.toString());
                        }
                    });
        }
    }

    private void callEditInterestApi(String getSelectedPath, String interestName) {
        if (Utils.isInternetAvailable(activity)) {
            progressCallback.startProgress();
            File file = new File(getSelectedPath);
            RequestBody postBody = RequestBody.create(MediaType.parse("image/*"), file);
            RequestBody banerBody = RequestBody.create(MediaType.parse("image/*"), "");
            MultipartBody.Part interestIconImagesParts = MultipartBody.Part.createFormData("icon", file.getName(), postBody);
            MultipartBody.Part interestBanerImageParts = MultipartBody.Part.createFormData("baner", "", banerBody);
            RequestBody interestNameBody = RequestBody.create(MediaType.parse("multipart/form-data"), interestName);


            ArtalentApplication.apiServicePost.editInterest(ArtalentApplication.prefManager.getUserObj().getId(), selectedInterest.getInterestMasterId(), interestIconImagesParts, interestBanerImageParts, interestNameBody)
                    .enqueue(new Callback<BaseResponse>() {
                        @Override
                        public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                            int statusCode = response.code();
                            String mess = response.body().getMessage();
                            if (statusCode == 200) {
                                progressCallback.hideCircularProgress();
                                Utils.showToast(activity, mess);
                                dismiss();
                            } else {
                                progressCallback.hideCircularProgress();
                                Utils.showToast(activity, mess);
                            }
                        }

                        @Override
                        public void onFailure(Call<BaseResponse> call, Throwable t) {
                            // Log error here since request failed
                            progressCallback.hideCircularProgress();
                            Utils.showToast(activity, "onFailure : " + t.toString());
                        }
                    });
        }
    }

    private void callEditInterestApi(String interestName) {
        if (Utils.isInternetAvailable(activity)) {
            progressCallback.startProgress();
            RequestBody postBody = RequestBody.create(MediaType.parse("image/*"), "");
            RequestBody banerBody = RequestBody.create(MediaType.parse("image/*"), "");
            MultipartBody.Part interestIconImagesParts = MultipartBody.Part.createFormData("icon", "", postBody);
            MultipartBody.Part interestBanerImageParts = MultipartBody.Part.createFormData("baner", "", banerBody);
            RequestBody interestNameBody = RequestBody.create(MediaType.parse("multipart/form-data"), interestName);


            ArtalentApplication.apiServicePost.editInterest(ArtalentApplication.prefManager.getUserObj().getId(), selectedInterest.getInterestMasterId(), interestIconImagesParts, interestBanerImageParts, interestNameBody)
                    .enqueue(new Callback<BaseResponse>() {
                        @Override
                        public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                            int statusCode = response.code();
                            String mess = response.body().getMessage();
                            if (statusCode == 200) {
                                progressCallback.hideCircularProgress();
                                Utils.showToast(activity, mess);
                                dismiss();
                            } else {
                                progressCallback.hideCircularProgress();
                                Utils.showToast(activity, mess);
                            }
                        }

                        @Override
                        public void onFailure(Call<BaseResponse> call, Throwable t) {
                            // Log error here since request failed
                            progressCallback.hideCircularProgress();
                            Utils.showToast(activity, "onFailure : " + t.toString());
                        }
                    });
        }
    }


    private void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(getString(R.string.dialog_permission_title));
        builder.setMessage(getString(R.string.dialog_permission_message));
        builder.setPositiveButton(getString(R.string.go_to_settings), (dialog, which) -> {
            dialog.cancel();
            openSettings();
        });
        builder.setNegativeButton(getString(android.R.string.cancel), (dialog, which) -> dialog.cancel());
        builder.show();
    }


    private void showImagePickerOptions() {
        ImagePickerActivity.showImagePickerOptions(activity, new ImagePickerActivity.PickerOptionListener() {
            @Override
            public void onTakeCameraSelected() {
                launchCameraIntent();
            }

            @Override
            public void onChooseGallerySelected() {
                launchGalleryIntent();
            }

            @Override
            public void onDeleteImage() {

            }
        });
    }

    private void launchCameraIntent() {
        Intent intent = new Intent(activity, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_IMAGE_CAPTURE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);

        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 1000);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 1000);

        startActivityForResult(intent, REQUEST_IMAGE);
    }

    private void launchGalleryIntent() {
        Intent intent = new Intent(activity, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_GALLERY_IMAGE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);
        startActivityForResult(intent, REQUEST_IMAGE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                Uri uri = null;
                if (data != null) {
                    uri = data.getParcelableExtra("path");
                    Log.d(TAG, "onActivityResult: uri -> " + uri.getPath());
                }
                loadProfile(uri);
            }
        }
    }

    private String getFilename() {
        File file = new File(Environment.getExternalStorageDirectory()
                .getPath(), "Artalent/TalentCategoryIcon");

        if (!file.exists()) file.mkdirs();
        return (file.getAbsolutePath() + "/art_"
                + System.currentTimeMillis() + ".jpg");
    }

    private void loadProfile(Uri url) {
        Picasso.get().load(url)
                .into(ivInterestIcon);
        ivInterestIcon.setColorFilter(ContextCompat.getColor(activity, android.R.color.transparent));
        new Handler().postDelayed(() -> {
            Bitmap bm = ((BitmapDrawable) ivInterestIcon.getDrawable()).getBitmap();
            saveImageFile(bm);
        }, 1500);
    }

    private String selectedFileName;

    private void saveImageFile(Bitmap bitmap) {
        FileOutputStream out;
        selectedFileName = getFilename();
        try {
            out = new FileOutputStream(selectedFileName);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", activity.getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }
}
