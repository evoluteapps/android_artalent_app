package com.artalent.ui.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.artalent.ArtalentApplication;
import com.artalent.R;
import com.artalent.Utils;
import com.artalent.ui.activity.ContestListActivity;
import com.artalent.ui.activity.InterestListActivity;
import com.artalent.ui.activity.MainActivity;
import com.artalent.ui.activity.NoInternetActivity;
import com.artalent.ui.activity.WinnerListActivity;
import com.artalent.ui.adapter.AdvertisePanelTabAdapter;
import com.artalent.ui.adapter.InterestListAdapter;
import com.artalent.ui.adapter.PagerSpecialContestAdapter;
import com.artalent.ui.adapter.RunningContestListAdapter;
import com.artalent.ui.adapter.WinningContestListAdapter;
import com.artalent.ui.model.ContestHistoryObj;
import com.artalent.ui.model.ContestHistoryResponse;
import com.artalent.ui.model.ContestObj;
import com.artalent.ui.model.ContestRegistrationObj;
import com.artalent.ui.model.ContestResponse;
import com.artalent.ui.model.InterestObj;
import com.artalent.ui.model.InterestResponse;
import com.artalent.ui.model.PostContestObj;
import com.artalent.ui.model.RegisteredContestObj;
import com.artalent.ui.model.SpecialContestObj;
import com.artalent.ui.model.SpecialContestResponse;
import com.artalent.ui.model.UserObj;
import com.artalent.ui.utils.AutoScrollViewPager;
import com.artalent.ui.utils.Constants;
import com.google.android.material.tabs.TabLayout;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@SuppressLint("SourceLockedOrientationActivity")
public class ContestHomeFragment extends BaseFragment {


    @BindView(R.id.category_recycler_view)
    RecyclerView categoryRecyclerView;

    InterestListAdapter interestListAdapter = null;

    @BindView(R.id.running_contest_recycler_view)
    RecyclerView runningContestRecyclerView;

    @BindView(R.id.winning_contest_recycler_view)
    RecyclerView winningContestRecyclerView;


    @BindView(R.id.pullToRefresh)
    SwipeRefreshLayout pullToRefresh;

    @BindView(R.id.nested_parent)
    NestedScrollView nestedScrollView;

    @BindView(R.id.contest_show_all_relative)
    RelativeLayout contestShowAll;

    @BindView(R.id.running_contest_relative)
    RelativeLayout runningContest;

    @BindView(R.id.winning_contest_relative)
    RelativeLayout winningContestUser;

    @BindView(R.id.winner_relative)
    RelativeLayout winnerShowAll;

    @BindView(R.id.show_all_winner_btn)
    Button showAllWinner;

    @BindView(R.id.vpSpecialContest)
    AutoScrollViewPager vpSpecialContest;

    @BindView(R.id.pagerPageIndicator)
    TabLayout pageIndicator;


    @BindView(R.id.rlSpecialContest)
    RelativeLayout rlSpecialContest;

    @BindView(R.id.talent_label)
    TextView talentLabel;

    @BindView(R.id.show_all_category_btn)
    Button showAllCategory;

    @BindView(R.id.llWinner)
    LinearLayout llWinner;


    private Context mContext;
    private List<PostContestObj> postObjs = new ArrayList<>();
    private List<InterestObj> interestList = new ArrayList<>();
    private List<ContestObj> contestObjs = new ArrayList<>();
    private List<ContestObj> registeredContestList = new ArrayList<>();
    private List<SpecialContestObj> specialContests = new ArrayList<>();
    private List<SpecialContestObj> registeredSpecialContestObj = new ArrayList<>();
    private RegisteredContestObj registeredContestObj = new RegisteredContestObj();

    private List<ContestHistoryObj> contestHistoryObj = new ArrayList<>();
    private AdvertisePanelTabAdapter adapter;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_contest_home, container, false);
        ButterKnife.bind(this, view);
        rlSpecialContest.setVisibility(View.GONE);
        talentLabel.setVisibility(View.GONE);
        vpSpecialContest.startAutoScroll();
        vpSpecialContest.setInterval(3000);
        vpSpecialContest.setCycle(true);
        callAllBackendAPI();
        pullToRefresh.setOnRefreshListener(this::callAllBackendAPI);
        callWinnerApi();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (MainActivity.isContestReg) {
            MainActivity.isContestReg = false;
            callRunningContestApi();
        }
        if (MainActivity.isContestPost) {
            MainActivity.isContestPost = false;
            postObjs.clear();
        }
    }


    @Override
    public void onStart() {
        super.onStart();
    }


    private void callAllBackendAPI() {
        if (Utils.isInternetAvailable(mContext)) {
            postObjs.clear();
            callSpecialContestApi();
            callInterestApi();
            callRunningContestApi();

        } else {
            Intent intent = new Intent(mContext, NoInternetActivity.class);
            startActivityForResult(intent, Constants.NO_INTERNET);
        }

    }

    private void callSpecialContestApi() {
        if (Utils.isInternetAvailable(mContext)) {
            Call<SpecialContestResponse> call = ArtalentApplication.apiService.getAdminContest(ArtalentApplication.prefManager.getUserObj().getId(), 1, 0);
            call.enqueue(new Callback<SpecialContestResponse>() {
                @Override
                public void onResponse(Call<SpecialContestResponse> call, Response<SpecialContestResponse> response) {
                    specialContests.clear();
                    registeredSpecialContestObj.clear();
                    if (response.body() != null && response.body().getObj() != null) {
                        if (response.body().getObj().size() > 0) {
                            specialContests.addAll(response.body().getObj());
                        }
                        for (SpecialContestObj specialContestObj : specialContests) {
                            if (specialContestObj.getIsRegistered() == 1) {
                                registeredSpecialContestObj.add(specialContestObj);
                            }
                        }
                        setSpecialContest();
                    }
                }

                @Override
                public void onFailure(Call<SpecialContestResponse> call, Throwable t) {
                    hideProgress();
                }
            });
        } else {
            hideProgress();
            Intent intent = new Intent(getActivity(), NoInternetActivity.class);
            startActivityForResult(intent, Constants.NO_INTERNET);
        }

    }

    private void setSpecialContest() {
        Picasso picasso = Picasso.get();
        PagerSpecialContestAdapter myPagerAdapter = new PagerSpecialContestAdapter(picasso, specialContests, mActivity);
        vpSpecialContest.setClipToPadding(false);
        vpSpecialContest.setPadding(20, 0, 20, 0);
        vpSpecialContest.setPageMargin(50);
        vpSpecialContest.setAdapter(myPagerAdapter);
        pageIndicator.setupWithViewPager(vpSpecialContest);
        if (specialContests.size() > 0) {
            rlSpecialContest.setVisibility(View.VISIBLE);
            pageIndicator.setVisibility(View.GONE);
        } else {
            rlSpecialContest.setVisibility(View.GONE);
            pageIndicator.setVisibility(View.GONE);
        }
    }


    List<SpecialContestObj> getRegisteredSpecialContest() {
        return this.registeredSpecialContestObj;
    }

    RegisteredContestObj getRegisteredContest() {
        return this.registeredContestObj;
    }

    private void setContestCategoryAdapter() {
        interestListAdapter = new InterestListAdapter(interestList, Constants.INTEREST_CONTEST_LIST_NAME, mActivity);
        categoryRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        categoryRecyclerView.setAdapter(interestListAdapter);
    }

    private void setContestWinningAdapter() {
        WinningContestListAdapter winningContestListAdapter = new WinningContestListAdapter(contestHistoryObj, mActivity);
        winningContestRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        winningContestRecyclerView.setAdapter(winningContestListAdapter);
        if (contestHistoryObj.size() > 0) {
            llWinner.setVisibility(View.VISIBLE);
        } else {
            llWinner.setVisibility(View.GONE);
        }
    }


    private void setContestRunningAdapter() {
        if (contestObjs.size() > 0) {
            runningContest.setVisibility(View.VISIBLE);
            contestShowAll.setVisibility(View.VISIBLE);
            RunningContestListAdapter runningContestListAdapter = new RunningContestListAdapter(contestObjs, mContext);
            runningContestListAdapter.notifyDataSetChanged();
            runningContestRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
            runningContestRecyclerView.setAdapter(runningContestListAdapter);
        }
    }

    private void callInterestApi() {
        if (Utils.isInternetAvailable(mActivity)) {
            Call<InterestResponse> call = ArtalentApplication.apiService.getInterest();
            call.enqueue(new Callback<InterestResponse>() {
                @Override
                public void onResponse(Call<InterestResponse> call, Response<InterestResponse> response) {
                    int statusCode = response.code();
                    interestList.clear();//
                    if (response.body() != null && response.body().getObj() != null && response.body().getObj().size() > 0) {
                        interestList = response.body().getObj();
                        setContestCategoryAdapter();
                    }
                }

                @Override
                public void onFailure(Call<InterestResponse> call, Throwable t) {
                    // Log error here since request failed
                }
            });
        } else {
            Intent intent = new Intent(getActivity(), NoInternetActivity.class);
            startActivityForResult(intent, Constants.NO_INTERNET);
        }
    }

    protected void callWinnerApi() {
        if (Utils.isInternetAvailable(Objects.requireNonNull(getActivity()))) {
            Call<ContestHistoryResponse> call = ArtalentApplication.apiService.contestHistory(ArtalentApplication.prefManager.getUserObj().getId(), 0);
            call.enqueue(new Callback<ContestHistoryResponse>() {
                @Override
                public void onResponse(Call<ContestHistoryResponse> call, Response<ContestHistoryResponse> response) {
                    int statusCode = response.code();
                    contestHistoryObj.clear();
                    if (response.body() != null && response.body().getObj() != null && response.body().getObj().size() > 0) {
                        for (ContestHistoryObj contestHistoryObj1 : response.body().getObj()) {
                            if (contestHistoryObj1 != null && contestHistoryObj1.getWinner1() != null && contestHistoryObj1.getWinner1().getId() > 0) {
                                contestHistoryObj.add(contestHistoryObj1);
                            }

                        }

                        setContestWinningAdapter();
                    }
                }

                @Override
                public void onFailure(Call<ContestHistoryResponse> call, Throwable t) {
                }
            });
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.mContext = null;
    }

    private void callRunningContestApi() {
        if (Utils.isInternetAvailable(mContext)) {
            Call<ContestResponse> call = ArtalentApplication.apiService.getContestRegistration(ArtalentApplication.prefManager.getUserObj().getId());
            call.enqueue(new Callback<ContestResponse>() {
                @Override
                public void onResponse(Call<ContestResponse> call, Response<ContestResponse> response) {
                    int statusCode = response.code();
                    contestObjs.clear();
                    registeredContestList.clear();
                    if (response.body() != null && response.body().getObj() != null && response.body().getObj().size() > 0) {
                        contestShowAll.setVisibility(View.VISIBLE);
                        runningContest.setVisibility(View.VISIBLE);
                        for (ContestObj contestObj : response.body().getObj()) {
                            boolean isAvailable = false;
                            for (ContestRegistrationObj contestRegistrationObj : contestObj.getRegistrationDetails()) {
                                if (ArtalentApplication.prefManager.getUserObj() != null && contestRegistrationObj != null && contestRegistrationObj.getUser() != null) {
                                    UserObj userObj = ArtalentApplication.prefManager.getUserObj();
                                    if (contestRegistrationObj.getUser().getId() == userObj.getId()) {
                                        isAvailable = true;
                                    }
                                }
                            }
                            if (isAvailable) {
                                contestObjs.add(contestObj);
                                registeredContestList.add(contestObj);
                            }
                        }

                        for (ContestObj contestObj : response.body().getObj()) {
                            boolean isAvailable = false;
                            for (ContestRegistrationObj contestRegistrationObj : contestObj.getRegistrationDetails()) {
                                if (ArtalentApplication.prefManager.getUserObj() != null && contestRegistrationObj != null && contestRegistrationObj.getUser() != null) {
                                    UserObj userObj = ArtalentApplication.prefManager.getUserObj();
                                    if (contestRegistrationObj.getUser().getId() == userObj.getId()) {
                                        isAvailable = true;
                                    }
                                }
                            }
                            if (!isAvailable) {
                                contestObjs.add(contestObj);
                            }
                        }
                    } else {
                        runningContest.setVisibility(View.GONE);
                        contestShowAll.setVisibility(View.GONE);
                    }
                    registeredContestObj.setContestObjList(registeredContestList);
                    pullToRefresh.setRefreshing(false);
                    setContestRunningAdapter();
                }

                @Override
                public void onFailure(Call<ContestResponse> call, Throwable t) {
                    // Log error here since request failed
                    hideProgress();
                    pullToRefresh.setRefreshing(false);
                }
            });
        } else {
            pullToRefresh.setRefreshing(false);
            hideProgress();
            Intent intent = new Intent(mContext, NoInternetActivity.class);
            startActivityForResult(intent, Constants.NO_INTERNET);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == Constants.NO_INTERNET) {
            if (resultCode == Activity.RESULT_OK) {
                callAllBackendAPI();
            }
        } else if (requestCode == Constants.REFRESH_CONTEST_LIST) {
            if (resultCode == Activity.RESULT_OK) {
                callRunningContestApi();
            }
        }
    }

    @OnClick(R.id.show_all_contest_btn)
    void showAllContestClick() {
        if (contestObjs.size() > 0) {
            ContestListActivity.startActivity(getActivity());
        }
    }

    @OnClick(R.id.show_all_winner_btn)
    void showAllWinner() {
        Intent intent = new Intent(mContext, WinnerListActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.show_all_category_btn)
    void showAllCategory() {
        Intent intent = new Intent(mContext, InterestListActivity.class);
        startActivity(intent);
    }

    @Override
    public void onPause() {
        super.onPause();
    }


    @Override
    public void onStop() {
        super.onStop();
    }

}
