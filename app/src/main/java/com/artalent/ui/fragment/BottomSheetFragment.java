package com.artalent.ui.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.artalent.R;
import com.artalent.ui.activity.DraftVideoPlayActivity;
import com.artalent.ui.activity.MainActivity;
import com.artalent.ui.adapter.ContestDialogAdapter;
import com.artalent.ui.model.room.ContestVideoDraft;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BottomSheetFragment extends BottomSheetDialogFragment {

    private static final String TAG = "BottomSheetFragment";
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.ivClose)
    ImageView ivClose;


    private List<Object> objectContestList = new ArrayList<>();
    private ContestVideoDraft contestVideoDraft = null;
    private boolean isFromDraftAdapterToBottomSheet = false;

    public BottomSheetFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_bottom_sheet, container, false);

        ButterKnife.bind(this, view);
        if (getArguments() != null) {
            objectContestList = (ArrayList<Object>) getArguments().getSerializable("RegisteredContest");
            contestVideoDraft = (ContestVideoDraft) getArguments().getSerializable("SelectedDraft");
            if (contestVideoDraft != null) {
                isFromDraftAdapterToBottomSheet = true;
            }
            setAdapter();
        }
        return view;
    }

    public void setAdapter() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        ContestDialogAdapter contestDialogAdapter;
        if (isFromDraftAdapterToBottomSheet) {
            contestDialogAdapter = new ContestDialogAdapter(getActivity(), objectContestList, contestVideoDraft, true);
        } else {
            Log.d(TAG, "setAdapter: List<Object> objectContestList" + objectContestList.toString());
            contestDialogAdapter = new ContestDialogAdapter(getActivity(), objectContestList);
        }
        recyclerView.setAdapter(contestDialogAdapter);

        ivClose.setOnClickListener(v -> {
            if (getContext() instanceof MainActivity) {
                if (((MainActivity) getContext()).bottomSheetFragment != null) {
                    ((MainActivity) getContext()).bottomSheetFragment.dismiss();
                }
            } else {
                if (((DraftVideoPlayActivity) getContext()).getBottomSheetFragment() != null) {
                    ((DraftVideoPlayActivity) getContext()).getBottomSheetFragment().dismiss();
                }
            }
        });
    }
}