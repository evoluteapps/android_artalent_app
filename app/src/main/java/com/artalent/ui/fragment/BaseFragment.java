package com.artalent.ui.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.artalent.R;
import com.artalent.ui.progressbar.ChromeFloatingCirclesDrawable;
import com.artalent.ui.progressbar.FoldingCirclesDrawable;
import com.artalent.ui.progressbar.GoogleMusicDicesDrawable;
import com.artalent.ui.progressbar.NexusRotationCrossDrawable;


public class BaseFragment extends Fragment {

    public AppCompatActivity mActivity;
    public ProgressBar mProgressBar;
    public RelativeLayout rlProgress;

    private static final int FOLDING_CIRCLES = 0;
    private static final int MUSIC_DICES = 1;
    private static final int NEXUS_CROSS_ROTATION = 2;
    private static final int CHROME_FLOATING_CIRCLES = 3;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof AppCompatActivity) {
            AppCompatActivity activity = (AppCompatActivity) context;
            this.mActivity = activity;
        }
    }

    public void showProgress(View rootView) {
        if (mProgressBar == null) {
            mProgressBar = rootView.findViewById(R.id.google_progress);
        }
        if (rlProgress == null) {
            rlProgress = rootView.findViewById(R.id.rlProgress);
        }
        if (mProgressBar != null) {
            Rect bounds = mProgressBar.getIndeterminateDrawable().getBounds();
            mProgressBar.setIndeterminateDrawable(getProgressDrawable());
            mProgressBar.getIndeterminateDrawable().setBounds(bounds);
            if (rlProgress != null) {
                Log.e("progressRunning", "Fragment Progress Running");
                rlProgress.setVisibility(View.VISIBLE);
            }
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void showProgressTitle(View rootView) {
        if (mProgressBar == null) {
            mProgressBar = rootView.findViewById(R.id.google_progress);
        }
        if (rlProgress == null) {
            rlProgress = rootView.findViewById(R.id.rlProgress);
        }
        if (mProgressBar != null) {
            Rect bounds = mProgressBar.getIndeterminateDrawable().getBounds();
            mProgressBar.setTooltipText("Download Image");
            mProgressBar.setIndeterminateDrawable(getProgressDrawable());
            mProgressBar.getIndeterminateDrawable().setBounds(bounds);
            if (rlProgress != null) {
                Log.e("progressRunning", "Fragment Progress Running");
                rlProgress.setVisibility(View.VISIBLE);
            }
        }

    }

    public void hideProgress() {
        if (rlProgress != null) {
            rlProgress.setVisibility(View.GONE);
        }
    }

    private Drawable getProgressDrawable() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mActivity);
        int value = Integer.parseInt(prefs.getString(getString(R.string.progressBar_pref_key), getString(R.string.progressBar_pref_defValue)));
        Drawable progressDrawable = null;
        switch (value) {
            case FOLDING_CIRCLES:
                progressDrawable = new FoldingCirclesDrawable.Builder(mActivity)
                        .colors(getProgressDrawableColors())
                        .build();
                break;

            case MUSIC_DICES:
                progressDrawable = new GoogleMusicDicesDrawable.Builder().build();
                break;

            case NEXUS_CROSS_ROTATION:
                progressDrawable = new NexusRotationCrossDrawable.Builder(mActivity)
                        .colors(getProgressDrawableColors())
                        .build();
                break;

            case CHROME_FLOATING_CIRCLES:
                progressDrawable = new ChromeFloatingCirclesDrawable.Builder(mActivity)
                        .colors(getProgressDrawableColors())
                        .build();
                break;
        }

        return progressDrawable;
    }

    private int[] getProgressDrawableColors() {
        int[] colors = new int[4];
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mActivity);
        colors[0] = prefs.getInt(getString(R.string.firstcolor_pref_key), getResources().getColor(R.color.red));
        colors[1] = prefs.getInt(getString(R.string.secondcolor_pref_key), getResources().getColor(R.color.blue));
        colors[2] = prefs.getInt(getString(R.string.thirdcolor_pref_key), getResources().getColor(R.color.yellow));
        colors[3] = prefs.getInt(getString(R.string.fourthcolor_pref_key), getResources().getColor(R.color.green));
        return colors;
    }


}
