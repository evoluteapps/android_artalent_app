package com.artalent.ui.fragment;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.artalent.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddCategoryFragment extends Fragment {

    @BindView(R.id.category_name)
    EditText etCategoryName;

    @BindView(R.id.category_desc)
    EditText etCategoryDesc;

    @BindView(R.id.add_button)
    AppCompatButton btnAddCategory;



    String categoryName, categoryDesc;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_category,container,false);
        ButterKnife.bind(this, view);
        return view;
    }

    public boolean validateUserInput(){
        // TODO Auto-generated method stub
        if( etCategoryName.getText().toString().trim().length() == 0 )
        {
            etCategoryName.setError( "Category name is required" );
            return false;
        }
        if( etCategoryDesc.getText().toString().trim().length() == 0 )
        {
            etCategoryDesc.setError( "Category Description is required" );
            return false;
        }
        return true;
    }

    @OnClick(R.id.add_button)
    public void addCategory(){

        if(validateUserInput()) {
            categoryName = etCategoryName.getText().toString();
            categoryDesc = etCategoryDesc.getText().toString();
        }

    }
}
