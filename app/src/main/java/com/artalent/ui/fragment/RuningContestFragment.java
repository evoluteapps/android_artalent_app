package com.artalent.ui.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.artalent.ArtalentApplication;
import com.artalent.R;
import com.artalent.Utils;
import com.artalent.ui.activity.AddSpecialContestActivity;
import com.artalent.ui.activity.NoInternetActivity;
import com.artalent.ui.adapter.SpecialContestAdapter;
import com.artalent.ui.model.BaseResponse;
import com.artalent.ui.model.SpecialContestObj;
import com.artalent.ui.model.SpecialContestResponse;
import com.artalent.ui.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RuningContestFragment extends BaseFragment implements View.OnClickListener {

    @BindView(R.id.pullToRefresh)
    SwipeRefreshLayout pullToRefresh;

    @BindView(R.id.recycler_view)
    RecyclerView advRecyclerView;

    @BindView(R.id.tvNoResult)
    TextView tvNoResult;

    private int offset = 0;
    List<SpecialContestObj> postObjs = new ArrayList<>();


    private LinearLayoutManager mLayoutManager;
    private boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    private SpecialContestAdapter activeAdvertiseAdapter;

    private int sType = 1;
    private View rootView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_active_advertise, container, false);
        ButterKnife.bind(this, rootView);
        tvNoResult.setVisibility(View.GONE);
        tvNoResult.setText(getString(R.string.no_special_yet));
        setAdapter();
        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refresh();
            }
        });
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        refresh();
    }

    public void setType(int id) {
        this.sType = id;
    }


    private void refresh() {
        postObjs.clear();
        offset = 0;
        callSpecialContestApi();
    }

    private void callSpecialContestApi() {
        if (Utils.isInternetAvailable(mActivity)) {
            if (offset == 0) {
                if (!pullToRefresh.isRefreshing()) {
                    showProgress(rootView);
                }
            }

            Call<SpecialContestResponse> call = ArtalentApplication.apiService.getAdminContest(ArtalentApplication.prefManager.getUserObj().getId(), sType, offset);
            call.enqueue(new Callback<SpecialContestResponse>() {
                @Override
                public void onResponse(Call<SpecialContestResponse> call, Response<SpecialContestResponse> response) {
                    hideProgress();
                    int statusCode = response.code();
                    pullToRefresh.setRefreshing(false);
                    loading = true;
                    if (response.body() != null && response.body().getObj() != null) {
                        offset = response.body().getIndex();
                        if (response.body().getObj().size() > 0) {
                            postObjs.addAll(response.body().getObj());
                        }
                        activeAdvertiseAdapter.notifyDataSetChanged();
                    }
                    if (postObjs.size() > 0) {
                        tvNoResult.setVisibility(View.GONE);
                    } else {
                        tvNoResult.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onFailure(Call<SpecialContestResponse> call, Throwable t) {
                    // Log error here since request failed
                    hideProgress();
                    loading = true;
                    Log.e("TAG", t.toString());
                    pullToRefresh.setRefreshing(false);
                }
            });
        } else {
            pullToRefresh.setRefreshing(false);
            Intent intent = new Intent(mActivity, NoInternetActivity.class);
            startActivityForResult(intent, Constants.NO_INTERNET);
        }

    }

    protected void setAdapter() {

        mLayoutManager = new LinearLayoutManager(mActivity);
        advRecyclerView.setLayoutManager(mLayoutManager);
        activeAdvertiseAdapter = new SpecialContestAdapter(postObjs, this);
        advRecyclerView.setAdapter(activeAdvertiseAdapter);
    }

    public void open(int sID) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
        alertDialogBuilder.setMessage(getString(R.string.are_you_sure_you_wanted));
        alertDialogBuilder.setPositiveButton("yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        arg0.dismiss();
                        deleteInterest(sID);
                    }
                });

        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void deleteInterest(int sID) {
        if (Utils.isInternetAvailable(mActivity)) {
            ArtalentApplication.apiService.deleteSpecialContest(ArtalentApplication.prefManager.getUserObj().getId(), sID)
                    .enqueue(new Callback<BaseResponse>() {
                        @Override
                        public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                            // callInterestApi();
                            refresh();
                        }

                        @Override
                        public void onFailure(Call<BaseResponse> call, Throwable t) {

                        }
                    });
        }
    }


    @Override
    public void onClick(View v) {
        SpecialContestObj specialContestObj;
        switch (v.getId()) {
            case R.id.btnDelete:
                specialContestObj = (SpecialContestObj) v.getTag();
                if (specialContestObj != null) {
                    open(specialContestObj.getAdminContestId());
                }
                break;
            case R.id.btnEditContest:
                specialContestObj = (SpecialContestObj) v.getTag();
                if (specialContestObj != null) {
                    Intent intent = new Intent(getContext(), AddSpecialContestActivity.class);
                    intent.putExtra(Constants.DATA, specialContestObj);
                    startActivity(intent);
                }
                break;
        }
    }
}
