package com.artalent.ui.fragment;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextSwitcher;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.artalent.ArtalentApplication;
import com.artalent.R;
import com.artalent.Utils;
import com.artalent.autoplayrecyclervideoplayer.view.CenterLayoutManager;
import com.artalent.ui.activity.NoInternetActivity;
import com.artalent.ui.activity.SpecialContestAllActivity;
import com.artalent.ui.activity.WinnerListActivity;
import com.artalent.ui.adapter.PagerSpecialContestAdapter;
import com.artalent.ui.model.BaseResponse;
import com.artalent.ui.model.PostAdminContestObj;
import com.artalent.ui.model.PostAdminContestResponse;
import com.artalent.ui.model.SpecialContestObj;
import com.artalent.ui.model.SpecialContestResponse;
import com.artalent.ui.model.VoteResponse;
import com.artalent.ui.model.request.PostReq;
import com.artalent.ui.utils.AutoScrollViewPager;
import com.artalent.ui.utils.Constants;
import com.google.android.material.tabs.TabLayout;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import im.ene.toro.CacheManager;
import im.ene.toro.widget.Container;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import com.artalent.exo.AdminContestPostAdapter;

@SuppressLint("SourceLockedOrientationActivity")
public class MySpecialContestPostFragment extends BaseFragment implements View.OnClickListener, AdminContestPostAdapter.OnFeedItemClickListener {

    private static final String TAG = "MySpecialContestPostFragment";

    public void setType(int id) {
        this.otherUserId = id;
    }

    private int otherUserId = 0;

    @BindView(R.id.winning_contest_recycler_view)
    RecyclerView winningContestRecyclerView;

    @BindView(R.id.winner_relative)
    RelativeLayout winnerShowAll;

    @BindView(R.id.winning_contest_relative)
    RelativeLayout winningContestRelative;

    @BindView(R.id.show_all_winner_btn)
    Button showAllWinner;

    @BindView(R.id.show_all_contest_btn)
    Button showAllContestBtn;

    @BindView(R.id.pullToRefresh)
    SwipeRefreshLayout pullToRefresh;

    @BindView(R.id.talent_label)
    TextView talentLabel;

    @BindView(R.id.vpSpecialContest)
    AutoScrollViewPager vpSpecialContest;

    @BindView(R.id.pagerPageIndicator)
    TabLayout pageIndicator;

    @BindView(R.id.rlSpecialContest)
    RelativeLayout rlSpecialContest;

    @BindView(R.id.contest_show_all_relative)
    RelativeLayout rlContestShowAll;

    @BindView(R.id.nested_parent)
    NestedScrollView nestedScrollView;

    @BindView(R.id.container)
    public Container container;

    @BindView(R.id.tvNoResultFound)
    TextView tvNoResult;


    private Context mContext;
    private boolean loading = true;
    private int offset = 0;

    private View view;

    private AdminContestPostAdapter mAdapter;
    private List<PostAdminContestObj> postObjs = new ArrayList<>();
    private List<SpecialContestObj> specialContests = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        Log.d(TAG, "onCreateView: called");

        view = inflater.inflate(R.layout.fragment_my_special_post, container, false);
        ButterKnife.bind(this, view);
        tvNoResult.setVisibility(View.GONE);
        tvNoResult.setText(getString(R.string.no_contest_post_yet));
        rlSpecialContest.setVisibility(View.GONE);
        rlContestShowAll.setVisibility(View.GONE);
        talentLabel.setVisibility(View.GONE);
        winnerShowAll.setVisibility(View.GONE);
        winningContestRelative.setVisibility(View.GONE);
        vpSpecialContest.stopAutoScroll();
        vpSpecialContest.setCycle(true);
        initView();
        callAllBackendAPI();
        initScrollListener();
        pullToRefresh.setOnRefreshListener(this::callAllBackendAPI);
        return view;
    }

    private void callAllBackendAPI() {
        Log.d(TAG, "callAllBackendAPI: called");

        if (Utils.isInternetAvailable(mContext)) {
            postObjs.clear();
            offset = 0;
            showProgress(view);
            callGetAllPostAPI();
        } else {
            Intent intent = new Intent(mContext, NoInternetActivity.class);
            startActivityForResult(intent, Constants.NO_INTERNET);
        }

    }


    private void initScrollListener() {
        Log.d(TAG, "initScrollListener: called");

        if (nestedScrollView != null) {
            nestedScrollView.setOnScrollChangeListener((NestedScrollView.OnScrollChangeListener) (v, scrollX, scrollY, oldScrollX, oldScrollY) -> {
                if (scrollY > oldScrollY) {
                }
                if (scrollY < oldScrollY) {
                }

                if (scrollY == 0) {
                }

                if (scrollY == (v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight())) {
                    if (loading) {
                        if (offset != -1) {
                            loading = false;
                            callGetAllPostAPI();
                        }
                    }
                }
            });
        }
    }


    private void setContestWinningAdapter() {
        winnerShowAll.setVisibility(View.GONE);
        winningContestRelative.setVisibility(View.GONE);
        // WinningContestListAdapter winningContestListAdapter = new WinningContestListAdapter();
        // winningContestRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        //winningContestRecyclerView.setAdapter(winningContestListAdapter);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.mContext = null;
    }

    private void callSpecialContestApi() {
        if (Utils.isInternetAvailable(mContext)) {
            Call<SpecialContestResponse> call = ArtalentApplication.apiService.getAdminContest(ArtalentApplication.prefManager.getUserObj().getId(), 1, 0);
            call.enqueue(new Callback<SpecialContestResponse>() {
                @Override
                public void onResponse(Call<SpecialContestResponse> call, Response<SpecialContestResponse> response) {
                    specialContests.clear();

                    if (response.body() != null && response.body().getObj() != null) {
                        if (response.body().getObj().size() > 0) {
                            specialContests.addAll(response.body().getObj());
                        }
                        setSpecialContest();
                    }
                }

                @Override
                public void onFailure(Call<SpecialContestResponse> call, Throwable t) {
                    hideProgress();
                }
            });
        } else {
            hideProgress();
            Intent intent = new Intent(getActivity(), NoInternetActivity.class);
            startActivityForResult(intent, Constants.NO_INTERNET);
        }

    }

    private void setSpecialContest() {
        Log.d(TAG, "setSpecialContest: called");

        Picasso picasso = Picasso.get();
        PagerSpecialContestAdapter myPagerAdapter = new PagerSpecialContestAdapter(picasso, specialContests, mActivity);
        vpSpecialContest.setClipToPadding(false);
        vpSpecialContest.setPadding(20, 0, 20, 0);
        vpSpecialContest.setPageMargin(50);
        vpSpecialContest.setAdapter(myPagerAdapter);
        pageIndicator.setupWithViewPager(vpSpecialContest);

        Log.d(TAG, "setSpecialContest: size -> " + specialContests.size());
        if (specialContests.size() > 0) {
            rlContestShowAll.setVisibility(View.VISIBLE);
            rlSpecialContest.setVisibility(View.VISIBLE);
        } else {
            rlContestShowAll.setVisibility(View.GONE);
            rlSpecialContest.setVisibility(View.GONE);
        }
    }

    private void initView() {
        Log.d(TAG, "initView: called");
        Log.d(TAG, "initView: postObj.size -> " + postObjs.size());
        int pos = -1;
        for (int i = 0; i < postObjs.size(); i++) {
            if (postObjs.get(i) == null) {
                pos = i;
            }
        }
        if (pos != -1) {
            postObjs.remove(pos);
        }
        postObjs.add(null);
        mAdapter = new AdminContestPostAdapter(mActivity, postObjs);
        mAdapter.setOnFeedItemClickListener(this);
        container.setLayoutManager(new CenterLayoutManager(mActivity));
        container.setAdapter(mAdapter);
        container.setCacheManager(CacheManager.DEFAULT);
    }


    private void callGetAllPostAPI() {
        Log.d(TAG, "callGetAllPostAPI: called");
        if (Utils.isInternetAvailable(mContext)) {
            Call<PostAdminContestResponse> call = ArtalentApplication.apiService.getAllAdminContestPost(ArtalentApplication.prefManager.getUserObj().getId(), offset, otherUserId);
            call.enqueue(new Callback<PostAdminContestResponse>() {
                @Override
                public void onResponse(Call<PostAdminContestResponse> call, Response<PostAdminContestResponse> response) {
                    int statusCode = response.code();
                    pullToRefresh.setRefreshing(false);
                    loading = true;
                    if (response.body() != null && response.body().getObj() != null) {
                        if (response.body().getObj().size() > 0) {
                            Log.d(TAG, "onResponse: getting data " + response.body().getObj().toString());
                            postObjs.addAll(response.body().getObj());
                        }

                        mAdapter.notifyDataSetChanged();
                        offset = response.body().getIndex();
                    }
                    Log.d(TAG, "onResponse: postObj.size -> " + postObjs.size());
                    if (postObjs.size() > 0) {
                        talentLabel.setVisibility(View.GONE);
                        tvNoResult.setVisibility(View.GONE);
                    } else {
                        tvNoResult.setVisibility(View.VISIBLE);
                        talentLabel.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onFailure(Call<PostAdminContestResponse> call, Throwable t) {
                    // Log error here since request failed
                    loading = true;
                    hideProgress();
                    pullToRefresh.setRefreshing(false);
                }
            });
        } else {
            hideProgress();
            pullToRefresh.setRefreshing(false);
        }

    }

    protected void setupFeed() {

    }


    @Override
    public void onCommentsClick(View v, int position) {

    }

    @Override
    public void onVolumeClick(ImageView btnVolume, int position) {

    }

    @Override
    public void onRegisterClick(int position) {

    }


    @Override
    public void onProfileClick(View v) {

    }

    @Override
    public void onLikeClick(ImageView btnLike, int position, TextSwitcher tsLikesCounter) {

        PostAdminContestObj postObj = postObjs.get(position);
        if (postObj != null && postObj.getContestPostId() > 0) {
            callVoteApi(btnLike, position, postObj, tsLikesCounter);
        }
    }


    private void callVoteApi(ImageView btnLike, int position, PostAdminContestObj postObj, TextSwitcher tsLikesCounter) {
        if (Utils.isInternetAvailable(mContext)) {
            PostReq postReq = new PostReq();
            postReq.setPostId(postObj.getContestPostId());
            Call<VoteResponse> call = ArtalentApplication.apiService.callVoteAdmin(ArtalentApplication.prefManager.getUserObj().getId(), postReq);
            call.enqueue(new Callback<VoteResponse>() {
                @Override
                public void onResponse(Call<VoteResponse> call, Response<VoteResponse> response) {
                    int statusCode = response.code();
                    if (response.body() != null) {
                        if (response.body().getObj() != null && !TextUtils.isEmpty(response.body().getObj())) {
                            btnLike.setImageResource(response.body().getObj().equals("0") ? R.drawable.ic_voted_grey_outline : R.drawable.voted_colored);
                            postObj.setIsVoted(response.body().getObj());
                            if (response.body().getObj().equals("0")) {
                                postObj.setVoteCount(postObj.getVoteCount() - 1);
                            } else {
                                postObj.setVoteCount(postObj.getVoteCount() + 1);
                            }
                            tsLikesCounter.setCurrentText(getResources().getQuantityString(
                                    R.plurals.likes_count, postObj.getVoteCount(), postObj.getVoteCount()
                            ));
                        }
                        Utils.showToast(getActivity(), response.body().getMessage());
                    }
                }

                @Override
                public void onFailure(Call<VoteResponse> call, Throwable t) {
                    // Log error here since request failed
                }
            });
        }
    }

    @OnClick(R.id.show_all_winner_btn)
    void showAllWinner() {
        Intent intent = new Intent(mContext, WinnerListActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.show_all_contest_btn)
    void showAllContestBtn() {
        Intent intent = new Intent(mContext, SpecialContestAllActivity.class);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onDeleteClick(@NotNull PostAdminContestObj postContestObj, int position) {
        open(postContestObj, position);
    }

    public void open(PostAdminContestObj postObj, int position) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setMessage("Are you sure, You wanted to delete Contest post?");
        alertDialogBuilder.setPositiveButton("yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        arg0.dismiss();
                        deleteInterest(postObj, position);
                    }
                });

        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void deleteInterest(PostAdminContestObj postObj, int position) {
        if (Utils.isInternetAvailable(mActivity)) {
            ArtalentApplication.apiService.deleteAdminContestPost(ArtalentApplication.prefManager.getUserObj().getId(), postObj.getContestPostId())
                    .enqueue(new Callback<BaseResponse>() {
                        @Override
                        public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                            //callInterestApi();
                            if (response.body() != null) {
                                postObjs.remove(position);
                                mAdapter.notifyDataSetChanged();
                            }
                        }

                        @Override
                        public void onFailure(Call<BaseResponse> call, Throwable t) {

                        }
                    });
        }
    }
}
