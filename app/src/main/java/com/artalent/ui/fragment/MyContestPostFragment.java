package com.artalent.ui.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextSwitcher;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.artalent.ArtalentApplication;
import com.artalent.R;
import com.artalent.Utils;
import com.artalent.autoplayrecyclervideoplayer.view.CenterLayoutManager;
import com.artalent.fcm.Config;
import com.artalent.ui.activity.NoInternetActivity;
import com.artalent.ui.model.PostContestObj;
import com.artalent.ui.model.PostContestResponse;
import com.artalent.ui.utils.Constants;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import im.ene.toro.CacheManager;
import im.ene.toro.widget.Container;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import com.artalent.exo.NewContestPostAdapter;

@SuppressLint("SourceLockedOrientationActivity")
public class MyContestPostFragment extends BaseFragment implements View.OnClickListener, NewContestPostAdapter.OnFeedItemClickListener {


    private boolean loading = true;
    private int offset = 0;
    private List<PostContestObj> postObjs = new ArrayList<>();

    @BindView(R.id.pullToRefresh)
    SwipeRefreshLayout pullToRefresh;

    @BindView(R.id.tvNoResult)
    TextView tvNoResult;

    @BindView(R.id.container)
    public Container container;


    private int otherUserId;


    public MyContestPostFragment() {
    }

    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private NewContestPostAdapter mAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my_contest_post, container, false);
        ButterKnife.bind(this, view);

        tvNoResult.setVisibility(View.GONE);
        tvNoResult.setText(getString(R.string.no_contest_post_yet));
        initView();
        initScrollListener();
        pullToRefresh.setOnRefreshListener(this::refresh);


        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
/*
                if (intent.getAction().equals(Config.STOP_VIDEO)) {
                    // new push notification is received
                }
*/
            }
        };

        LocalBroadcastManager.getInstance(mActivity).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.STOP_VIDEO));

        return view;
    }


    private void initScrollListener() {
        container.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (!recyclerView.canScrollVertically(1) && newState == RecyclerView.SCROLL_STATE_IDLE) {
                    if (loading) {
                        if (offset != -1) {
                            loading = false;
                            callHomePostApi();
                        }
                    }
                }
            }
        });

    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }


    private void refresh() {
        postObjs.clear();
        offset = 0;
        callHomePostApi();
    }

    private void initView() {
        int pos = -1;
        for (int i = 0; i < postObjs.size(); i++) {
            if (postObjs.get(i) == null) {
                pos = i;
            }
        }
        if (pos != -1) {
            postObjs.remove(pos);
        }
        postObjs.add(null);
        mAdapter = new NewContestPostAdapter(mActivity, postObjs);
        mAdapter.setOnFeedItemClickListener(this);
        container.setLayoutManager(new CenterLayoutManager(mActivity));
        container.setAdapter(mAdapter);
        container.setCacheManager(CacheManager.DEFAULT);
    }

    private void callHomePostApi() {
        if (Utils.isInternetAvailable(mActivity)) {
            Call<PostContestResponse> call = ArtalentApplication.apiService.getMyContestPost(ArtalentApplication.prefManager.getUserObj().getId(), offset, otherUserId);
            call.enqueue(new Callback<PostContestResponse>() {
                @Override
                public void onResponse(Call<PostContestResponse> call, Response<PostContestResponse> response) {
//                    int statusCode = response.code();
                    pullToRefresh.setRefreshing(false);
                    loading = true;
                    if (response.body() != null && response.body().getObj() != null) {
                        if (response.body().getObj().size() > 0) {
                            postObjs.addAll(response.body().getObj());
                        }

                        mAdapter.notifyDataSetChanged();
                        offset = response.body().getIndex();
                    }
                    if (postObjs.size() > 0) {
                        tvNoResult.setVisibility(View.GONE);
                    } else {
                        tvNoResult.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onFailure(Call<PostContestResponse> call, Throwable t) {
                    // Log error here since request failed
                    loading = true;
                    pullToRefresh.setRefreshing(false);
                }
            });
        } else {
            pullToRefresh.setRefreshing(false);
            Intent intent = new Intent(mActivity, NoInternetActivity.class);
            startActivityForResult(intent, Constants.NO_INTERNET);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == Constants.NO_INTERNET) {
            if (resultCode == Activity.RESULT_OK) {
                callHomePostApi();
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        //add this code to pause videos (when app is minimised or paused)
        LocalBroadcastManager.getInstance(mActivity).unregisterReceiver(mRegistrationBroadcastReceiver);
    }

    public void setType(int id) {
        this.otherUserId = id;
    }

    @Override
    public void onCommentsClick(View v, int position) {

    }

    @Override
    public void onVolumeClick(ImageView btnVolume, int position) {

    }

    @Override
    public void onRegisterClick(int position) {

    }

    @Override
    public void onProfileClick(View v) {

    }

    @Override
    public void onLikeClick(ImageView btnLike, int position, TextSwitcher tsLikesCounter) {

    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onDeleteClick(@NotNull PostContestObj postContestObj, int position) {

    }
}
