package com.artalent.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.artalent.R
import com.artalent.ui.adapter.DraftVideoFilterAdapter
import com.otaliastudios.cameraview.filter.Filters

class DraftEditFragment : Fragment() {

    lateinit var draftFilterPreviewRv: RecyclerView
    private val mAllFilters: Array<Filters> = Filters.values()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_draft_edit, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        draftFilterPreviewRv = view.findViewById(R.id.draft_filter_preview_recyclerview)

        val draftVideoFilterAdapter = DraftVideoFilterAdapter(mAllFilters)
        draftFilterPreviewRv.adapter = draftVideoFilterAdapter
    }
}