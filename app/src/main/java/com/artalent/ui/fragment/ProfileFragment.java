package com.artalent.ui.fragment;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.artalent.ArtalentApplication;
import com.artalent.R;
import com.artalent.Utils;
import com.artalent.fcm.Config;
import com.artalent.ui.activity.AppCameraActivity;
import com.artalent.ui.activity.EditProfileActivity;
import com.artalent.ui.activity.InterestUserListActivity;
import com.artalent.ui.activity.LevelInfoActivity;
import com.artalent.ui.activity.MyConPaActivity;
import com.artalent.ui.activity.MyFollowerUserActivity;
import com.artalent.ui.activity.MyFollowingUserActivity;
import com.artalent.ui.activity.NoInternetActivity;
import com.artalent.ui.activity.PostCommentActivity;
import com.artalent.ui.adapter.MyPostAdapter;
import com.artalent.ui.model.BaseResponse;
import com.artalent.ui.model.InterestObj;
import com.artalent.ui.model.PostObj;
import com.artalent.ui.model.PostResponse;
import com.artalent.ui.model.StoryObj;
import com.artalent.ui.model.StoryResponse;
import com.artalent.ui.model.UserObj;
import com.artalent.ui.model.UserResponse;
import com.artalent.ui.model.request.FollowingReq;
import com.artalent.ui.utils.CircleTransformation;
import com.artalent.ui.utils.Constants;
import com.artalent.ui.utils.ImageQuality;
import com.artalent.ui.utils.Options;
import com.artalent.ui.video.StoryViewActivity;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import pl.pawelkleczkowski.customgauge.CustomGauge;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.artalent.ui.fragment.HomeFragment.REQUEST_COUNTRY;


public class ProfileFragment extends BaseFragment implements View.OnClickListener, MyPostAdapter.OnProfileDataChangedListener {

    private StoryObj currentStoryObj = null;
    private UserObj currentProfileUser = null;

    private GridLayoutManager mLayoutManager;

    @BindView(R.id.recyclerViewMyPost)
    RecyclerView recyclerViewMyPost;

    private MyPostAdapter feedAdapter;
    List<PostObj> postObjs = new ArrayList<>();
    private int offset = 0;

    @BindView(R.id.user__fullName)
    TextView userFullName;

    @BindView(R.id.user_name)
    TextView userName;

    @BindView(R.id.user_level)
    TextView userLevel;

    @BindView(R.id.btnFollow)
    Button btnFollow;

    @BindView(R.id.btnEditProfile)
    Button btnEditProfile;

    @BindView(R.id.total_post)
    TextView totalPost;

    @BindView(R.id.total_contest)
    TextView totalContest;

    @BindView(R.id.total_followers)
    TextView totalFollowers;

    @BindView(R.id.total_following)
    TextView totalFollowing;

    @BindView(R.id.following_title_txt)
    TextView followingTitleTxt;

    @BindView(R.id.appBarLayout)
    NestedScrollView scroller;

    @BindView(R.id.contest_title_txt)
    TextView contestTitleTxt;

    @BindView(R.id.ivUserProfilePhoto)
    CircleImageView ivUserProfilePhoto;

    @BindView(R.id.gauge1)
    CustomGauge userLevelGauge;

    @BindView(R.id.level_tv)
    TextView levelTV;

    @BindView(R.id.follower_count_tv)
    TextView followerCountTV;

    @BindView(R.id.user_interests)
    TextView userInterest;

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    private int selectedUserId = 0;
    private static final String TAG = "ProfileFragment";

    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_new_user_profile, container, false);
        ButterKnife.bind(this, view);
        initScrollListener();
        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        new Handler().postDelayed(() -> {
            Utils.updateToolbar(Objects.requireNonNull(getActivity()), R.color.white, true);
            Log.d(TAG, "setUserVisibleHint: called");
//            setUserData(getUserDetails());//1
            currentProfileUser = getUserDetails();
            initUserData();
        }, 100);
    }

    public void setUserData(UserObj userObj) {
        currentProfileUser = userObj;
        Log.d(TAG, "setUserData: called");
        if (userObj != null) {

            // Setting boundary in profile image
            checkUserHaveStoriesOrNot();
            if (currentStoryObj != null) {
                ivUserProfilePhoto.setBorderColor(ContextCompat.getColor(requireContext(), R.color.black));
                ivUserProfilePhoto.setBorderWidth(3);
            } else {
                ivUserProfilePhoto.setBorderColor(ContextCompat.getColor(requireContext(), R.color.white));
                ivUserProfilePhoto.setBorderWidth(0);
            }


            if (!TextUtils.isEmpty(userObj.getProfileImage())) {
                try {
                    Picasso.get()
                            .load(userObj.getProfileImage())
                            .placeholder(R.drawable.man)
                            .resize(getResources().getDimensionPixelSize(R.dimen.dimen_90), getResources().getDimensionPixelSize(R.dimen.dimen_90))
                            .centerInside()
                            .transform(new CircleTransformation())
                            .into(ivUserProfilePhoto);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            String fullName = userObj.getFirstName() + " " + userObj.getLastName();
            if (userObj.getInterest().size() > 0) {
                List<InterestObj> interestObjList = userObj.getInterest();
                StringBuilder stringBuilder = new StringBuilder();
                if (interestObjList.size() > 2) {
                    for (int i = 0; i < 2; i++) {
                        if (i == 0) {
                            stringBuilder.append(interestObjList.get(i).getInterest());
                        } else {
                            stringBuilder.append(", ").append(interestObjList.get(i).getInterest());
                        }
                    }
                    stringBuilder.append(" +" + (interestObjList.size() - 2));
                } else {
                    for (int i = 0; i < interestObjList.size(); i++) {
                        if (i == 0) {
                            stringBuilder.append(interestObjList.get(i).getInterest());
                        } else {
                            stringBuilder.append(", ").append(interestObjList.get(i).getInterest());
                        }
                    }
                }

                userInterest.setText(stringBuilder);
                Log.d(TAG, "setUserData: Set tag before : " + userObj.toString());
                userInterest.setTag(userObj);
            }

//            Typeface phenomenaLightTypeface = Typeface.createFromAsset(
//                    requireContext().getAssets(),
//                    "phenomena_light.ttf");
            userName.setTypeface(ResourcesCompat.getFont(requireContext(), R.font.phenomena_light));
//
//            Typeface phenomenaBoldTypeface = Typeface.createFromAsset(
//                    getActivity().getAssets(),
//                    "phenomena_bold.ttf");
            userInterest.setTypeface(ResourcesCompat.getFont(requireContext(), R.font.phenomena_bold));

            userFullName.setText(fullName);
            userName.setText(userObj.getUserName());
            userLevel.setText(userObj.getLevel());
            totalPost.setText("" + userObj.getPostCount());
            totalFollowers.setText("" + userObj.getFollowerCount());
            totalFollowing.setText("" + userObj.getFollowingCount());
            totalContest.setText("" + userObj.getContestPostCount());
            if (userObj.getFollowingCount() > 1)
                followingTitleTxt.setText("Followings");
            else
                followingTitleTxt.setText("Following");
            if (userObj.getContestPostCount() > 1)
                contestTitleTxt.setText("Contests");
            else
                contestTitleTxt.setText("Contest");

            if (userObj.getIsFollowed() == 1) {
                btnFollow.setText(mActivity.getString(R.string.unfollow));
            } else {
                btnFollow.setText(mActivity.getString(R.string.follow));
            }

            userLevelGauge.setValue(userObj.getFollowerCount());
            levelTV.setText(userObj.getLevel());
            followerCountTV.setText(String.valueOf(userObj.getFollowerCount()));
            btnFollow.setTag(userObj);
            selectedUserId = userObj.getId();
            setViewPager(userObj.getId());

        }
    }

    // for instagram story boundary
    private void checkUserHaveStoriesOrNot() {
        Log.d(TAG, "checkUserHaveStoriesOrNot: called");
        if (Utils.isInternetAvailable(mActivity)) {
            currentStoryObj = null;
            Call<StoryResponse> call = ArtalentApplication.apiService.getStory(ArtalentApplication.prefManager.getUserObj().getId());
            call.enqueue(new Callback<StoryResponse>() {
                @Override
                public void onResponse(Call<StoryResponse> call, Response<StoryResponse> response) {
                    if (response.body() != null && response.body().getObj() != null) {
                        Log.d(TAG, "onResponse: getStory :- " + response.body().toString());
                        if (response.body().getObj().size() > 0) {
                            response.body().getObj().forEach(it -> mapingForStory(it));
//                            currentStoryObj = response.body().getObj().get(0);
                        }
                    }
                }

                @Override
                public void onFailure(Call<StoryResponse> call, Throwable t) {
                    Utils.showToastWithTitle(getActivity(), "Try Again", "Alert Message");
                }
            });
        }
    }

    private void mapingForStory(StoryObj it) {
        Log.d(TAG, "mapingForStory: called");
        if (it.getUserId() == currentProfileUser.getId()) {
            currentStoryObj = it;
        }
        if (currentStoryObj != null) {
            ivUserProfilePhoto.setBorderColor(ContextCompat.getColor(requireContext(), R.color.black));
            ivUserProfilePhoto.setBorderWidth(3);
        } else {
            ivUserProfilePhoto.setBorderColor(ContextCompat.getColor(requireContext(), R.color.white));
            ivUserProfilePhoto.setBorderWidth(0);
        }
    }
    // end of instagram story boundary


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        if (requestCode == REQUEST_COUNTRY) {
            if (resultCode == Activity.RESULT_OK) {
                boolean isStoryDeleted = data.getBooleanExtra(Constants.IS_STORY_DELETED, false);
                Log.d(TAG, "onActivityResult: [ProfileFragment] IS_STORY_DELETED - " + isStoryDeleted);

                if (isStoryDeleted) {
                    Utils.showToast(requireActivity(), "Story deleted successfully");
                    checkUserHaveStoriesOrNot();
                    HomeFragment.doYouWantStoryRefresh = true;
                    return;
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void stopVideo() {
       /* if (contestPostFragment != null) {
            if (contestPostFragment.listFeed.getHandingVideoHolder() != null)
                contestPostFragment.listFeed.getHandingVideoHolder().stopVideo();
        }*/
    }

    @OnClick(R.id.llFollowing)
    public void onFollowingClick() {
        MyFollowingUserActivity.startActivity(getActivity(), selectedUserId);
    }

    @OnClick(R.id.llContest)
    public void onContestClick() {
        MyConPaActivity.startActivity(getActivity(), selectedUserId);
    }

    @OnClick(R.id.user_interests)
    public void onUserInterestsClick() {
        UserObj userObj = (UserObj) userInterest.getTag();
        if (userObj != null) {
            Intent intent = new Intent(mActivity, InterestUserListActivity.class);
            Log.d(TAG, "onUserInterestsClick: " + userObj.getInterest().toString());
            intent.putExtra(Constants.INTEREST_LIST, (Serializable) userObj.getInterest());
            startActivityForResult(intent, 0);
        }
    }

    @OnClick(R.id.llFollower)
    void onFollowerClick() {
        MyFollowerUserActivity.startActivity(getActivity(), selectedUserId);
    }

    @OnClick(R.id.btnFollow)
    void onFollowClick() {
        UserObj userObj = (UserObj) btnFollow.getTag();
        if (userObj != null) {
            if (userObj.getIsFollowed() == 1) {
                postFollowDelete(userObj);
            } else {
                postFollow(userObj);
            }
        }
    }

    private FollowingReq fillDataFollow(int id) {
        FollowingReq createReq = new FollowingReq();
        createReq.setFollowingId(id);
        return createReq;
    }

    private void postFollow(UserObj userObj) {
        if (Utils.isInternetAvailable(mActivity)) {
            ArtalentApplication.apiService.postFollow(ArtalentApplication.prefManager.getUserObj().getId(), fillDataFollow(userObj.getId()))
                    .enqueue(new Callback<BaseResponse>() {
                        @Override
                        public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                            int statusCode = response.code();
                            if (response.body() != null) {
                                btnFollow.setText(mActivity.getString(R.string.unfollow));
                                UserObj userObj = (UserObj) btnFollow.getTag();
                                userObj.setIsFollowed(1);
                                btnFollow.setTag(userObj);
                            }
                        }

                        @Override
                        public void onFailure(Call<BaseResponse> call, Throwable t) {
                            // Log error here since request failed
                        }
                    });
        } else {
            Utils.showToast(getActivity(), mActivity.getResources().getString(R.string.please_check_your_internet));
        }
    }

    private void postFollowDelete(UserObj userObj) {
        if (Utils.isInternetAvailable(mActivity)) {
            ArtalentApplication.apiService.postFollowDelete(ArtalentApplication.prefManager.getUserObj().getId(), (userObj.getId()))
                    .enqueue(new Callback<BaseResponse>() {
                        @Override
                        public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                            int statusCode = response.code();
                            if (response.body() != null) {
                                btnFollow.setText(mActivity.getString(R.string.follow));
                                UserObj userObj = (UserObj) btnFollow.getTag();
                                userObj.setIsFollowed(0);
                                btnFollow.setTag(userObj);
                            }
                        }

                        @Override
                        public void onFailure(Call<BaseResponse> call, Throwable t) {
                            // Log error here since request failed
                        }
                    });
        } else {
            Utils.showToast(getActivity(), mActivity.getResources().getString(R.string.please_check_your_internet));
        }
    }

    @OnClick(R.id.btnEditProfile)
    void onEditProfileClick() {
        startActivity(new Intent(getActivity(), EditProfileActivity.class));
    }

    @OnClick(R.id.gauge1)
    void onLevelInfoClick() {
        startActivity(new Intent(getActivity(), LevelInfoActivity.class));
    }

    @OnClick(R.id.ivUserProfilePhoto)
    void onUserProfilePhotoClick() {
        getCurrentUsersStories();
    }

    private void getCurrentUsersStories() {

        if (Utils.isInternetAvailable(mActivity)) {
            currentStoryObj = null;
            Call<StoryResponse> call = ArtalentApplication.apiService.getStory(ArtalentApplication.prefManager.getUserObj().getId());
            call.enqueue(new Callback<StoryResponse>() {
                @Override
                public void onResponse(Call<StoryResponse> call, Response<StoryResponse> response) {
                    if (response.body() != null && response.body().getObj() != null) {
                        Log.d(TAG, "onResponse: getStory :- " + response.body().toString());
                        if (response.body().getObj().size() > 0) {
                            response.body().getObj().forEach(it -> maping(it));
//                            currentStoryObj = response.body().getObj().get(0);
                        }
                    }
                    openStoryOrCamera();
                }

                @Override
                public void onFailure(Call<StoryResponse> call, Throwable t) {
                    Utils.showToastWithTitle(getActivity(), "Try Again", "Alert Message");
                }
            });
        }
//        currentStoryObj = new StoryObj();
//        openStoryOrCamera();
    }

    private void maping(StoryObj it) {
        if (it.getUserId() == currentProfileUser.getId()) {
            currentStoryObj = it;
        }
    }

    private void openStoryOrCamera() {
        if (currentStoryObj != null && currentStoryObj.getStories() != null && currentStoryObj.getStories().size() > 0) {
            Log.d(TAG, "openStoryOrCamera: current story obj : " + currentStoryObj.toString());
            Intent intent = new Intent(getActivity(), StoryViewActivity.class);
            intent.putExtra(Constants.INTEREST_LIST, (Serializable) currentStoryObj.getStories());
            intent.putExtra(Constants.USER_NAME, currentStoryObj.getOwner().getUserName());
            intent.putExtra(Constants.PROFILE_URL, currentStoryObj.getOwner().getProfileImage());
            intent.putExtra(Constants.PROFILE_ID_FOR_STORY, currentStoryObj.getOwner().getId());
            int nextPosition = -1;
            intent.putExtra(Constants.POSITION, nextPosition);
            startActivityForResult(intent, REQUEST_COUNTRY);
        } else {
            if (currentProfileUser.getId() == getUserDetails().getId()) {
                Options options = Options.init()
                        .setRequestCode(100)
                        .setCount(1)
                        .setFrontfacing(false)
                        .setImageQuality(ImageQuality.LOW)
                        .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)
                        .setPath("/akshay/new");
                AppCameraActivity.start((FragmentActivity) mActivity, options, 1);
            } else {
                Utils.showToastWithTitle(getActivity(), "No stories found!", "Alert Message");
            }
        }
    }

    public UserObj getUserDetails() {
//        Log.d(TAG, "getUserDetails: called");
        return ArtalentApplication.prefManager.getUserObj();
    }

    private void initUserData() {
        Log.d(TAG, "initUserData: called");
        if (currentProfileUser != null)
            Log.d(TAG, "initUserData: currentProfileUserId -> " + currentProfileUser.getId());
        else
            Log.d(TAG, "initUserData: currentProfileUserId -> null");
        Log.d(TAG, "initUserData: LoggedInProfileUserId -> " + getUserDetails().getId());


        if (currentProfileUser != null && currentProfileUser.getId() == getUserDetails().getId()) {

            if (Utils.isInternetAvailable(mActivity)) {

                ArtalentApplication.apiService.getOtherUserDetails(ArtalentApplication.prefManager.getUserObj().getId()).enqueue(new Callback<UserResponse>() {
                    @Override
                    public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                        int statusCode = response.code();
                        if (response.body() != null && response.body().getObj() != null) {
                            UserObj userObj = response.body().getObj();
                            ArtalentApplication.prefManager.setUserObj(userObj);
                            setUserData(userObj);
                        }
                    }

                    @Override
                    public void onFailure(Call<UserResponse> call, Throwable t) {
                        // Log error here since request failed
                        Log.e("TAG", t.toString());
                        setUserData(getUserDetails());
                    }
                });
            } else {
                setUserData(getUserDetails());
            }
        }
    }

    @Override
    public void onProfileDataChanged() {
        Log.d(TAG, "onProfileDataChanged: called");
        initUserData();
    }

    private void setViewPager(int id) {
        if (id == ArtalentApplication.prefManager.getUserObj().getId()) {
            btnEditProfile.setVisibility(View.VISIBLE);
            btnFollow.setVisibility(View.GONE);
        } else {
            btnEditProfile.setVisibility(View.GONE);
            btnFollow.setVisibility(View.VISIBLE);
        }
        mLayoutManager = new GridLayoutManager(getContext(), 3);
        recyclerViewMyPost.setLayoutManager(mLayoutManager);
        feedAdapter = new MyPostAdapter(getActivity(), postObjs, this);
        feedAdapter.setOnProfileDataChangedListener(this::onProfileDataChanged);
        //feedAdapter.setOnFeedItemClickListener(this);
        Log.d(TAG, "setViewPager: PostObjs: " + postObjs.toString());
        recyclerViewMyPost.setAdapter(feedAdapter);
        refresh();

        new Handler().postDelayed(() -> {
            Intent pushNotification = new Intent(Config.STOP_VIDEO);
            pushNotification.putExtra("message", "message");
            LocalBroadcastManager.getInstance(mActivity).sendBroadcast(pushNotification);
        }, 3000);

    }

    private void refresh() {
        if (selectedUserId > 0) {
            progressBar.setVisibility(View.VISIBLE);

            postObjs.clear();
            offset = 0;
            callHomePostApi();
        }
    }


    @Override
    public void onResume() {
        Log.d(TAG, "onResume: called");
        super.onResume();
        initUserData();
        refresh();
    }

    @Override
    public void onPause() {
        Log.d(TAG, "onPause: called");
        super.onPause();
    }

    private void initScrollListener() {


        if (scroller != null) {

            scroller.setOnScrollChangeListener((NestedScrollView.OnScrollChangeListener) (v, scrollX, scrollY, oldScrollX, oldScrollY) -> {

                if (scrollY > oldScrollY) {
                    Log.i("daw_ss", "Scroll DOWN");
                }
                if (scrollY < oldScrollY) {
                    Log.i("daw_ss", "Scroll UP");
                }

                if (scrollY == 0) {
                    Log.i("daw_ss", "TOP SCROLL");
                }

                if (scrollY == v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight()) {
                    Log.i("daw_ss", "BOTTOM SCROLL");
                    if (loading) {
                        if (offset != -1) {
                            loading = false;
                            Log.e("mLayoutManager", "Last Item Wow !");
                            callHomePostApi();
                        }
                    }
                }
            });
        }

    }


    private boolean loading = true;

    private void callHomePostApi() {
        if (Utils.isInternetAvailable(mActivity)) {
            Log.e("selectedUserId", "selectedUserId : " + selectedUserId);
            Call<PostResponse> call = ArtalentApplication.apiService.getMyPost(ArtalentApplication.prefManager.getUserObj().getId(), offset, selectedUserId);
            call.enqueue(new Callback<PostResponse>() {
                @Override
                public void onResponse(Call<PostResponse> call, Response<PostResponse> response) {
                    int statusCode = response.code();
                    loading = true;
                    if (offset == 0) {
                        postObjs.clear();
                        feedAdapter.notifyDataSetChanged();
                        progressBar.setVisibility(View.GONE);
                    }
                    if (response.body() != null && response.body().getObj() != null) {
                        if (response.body().getObj().size() > 0) {
                            Log.d(TAG, "onResponse: Direct api response - "+ response.body().toString());
                            Log.d(TAG, "onResponse: Direct api response length - "+ response.body().getObj().size());

                            for (PostObj postObj : response.body().getObj()) {
                                boolean check = false;
                                for (PostObj postObj1 : postObjs) {
                                    if (postObj1.getPostId() == postObj.getPostId()) {
                                        check = true;
                                    }
                                }
                                if (!check) {
                                    postObjs.add(postObj);
                                }
                            }
                        }
                        offset = response.body().getIndex();
                        feedAdapter.notifyDataSetChanged();
                        progressBar.setVisibility(View.GONE);
                    }

                }

                @Override
                public void onFailure(Call<PostResponse> call, Throwable t) {
                    // Log error here since request failed
                    loading = true;
                    Log.e("TAG", t.toString());
                }
            });
        } else {
            Intent intent = new Intent(mActivity, NoInternetActivity.class);
            startActivityForResult(intent, Constants.NO_INTERNET);
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.thumbnail) {
            PostObj postObj = (PostObj) v.getTag();
            if (postObj != null) {
                PostCommentActivity.startActivity(getActivity(), postObj);
            }
        }
    }
}
