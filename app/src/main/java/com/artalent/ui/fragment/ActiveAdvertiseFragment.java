package com.artalent.ui.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.artalent.ArtalentApplication;
import com.artalent.R;
import com.artalent.Utils;
import com.artalent.ui.activity.NoInternetActivity;
import com.artalent.ui.adapter.ActiveAdvertiseAdapter;
import com.artalent.ui.model.AdvertiseObj;
import com.artalent.ui.model.AdvertiseResponse;
import com.artalent.ui.model.BaseResponse;
import com.artalent.ui.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActiveAdvertiseFragment extends BaseFragment implements View.OnClickListener {

    @BindView(R.id.pullToRefresh)
    SwipeRefreshLayout pullToRefresh;

    @BindView(R.id.recycler_view)
    RecyclerView advRecyclerView;

    private int offset = 0;
    List<AdvertiseObj> postObjs = new ArrayList<>();
    private View rootView;

    private LinearLayoutManager mLayoutManager;
    private boolean loading = true;

    int pastVisiblesItems, visibleItemCount, totalItemCount;
    private ActiveAdvertiseAdapter activeAdvertiseAdapter;

    @BindView(R.id.tvNoResult)
    TextView tvNoResult;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_active_advertise, container, false);
        ButterKnife.bind(this, rootView);
        tvNoResult.setVisibility(View.GONE);
        tvNoResult.setText(getString(R.string.no_advertise_yet));
        pullToRefresh.setOnRefreshListener(this::refresh);
        setContestWinningAdapter();
        refresh();
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        refresh();
    }

    private void refresh() {
        postObjs.clear();
        offset = 0;
        callHomePostApi();
    }

    private void callHomePostApi() {
        if (Utils.isInternetAvailable(mActivity)) {
            if (offset == 0) {
                if (!pullToRefresh.isRefreshing()) {
                    showProgress(rootView);
                }
            }

            Call<AdvertiseResponse> call = ArtalentApplication.apiService.getAllAdvertise(ArtalentApplication.prefManager.getUserObj().getId(), offset);
            call.enqueue(new Callback<AdvertiseResponse>() {
                @Override
                public void onResponse(Call<AdvertiseResponse> call, Response<AdvertiseResponse> response) {
                    hideProgress();
                    int statusCode = response.code();
                    pullToRefresh.setRefreshing(false);
                    loading = true;
                    if (response.body() != null && response.body().getObj() != null) {
                        offset = response.body().getIndex();
                        if (response.body().getObj().size() > 0) {
                            for (AdvertiseObj advertiseObj : response.body().getObj()) {
                                boolean check = false;
                                for (AdvertiseObj advertiseObj1 : postObjs) {
                                    if (advertiseObj1.getId() == advertiseObj.getId()) {
                                        check = true;
                                    }
                                }
                                if (!check) {
                                    postObjs.add(advertiseObj);
                                }

                            }

                        }
                        activeAdvertiseAdapter.notifyDataSetChanged();
                    }
                    if (postObjs.size() > 0) {
                        tvNoResult.setVisibility(View.GONE);
                    } else {
                        tvNoResult.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onFailure(Call<AdvertiseResponse> call, Throwable t) {
                    // Log error here since request failed
                    hideProgress();
                    loading = true;
                    Log.e("TAG", t.toString());
                    pullToRefresh.setRefreshing(false);
                }
            });
        } else {
            pullToRefresh.setRefreshing(false);
            Intent intent = new Intent(mActivity, NoInternetActivity.class);
            startActivityForResult(intent, Constants.NO_INTERNET);
        }

    }

    protected void setContestWinningAdapter() {
        mLayoutManager = new LinearLayoutManager(mActivity);
        advRecyclerView.setLayoutManager(mLayoutManager);
        activeAdvertiseAdapter = new ActiveAdvertiseAdapter(postObjs, this, getContext());
        advRecyclerView.setAdapter(activeAdvertiseAdapter);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivDelete:
                int position = (int) v.getTag();
                if (postObjs.size() > position) {
                    AdvertiseObj advertiseObj = postObjs.get(position);
                    if (advertiseObj != null && advertiseObj.getId() > 0) {
                        open(advertiseObj, position);
                    }
                }
                break;
            case R.id.btnCompanyURL: {

            }
        }
    }

    public void open(AdvertiseObj postObj, int position) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setMessage("Are you sure, You want to delete Advertise?");
        alertDialogBuilder.setPositiveButton("yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        arg0.dismiss();
                        deleteInterest(postObj, position);
                    }
                });

        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void deleteInterest(AdvertiseObj postObj, int position) {
        if (Utils.isInternetAvailable(mActivity)) {
            ArtalentApplication.apiService.deleteAdvertise(ArtalentApplication.prefManager.getUserObj().getId(), postObj.getId())
                    .enqueue(new Callback<BaseResponse>() {
                        @Override
                        public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                            //callInterestApi();
                            if (response.body() != null) {
                                postObjs.remove(position);
                                activeAdvertiseAdapter.notifyDataSetChanged();
                            }
                        }

                        @Override
                        public void onFailure(Call<BaseResponse> call, Throwable t) {

                        }
                    });
        }
    }
}
