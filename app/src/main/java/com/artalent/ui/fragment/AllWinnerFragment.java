package com.artalent.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.artalent.ArtalentApplication;
import com.artalent.R;
import com.artalent.Utils;
import com.artalent.ui.adapter.LastWeekWinnerAdapter;
import com.artalent.ui.model.ContestHistoryObj;
import com.artalent.ui.model.ContestHistoryResponse;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AllWinnerFragment extends BaseFragment {

    @BindView(R.id.pullToRefresh)
    SwipeRefreshLayout pullToRefresh;

    @BindView(R.id.recycler_view)
    RecyclerView advRecyclerView;

    @BindView(R.id.tvNoResult)
    TextView tvNoResult;


    private View rootView;
    private LinearLayoutManager mLayoutManager;
    private LastWeekWinnerAdapter adapter;
    private int nextIndex = 0;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_active_advertise, container, false);
        ButterKnife.bind(this, rootView);
        tvNoResult.setText(getActivity().getString(R.string.no_winner_found));
        setContestWinningAdapter();
        contestHistoryObj.clear();
        callWinnerApi(nextIndex);
        pullToRefresh.setOnRefreshListener(this::callAllBackendAPI);
        return rootView;
    }

    private void callAllBackendAPI() {
        nextIndex = 0;
        contestHistoryObj.clear();
        adapter.notifyDataSetChanged();
        callWinnerApi(nextIndex);
    }

    private List<ContestHistoryObj> contestHistoryObj = new ArrayList<>();

    protected void callWinnerApi(int i) {
        if (Utils.isInternetAvailable(Objects.requireNonNull(getActivity()))) {
            Call<ContestHistoryResponse> call = ArtalentApplication.apiService.contestHistory(ArtalentApplication.prefManager.getUserObj().getId(), i);
            call.enqueue(new Callback<ContestHistoryResponse>() {
                @Override
                public void onResponse(Call<ContestHistoryResponse> call, Response<ContestHistoryResponse> response) {
                    pullToRefresh.setRefreshing(false);
                    int statusCode = response.code();
                    if (response.body().getObj() != null && response.body().getObj().size() > 0) {
                        for (ContestHistoryObj contestHistoryObj1 : response.body().getObj()) {
                            if (contestHistoryObj1 != null && contestHistoryObj1.getWinner1() != null && contestHistoryObj1.getWinner1().getId() > 0) {
                                contestHistoryObj.add(contestHistoryObj1);
                            }

                        }
                        adapter.notifyDataSetChanged();
                        nextIndex = response.body().getIndex();
                        if (nextIndex != -1) {
                            if (contestHistoryObj.size() < 10) {
                                callWinnerApi(nextIndex);
                            }
                        }
                    }
                    if (contestHistoryObj.size() == 0) {
                        tvNoResult.setVisibility(View.VISIBLE);
                    } else {
                        tvNoResult.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onFailure(Call<ContestHistoryResponse> call, Throwable t) {
                    pullToRefresh.setRefreshing(false);
                    // Log error here since request failed
                }
            });
        } else {
            pullToRefresh.setRefreshing(false);
        }
    }

    protected void setContestWinningAdapter() {
        mLayoutManager = new LinearLayoutManager(mActivity);
        advRecyclerView.setLayoutManager(mLayoutManager);
        adapter = new LastWeekWinnerAdapter(contestHistoryObj, mActivity);
        advRecyclerView.setAdapter(adapter);
    }

}
