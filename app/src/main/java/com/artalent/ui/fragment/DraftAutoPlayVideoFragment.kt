package com.artalent.ui.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.artalent.R
import com.artalent.autoplayrecyclervideoplayer.view.CenterLayoutManager
import com.artalent.exo.DraftContestPostAdapter
import com.artalent.ui.model.room.ContestVideoDraft
import im.ene.toro.CacheManager
import im.ene.toro.widget.Container
import java.util.*

class DraftAutoPlayVideoFragment : BaseFragment(),
    DraftContestPostAdapter.OnDraftFeedItemClickListener {

    lateinit var container: Container

    var draft: ContestVideoDraft? = null
    var contestVideoDraftObjs: MutableList<ContestVideoDraft> = ArrayList()

    private val TAG = "DraftAutoPlayVideoFragment"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_draft_auto_play_video, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        container = view.findViewById(R.id.container)

        if (arguments != null) {
            draft = arguments!!.getSerializable("draft") as ContestVideoDraft
            contestVideoDraftObjs.add(draft!!)
            Log.d(
                TAG,
                "AutoPlayVideoFragment : onCreateView: " + draft.toString()
            )
        }
        refresh()
    }

    private fun refresh() {
        Log.d(TAG, "refresh: called")
        initView()
    }

    private fun initView() {
        Log.d(TAG, "initView: called")

        val adapter = DraftContestPostAdapter(requireActivity(), contestVideoDraftObjs)

        adapter.setOnFeedItemClickListener(this)
        container.layoutManager = CenterLayoutManager(activity)
        container.adapter = adapter
        container.cacheManager = CacheManager.DEFAULT
    }

    override fun onEditBtnClick(draftObj: ContestVideoDraft) {
        Log.d(TAG, "onEditBtnClick: : -> $draftObj")
    }
}