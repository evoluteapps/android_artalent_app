package com.artalent.ui.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.artalent.ArtalentApplication;
import com.artalent.R;
import com.artalent.Utils;
import com.artalent.ui.activity.NoInternetActivity;
import com.artalent.ui.adapter.ContestPostAdapter;
import com.artalent.ui.model.PostContestObj;
import com.artalent.ui.model.PostContestResponse;
import com.artalent.ui.utils.Constants;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MyVotedPostFragment extends BaseFragment {

    @BindView(R.id.recyclerViewMyPost)
    RecyclerView recyclerViewMyPost;

    private LinearLayoutManager mLayoutManager;
    private boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;

    private ContestPostAdapter feedAdapter;
    List<PostContestObj> postObjs = new ArrayList<>();
    private int offset = 0;

    @BindView(R.id.pullToRefresh)
    SwipeRefreshLayout pullToRefresh;

    @BindView(R.id.tvNoResult)
    TextView tvNoResult;

    private int otherUserId;

    public MyVotedPostFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my_post, container, false);
        ButterKnife.bind(this, view);
        tvNoResult.setVisibility(View.GONE);
        tvNoResult.setText(getString(R.string.no_vote_post_yet));
        setupFeed();
        refresh();
        initScrollListener();
        pullToRefresh.setOnRefreshListener(this::refresh);
        return view;
    }

    private void initScrollListener() {
        recyclerViewMyPost.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NotNull RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();
                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            if (offset != -1) {
                                loading = false;
                                Log.e("mLayoutManager", "Last Item Wow !");
                                callHomePostApi();
                            }
                            //Do pagination.. i.e. fetch new data
                        }
                    }
                }
            }
        });
    }

    private void refresh() {
        postObjs.clear();
        offset = 0;
        callHomePostApi();
    }

    private void callHomePostApi() {
        if (Utils.isInternetAvailable(mActivity)) {
            Call<PostContestResponse> call = ArtalentApplication.apiService.getMyVotedPost(ArtalentApplication.prefManager.getUserObj().getId(), offset, otherUserId);
            call.enqueue(new Callback<PostContestResponse>() {
                @Override
                public void onResponse(Call<PostContestResponse> call, Response<PostContestResponse> response) {
                    int statusCode = response.code();
                    pullToRefresh.setRefreshing(false);
                    loading = true;
                    if (response.body() != null && response.body().getObj() != null) {
                        offset = response.body().getIndex();
                        if (response.body().getObj().size() > 0) {
                            postObjs.addAll(response.body().getObj());
                        }
                        feedAdapter.notifyDataSetChanged();
                    }
                    if (postObjs.size() > 0) {
                        tvNoResult.setVisibility(View.GONE);
                    } else {
                        tvNoResult.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onFailure(Call<PostContestResponse> call, Throwable t) {
                    // Log error here since request failed
                    loading = true;
                    Log.e("TAG", t.toString());
                    pullToRefresh.setRefreshing(false);
                }
            });
        } else {
            pullToRefresh.setRefreshing(false);
            Intent intent = new Intent(mActivity, NoInternetActivity.class);
            startActivityForResult(intent, Constants.NO_INTERNET);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == Constants.NO_INTERNET) {
            if (resultCode == Activity.RESULT_OK) {
                callHomePostApi();
            }
        }
    }

    protected void setupFeed() {
        mLayoutManager = new LinearLayoutManager(getContext());
        recyclerViewMyPost.setLayoutManager(mLayoutManager);
        feedAdapter = new ContestPostAdapter(getActivity(), postObjs);
        //feedAdapter.setOnFeedItemClickListener(this);
        recyclerViewMyPost.setAdapter(feedAdapter);
    }

    public void setType(int id) {
        this.otherUserId = id;
    }
}
