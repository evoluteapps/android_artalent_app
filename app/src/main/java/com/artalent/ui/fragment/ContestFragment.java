package com.artalent.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.artalent.autoplayrecyclervideoplayer.adapter.AutoPlayVideoFragment;
import com.artalent.exo.DemoApp;
import com.artalent.ui.activity.MainActivity;
import com.google.android.material.tabs.TabLayout;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.artalent.R;
import com.artalent.Utils;
import com.artalent.ui.model.RegisteredContestObj;
import com.artalent.ui.model.SpecialContestObj;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.ButterKnife;

public class ContestFragment extends BaseFragment implements MainActivity.FragmentCallback {

    private Context mContext;
    private RegisteredContestObj registeredContestObj;
    private List<SpecialContestObj> registeredSpecialContestObj = new ArrayList<>();

    public AutoPlayVideoFragment autoPlayVideoFragment;
    ContestHomeFragment contestHomeFragment;
    SpecialContestFragment specialContestFragment;
    ViewPager viewPager;
    TabLayout tabLayout;


    static class ViewPagerAdapter1 extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        ViewPagerAdapter1(FragmentManager manager) {
            super(manager);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_contest, container, false);
        ButterKnife.bind(this, view);

        viewPager = view.findViewById(R.id.viewPager);
        tabLayout = view.findViewById(R.id.tabLayout);
        setPagerAdapter();
        return view;
    }

    private void setPagerAdapter() {
        autoPlayVideoFragment = new AutoPlayVideoFragment();
        contestHomeFragment = new ContestHomeFragment();
        specialContestFragment = new SpecialContestFragment();

        ViewPagerAdapter1 adapter = new ViewPagerAdapter1(getChildFragmentManager());
        adapter.addFragment(autoPlayVideoFragment, "Contest Feed");
        adapter.addFragment(contestHomeFragment, "Contest");
        adapter.addFragment(specialContestFragment, "Special Contest");
        viewPager.setOffscreenPageLimit(2);
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setCurrentItem(1);


        registeredContestObj = contestHomeFragment.getRegisteredContest();
        registeredSpecialContestObj = contestHomeFragment.getRegisteredSpecialContest();

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageScrollStateChanged(int state) {
            }

            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            public void onPageSelected(int position) {
                if (position != 0) {
                    if (DemoApp.Companion.getHelper() != null) {
                        DemoApp.Companion.getHelper().pause();
                    }
                }
                // Check if this is the page you want.
            }
        });
    }

    public void stopVideo() {
        /*if (autoPlayVideoActivity != null){
            if (autoPlayVideoActivity.listFeed != null && autoPlayVideoActivity.listFeed.getHandingVideoHolder() != null) {
                autoPlayVideoActivity.listFeed.getHandingVideoHolder().stopVideo();
            }
        }*/

    }

    public RegisteredContestObj getRegisteredContest() {
        return this.registeredContestObj;
    }

    public List<SpecialContestObj> getRegisteredSpecialContest() {
        return this.registeredSpecialContestObj;
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        new Handler().postDelayed(() -> Utils.updateToolbar(Objects.requireNonNull(getActivity()), R.color.black, false), 100);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.mContext = null;
    }

    @Override
    public void onPageChanged() {

    }
}
