package com.artalent.ui.fragment;

import android.os.Bundle;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.artalent.R;
import com.artalent.ui.storyview.StoryModel;
import com.artalent.ui.storyview.StoryView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class OneFragment extends Fragment {
    @BindView(R.id.storyView)
    StoryView storyView;

    public OneFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_one, container, false);
        ButterKnife.bind(this, view);
        storyView.resetStoryVisits();
        ArrayList<StoryModel> uris = new ArrayList<>();
        uris.add(new StoryModel("https://images.pexels.com/photos/87840/daisy-pollen-flower-nature-87840.jpeg?cs=srgb&dl=plant-flower-macro-87840.jpg&fm=jpg", "Ankit Kumar", "12:00 PM"));
        uris.add(new StoryModel("https://bornrealist.com/wp-content/uploads/2017/11/Here-Are-Top-10-Cute-Animals-That-Might-Actually-Kill-You.jpg", "Panda Man", "01:00 AM"));
        uris.add(new StoryModel("https://bornrealist.com/wp-content/uploads/2017/11/Here-Are-Top-10-Cute-Animals-That-Might-Actually-Kill-You.jpg", "Steve", "Yesterday"));
        uris.add(new StoryModel("https://bornrealist.com/wp-content/uploads/2017/11/Here-Are-Top-10-Cute-Animals-That-Might-Actually-Kill-You.jpg", "Grambon", "10:15 PM"));
        storyView.setImageUris(uris);
        return view;
    }

}
