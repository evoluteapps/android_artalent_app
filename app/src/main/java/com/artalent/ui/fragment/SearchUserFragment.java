package com.artalent.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.artalent.ArtalentApplication;
import com.artalent.R;
import com.artalent.Utils;
import com.artalent.ui.activity.FilterActivity;
import com.artalent.ui.activity.NoInternetActivity;
import com.artalent.ui.activity.OtherUserProfileActivity;
import com.artalent.ui.adapter.SearchUserAdapter;
import com.artalent.ui.model.AllUserListResponse;
import com.artalent.ui.model.BaseResponse;
import com.artalent.ui.model.Country;
import com.artalent.ui.model.DistrictsObj;
import com.artalent.ui.model.InterestObj;
import com.artalent.ui.model.StateObj;
import com.artalent.ui.model.UserObj;
import com.artalent.ui.model.UserResponse;
import com.artalent.ui.model.request.ContestRegistrationReq;
import com.artalent.ui.model.request.FollowingReq;
import com.artalent.ui.utils.Constants;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

public class SearchUserFragment extends BaseFragment implements View.OnClickListener {
    private List<UserObj> userObjList = new ArrayList<>();
    private RecyclerView recyclerView;
    private EditText edtSearch;
    private String sKeyWords;


    @BindView(R.id.pullToRefresh)
    SwipeRefreshLayout pullToRefresh;

    @BindView(R.id.ivFilter)
    TextView ivFilter;

    @BindView(R.id.no_users_tv)
    TextView noUsersTv;


    private SearchUserAdapter feedAdapter;
    private int offset = 0;

    private LinearLayoutManager mLayoutManager;
    private boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;

    public static final int REQUEST_FILTER = 400;
    private Country selectedCountry = null;
    List<InterestObj> selectedInterestList = new ArrayList<>();
    private StateObj selectedStateObj = null;
    List<DistrictsObj> selectedDistrictsList = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search_user, container, false);

        ButterKnife.bind(this, view);
        recyclerView = view.findViewById(R.id.recycler_view);
        edtSearch = view.findViewById(R.id.edtSearch);
        setupFeed();
        refresh();
        initScrollListener();
        pullToRefresh.setOnRefreshListener(this::refresh);
        ivFilter.setOnClickListener(this);
        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (edtSearch.getText().toString().trim().length() > 0) {
                    sKeyWords = edtSearch.getText().toString().trim();
                } else {
                    sKeyWords = "";
                }
                filter(sKeyWords);
                //callHistoryApi();
            }
        });
        return view;
    }


    void filter(String text) {
        if (!TextUtils.isEmpty(text)) {
            List<UserObj> temp = new ArrayList();
            for (UserObj d : userObjList) {
                //or use .equal(text) with you want equal match
                //use .toLowerCase() for better matches
                if (d.getFirstName().toLowerCase().contains(text.toLowerCase())) {
                    temp.add(d);
                } else if (d.getLastName().toLowerCase().contains(text.toLowerCase())) {
                    temp.add(d);
                } else if (d.getUserName().toLowerCase().contains(text.toLowerCase())) {
                    temp.add(d);
                }
            }
            //update recyclerview
            feedAdapter.updateList(temp);
        } else {
            feedAdapter.updateList(userObjList);
        }

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        new Handler().postDelayed(() -> Utils.updateToolbar(Objects.requireNonNull(getActivity()), R.color.white, true), 100);
    }

    private void initScrollListener() {
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NotNull RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();
                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            if (offset != -1) {
                                loading = false;
                                Log.e("mLayoutManager", "Last Item Wow !");
                                callHomePostApi();
                            }
                        }
                    }
                }
            }
        });
    }

    protected void setupFeed() {
        mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        feedAdapter = new SearchUserAdapter(userObjList, mActivity, this);
        recyclerView.setAdapter(feedAdapter);
    }

    boolean isGoNext = false;

    @Override
    public void onResume() {
        super.onResume();
        if (isGoNext) {
            isGoNext = false;
            refresh();
        }
    }

    private void refresh() {
        userObjList.clear();
        feedAdapter.notifyDataSetChanged();
        offset = 0;
        callHomePostApi();
    }

    private void callHomePostApi() {
        if (Utils.isInternetAvailable(mActivity)) {
            Call<AllUserListResponse> call = ArtalentApplication.apiService.getSearchUser(ArtalentApplication.prefManager.getUserObj().getId(), offset, fillData());
            call.enqueue(new Callback<AllUserListResponse>() {
                @Override
                public void onResponse(Call<AllUserListResponse> call, Response<AllUserListResponse> response) {
                    pullToRefresh.setRefreshing(false);
                    loading = true;
                    if (response.body() != null && response.body().getObj() != null) {
                        offset = response.body().getIndex();
                        if (response.body().getObj().size() > 0) {
                            noUsersTv.setVisibility(View.GONE);
                            recyclerView.setVisibility(View.VISIBLE);
                            //userObjList.addAll(response.body().getObj());
                            for (UserObj userObj : response.body().getObj()) {
                                if (userObj != null && userObj.getFirstName() != null && !TextUtils.isEmpty(userObj.getFirstName())) {
                                    userObjList.add(userObj);
                                }

                            }
                        }
                        feedAdapter.notifyDataSetChanged();
                        if (userObjList.size() < 10 && offset != -1) {
                            callHomePostApi();
                        }
                    }
                }

                @Override
                public void onFailure(Call<AllUserListResponse> call, Throwable t) {
                    // Log error here since request failed
                    loading = true;
                    Log.e("TAG", t.toString());
                    pullToRefresh.setRefreshing(false);
                }
            });
        } else {
            pullToRefresh.setRefreshing(false);
            Intent intent = new Intent(mActivity, NoInternetActivity.class);
            startActivityForResult(intent, Constants.NO_INTERNET);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == Constants.NO_INTERNET) {
            if (resultCode == RESULT_OK) {
                refresh();
            }
        }
        if (requestCode == REQUEST_FILTER) {
            if (resultCode == getActivity().RESULT_OK) {
                selectedInterestList = (ArrayList<InterestObj>) data.getSerializableExtra(Constants.INTEREST_LIST);
                selectedCountry = (Country) data.getSerializableExtra(Constants.COUNTRY_OBJ);
                selectedStateObj = (StateObj) data.getSerializableExtra(Constants.STATE_OBJ);
                selectedDistrictsList = (ArrayList<DistrictsObj>) data.getSerializableExtra(Constants.DISTRICTS_LIST);
                refresh();
            }
        }
    }

    private ContestRegistrationReq fillData() {
        ContestRegistrationReq createReq = new ContestRegistrationReq();
        int countFilter = 0;
        String sDistrictIds = "";
        if (selectedDistrictsList.size() > 0) {
            countFilter = countFilter + selectedDistrictsList.size();
            for (DistrictsObj districtsObj : selectedDistrictsList) {
                if (TextUtils.isEmpty(sDistrictIds)) {
                    sDistrictIds = "" + districtsObj.getDistrictId();
                } else {
                    sDistrictIds = sDistrictIds + "," + districtsObj.getDistrictId();
                }

            }
        }

        String sInterestIds = "";
        if (selectedInterestList.size() > 0) {
            countFilter = countFilter + selectedInterestList.size();
            for (InterestObj interestObj : selectedInterestList) {
                if (TextUtils.isEmpty(sInterestIds)) {
                    sInterestIds = "" + interestObj.getInterestMasterId();
                } else {
                    sInterestIds = sInterestIds + "," + interestObj.getInterestMasterId();
                }

            }
        }

        if (selectedCountry != null && selectedCountry.getCountryId() > 0) {
            countFilter = countFilter + 1;
            createReq.setCountryId("" + selectedCountry.getCountryId());
        }
        if (selectedStateObj != null && selectedStateObj.getStateId() > 0) {
            countFilter = countFilter + 1;
            createReq.setStateId("" + selectedStateObj.getStateId());
        }

        if (countFilter == 0) {
            ivFilter.setText("");
            ivFilter.setTextColor(ContextCompat.getColor(mActivity, R.color.primary));
        } else {
            ivFilter.setText(" (" + countFilter + ")");
            ivFilter.setTextColor(ContextCompat.getColor(mActivity, R.color.primary));
        }
        createReq.setDistrictId(sDistrictIds);
        createReq.setInterestId(sInterestIds);

        return createReq;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnFollow:
                int position = (int) v.getTag();
                UserObj userObj = userObjList.get(position);
                if (userObj != null) {
                    if (userObj.getIsFollowed() == 1) {
                        postFollowDelete(userObj, position);
                    } else {
                        postFollow(userObj, position);
                    }
                }
                break;
            case R.id.rlTop:
                UserObj userObj1 = (UserObj) v.getTag();
                if (userObj1 != null) {
                    isGoNext = true;
                    Intent intent = new Intent(getContext(), OtherUserProfileActivity.class);
                    intent.putExtra(Constants.DATA, userObj1);
                    startActivity(intent);
                }
                break;
            case R.id.ivFilter:
                Intent intent = new Intent(mActivity, FilterActivity.class);
                intent.putExtra(Constants.INTEREST_LIST, (Serializable) selectedInterestList);
                intent.putExtra(Constants.COUNTRY_OBJ, selectedCountry);
                intent.putExtra(Constants.STATE_OBJ, selectedStateObj);
                intent.putExtra(Constants.DISTRICTS_LIST, (Serializable) selectedDistrictsList);
                startActivityForResult(intent, REQUEST_FILTER);
                break;

        }
    }


    private void postFollow(UserObj userObj, int position) {
        if (Utils.isInternetAvailable(mActivity)) {
            ArtalentApplication.apiService.postFollow(ArtalentApplication.prefManager.getUserObj().getId(), fillDataFollow(userObj.getId()))
                    .enqueue(new Callback<BaseResponse>() {
                        @Override
                        public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                            int statusCode = response.code();
                            if (response.body() != null) {
                                userObjList.get(position).setIsFollowed(1);
                                feedAdapter.notifyItemChanged(position);
                                getUserData();
                            }
                        }

                        @Override
                        public void onFailure(Call<BaseResponse> call, Throwable t) {
                            // Log error here since request failed
                            Log.e("TAGArtalent", t.toString());
                        }
                    });
        } else {
            Utils.showToast(mActivity, getResources().getString(R.string.please_check_your_internet));
        }
    }

    private void postFollowDelete(UserObj userObj, int position) {
        if (Utils.isInternetAvailable(getActivity())) {
            ArtalentApplication.apiService.postFollowDelete(ArtalentApplication.prefManager.getUserObj().getId(), (userObj.getId()))
                    .enqueue(new Callback<BaseResponse>() {
                        @Override
                        public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                            int statusCode = response.code();
                            if (response.body() != null) {
                                userObjList.get(position).setIsFollowed(0);
                                feedAdapter.notifyItemChanged(position);
                                getUserData();
                            }
                        }

                        @Override
                        public void onFailure(Call<BaseResponse> call, Throwable t) {
                            // Log error here since request failed
                            Log.e("TAGArtalent", t.toString());
                        }
                    });
        } else {
            Utils.showToast(getActivity(), getActivity().getResources().getString(R.string.please_check_your_internet));
        }
    }


    private FollowingReq fillDataFollow(int id) {
        FollowingReq createReq = new FollowingReq();
        createReq.setFollowingId(id);
        return createReq;
    }

    private void getUserData() {
        if (Utils.isInternetAvailable(mActivity)) {
            ArtalentApplication.apiService.getOtherUserDetails(ArtalentApplication.prefManager.getUserObj().getId()).enqueue(new Callback<UserResponse>() {
                @Override
                public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                    int statusCode = response.code();
                    if (response.body() != null && response.body().getObj() != null) {
                        UserObj userObj = response.body().getObj();
                    }
                }

                @Override
                public void onFailure(Call<UserResponse> call, Throwable t) {
                    // Log error here since request failed
                    Log.e("TAG", t.toString());
                }
            });
        } else {
            Intent intent = new Intent(mActivity, NoInternetActivity.class);
            startActivityForResult(intent, Constants.NO_INTERNET);
        }
    }

}
