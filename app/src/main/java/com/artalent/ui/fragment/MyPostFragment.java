package com.artalent.ui.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.artalent.ArtalentApplication;
import com.artalent.R;
import com.artalent.Utils;
import com.artalent.ui.activity.NoInternetActivity;
import com.artalent.ui.adapter.MyPostAdapter;
import com.artalent.ui.model.PostObj;
import com.artalent.ui.model.PostResponse;
import com.artalent.ui.utils.Constants;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MyPostFragment extends BaseFragment implements View.OnClickListener{

    @BindView(R.id.recyclerViewMyPost)
    RecyclerView recyclerViewMyPost;

    @BindView(R.id.tvNoResult)
    TextView tvNoResult;

    private GridLayoutManager mLayoutManager;
    private boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;

    private MyPostAdapter feedAdapter;
    List<PostObj> postObjs = new ArrayList<>();
    private int offset = 0;

    @BindView(R.id.pullToRefresh)
    SwipeRefreshLayout pullToRefresh;

    private int otherUserId;

    public MyPostFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my_post, container, false);
        ButterKnife.bind(this, view);
        tvNoResult.setVisibility(View.GONE);
        tvNoResult.setText(getString(R.string.no_post_yet));
        setupFeed();
        initScrollListener();
        pullToRefresh.setOnRefreshListener(this::refresh);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        refresh();
    }

    private void initScrollListener() {
        recyclerViewMyPost.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NotNull RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();
                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            if (offset != -1) {
                                loading = false;
                                Log.e("mLayoutManager", "Last Item Wow !");
                                callHomePostApi();
                            }
                        }
                    }
                }
            }
        });
    }

    private void refresh() {
        postObjs.clear();
        offset = 0;
        callHomePostApi();
    }

    private void callHomePostApi() {
        if (Utils.isInternetAvailable(mActivity)) {
            Call<PostResponse> call = ArtalentApplication.apiService.getMyPost(ArtalentApplication.prefManager.getUserObj().getId(), offset, otherUserId);
            call.enqueue(new Callback<PostResponse>() {
                @Override
                public void onResponse(Call<PostResponse> call, Response<PostResponse> response) {
                    int statusCode = response.code();
                    pullToRefresh.setRefreshing(false);
                    loading = true;
                    if (response.body() != null && response.body().getObj() != null) {
                        offset = response.body().getIndex();
                        if (response.body().getObj().size() > 0) {
                            postObjs.addAll(response.body().getObj());
                        }
                        feedAdapter.notifyDataSetChanged();
                    }
                    if (postObjs.size() > 0) {
                        tvNoResult.setVisibility(View.GONE);
                    } else {
                        tvNoResult.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onFailure(Call<PostResponse> call, Throwable t) {
                    // Log error here since request failed
                    loading = true;
                    Log.e("TAG", t.toString());
                    pullToRefresh.setRefreshing(false);
                }
            });
        } else {
            pullToRefresh.setRefreshing(false);
            Intent intent = new Intent(mActivity, NoInternetActivity.class);
            startActivityForResult(intent, Constants.NO_INTERNET);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == Constants.NO_INTERNET) {
            if (resultCode == Activity.RESULT_OK) {
                callHomePostApi();
            }
        }
    }

    protected void setupFeed() {
        mLayoutManager = new GridLayoutManager(getContext(), 3);
        recyclerViewMyPost.setLayoutManager(mLayoutManager);

        feedAdapter = new MyPostAdapter(getActivity(), postObjs, this);
        //feedAdapter.setOnFeedItemClickListener(this);
        recyclerViewMyPost.setAdapter(feedAdapter);
    }

    public void setType(int id) {
        this.otherUserId = id;
    }

    @Override
    public void onClick(View v) {

    }
}
