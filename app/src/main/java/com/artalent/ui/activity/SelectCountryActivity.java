package com.artalent.ui.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.artalent.ArtalentApplication;
import com.artalent.R;
import com.artalent.Utils;
import com.artalent.ui.adapter.FilterCountryAdapter;
import com.artalent.ui.model.BaseResponse;
import com.artalent.ui.model.Country;
import com.artalent.ui.model.CountryResponse;
import com.artalent.ui.model.request.CountryReq;
import com.artalent.ui.utils.Constants;
import com.artalent.ui.utils.RecyclerTouchListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.OnClickListener;


/**
 * Created by Mahajan on 16-09-2019.
 */

public class SelectCountryActivity extends BaseActivityOther implements OnClickListener {

    RecyclerView recyclerView;
    FloatingActionButton save, addFab;
    FilterCountryAdapter selectCountryAdapter;
    List<Country> countries = new ArrayList<>();
    private Country selectedCountry = null;
    private int fromData;
    private TextView tvNoListItem;
    private RecyclerTouchListener
            touchListener;

    public static void startActivity(Activity mActivity, Country country, int requestCode) {
        Intent intent = new Intent(mActivity, SelectCountryActivity.class);
        intent.putExtra(Constants.COUNTRY_OBJ, country);
        mActivity.startActivityForResult(intent, requestCode);
    }

    public static void startActivityFromHome(Activity mActivity, Country country, int requestCode) {
        Intent intent = new Intent(mActivity, SelectCountryActivity.class);
        intent.putExtra(Constants.COUNTRY_OBJ, country);
        intent.putExtra(Constants.FROM, requestCode);
        mActivity.startActivityForResult(intent, requestCode);
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_countery);
        init1();
        setupToolbarBack(getString(R.string.manage_country));
        initializeUi();
        if (fromData == 1) {
            initializeRecyclerviewTouchListener();
        }
        callCountryApi();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (fromData == 1) {
            recyclerView.addOnItemTouchListener(touchListener);
        }
    }

    private void initializeRecyclerviewTouchListener() {
        touchListener = new RecyclerTouchListener(this, recyclerView);
        touchListener
                .setClickable(new RecyclerTouchListener.OnRowClickListener() {
                    @Override
                    public void onRowClicked(int position) {
                        Country country = countries.get(position);
                        SelectStateActivity.startActivity(mActivity, null, country.getCountryId(), 0, fromData);
                    }

                    @Override
                    public void onIndependentViewClicked(int independentViewID, int position) {
                    }
                })
                .setSwipeOptionViews(R.id.delete_task, R.id.edit_task)
                .setSwipeable(R.id.rowFG, R.id.rowBG, (viewID, position) -> {
                    switch (viewID) {
                        case R.id.delete_task:
                            callDeleteCountryApi(countries.get(position), position);
                            break;
                        case R.id.edit_task:
                            showDialogData(countries.get(position), true);
                            break;
                    }
                });
    }


    private void callCountryApi() {
        if (Utils.isInternetAvailable(mActivity)) {
            showProgress();
            Call<CountryResponse> call = ArtalentApplication.apiService.getCountries();
            call.enqueue(new Callback<CountryResponse>() {
                @Override
                public void onResponse(Call<CountryResponse> call, Response<CountryResponse> response) {
                    int statusCode = response.code();
                    if (response.body().getObj() != null && response.body().getObj().size() > 0) {
                        tvNoListItem.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                        hideProgress();
                        countries.clear();
                        countries.addAll(response.body().getObj());
                        for (Country country : countries) {
                            if (selectedCountry != null) {
                                if (country.getCountryId() == selectedCountry.getCountryId()) {
                                    country.setCheck(true);
                                }
                            } else {
                                if (country != null && country.getCountryName() != null && !TextUtils.isEmpty(country.getCountryName()) && country.getCountryName().equals("India")) {
                                    selectedCountry = country;
                                    country.setCheck(true);
                                }
                            }
                        }
                        setAdapter();
                    } else {
                        tvNoListItem.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.GONE);
                        hideProgress();
                    }
                }

                @Override
                public void onFailure(Call<CountryResponse> call, Throwable t) {
                    // Log error here since request failed
                    hideProgress();
                }
            });
        }
    }

    private CountryReq fillDataCountry(String countryName) {
        CountryReq countryReq = new CountryReq();
        countryReq.setCountryName(countryName);
        return countryReq;
    }

    private void callDeleteCountryApi(Country country, int position) {
        if (Utils.isInternetAvailable(mActivity)) {
            showProgress();
            Call<BaseResponse> call = ArtalentApplication.apiService.deleteCountry(ArtalentApplication.prefManager.getUserObj().getId(), country.getCountryId());
            call.enqueue(new Callback<BaseResponse>() {
                @Override
                public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                    int statusCode = response.code();
                    String mess = response.body().getMessage();
                    if (statusCode == 200) {
                        hideProgress();
                        countries.remove(position);
                        selectCountryAdapter.setTaskList(countries);
                        Utils.showToast(mActivity, mess);
                    } else {
                        hideProgress();
                        Utils.showToast(mActivity, mess);
                    }
                }

                @Override
                public void onFailure(Call<BaseResponse> call, Throwable t) {
                    // Log error here since request failed
                    dialog.dismiss();
                    hideProgress();
                    Utils.showToast(mActivity, "onFailure : " + t.toString());
                }
            });
        }
    }

    private void callEditCountryApi(Country country) {
        if (Utils.isInternetAvailable(mActivity)) {
            showProgress();
            Call<BaseResponse> call = ArtalentApplication.apiService.editCountry(ArtalentApplication.prefManager.getUserObj().getId(), country.getCountryId(), fillDataCountry(country.getCountryName()));
            call.enqueue(new Callback<BaseResponse>() {
                @Override
                public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                    int statusCode = response.code();
                    String mess = response.body().getMessage();
                    if (statusCode == 200) {
                        hideProgress();
                        dialog.dismiss();
                        Utils.showToast(mActivity, mess);
                        callCountryApi();
                    } else {
                        hideProgress();
                        dialog.dismiss();
                        Utils.showToast(mActivity, mess);
                    }
                }

                @Override
                public void onFailure(Call<BaseResponse> call, Throwable t) {
                    // Log error here since request failed
                    dialog.dismiss();
                    hideProgress();
                    Utils.showToast(mActivity, "onFailure : " + t.toString());
                }
            });
        }
    }


    private void callAddCountryApi(String countryName) {
        if (Utils.isInternetAvailable(mActivity)) {
            showProgress();
            Call<BaseResponse> call = ArtalentApplication.apiService.addCountry(ArtalentApplication.prefManager.getUserObj().getId(), fillDataCountry(countryName));
            call.enqueue(new Callback<BaseResponse>() {
                @Override
                public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                    int statusCode = response.code();
                    String mess = response.body().getMessage();
                    if (statusCode == 200) {
                        callCountryApi();
                        hideProgress();
                        dialog.dismiss();
                        Utils.showToast(mActivity, mess);
                    } else {
                        hideProgress();
                        dialog.dismiss();
                        Utils.showToast(mActivity, mess);
                    }
                }

                @Override
                public void onFailure(Call<BaseResponse> call, Throwable t) {
                    // Log error here since request failed
                    dialog.dismiss();
                    hideProgress();
                    Utils.showToast(mActivity, "onFailure : " + t.toString());
                }
            });
        }

    }


    private void setAdapter() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        selectCountryAdapter = new FilterCountryAdapter(mActivity, countries, this, fromData);
        selectCountryAdapter.setTaskList(countries);
        recyclerView.setAdapter(selectCountryAdapter);

    }

    private void init1() {
        selectedCountry = (Country) getIntent().getSerializableExtra(Constants.COUNTRY_OBJ);
        fromData = getIntent().getIntExtra(Constants.FROM, 0);
    }


    @SuppressLint("RestrictedApi")
    public void initializeUi() {
        tvNoListItem = findViewById(R.id.tv_no_any_item);

        recyclerView = findViewById(R.id.recyclerView);
        save = findViewById(R.id.save);
        addFab = findViewById(R.id.addFab);
        save.setOnClickListener(this);
        addFab.setOnClickListener(this);
        if (fromData == 1) {
            addFab.setVisibility(View.VISIBLE);
            save.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.addFab:
                showDialogData(null, false);
                break;
            case R.id.save:
                if (selectedCountry != null) {
                    Intent output = new Intent();
                    output.putExtra(Constants.COUNTRY_OBJ, selectedCountry);
                    setResult(RESULT_OK, output);
                    finish();
                } else {
                    Utils.showToast(mActivity, "Please Select Country");
                }

                break;
            case R.id.llMain:
                if (v.getTag() instanceof Country) {
                    Country country = (Country) v.getTag();

                    if (country != null) {
                        for (Country country1 : countries) {
                            if (country1.getCountryId() == country.getCountryId()) {
                                country1.setCheck(!country1.isCheck());
                                if (country1.isCheck()) {
                                    selectedCountry = country;
                                } else {
                                    selectedCountry = null;
                                }
                            } else {
                                country1.setCheck(false);
                            }
                        }
                        selectCountryAdapter.notifyDataSetChanged();
                    }

                }
                break;
        }
    }

    Dialog dialog;

    private void showDialogData(Country country, boolean isEdit) {
        dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_add_data);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setTitle("Title...");
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        TextView titleTextView = dialog.findViewById(R.id.titleTextView);
        TextInputEditText name_et = dialog.findViewById(R.id.name_et);
        name_et.setHint("Enter country name");
        if (isEdit) {
            titleTextView.setText(R.string.edit_country);
            name_et.setText(country.getCountryName());
        } else {
            titleTextView.setText(R.string.add_country);
        }
        Button downloadButton = dialog.findViewById(R.id.downloadButton);
        Button cancelButton = dialog.findViewById(R.id.cancelButton);
        downloadButton.setOnClickListener(v -> {
            if (!TextUtils.isEmpty(name_et.getText().toString())) {
                if (isEdit) {
                    country.setCountryName(name_et.getText().toString());
                    callEditCountryApi(country);
                } else {
                    callAddCountryApi(name_et.getText().toString());
                }
            } else {
                Utils.showToast(SelectCountryActivity.this, "Please enter country name");
            }
        });
        cancelButton.setOnClickListener(v -> dialog.dismiss());
        dialog.show();
    }
}
