package com.artalent.ui.activity;

import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.artalent.ArtalentApplication;
import com.artalent.BuildConfig;
import com.artalent.R;
import com.artalent.Utils;
import com.artalent.ui.model.OtpServiceResponse;
import com.artalent.ui.model.UserObj;
import com.artalent.ui.model.UserResponse;
import com.artalent.ui.model.request.CreateReq;
import com.artalent.ui.utils.Constants;

import java.util.Random;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Mahajan on 08-09-2019.
 */

public class LoginActivity extends BaseActivityOther {

    private final static String API_KEY = "7e8f60e325cd06e164799af1e317d7a7";

    @BindView(R.id.edtPhoneNumber)
    EditText edtPhoneNumber;

    @BindView(R.id.edtReferral)
    EditText edtReferral;

    @BindView(R.id.btnLogin)
    Button btnLogin;

    @BindView(R.id.tvNeedHelp)
    TextView tvNeedHelp;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
//        edtPhoneNumber.setBackground(ContextCompat.getDrawable(this, R.drawable.edit_style));
        // ArtalentApplication.prefManager.setAuthorizationToken("");
        //  ArtalentApplication.prefManager.setUserObj();
        // edtPhoneNumber.addTextChangedListener(textWatcher);
    }

    @OnClick(R.id.btnLogin)
    public void onLoginClick() {

        if (checkValidation()) {
            if (Utils.isInternetAvailable(mActivity)) {
                //callCheckOTPBalanceApi();
                callLoginApi();
            } else {
                Utils.showToast(mActivity, getString(R.string.no_internet_connection_available));
            }

            //callLoginApi();
        }
    }

    private boolean checkValidation() {
        boolean check = false;
        if (isValidMobile(edtPhoneNumber.getText().toString().trim())) {
            if (Utils.isInternetAvailable(mActivity)) {
                check = true;
            } else {
                Utils.showToast(mActivity, getString(R.string.no_internet_connection_available));
            }
        } else {
            Utils.showToast(mActivity, getString(R.string.please_enter_phone_number));
        }
        return check;

    }

    TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (s.toString().length() == 1 && s.toString().startsWith("0")) {
                s.clear();
            }
        }
    };

    private boolean isValidMobile(String phone) {
        return android.util.Patterns.PHONE.matcher(phone).matches() && phone.length() >= 10;
    }

    private void callSendOTPApi(String randomOTP) {
        ArtalentApplication.apiServiceOTP.sendOTP(Constants.ARTALENT_OTP_API_KEY, edtPhoneNumber.getText().toString().trim(), randomOTP, Constants.ARTALENT_OTP_TEMPLATE_NAME)
                .enqueue(new Callback<OtpServiceResponse>() {
                    @Override
                    public void onResponse(Call<OtpServiceResponse> call, Response<OtpServiceResponse> response) {
                        int statusCode = response.code();
                        if (response.body() != null) {
                            String details = response.body().getDetails();
                        } else {
                            Utils.showToast(mActivity, mActivity.getString(R.string.please_try_again));
                        }
                    }

                    @Override
                    public void onFailure(Call<OtpServiceResponse> call, Throwable t) {
                        // Log error here since request failed
                        Log.e("TAGArtalent", t.toString());
                        Utils.showToast(mActivity, "onFailure : " + t.toString());
                    }
                });
    }


    private void callLoginApi() {
        if (Utils.isInternetAvailable(this)) {
            ArtalentApplication.apiService.createUser(fillData())
                    .enqueue(new Callback<UserResponse>() {
                        @Override
                        public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                            int statusCode = response.code();
                            if (response.body() != null && response.body().getObj() != null) {
                                UserObj userObj = response.body().getObj();
                                if (userObj != null && !TextUtils.isEmpty(userObj.getMobile())) {
                                    ArtalentApplication.prefManager.setUserObj(userObj);
                                    if (BuildConfig.DEBUG) {
                                        OTPActivity.startActivity(mActivity, userObj.getMobile(), "1111");
                                    } else {
                                        String randomOTP = getRandomNumber();
                                        callSendOTPApi(randomOTP);
                                        OTPActivity.startActivity(mActivity, userObj.getMobile(), randomOTP);
                                    }
                                } else {
                                    Utils.showToast(mActivity, mActivity.getString(R.string.please_try_again));
                                }
                            } else {
                                Utils.showToast(mActivity, mActivity.getString(R.string.please_try_again));
                            }

                        }

                        @Override
                        public void onFailure(Call<UserResponse> call, Throwable t) {
                            // Log error here since request failed
                            Log.e("TAGArtalent", t.toString());
                            Utils.showToast(mActivity, "onFailure : " + t.toString());
                        }
                    });
        }

    }

    private void callCheckOTPBalanceApi() {
        ArtalentApplication.apiServiceOTP.getOTPBalance(Constants.ARTALENT_OTP_API_KEY)
                .enqueue(new Callback<OtpServiceResponse>() {
                    @Override
                    public void onResponse(Call<OtpServiceResponse> call, Response<OtpServiceResponse> response) {
                        int statusCode = response.code();
                        if (response.body() != null) {
                            String status = response.body().getStatus();
                            int otpBalance = Integer.parseInt(response.body().getDetails());
                            Utils.showToast(mActivity, status + " OTP Balance " + otpBalance);
                            if (status.equalsIgnoreCase("Success")) {
                                if (otpBalance > 0) {
                                    callLoginApi();
                                } else {
                                    callLoginApi();
                                }

                            }

                            /*UserObj userObj = response.body().getObj();
                            if (userObj != null && !TextUtils.isEmpty(userObj.getMobile())) {
                                ArtalentApplication.prefManager.setUserObj(userObj);
                                OTPActivity.startActivity(mActivity, userObj.getMobile());
                            } else {
                                Utils.showToast(mActivity, mActivity.getString(R.string.please_try_again));
                            }*/
                        } else {
                            Utils.showToast(mActivity, mActivity.getString(R.string.please_try_again));
                        }

                    }

                    @Override
                    public void onFailure(Call<OtpServiceResponse> call, Throwable t) {
                        // Log error here since request failed
                        Log.e("TAGArtalent", t.toString());
                        Utils.showToast(mActivity, "onFailure : " + t.toString());
                    }
                });
    }


    public String getRandomNumber() {
        Random random = new Random();
        return String.format("%04d", random.nextInt(10000));
    }

    private CreateReq fillData() {
        CreateReq createReq = new CreateReq();
        createReq.setMobile(edtPhoneNumber.getText().toString().trim());
        createReq.setReferalCode(edtReferral.getText().toString().trim());
        return createReq;
    }


    /*@Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLogin:
                Intent intent = new Intent(LoginActivity.this, OTPActivity.class);
                startActivity(intent);
                break;

        }

    }*/
}
