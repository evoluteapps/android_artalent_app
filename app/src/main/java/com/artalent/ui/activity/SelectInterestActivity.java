package com.artalent.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.artalent.ArtalentApplication;
import com.artalent.R;
import com.artalent.Utils;
import com.artalent.ui.adapter.SelectInterestAdapter;
import com.artalent.ui.fragment.AddInterestFragment;
import com.artalent.ui.model.BaseResponse;
import com.artalent.ui.model.InterestListObj;
import com.artalent.ui.model.InterestObj;
import com.artalent.ui.model.InterestResponse;
import com.artalent.ui.model.UserObj;
import com.artalent.ui.model.UserResponse;
import com.artalent.ui.model.request.InterestReq;
import com.artalent.ui.model.request.InterestReqObj;
import com.artalent.ui.utils.Constants;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by Mahajan on 16-09-2019.
 */

public class SelectInterestActivity extends BaseActivityOther implements View.OnClickListener {

    RecyclerView recyclerView;
    TextView talentHeadline;
    FloatingActionButton save;
    SelectInterestAdapter selectCountryAdapter;
    List<InterestObj> interestList = new ArrayList<>();
    InterestListObj interestListObj = new InterestListObj();
    private static boolean isRegister = false;
    private static boolean isEditInterestActivity;

    public static void startActivityFromEdit(Activity mActivity, InterestListObj interestObj, int requestCode, boolean isRegisterActivity) {
        isRegister = isRegisterActivity;
        Intent intent = new Intent(mActivity, SelectInterestActivity.class);
        intent.putExtra(Constants.INTEREST_OBJ, interestObj);
        mActivity.startActivityForResult(intent, requestCode);

    }


    public static void startActivity(Activity mActivity, boolean isEditActivity, boolean isRegisterActivity) {
        isRegister = isRegisterActivity;
        isEditInterestActivity = isEditActivity;
        Intent intent = new Intent(mActivity, SelectInterestActivity.class);
        mActivity.startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        callInterestApi();
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_interest);

        setupToolbarBack(getString(R.string.select_interest));
        UserObj userObj = ArtalentApplication.prefManager.getUserObj();
        String firstName = userObj.getFirstName();
        String lastName = userObj.getLastName();
        String fullName = firstName + " " + lastName;
        initializeUi();
        if (isEditInterestActivity) save.setImageResource(R.drawable.ic_add_box_blackwhite_24dp);
        talentHeadline.setText("Hi " + fullName + ", " + getResources().getString(R.string.talent_heading));
    }

    TextView textViewBottomSheetState;

    @Override
    protected void onStart() {
        super.onStart();
        callInterestApi();
    }

    BottomSheetBehavior bottomSheetBehavior;
    private static final String TAG = "SelectInterestActivity";


    public void openBottomSheet() {
        AddInterestFragment addInterestFragment = new AddInterestFragment(mActivity, null);
        addInterestFragment.setOnProgressListener(new AddInterestFragment.ProgressCallback() {
            @Override
            public void startProgress() {
                showProgress();
            }

            @Override
            public void hideCircularProgress() {
                hideProgress();
            }
        });
//        Bundle bundle = new Bundle();
//        bundle.putSerializable("RegisteredContest", (Serializable) objectContestList);
//        bottomSheetFragment.setArguments(bundle);
        addInterestFragment.show(getSupportFragmentManager(), addInterestFragment.getTag());

    }

    protected void callInterestApi() {
        if (Utils.isInternetAvailable(mActivity)) {
            Call<InterestResponse> call = ArtalentApplication.apiService.getInterest();
            call.enqueue(new Callback<InterestResponse>() {
                @Override
                public void onResponse(Call<InterestResponse> call, Response<InterestResponse> response) {
                    int statusCode = response.code();
                    if (response.body().getObj() != null && response.body().getObj().size() > 0) {
                        interestList.clear();
                        interestList.addAll(response.body().getObj());
                        UserObj userObj = ArtalentApplication.prefManager.getUserObj();
                        if (userObj != null && userObj.getInterest() != null && userObj.getInterest().size() > 0) {
                            for (InterestObj interestListObj : interestList) {
                                for (InterestObj interestListObj1 : userObj.getInterest()) {
                                    if (interestListObj1.getInterestMasterId() == interestListObj.getInterestMasterId()) {
                                        interestListObj.setCheck(true);
                                    }
                                }
                            }
                        }
                        setAdapter();
                    }
                }

                @Override
                public void onFailure(Call<InterestResponse> call, Throwable t) {
                    // Log error here since request failed
                    Log.e("TAG", t.toString());
                }
            });
        }
    }

    private void setAdapter() {
        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        selectCountryAdapter = new SelectInterestAdapter(mActivity, interestList, this);
        selectCountryAdapter.notifyDataSetChanged();
        recyclerView.setAdapter(selectCountryAdapter);
    }


    public void initializeUi() {
        recyclerView = findViewById(R.id.recyclerView);
        talentHeadline = findViewById(R.id.talent_headline);
        save = findViewById(R.id.save);
        save.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.save:
                if (isEditInterestActivity) {
                    openBottomSheet();
                } else {
                    boolean isCheck = false;
                    for (InterestObj country : interestList) {
                        if (country.isCheck()) {
                            isCheck = true;
                        }
                    }
                    if (isCheck) {
                        callLoginApi();

                    } else {
                        Utils.showToast(mActivity, "Please Select Interest");
                    }

                }

                break;
            case R.id.llMain:
                if (v.getTag() instanceof InterestObj) {
                    InterestObj country = (InterestObj) v.getTag();
                    if (country != null) {
                        if (country.isCheck()) {
                            country.setCheck(false);
                        } else {
                            country.setCheck(true);
                        }
                        selectCountryAdapter.notifyDataSetChanged();
                    }
                }

                break;
        }
    }

    private void callLoginApi() {
        ArtalentApplication.apiService.updateInterests(ArtalentApplication.prefManager.getUserObj().getId(), fillData())
                .enqueue(new Callback<BaseResponse>() {
                    @Override
                    public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                        int statusCode = response.code();
                        if (response.body() != null) {
                            getUserData();
                        } else {
                            finishAffinity();
                        }
                    }

                    @Override
                    public void onFailure(Call<BaseResponse> call, Throwable t) {
                        // Log error here since request failed
                        Log.e("TAGArtalent", t.toString());
                        Utils.showToast(mActivity, "onFailure : " + t.toString());
                    }
                });
    }

    private InterestReq fillData() {
        InterestReq createReq = new InterestReq();
        List<InterestReqObj> interestListReqObjs = new ArrayList<>();
        for (InterestObj country : interestList) {
            if (country.isCheck()) {
                InterestReqObj interestListReqObj = new InterestReqObj();
                interestListReqObj.setId(country.getInterestMasterId());
                interestListReqObj.setInterest(country.getInterest());
                interestListReqObjs.add(interestListReqObj);
            }
        }
        createReq.setObj(interestListReqObjs);
        return createReq;
    }

    private void getUserData() {
        if (Utils.isInternetAvailable(mActivity)) {
            ArtalentApplication.apiService.getOtherUserDetails(ArtalentApplication.prefManager.getUserObj().getId()).enqueue(new Callback<UserResponse>() {
                @Override
                public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                    int statusCode = response.code();
                    if (response.body() != null && response.body().getObj() != null) {
                        if (!isRegister) {
                            UserObj userObj = response.body().getObj();
                            ArtalentApplication.prefManager.setUserObj(userObj);
                            Intent output = new Intent();
                            interestListObj.setInterestObjList(ArtalentApplication.prefManager.getUserObj().getInterest());
                            output.putExtra(Constants.INTEREST_OBJ, interestListObj);
                            setResult(RESULT_OK, output);
                            finish();
                        } else {
                            UserObj userObj = response.body().getObj();
                            ArtalentApplication.prefManager.setUserObj(userObj);
                            MainActivity.startActivity(mActivity);
                            finishAffinity();
                        }

                    } else {
                        finishAffinity();
                    }
                }

                @Override
                public void onFailure(Call<UserResponse> call, Throwable t) {
                    // Log error here since request failed
                    Log.e("TAG", t.toString());
                }
            });
        }
    }
}
