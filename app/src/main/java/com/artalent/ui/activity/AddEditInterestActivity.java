package com.artalent.ui.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.artalent.ArtalentApplication;
import com.artalent.R;
import com.artalent.Utils;
import com.artalent.ui.adapter.SelectInterestAdapter;
import com.artalent.ui.fragment.AddInterestFragment;
import com.artalent.ui.model.BaseResponse;
import com.artalent.ui.model.InterestListObj;
import com.artalent.ui.model.InterestObj;
import com.artalent.ui.model.InterestResponse;
import com.artalent.ui.model.UserObj;
import com.artalent.ui.model.UserResponse;
import com.artalent.ui.model.request.InterestReq;
import com.artalent.ui.model.request.InterestReqObj;
import com.artalent.ui.utils.Constants;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by Mahajan on 16-09-2019.
 */

public class AddEditInterestActivity extends BaseActivityOther implements View.OnClickListener {

    RecyclerView recyclerView;
    TextView talentHeadline;
    FloatingActionButton save;
    SelectInterestAdapter selectCountryAdapter;
    List<InterestObj> interestList = new ArrayList<>();
    InterestListObj interestListObj = new InterestListObj();
    private static boolean isRegister = false;
    private static boolean isEditInterestActivity;
    private InterestObj selectedInterest = null;

    public static void startActivityFromEdit(Activity mActivity, InterestListObj interestObj, int requestCode, boolean isRegisterActivity) {
        isRegister = isRegisterActivity;
        Intent intent = new Intent(mActivity, AddEditInterestActivity.class);
        intent.putExtra(Constants.INTEREST_OBJ, interestObj);
        mActivity.startActivityForResult(intent, requestCode);

    }

    @Override
    protected void onResume() {
        super.onResume();
        callInterestApi();
    }

    public static void startActivity(Activity mActivity, boolean isEditActivity) {
        isEditInterestActivity = isEditActivity;
        Intent intent = new Intent(mActivity, AddEditInterestActivity.class);
        mActivity.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_interest);

        setupToolbarBack(getString(R.string.manage_interest));
        UserObj userObj = ArtalentApplication.prefManager.getUserObj();
        String firstName = userObj.getFirstName();
        String lastName = userObj.getLastName();
        String fullName = firstName + " " + lastName;
        initializeUi();
        if (isEditInterestActivity) save.setImageResource(R.drawable.ic_add_box_blackwhite_24dp);
        talentHeadline.setText("Hi " + fullName + ", " + getResources().getString(R.string.talent_heading));
    }

    TextView textViewBottomSheetState;

    @Override
    protected void onStart() {
        super.onStart();
        callInterestApi();
    }

    BottomSheetBehavior bottomSheetBehavior;
    private static final String TAG = "SelectInterestActivity";


    public void openBottomSheet(InterestObj selectedInterestAdded) {
        AddInterestFragment addInterestFragment = new AddInterestFragment(mActivity, selectedInterestAdded);
        addInterestFragment.setOnProgressListener(new AddInterestFragment.ProgressCallback() {
            @Override
            public void startProgress() {
                showProgress();
            }

            @Override
            public void hideCircularProgress() {
                callInterestApi();
                hideProgress();
            }
        });
//        Bundle bundle = new Bundle();
//        bundle.putSerializable("RegisteredContest", (Serializable) objectContestList);
//        bottomSheetFragment.setArguments(bundle);
        addInterestFragment.show(getSupportFragmentManager(), addInterestFragment.getTag());

    }

    protected void callInterestApi() {
        if (Utils.isInternetAvailable(mActivity)) {
            Call<InterestResponse> call = ArtalentApplication.apiService.getInterest();
            call.enqueue(new Callback<InterestResponse>() {
                @Override
                public void onResponse(Call<InterestResponse> call, Response<InterestResponse> response) {
                    int statusCode = response.code();
                    if (response.body().getObj() != null && response.body().getObj().size() > 0) {
                        selectedInterest = null;
                        interestList.clear();
                        interestList.addAll(response.body().getObj());
                        Log.d(TAG, "onResponse: " + response.body().getObj().toString());
                        setAdapter();
                    }
                }

                @Override
                public void onFailure(Call<InterestResponse> call, Throwable t) {
                    // Log error here since request failed
                    Log.e("TAG", t.toString());
                }
            });
        }
    }

    private void setAdapter() {
        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        selectCountryAdapter = new SelectInterestAdapter(mActivity, interestList, this);
        selectCountryAdapter.notifyDataSetChanged();
        recyclerView.setAdapter(selectCountryAdapter);
    }


    public void initializeUi() {
        recyclerView = findViewById(R.id.recyclerView);
        talentHeadline = findViewById(R.id.talent_headline);
        save = findViewById(R.id.save);
        save.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.save:
                if (isEditInterestActivity) {
                    openBottomSheet(null);
                } else {
                    boolean isCheck = false;
                    for (InterestObj country : interestList) {
                        if (country.isCheck()) {
                            isCheck = true;
                        }
                    }
                    if (isCheck) {
                        callLoginApi();

                    } else {
                        Utils.showToast(mActivity, "Please Select Interest");
                    }

                }

                break;
            case R.id.llMain:
                if (v.getTag() instanceof InterestObj) {

                    InterestObj country = (InterestObj) v.getTag();
                    for (InterestObj interestObj : interestList) {
                        if (interestObj.getInterestMasterId() == country.getInterestMasterId()) {
                            interestObj.setCheck(!country.isCheck());
                            if (interestObj.isCheck()) {
                                selectedInterest = interestObj;
                            } else {
                                selectedInterest = null;
                            }
                        } else {
                            interestObj.setCheck(false);
                        }

                    }
                    selectCountryAdapter.notifyDataSetChanged();
                }

                break;
        }
    }

    private void callLoginApi() {
        ArtalentApplication.apiService.updateInterests(ArtalentApplication.prefManager.getUserObj().getId(), fillData())
                .enqueue(new Callback<BaseResponse>() {
                    @Override
                    public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                        int statusCode = response.code();
                        if (response.body() != null) {
                            getUserData();
                        } else {
                            finishAffinity();
                        }
                    }

                    @Override
                    public void onFailure(Call<BaseResponse> call, Throwable t) {
                        // Log error here since request failed
                        Log.e("TAGArtalent", t.toString());
                        Utils.showToast(mActivity, "onFailure : " + t.toString());
                    }
                });
    }

    private InterestReq fillData() {
        InterestReq createReq = new InterestReq();
        List<InterestReqObj> interestListReqObjs = new ArrayList<>();
        for (InterestObj country : interestList) {
            if (country.isCheck()) {
                InterestReqObj interestListReqObj = new InterestReqObj();
                interestListReqObj.setId(country.getInterestMasterId());
                interestListReqObj.setInterest(country.getInterest());
                interestListReqObjs.add(interestListReqObj);
            }
        }
        createReq.setObj(interestListReqObjs);
        return createReq;
    }

    private void getUserData() {
        if (Utils.isInternetAvailable(mActivity)) {
            ArtalentApplication.apiService.getOtherUserDetails(ArtalentApplication.prefManager.getUserObj().getId()).enqueue(new Callback<UserResponse>() {
                @Override
                public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                    int statusCode = response.code();
                    if (response.body() != null && response.body().getObj() != null) {
                        if (!isRegister) {
                            UserObj userObj = response.body().getObj();
                            ArtalentApplication.prefManager.setUserObj(userObj);
                            MainActivity.startActivity(mActivity);
                            finishAffinity();
                        } else {
                            Intent output = new Intent();
                            interestListObj.setInterestObjList(interestList);
                            output.putExtra(Constants.INTEREST_OBJ, interestListObj);
                            setResult(RESULT_OK, output);
                            finish();
                        }

                    } else {
                        finishAffinity();
                    }
                }

                @Override
                public void onFailure(Call<UserResponse> call, Throwable t) {
                    // Log error here since request failed
                    Log.e("TAG", t.toString());
                }
            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_manage_location, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_edit) {
            if (selectedInterest != null) {
                openBottomSheet(selectedInterest);
            } else {
                Utils.showToast(this, getString(R.string.select_interest));
            }
        } else if (item.getItemId() == R.id.action_delete) {
            if (selectedInterest != null) {
                open();
            } else {
                Utils.showToast(this, getString(R.string.select_interest));
            }
        }
        return super.onOptionsItemSelected(item);
    }


    public void open() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Are you sure, You want to delete interest?");
        alertDialogBuilder.setPositiveButton("yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        arg0.dismiss();
                        deleteInterest();
                    }
                });

        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void deleteInterest() {
        if (Utils.isInternetAvailable(mActivity)) {
            ArtalentApplication.apiService.deleteInterest(ArtalentApplication.prefManager.getUserObj().getId(), selectedInterest.getInterestMasterId())
                    .enqueue(new Callback<BaseResponse>() {
                        @Override
                        public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                            callInterestApi();
                        }

                        @Override
                        public void onFailure(Call<BaseResponse> call, Throwable t) {

                        }
                    });
        }
    }
}
