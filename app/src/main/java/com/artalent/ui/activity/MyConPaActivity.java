package com.artalent.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.artalent.R;
import com.artalent.ui.adapter.AdvertisePanelTabAdapter;
import com.artalent.ui.fragment.MySpecialContestPostFragment;
import com.artalent.ui.utils.Constants;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;

import butterknife.BindView;

public class MyConPaActivity extends BaseActivityOther implements View.OnClickListener {

    @BindView(R.id.add_advertise_fab)
    FloatingActionButton addAdvertise;

    AdvertisePanelTabAdapter adapter;

    private int userID = 0;

    public static void startActivity(Activity mActivity, int userID) {
        Intent intent = new Intent(mActivity, MyConPaActivity.class);
        intent.putExtra(Constants.ID, userID);
        mActivity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_contest_feed);
        userID = getIntent().getIntExtra(Constants.ID, 0);
        addAdvertise.setOnClickListener(this);
        addAdvertise.setVisibility(View.GONE);
        //setupToolbarBack(getString(R.string.contest_feed));
        setupToolbarBack(getString(R.string.contest_feed), R.drawable.ic_arrow_back_white_24dp, R.color.primary, R.color.black);

        ViewPager viewPager = findViewById(R.id.viewPager);
        TabLayout tabLayout = findViewById(R.id.tabLayout);

        MyConPostFragment myConPostFragment = new MyConPostFragment();
        MySpecialContestPostFragment myConPostFragmentGlobal = new MySpecialContestPostFragment();

        adapter = new AdvertisePanelTabAdapter(getSupportFragmentManager(), FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        adapter.addFragment(myConPostFragment, "Contest");
        myConPostFragment.setType(userID);
        adapter.addFragment(myConPostFragmentGlobal, "Special");
        myConPostFragmentGlobal.setType(userID);
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.add_advertise_fab) {
            Intent intent = new Intent(getApplicationContext(), AddSpecialContestActivity.class);
            startActivity(intent);
        }
    }
}
