package com.artalent.ui.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.RecyclerView
import com.artalent.R
import com.artalent.ui.adapter.LevelInfoAdapter
import com.artalent.ui.model.LevelInfoModel

class LevelInfoActivity : BaseActivityOther() {

    lateinit var rvLevelInfo: RecyclerView
    var levelInfoModelList: MutableList<LevelInfoModel> = mutableListOf()
    lateinit var levelInfoAdapter: LevelInfoAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_level_info)

        setupToolbarBack(
            "Level Information",
            R.drawable.ic_vector_arrow_back_black,
            R.color.black,
            R.color.white
        )

        init()
        setUpAdapter()
    }

    private fun setUpAdapter() {
        levelInfoAdapter = LevelInfoAdapter(levelInfoModelList)
        rvLevelInfo.adapter = levelInfoAdapter
    }

    private fun init() {
        rvLevelInfo = findViewById(R.id.rv_level_info)

        levelInfoModelList.add(LevelInfoModel("Level 1", "₹ 10", "0-50"))
        levelInfoModelList.add(LevelInfoModel("Level 2", "₹ 20", "51-100"))
        levelInfoModelList.add(LevelInfoModel("Level 3", "₹ 30", "101-200"))
        levelInfoModelList.add(LevelInfoModel("Level 4", "₹ 40", "201-500"))
        levelInfoModelList.add(LevelInfoModel("Level 5", "₹ 50", "above 500"))
    }
}