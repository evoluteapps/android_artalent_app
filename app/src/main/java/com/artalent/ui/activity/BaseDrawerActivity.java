package com.artalent.ui.activity;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.artalent.ui.model.BaseResponse;
import com.artalent.ui.roomdb.viewmodel.DraftViewModel;
import com.artalent.ui.utils.Constants;
import com.artalent.ui.video.StoryViewActivity;
import com.google.android.material.navigation.NavigationView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.ViewModelProvider;

import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.artalent.ArtalentApplication;
import com.artalent.R;
import com.artalent.Utils;

import com.artalent.ui.dialog.RateDialog;
import com.artalent.ui.model.UserObj;

import butterknife.BindDimen;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Miroslaw Stanek on 15.07.15.
 */
@SuppressLint("WrongConstant")
public class BaseDrawerActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.drawerLayout)
    DrawerLayout drawerLayout;

    @BindView(R.id.vNavigation)
    NavigationView vNavigation;

    @BindDimen(R.dimen.dimen_60)
    int avatarSize;

    @BindString(R.string.user_profile_photo)
    String profilePhoto;

    //Cannot be bound via Butterknife, hosting view is initialized later (see setupHeader() method)
    private ImageView ivMenuUserProfilePhoto;
    private TextView userFullName;

    @BindView(R.id.tvUserName)
    TextView tvUserName;

    @BindView(R.id.tvUserType)
    TextView tvUserType;

    @BindView(R.id.llAdvertise)
    LinearLayout llAdvertise;

    @BindView(R.id.llAdmin)
    LinearLayout llAdmin;

    @BindView(R.id.llDraft)
    LinearLayout llDraft;

    private DraftViewModel draftViewModel;


    @Override
    public void setContentView(int layoutResID) {
        super.setContentViewWithoutInject(R.layout.activity_drawer);
        ViewGroup viewGroup = findViewById(R.id.flContentRoot);
        LayoutInflater.from(this).inflate(layoutResID, viewGroup, true);
        bindViews();
        setupHeader();
    }

    @Override
    protected void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        draftViewModel = new ViewModelProvider(this).get(DraftViewModel.class);
    }

    public void setUserData() {
        UserObj userObj = ArtalentApplication.prefManager.getUserObj();

        if (userObj != null && !TextUtils.isEmpty(userObj.getUserName())) {
            tvUserName.setText("#" + userObj.getUserName());
        } else {
            tvUserName.setText("");
        }

        if (userObj != null) {
            userFullName.setText(userObj.getFirstName() + " " + userObj.getLastName());
            Utils.setImageFromURL(userObj.getProfileImage(), avatarSize, ivMenuUserProfilePhoto);
            if (userObj.getUserType() == 0) {
                tvUserType.setVisibility(View.GONE);
                llAdmin.setVisibility(View.GONE);
            } else {
                tvUserType.setVisibility(View.VISIBLE);
                llAdmin.setVisibility(View.VISIBLE);
                if (userObj.getUserType() == 2) {
                    tvUserType.setText(this.getString(R.string.admin));
                } else {
                    tvUserType.setText(this.getString(R.string.subadmin));
                }
            }
        }
    }

    @Override
    protected void setupToolbar() {
        super.setupToolbar();
        if (getToolbar() != null) {
            getToolbar().setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    drawerLayout.openDrawer(Gravity.LEFT);

                }
            });
        }
    }

    private void setupHeader() {
        tvUserType.setVisibility(View.GONE);
        llAdmin.setVisibility(View.GONE);
        vNavigation.setNavigationItemSelectedListener(this);
        vNavigation.setNavigationItemSelectedListener(this);
        ivMenuUserProfilePhoto = findViewById(R.id.ivMenuUserProfilePhoto);
        userFullName = findViewById(R.id.user_full_name);
        findViewById(R.id.vGlobalMenuHeader).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onGlobalMenuHeaderClick(v);
            }
        });
        setUserData();
    }

    public void onGlobalMenuHeaderClick(final View v) {
        drawerLayout.closeDrawer(Gravity.LEFT);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(BaseDrawerActivity.this, EditProfileActivity.class);
                startActivity(intent);

            }
        }, 200);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        /*if (id == R.id.menu_message) {//DO your stuff }
            Toast.makeText(BaseDrawerActivity.this, "tototo", Toast.LENGTH_LONG).show();
            drawerLayout.closeDrawer(Gravity.LEFT);
        }*/
        return false;
    }


    @OnClick(R.id.llMyWallet)
    public void onMyWalletClick() {
        drawerLayout.closeDrawer(Gravity.START);
        Intent intent = new Intent(BaseDrawerActivity.this, ArtalentWalletActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.llAdvertise)
    public void onAdvertiseClick() {
        drawerLayout.closeDrawer(Gravity.START);
        Intent intent = new Intent(BaseDrawerActivity.this, AdvertisePanelActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.llInviteFriends)
    public void onInviteFriendsClick() {
        String mShare = "Hey! Check out the Artalent app.\n" +
                "\n" +
                "Download the app at :\n" +
                "http://www.fusionik.in";
        drawerLayout.closeDrawer(Gravity.START);
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, mShare);
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, "Share via"));

    }

    @OnClick(R.id.llMessages)
    public void onMessagesClick() {
        drawerLayout.closeDrawer(Gravity.START);
        Intent intent = new Intent(BaseDrawerActivity.this, UserChatListActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.llMuxing)
    public void onMuxingClick() {
        drawerLayout.closeDrawer(Gravity.START);
        /*Intent intent = new Intent(BaseDrawerActivity.this, AudioVideoMuxing.class);
        startActivity(intent);*/
    }

    @OnClick(R.id.llReferEarn)
    public void onReferEarnClick() {
        drawerLayout.closeDrawer(Gravity.START);
        startActivity(new Intent(getApplicationContext(), ReferEarnActivity.class));
    }

    @OnClick(R.id.llRateUs)
    public void onRateUsClick() {
        drawerLayout.closeDrawer(Gravity.START);
        RateDialog rateDialog = new RateDialog(this, getResources().getString(R.string.rate_dialog_title),
                getResources().getString(R.string.rate_dialog_description), false);

        rateDialog.show();
    }

    @OnClick(R.id.llSettings)
    public void onSettingsClick() {
        drawerLayout.closeDrawer(Gravity.START);
    }

    @OnClick(R.id.llAbout)
    public void onAboutClick() {
        drawerLayout.closeDrawer(Gravity.START);
        Intent intent1 = new Intent(BaseDrawerActivity.this, AboutUsActivity.class);
        startActivity(intent1);
    }

    @OnClick(R.id.llMusicLibrary)
    public void onMusicLibraryClick() {
        drawerLayout.closeDrawer(Gravity.START);
        Intent intent1 = new Intent(BaseDrawerActivity.this, MusicLibraryAdminActivity.class);
        intent1.putExtra(Constants.FROM, true);
        startActivity(intent1);
    }

    @OnClick(R.id.llDraft)
    public void onDraftClick() {
        startActivity(new Intent(BaseDrawerActivity.this, ShowDraftsActivity.class));
    }

    @OnClick(R.id.llLogout)
    public void onLogoutClick() {

        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Confirm Logout");
        builder.setMessage("Are you sure you want to logout?");

        // add the buttons
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                drawerLayout.closeDrawer(Gravity.START);
                draftViewModel.clearAllSavedDrafts();
                deleteFirebaseToken();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                drawerLayout.closeDrawer(Gravity.START);
            }
        });

        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @OnClick(R.id.llInterest)
    public void onManageInterestClick() {
        AddEditInterestActivity.startActivity(this, true);
    }

    @OnClick(R.id.llLocation)
    public void onManageLocationsClick() {
        SelectCountryActivity.startActivityFromHome(this, null, 1);
    }

    private void deleteFirebaseToken() {
        if (Utils.isInternetAvailable(BaseDrawerActivity.this)) {
            ArtalentApplication.apiService.deleteFirebaseToken(ArtalentApplication.prefManager.getUserObj().getId(), ArtalentApplication.prefManager.getUserObj().getId())
                    .enqueue(new Callback<BaseResponse>() {
                        @Override
                        public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                            dataClear();
                        }

                        @Override
                        public void onFailure(Call<BaseResponse> call, Throwable t) {
                            dataClear();
                        }
                    });
        } else {
            dataClear();
        }
    }

    private void dataClear() {
        ArtalentApplication.prefManager.setAuthorizationToken("");
        ArtalentApplication.prefManager.setFCMToken(false);
        Intent intent1 = new Intent(BaseDrawerActivity.this, LoginActivity.class);
        startActivity(intent1);
        finishAffinity();
    }


    @OnClick(R.id.llSpecialContest)
    public void llSpecialContest() {
        drawerLayout.closeDrawer(Gravity.START);
        Intent intent = new Intent(BaseDrawerActivity.this, SpecialContestPanelActivity.class);
        startActivity(intent);
    }


    @OnClick(R.id.llTest)
    public void llTest() {
        drawerLayout.closeDrawer(Gravity.START);

        /*FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        AutoPlayVideoActivity autoPlayVideoActivity = new AutoPlayVideoActivity();
        transaction.add(R.id.container,autoPlayVideoActivity,"AutoPlayVideoFragment");
        transaction.addToBackStack(null);
        transaction.commit();*/
        Intent intent = new Intent(BaseDrawerActivity.this, StoryViewActivity.class);
        //Intent intent = new Intent(BaseDrawerActivity.this, VideoCompressorActivity.class);
        startActivity(intent);
    }


}
