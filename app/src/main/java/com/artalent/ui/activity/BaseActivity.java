package com.artalent.ui.activity;

import android.content.SharedPreferences;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.preference.PreferenceManager;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.artalent.R;
import com.artalent.ui.progressbar.ChromeFloatingCirclesDrawable;
import com.artalent.ui.progressbar.FoldingCirclesDrawable;
import com.artalent.ui.progressbar.GoogleMusicDicesDrawable;
import com.artalent.ui.progressbar.NexusRotationCrossDrawable;

import butterknife.ButterKnife;
import butterknife.BindView;


/**
 * Created by Miroslaw Stanek on 19.01.15.
 */
public class BaseActivity extends AppCompatActivity {

    @Nullable
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Nullable
    @BindView(R.id.ivLogo)
    TextView ivLogo;

    private MenuItem inboxMenuItem;

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        bindViews();
    }

    protected void bindViews() {
        ButterKnife.bind(this);
        setupToolbar();
    }

    public void setContentViewWithoutInject(int layoutResId) {
        super.setContentView(layoutResId);
    }

    protected void setupToolbar() {
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_menu_primary_24dp);
        }
    }

    protected void setupToolbarBack() {
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            if(getSupportActionBar() != null)
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
    }



    public Toolbar getToolbar() {
        return toolbar;
    }

    public MenuItem getInboxMenuItem() {
        return inboxMenuItem;
    }

    public TextView getIvLogo() {
        return ivLogo;
    }
    /*
     start progress
      */

    ProgressBar mProgressBar;
    RelativeLayout rlProgress;

    private static final int FOLDING_CIRCLES = 0;
    private static final int MUSIC_DICES = 1;
    private static final int NEXUS_CROSS_ROTATION = 2;
    private static final int CHROME_FLOATING_CIRCLES = 3;
    public void showProgress() {
        if (mProgressBar == null) {
            mProgressBar = findViewById(R.id.google_progress);
        }
        if (rlProgress == null) {
            rlProgress = findViewById(R.id.rlProgress);
        }
        if (mProgressBar != null) {
            Rect bounds = mProgressBar.getIndeterminateDrawable().getBounds();
            mProgressBar.setIndeterminateDrawable(getProgressDrawable());
            mProgressBar.getIndeterminateDrawable().setBounds(bounds);
            if (rlProgress != null) {
                rlProgress.setVisibility(View.VISIBLE);
            }
        }

    }

    public void hideProgress() {
        if (rlProgress != null) {
            rlProgress.setVisibility(View.GONE);
        }
    }

    private Drawable getProgressDrawable() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        int value = Integer.parseInt(prefs.getString(getString(R.string.progressBar_pref_key), getString(R.string.progressBar_pref_defValue)));
        Drawable progressDrawable = null;
        switch (value) {
            case FOLDING_CIRCLES:
                progressDrawable = new FoldingCirclesDrawable.Builder(this)
                        .colors(getProgressDrawableColors())
                        .build();
                break;

            case MUSIC_DICES:
                progressDrawable = new GoogleMusicDicesDrawable.Builder().build();
                break;

            case NEXUS_CROSS_ROTATION:
                progressDrawable = new NexusRotationCrossDrawable.Builder(this)
                        .colors(getProgressDrawableColors())
                        .build();
                break;

            case CHROME_FLOATING_CIRCLES:
                progressDrawable = new ChromeFloatingCirclesDrawable.Builder(this)
                        .colors(getProgressDrawableColors())
                        .build();
                break;
        }

        return progressDrawable;
    }

    private int[] getProgressDrawableColors() {
        int[] colors = new int[4];
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        colors[0] = prefs.getInt(getString(R.string.firstcolor_pref_key), getResources().getColor(R.color.red));
        colors[1] = prefs.getInt(getString(R.string.secondcolor_pref_key), getResources().getColor(R.color.blue));
        colors[2] = prefs.getInt(getString(R.string.thirdcolor_pref_key), getResources().getColor(R.color.yellow));
        colors[3] = prefs.getInt(getString(R.string.fourthcolor_pref_key), getResources().getColor(R.color.green));
        return colors;
    }

    /*
    end progress
     */

}
