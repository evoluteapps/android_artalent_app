package com.artalent.ui.activity;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.ThumbnailUtils;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.artalent.ArtalentApplication;
import com.artalent.R;
import com.artalent.Utils;
import com.artalent.ui.adapter.SelectedPostAdapter;
import com.artalent.ui.contestcamera.CameraActivity;
import com.artalent.ui.model.BaseResponse;
import com.artalent.ui.model.ContestObj;
import com.artalent.ui.model.ContestRegistrationObj;
import com.artalent.ui.model.InterestObj;
import com.artalent.ui.model.SpecialContestObj;
import com.artalent.ui.model.UserObj;
import com.artalent.ui.model.room.ContestVideoDraft;
import com.artalent.ui.rest.ApiClient;
import com.artalent.ui.roomdb.viewmodel.DraftViewModel;
import com.artalent.ui.utils.CircleTransformation;
import com.artalent.ui.utils.Constants;
import com.artalent.ui.utils.ImageQuality;
import com.artalent.ui.utils.Options;
import com.artalent.ui.utils.PermUtil;
import com.artalent.ui.video.VideoConstants;
import com.squareup.picasso.Picasso;

import net.gotev.uploadservice.protocols.multipart.MultipartUploadRequest;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Mahajan
 */
public class AddPostActivity extends BaseActivityOther implements View.OnClickListener {

    private DraftViewModel draftViewModel;
    //    private boolean isDraftSaveBtnClicked = false;
    private String finalVideoUrl = null;
    private int savedDraftCount;

    private String titleForDb = "";
    private String noteForDb = "";

    //add post activity
    @BindView(R.id.ivUserProfilePhoto)
    ImageView ivUserProfilePhoto;

    @BindView(R.id.editTextDes)
    EditText editTextDes;

    @BindView(R.id.rlPhoto)
    RelativeLayout rlPhoto;

    @BindView(R.id.recyclerViewMyPost)
    RecyclerView recyclerViewMyPost;

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @BindView(R.id.user_full_name)
    TextView userFullName;

    @BindView(R.id.user_interest)
    TextView userInterest;

    @BindView(R.id.divider_top)
    View dividerTop;

    @BindView(R.id.divider)
    View divider;

    @BindView(R.id.ivContestVideo)
    ImageView ivContestVideo;

    private SelectedPostAdapter adapter;

    private final String TAG = "AddPostActivity";
    private long startTime, endTime;

    private int avatarSize;
    private Options options;
    ArrayList<String> returnValue = new ArrayList<>();
    private int contestRegistrationId;

    private UserObj userObj = ArtalentApplication.prefManager.getUserObj();
    private int userId = userObj.getId();
    private boolean isContestPost;
    SpecialContestObj selectedSpecialContestObj = null;
    private File contestVideo = null;

    public boolean isFromBottomSheetWithDraft = false;
    private ContestVideoDraft contestVideoDraft = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MainActivity.isContestPost = true;
        Intent intent = getIntent();

        draftViewModel = new ViewModelProvider(this).get(DraftViewModel.class);

        draftViewModel.getSavedDraftCount().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                savedDraftCount = integer;
            }
        });

        if (intent != null) {
            isContestPost = intent.getBooleanExtra(Constants.CONTEST_POST, false);
            contestVideoDraft = (ContestVideoDraft) intent.getSerializableExtra("DraftObject");
            if (contestVideoDraft != null) {
                isFromBottomSheetWithDraft = true;
            }
        }
        if (isFromBottomSheetWithDraft) {
            Log.d("Draft", "AddPostActivity -> onCreate: " + isFromBottomSheetWithDraft);
            Log.d("Draft", "AddPostActivity -> onCreate: " + contestVideoDraft.toString());
        }

        if (isContestPost) {
            setContentView(R.layout.activity_add_contest_post);
            if (intent != null) {
                if (intent.getSerializableExtra("contestObj") instanceof ContestObj) {
                    setupToolbarBack(getString(R.string.add_contest_post), R.drawable.ic_arrow_back_white_24dp, R.color.primary, R.color.black);

                    ContestObj contestObj = (ContestObj) intent.getSerializableExtra("contestObj");
                    for (ContestRegistrationObj contestRegistrationObj : contestObj.getRegistrationDetails()) {
                        if (userId == contestRegistrationObj.getUser().getId()) {
                            contestRegistrationId = contestRegistrationObj.getRegisterationId();
                        }
                    }
                } else if (intent.getSerializableExtra("contestObj") instanceof SpecialContestObj) {
                    divider.setBackgroundColor(getResources().getColor(R.color.star_color));
                    dividerTop.setBackgroundColor(getResources().getColor(R.color.star_color));
                    setupToolbarSpecialBack(getString(R.string.add_special_post), R.drawable.ic_arrow_back_white_24dp, R.color.black);
                    selectedSpecialContestObj = (SpecialContestObj) intent.getSerializableExtra("contestObj");
                    contestRegistrationId = selectedSpecialContestObj.getUserContestRegistrationId();
                }
            }
        } else {
            setContentView(R.layout.activity_add_post);
            setupToolbarBack(getString(R.string.add_post));
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAlertForDraft();
                Log.d("Draft", "AddPostActivity: Toolbar back Pressed ");
            }
        });

        returnValue = new ArrayList<>();

//        if (isFromBottomSheetWithDraft) {
//            returnValue.add(contestVideoDraft.getStoragePath());
//        }

        avatarSize = getResources().getDimensionPixelSize(R.dimen.dimen_90);
        setUserData();
        editTextDes.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                showKeyboard();
            }
        });
        showKeyboard();
        options = Options.init()
                .setRequestCode(100)
                .setCount(3)
                .setFrontfacing(false)
                .setImageQuality(ImageQuality.LOW)
                .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)
                .setPath("/akshay/new");

        adapter = new SelectedPostAdapter(mActivity, returnValue, this);
        //StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
        recyclerViewMyPost.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        recyclerViewMyPost.setItemAnimator(new DefaultItemAnimator());
        recyclerViewMyPost.setAdapter(adapter);

        if (isFromBottomSheetWithDraft) {
            Log.d("Draft", "AddPostActivity -> if isFromDraftAutoPlayVideoFragment then: this");

            setUpForDraftVideo();
        } else {
            if (!isContestPost) {
                Log.d("Draft", "AddPostActivity -> if !isContestPost then: AppCameraActivity");
                AppCameraActivity.start(AddPostActivity.this, options);
            } else {
                goCameraVideoActivity();
            }
        }
    }

    /*@Override
    protected void onResume() {
        super.onResume();
        Utils.showToast(mActivity, "contestRegistrationId : " + contestRegistrationId);
    }*/

    public void showProgressDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(AddPostActivity.this);
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.custom_progress_layout, viewGroup, false);
        builder.setView(dialogView);

        ProgressBar progressBar = dialogView.findViewById(R.id.progress);
        TextView percentTV = dialogView.findViewById(R.id.progress_percentage);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public void setUserData() {
        UserObj userObj = ArtalentApplication.prefManager.getUserObj();
        if (userObj != null) {
            String fName = userObj.getFirstName();
            String lName = userObj.getLastName();
            StringBuilder interestBuilder = new StringBuilder();
            String interests;
            List<InterestObj> interestObjList = userObj.getInterest();

            int i = 0;
            for (InterestObj interestObj : interestObjList) {

                if (i++ == interestObjList.size() - 1) {
                    // Last iteration
                    interestBuilder.append(interestObj.getInterest());
                } else {
                    interestBuilder.append(interestObj.getInterest()).append(", ");
                }
            }
            interests = interestBuilder.toString();

            userFullName.setText(fName + " " + lName);
            userInterest.setText(interests);

            if (!TextUtils.isEmpty(userObj.getProfileImage())) {
                Picasso.get()
                        .load(userObj.getProfileImage())
                        .placeholder(R.drawable.man)
                        .resize(avatarSize, avatarSize)
                        .centerCrop()
                        .transform(new CircleTransformation())
                        .into(ivUserProfilePhoto);
            } else {
                Picasso.get()
                        .load(R.drawable.man)
                        .placeholder(R.drawable.man)
                        .resize(avatarSize, avatarSize)
                        .centerCrop()
                        .transform(new CircleTransformation())
                        .into(ivUserProfilePhoto);
            }

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == PermUtil.REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS) {// If request is cancelled, the result arrays are empty.
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                AppCameraActivity.start(AddPostActivity.this, options, 1);
            } else {
                Toast.makeText(mActivity, "Approve permissions to open Pix ImagePicker", Toast.LENGTH_LONG).show();
            }
        }
    }

    public void showKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        View v = getCurrentFocus();
        if (v != null)
            imm.showSoftInput(v, 0);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_post, menu);
        if (isContestPost) {
            Log.e("isContestPost", "true");
            for (int i = 0; i < menu.size(); i++) {
                MenuItem item = menu.getItem(i);
                SpannableString spanString = new SpannableString(menu.getItem(i).getTitle().toString());
                spanString.setSpan(new ForegroundColorSpan(Color.WHITE), 0, spanString.length(), 0); //fix the color to white
                item.setTitle(spanString);
            }
        }
        return true;
    }

    private boolean isClick = false;

    @SuppressWarnings("deprecation")
    public static Locale getSystemLocaleLegacy(Configuration config) {
        return config.locale;
    }

    @TargetApi(Build.VERSION_CODES.N)
    public static Locale getSystemLocale(Configuration config) {
        return config.getLocales().get(0);
    }

    private Locale getLocale() {
        Configuration config = getResources().getConfiguration();
        Locale sysLocale = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            sysLocale = getSystemLocale(config);
        } else {
            sysLocale = getSystemLocaleLegacy(config);
        }

        return sysLocale;
    }

    String destPath = "";
    // String inputPath = "";
    String mergeVideoPath = "";

    public void compressVideo() {
        showProgress();
        callCreatePostApiVideo(mergeVideoPath, thumbImage);
    }

    @Override
    public void onBackPressed() {
        Log.d("Draft", "AddPostActivity: onBackPressed ");
        showAlertForDraft();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_add) {
            if (isContestPost) {

//                setUpForDraftVideo();

                if (contestVideo != null) {
                    compressVideo();
                } else {
                    Toast.makeText(mActivity, "please add contest video", Toast.LENGTH_LONG).show();
                }
            } else {
                if (returnValue != null && returnValue.size() > 0) {
                    callCreatePostApi();
                } else {
                    Toast.makeText(mActivity, "please add photo", Toast.LENGTH_LONG).show();
                }
            }

        }
        return super.onOptionsItemSelected(item);
    }

    private void setUpForDraftVideo() {

        if (isFromBottomSheetWithDraft) {
            Log.d(TAG, "setUpForDraftVideo: Entering");

            if (contestVideoDraft != null && !(contestVideoDraft.getStoragePath().isEmpty())) {
                Log.d(TAG, "setUpForDraftVideo: 1");
                contestVideo = new File(contestVideoDraft.getStoragePath());
                mergeVideoPath = contestVideoDraft.getStoragePath();

                try {
                    Log.d(TAG, "setUpForDraftVideo: 2");
                    if (mergeVideoPath.contains("/storage/emulated/0")) {
                        Log.d(TAG, "setUpForDraftVideo: 3");
                        File filenew = new File(mergeVideoPath);
                        if (filenew.exists()) {
                            Log.d(TAG, "setUpForDraftVideo: 4");
                            contestVideo = filenew;
                            Bitmap bMap = ThumbnailUtils.createVideoThumbnail(filenew.getAbsolutePath(), MediaStore.Video.Thumbnails.MICRO_KIND);
                            if (bMap != null) {
                                Log.d(TAG, "setUpForDraftVideo: 5");
                                saveImageFile(bMap);
                                if (ivContestVideo != null) {
                                    Log.d(TAG, "setUpForDraftVideo: 6");
                                    ivContestVideo.setImageBitmap(bMap);
                                }
                            }
                        }
                    } else {
                        Log.d(TAG, "setUpForDraftVideo: 7");
                        File filenew = new File(new URI(mergeVideoPath));
                        if (filenew.exists()) {
                            Log.d(TAG, "setUpForDraftVideo: 8");
                            contestVideo = filenew;
                            Bitmap bMap = ThumbnailUtils.createVideoThumbnail(filenew.getAbsolutePath(), MediaStore.Video.Thumbnails.MICRO_KIND);
                            if (bMap != null) {
                                Log.d(TAG, "setUpForDraftVideo: 9");
                                saveImageFile(bMap);
                                if (ivContestVideo != null) {
                                    Log.d(TAG, "setUpForDraftVideo: 10");
                                    ivContestVideo.setImageBitmap(bMap);
                                }
                            }
                        }
                    }

                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @OnClick(R.id.rlPhoto)
    public void onRLPhotoClick() {
        if (isContestPost) {
            goCameraVideoActivity();
        } else {
            AppCameraActivity.start(AddPostActivity.this, options);
            //openBottomSheet();
        }
    }

    private void goCameraVideoActivity() {
        Intent intent = new Intent(AddPostActivity.this, CameraActivity.class);
        startActivityForResult(intent, 200);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100) { // Please, use a final int instead of hardcoded int value
            if (resultCode == RESULT_OK) {
                if (data.getStringArrayListExtra(AppCameraActivity.IMAGE_RESULTS) != null && (data.getStringArrayListExtra(AppCameraActivity.IMAGE_RESULTS)).size() > 0) {
                    returnValue.addAll(data.getStringArrayListExtra(AppCameraActivity.IMAGE_RESULTS));
                    adapter = new SelectedPostAdapter(mActivity, returnValue, this);
                    recyclerViewMyPost.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
                    recyclerViewMyPost.setItemAnimator(new DefaultItemAnimator());
                    recyclerViewMyPost.setAdapter(adapter);
                } else {
                    finish();
                }
            }
        } else if (requestCode == 200) { // Please, use a final int instead of hardcoded int value
            if (resultCode == RESULT_OK) {
                // Uri videoURI = data.getData();
                mergeVideoPath = data.getStringExtra(VideoConstants.EXTRA_VIDEO_PATH);
                Log.e("draft", "Merge Video Path -> " + mergeVideoPath);
                try {
                    if (mergeVideoPath.contains("/storage/emulated/0")) {
                        File filenew = new File(mergeVideoPath);
                        if (filenew.exists()) {
                            contestVideo = filenew;
                            Bitmap bMap = ThumbnailUtils.createVideoThumbnail(filenew.getAbsolutePath(), MediaStore.Video.Thumbnails.MICRO_KIND);
                            if (bMap != null) {
                                saveImageFile(bMap);
                                if (ivContestVideo != null) {
                                    ivContestVideo.setImageBitmap(bMap);
                                }
                            }
                        }
                    } else {
                        File filenew = new File(new URI(mergeVideoPath));
                        if (filenew.exists()) {
                            contestVideo = filenew;
                            Bitmap bMap = ThumbnailUtils.createVideoThumbnail(filenew.getAbsolutePath(), MediaStore.Video.Thumbnails.MICRO_KIND);
                            if (bMap != null) {
                                saveImageFile(bMap);
                                if (ivContestVideo != null) {
                                    ivContestVideo.setImageBitmap(bMap);
                                }
                            }
                        }
                    }

                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private String thumbImage = "";

    public String saveImageFile(Bitmap bitmap) {
        FileOutputStream out = null;
        String filename = getFilename();
        try {
            out = new FileOutputStream(filename);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
            Log.e("Picasso123", filename);
            thumbImage = filename;
            //
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return filename;
    }

    private String getFilename() {
        File file = new File(Environment.getExternalStorageDirectory()
                .getPath(), "Artalent");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/art_"
                + System.currentTimeMillis() + ".jpg");
        return uriSting;
    }

    private void callCreatePostApi() {
        if (!isClick) {
            if (Utils.isInternetAvailable(mActivity)) {
                showProgress();
                isClick = true;
                MultipartBody.Part[] postImagesParts = new MultipartBody.Part[returnValue.size()];
                for (int index = 0; index < returnValue.size(); index++) {
                    Log.d("TAG", "requestUploadSurvey: survey image " + index + "  " + returnValue.get(index));
                    File file = new File(returnValue.get(index));
                    RequestBody postBody = RequestBody.create(MediaType.parse("image/*"), file);
                    postImagesParts[index] = MultipartBody.Part.createFormData("files", file.getName(), postBody);
                }
                String des = "-";
                if (!TextUtils.isEmpty(editTextDes.getText().toString().trim())) {
                    des = editTextDes.getText().toString().trim();
                }
                RequestBody description = RequestBody.create(MediaType.parse("multipart/form-data"), des);
                Callback<BaseResponse> baseResponseCallback = new Callback<BaseResponse>() {

                    @Override
                    public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                        int statusCode = response.code();
                        /*if (statusCode == 200) {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    hideProgress();
                                    isClick = false;
                                    finish();
                                }
                            }, 2000);

                        } else {
                            hideProgress();
                            isClick = false;
                        }*/
                        hideProgress();
                        isClick = false;
                        finish();
                    }

                    @Override
                    public void onFailure(Call<BaseResponse> call, Throwable t) {
                        // Log error here since request failed
                        hideProgress();
                        Log.e("TAGArtalent", t.toString());
                        Utils.showToast(mActivity, "onFailure : " + t.toString());
                        isClick = false;
                    }
                };

                /*if (isContestPost) {
                    RequestBody contestRegistrationId = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(this.contestRegistrationId));
                    Log.e("registrationId", "contestRegistrationId = " + this.contestRegistrationId);
                    ArtalentApplication.apiServicePost.createContestPost(userId,
                            postImagesParts,
                            description, contestRegistrationId)
                            .enqueue(baseResponseCallback);
                } else {*/
                ArtalentApplication.apiServicePost.createPost(userId,
                        postImagesParts,
                        description)
                        .enqueue(baseResponseCallback);
                //}
            }
        }
    }

    private void callCreatePostApiVideo(String file, String thumbFiles) {
        if (!isClick) {
            if (Utils.isInternetAvailable(mActivity)) {
                if (selectedSpecialContestObj != null) {
                    try {
                        String urlBase = ApiClient.BASE_URL + "admin/" + userId + "/adminContestPost/";
                        String des = "-";
                        if (!TextUtils.isEmpty(editTextDes.getText().toString().trim())) {
                            des = editTextDes.getText().toString().trim();
                        }
                        new MultipartUploadRequest(this, urlBase)
                                .setMethod("POST")
                                .addHeader("Authorization", "Bearer " + ArtalentApplication.prefManager.getAuthorizationToken())
                                .addParameter("description", des)
                                .addParameter("registrationId", "" + (selectedSpecialContestObj.getUserContestRegistrationId()))
                                .addFileToUpload(
                                        file,
                                        "files"
                                ).addFileToUpload(
                                thumbFiles,
                                "thumbFiles"
                        ).startUpload();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        String urlBase = ApiClient.BASE_URL + "userinfo/" + userId + "/ContestPost/";
                        String des = "-";
                        if (!TextUtils.isEmpty(editTextDes.getText().toString().trim())) {
                            des = editTextDes.getText().toString().trim();
                        }
                        new MultipartUploadRequest(this, urlBase)
                                .setMethod("POST")
                                .addHeader("Authorization", "Bearer " + ArtalentApplication.prefManager.getAuthorizationToken())
                                .addParameter("description", des)
                                .addParameter("registrationId", "" + (contestRegistrationId))
                                .addFileToUpload(
                                        file,
                                        "files"
                                ).addFileToUpload(
                                thumbFiles,
                                "thumbFiles"
                        ).startUpload();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }
            }
            Utils.showToast(mActivity, "video uploading in background");
            new Handler().postDelayed(() -> {
                hideProgress();
                if (isFromBottomSheetWithDraft) {
                    startActivity(new Intent(mActivity, MainActivity.class));
                    finishAffinity();
                } else {
                    finish();
                }
            }, 3000);
        }
    }

    private void callCreatePostApiVideo(File file) {
        if (!isClick) {
            if (Utils.isInternetAvailable(mActivity)) {
                isClick = true;
                MultipartBody.Part[] postImagesParts = new MultipartBody.Part[1];
                RequestBody postBody = RequestBody.create(MediaType.parse("video/mp4"), file);
                postImagesParts[0] = MultipartBody.Part.createFormData("files", file.getName(), postBody);
                RequestBody description = RequestBody.create(MediaType.parse("text/plain; charset=utf-8"), editTextDes.getText().toString().trim());
                Log.e("PostDescription", editTextDes.getText().toString().trim());

                Callback<BaseResponse> baseResponseCallback = new Callback<BaseResponse>() {

                    @Override
                    public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {

                        int statusCode = response.code();
                        hideProgress();
                        isClick = false;
                        finish();
                    }

                    @Override
                    public void onFailure(Call<BaseResponse> call, Throwable t) {
                        hideProgress();
                        Log.e("TAGArtalent", t.toString());
                        Utils.showToast(mActivity, "onFailure : " + t.toString());
                        isClick = false;
                    }
                };

                if (isContestPost) {
                    if (selectedSpecialContestObj != null) {
                        RequestBody contestRegistrationId = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(selectedSpecialContestObj.getUserContestRegistrationId()));
                        ArtalentApplication.apiServicePost.createSpecialContestPost(userId,
                                postImagesParts,
                                description, contestRegistrationId)
                                .enqueue(baseResponseCallback);
                    } else {

                        RequestBody contestRegistrationId = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(this.contestRegistrationId));
                        Log.e("registrationId", "contestRegistrationId = " + this.contestRegistrationId);
                        ArtalentApplication.apiServicePost.createContestPost(userId,
                                postImagesParts,
                                description, contestRegistrationId)
                                .enqueue(baseResponseCallback);

                    }

                }
            }
        }
    }

    private void showAlertForDraft() {
        if (mergeVideoPath != null && !TextUtils.isEmpty(mergeVideoPath)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Unsaved Video");
            builder.setMessage("Do you want to save it in draft?");

            // add the buttons
            builder.setPositiveButton("Save Draft", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    if (savedDraftCount < 5) {
                        openCustomAlertDialogForDb();
                    } else {
                        Log.d("Draft", "AddPostActivity: savedDraft =  " + savedDraftCount);
                        Utils.showToastWithTitle(mActivity, "Draft limit is over", "Unable to save");
                    }
                }
            });

            builder.setNegativeButton("Discard", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    finish();
                }
            });

            // create and show the alert dialog
            AlertDialog dialog = builder.create();
            dialog.show();
        } else {
            finish();
        }
    }

    private void openCustomAlertDialogForDb() {
        Log.d("Draft", "VideoTrimmerActivity : openCustomAlertDialogForDb");

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("Enter title and note for draft");

        // set the custom layout
        final View customLayout
                = getLayoutInflater()
                .inflate(
                        R.layout.db_custom_alert_dialog,
                        null);

        builder.setView(customLayout);

        // add the buttons
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                EditText titleEdt = customLayout
                        .findViewById(
                                R.id.title_editText);
                titleForDb = titleEdt.getText().toString().trim();

                EditText noteEdt = customLayout
                        .findViewById(
                                R.id.note_editText);
                noteForDb = noteEdt.getText().toString().trim();

                Date date = Calendar.getInstance().getTime();
                ContestVideoDraft contestVideoDraft = new ContestVideoDraft(ArtalentApplication.prefManager.getUserObj().getId(), titleForDb, noteForDb, mergeVideoPath, false, date, date);
                try {
                    draftViewModel.insert(contestVideoDraft);
                } catch (Exception e) {
                    Utils.showToast(mActivity, "Try Again!");
                    finish();
                }
                Toast.makeText(AddPostActivity.this, "Draft Saved Successfully", Toast.LENGTH_SHORT).show();
                Utils.showToast(AddPostActivity.this, "Draft Saved Successfully");
                Log.d(TAG, "onClick: draft saved successfully");
                finish();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });

        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.ivClose) {
            //returnValue
            int position = (int) v.getTag();
            if (position < returnValue.size()) {
                returnValue.remove(position);
                adapter.notifyDataSetChanged();
            }
        }
    }

}
