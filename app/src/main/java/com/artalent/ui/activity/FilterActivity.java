package com.artalent.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.artalent.ArtalentApplication;
import com.artalent.R;
import com.artalent.Utils;
import com.artalent.ui.adapter.FilterCountryAdapter;
import com.artalent.ui.adapter.FilterDistrictsAdapter;
import com.artalent.ui.adapter.FilterInterestAdapter;
import com.artalent.ui.adapter.FilterStateAdapter;
import com.artalent.ui.model.Country;
import com.artalent.ui.model.CountryResponse;
import com.artalent.ui.model.DistrictsObj;
import com.artalent.ui.model.DistrictsResponse;
import com.artalent.ui.model.InterestObj;
import com.artalent.ui.model.InterestResponse;
import com.artalent.ui.model.StateObj;
import com.artalent.ui.model.StateResponse;
import com.artalent.ui.utils.Constants;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by Mahajan on 16-09-2019.
 */

public class FilterActivity extends BaseActivityOther implements View.OnClickListener {

    RecyclerView recyclerView;
    FloatingActionButton save;

    FilterCountryAdapter selectCountryAdapter;
    List<Country> countries = new ArrayList<>();
    private Country selectedCountry = null;

    List<StateObj> stateObjs = new ArrayList<>();
    FilterStateAdapter selectStateAdapter;
    private StateObj selectedStateObj = null;

    List<DistrictsObj> districtsObjs = new ArrayList<>();
    FilterDistrictsAdapter selectDistrictsAdapter;

    TextView tvInterestLine;
    TextView tvInterest;
    TextView tvCountryLine;
    TextView tvCountry;
    TextView tvStateLine;
    TextView tvState;
    TextView tvDistrictLine;
    TextView tvDistrict;

    enum FilterType {
        INTEREST,
        COUNTRY,
        STATE,
        DISTRICT,
    }

    List<InterestObj> interestList = new ArrayList<>();
    FilterInterestAdapter selectInterestAdapter;
    List<InterestObj> selectedInterestList = new ArrayList<>();
    List<DistrictsObj> selectedDistrictsList = new ArrayList<>();
    private String sType = FilterType.INTEREST.toString();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);
        init1();
        setupToolbarBack(getString(R.string.select_filter));
        initializeUi();
        callCountryApi(false);
        selectFilterType();
        callInterestApi();
    }

    protected void callInterestApi() {
        if (Utils.isInternetAvailable(mActivity)) {
            Call<InterestResponse> call = ArtalentApplication.apiService.getInterest();
            call.enqueue(new Callback<InterestResponse>() {
                @Override
                public void onResponse(Call<InterestResponse> call, Response<InterestResponse> response) {
                    int statusCode = response.code();
                    if (response.body().getObj() != null && response.body().getObj().size() > 0) {
                        interestList.clear();
                        interestList = response.body().getObj();
                        for (InterestObj interestObj : interestList) {
                            if (selectedInterestList.size() > 0) {
                                for (InterestObj interestObj1 : selectedInterestList) {
                                    if (interestObj.getInterestMasterId() == interestObj1.getInterestMasterId()) {
                                        interestObj.setCheck(true);
                                    }

                                }

                            }
                        }
                        setInterestAdapter();
                    }
                }

                @Override
                public void onFailure(Call<InterestResponse> call, Throwable t) {
                    // Log error here since request failed
                    Log.e("TAG", t.toString());
                }
            });
        }
    }

    private void setInterestAdapter() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        selectInterestAdapter = new FilterInterestAdapter(mActivity, interestList, this);
        recyclerView.setAdapter(selectInterestAdapter);
    }

    private void callCountryApi(boolean b) {
        if (Utils.isInternetAvailable(mActivity)) {
            Call<CountryResponse> call = ArtalentApplication.apiService.getCountries();
            call.enqueue(new Callback<CountryResponse>() {
                @Override
                public void onResponse(Call<CountryResponse> call, Response<CountryResponse> response) {
                    if (response.body().getObj() != null && response.body().getObj().size() > 0) {
                        countries.clear();
                        countries = response.body().getObj();

                        if (selectedCountry != null) {
                            for (Country country : countries) {
                                if (country.getCountryId() == selectedCountry.getCountryId()) {
                                    country.setCheck(true);
                                }

                            }
                        }

                    }
                    if (b) {
                        setAdapter();
                    }
                }

                @Override
                public void onFailure(Call<CountryResponse> call, Throwable t) {
                    // Log error here since request failed
                    Log.e("TAG", t.toString());
                }
            });
        }

    }

    private void setAdapter() {
        Log.e("countries","countries : "+countries.size());
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        selectCountryAdapter = new FilterCountryAdapter(mActivity, countries, this,0);
        recyclerView.setAdapter(selectCountryAdapter);
    }

    private void init1() {
        selectedCountry = (Country) getIntent().getSerializableExtra(Constants.COUNTRY_OBJ);
        selectedStateObj = (StateObj) getIntent().getSerializableExtra(Constants.STATE_OBJ);

        selectedInterestList = (ArrayList<InterestObj>) getIntent().getSerializableExtra(Constants.INTEREST_LIST);
        selectedDistrictsList = (ArrayList<DistrictsObj>) getIntent().getSerializableExtra(Constants.DISTRICTS_LIST);
    }


    public void initializeUi() {
        tvInterestLine = findViewById(R.id.tvInterestLine);
        tvInterest = findViewById(R.id.tvInterest);
        tvCountryLine = findViewById(R.id.tvCountryLine);
        tvCountry = findViewById(R.id.tvCountry);
        tvStateLine = findViewById(R.id.tvStateLine);
        tvState = findViewById(R.id.tvState);
        tvDistrictLine = findViewById(R.id.tvDistrictLine);
        tvDistrict = findViewById(R.id.tvDistrict);
        recyclerView = findViewById(R.id.recyclerView);
        save = findViewById(R.id.save);
        save.setOnClickListener(this);

        tvInterest.setOnClickListener(this);
        tvCountry.setOnClickListener(this);
        tvState.setOnClickListener(this);
        tvDistrict.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvInterest:
                sType = FilterType.INTEREST.toString();
                selectFilterType();
                break;
            case R.id.tvCountry:
                sType = FilterType.COUNTRY.toString();
                selectFilterType();
                break;
            case R.id.tvState:
                if (selectedCountry != null) {
                    sType = FilterType.STATE.toString();
                    selectFilterType();
                } else {
                    Utils.showToast(mActivity, mActivity.getString(R.string.select_country_first));
                }

                break;
            case R.id.tvDistrict:
                if (selectedCountry == null) {
                    Utils.showToast(mActivity, mActivity.getString(R.string.select_country_first));
                } else if (selectedStateObj == null) {
                    Utils.showToast(mActivity, mActivity.getString(R.string.select_state_first));
                } else {
                    sType = FilterType.DISTRICT.toString();
                    selectFilterType();
                }

                break;
            case R.id.save:
                selectedInterestList.clear();
                for (InterestObj interestObj : interestList) {
                    if (interestObj != null && interestObj.isCheck()) {
                        selectedInterestList.add(interestObj);
                    }

                }

                Intent output = new Intent();
                output.putExtra(Constants.INTEREST_LIST, (Serializable) selectedInterestList);
                output.putExtra(Constants.COUNTRY_OBJ, selectedCountry);
                output.putExtra(Constants.STATE_OBJ, selectedStateObj);
                output.putExtra(Constants.DISTRICTS_LIST, (Serializable) selectedDistrictsList);
                setResult(RESULT_OK, output);
                finish();


                break;
            case R.id.llMain:
                if (v.getTag() instanceof Country) {
                    Country country = (Country) v.getTag();
                    if (country != null) {
                        for (Country country1 : countries) {
                            if (country1.getCountryId() == country.getCountryId()) {
                                country1.setCheck(!country1.isCheck());
                                if (country1.isCheck()) {
                                    selectedCountry = country;
                                } else {
                                    selectedCountry = null;
                                }
                            } else {
                                country1.setCheck(false);
                            }
                        }
                        selectedStateObj = null;
                        selectedDistrictsList.clear();
                        selectCountryAdapter.notifyDataSetChanged();
                    }
                } else if (v.getTag() instanceof InterestObj) {
                    InterestObj interestObj = (InterestObj) v.getTag();
                    if (interestObj != null) {
                        for (InterestObj interestObj1 : interestList) {
                            if (interestObj1.getInterestMasterId() == interestObj.getInterestMasterId()) {
                                interestObj1.setCheck(!interestObj1.isCheck());
                            } else {
                                interestObj1.setCheck(false);
                            }
                        }
                        selectInterestAdapter.notifyDataSetChanged();
                    }
                } else if (v.getTag() instanceof StateObj) {
                    StateObj stateObj = (StateObj) v.getTag();
                    if (stateObj != null) {
                        for (StateObj stateObj1 : stateObjs) {
                            if (stateObj1.getStateId() == stateObj.getStateId()) {
                                stateObj1.setCheck(!stateObj1.isCheck());
                                if (stateObj1.isCheck()) {
                                    selectedStateObj = stateObj;
                                } else {
                                    selectedStateObj = null;
                                }
                            } else {
                                stateObj1.setCheck(false);
                            }
                        }
                        selectedDistrictsList.clear();
                        selectStateAdapter.notifyDataSetChanged();
                    }
                } else if (v.getTag() instanceof DistrictsObj) {
                    DistrictsObj districtsObj = (DistrictsObj) v.getTag();
                    if (districtsObj != null) {
                        for (DistrictsObj districtsObj1 : districtsObjs) {
                            if (districtsObj1.getDistrictId() == districtsObj.getDistrictId()) {
                                districtsObj1.setCheck(!districtsObj1.isCheck());
                            } else {
                                districtsObj1.setCheck(false);
                            }
                        }
                        selectDistrictsAdapter.notifyDataSetChanged();
                    }
                    selectedDistrictsList.clear();
                    for (DistrictsObj districtsObj1 : districtsObjs) {
                        if (districtsObj1.isCheck()) {
                            selectedDistrictsList.add(districtsObj1);
                        }
                    }
                }

                break;
        }
    }

    private void selectFilterType() {
        if (sType.equals(FilterType.INTEREST.toString())) {
            if (interestList.size() > 0) {
                setInterestAdapter();
            } else {
                callInterestApi();
            }
            tvInterestLine.setVisibility(View.VISIBLE);
            tvCountryLine.setVisibility(View.INVISIBLE);
            tvStateLine.setVisibility(View.INVISIBLE);
            tvDistrictLine.setVisibility(View.INVISIBLE);

            tvInterest.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
            tvCountry.setBackgroundColor(ContextCompat.getColor(this, R.color.bg_screen6));
            tvState.setBackgroundColor(ContextCompat.getColor(this, R.color.bg_screen6));
            tvDistrict.setBackgroundColor(ContextCompat.getColor(this, R.color.bg_screen6));


        } else if (sType.equals(FilterType.COUNTRY.toString())) {
            if (countries.size() > 0) {
                setAdapter();
            } else {
                callCountryApi(true);
            }
            tvInterestLine.setVisibility(View.INVISIBLE);
            tvCountryLine.setVisibility(View.VISIBLE);
            tvStateLine.setVisibility(View.INVISIBLE);
            tvDistrictLine.setVisibility(View.INVISIBLE);

            tvInterest.setBackgroundColor(ContextCompat.getColor(this, R.color.bg_screen6));
            tvCountry.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
            tvState.setBackgroundColor(ContextCompat.getColor(this, R.color.bg_screen6));
            tvDistrict.setBackgroundColor(ContextCompat.getColor(this, R.color.bg_screen6));

        } else if (sType.equals(FilterType.STATE.toString())) {
            callStateApi();
            tvInterestLine.setVisibility(View.INVISIBLE);
            tvCountryLine.setVisibility(View.INVISIBLE);
            tvStateLine.setVisibility(View.VISIBLE);
            tvDistrictLine.setVisibility(View.INVISIBLE);

            tvInterest.setBackgroundColor(ContextCompat.getColor(this, R.color.bg_screen6));
            tvCountry.setBackgroundColor(ContextCompat.getColor(this, R.color.bg_screen6));
            tvState.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
            tvDistrict.setBackgroundColor(ContextCompat.getColor(this, R.color.bg_screen6));

        } else if (sType.equals(FilterType.DISTRICT.toString())) {
            callDistrictsApi();
            tvInterestLine.setVisibility(View.INVISIBLE);
            tvCountryLine.setVisibility(View.INVISIBLE);
            tvStateLine.setVisibility(View.INVISIBLE);
            tvDistrictLine.setVisibility(View.VISIBLE);

            tvInterest.setBackgroundColor(ContextCompat.getColor(this, R.color.bg_screen6));
            tvCountry.setBackgroundColor(ContextCompat.getColor(this, R.color.bg_screen6));
            tvState.setBackgroundColor(ContextCompat.getColor(this, R.color.bg_screen6));
            tvDistrict.setBackgroundColor(ContextCompat.getColor(this, R.color.white));

        }
    }

    private void callStateApi() {
        if (Utils.isInternetAvailable(mActivity)) {
            Call<StateResponse> call = ArtalentApplication.apiService.getStates(selectedCountry.getCountryId());
            call.enqueue(new Callback<StateResponse>() {
                @Override
                public void onResponse(Call<StateResponse> call, Response<StateResponse> response) {
                    int statusCode = response.code();
                    if (response.body() != null && response.body().getObj() != null && response.body().getObj().size() > 0) {
                        stateObjs.clear();
                        stateObjs = response.body().getObj();
                        for (StateObj stateObj : stateObjs) {
                            if (selectedStateObj != null) {
                                if (stateObj.getStateId() == selectedStateObj.getStateId()) {
                                    stateObj.setCheck(true);
                                }

                            }

                        }
                        setAdapterState();
                    }


                }

                @Override
                public void onFailure(Call<StateResponse> call, Throwable t) {
                    // Log error here since request failed
                    Log.e("TAG", t.toString());
                }
            });
        }

    }

    private void setAdapterState() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        selectStateAdapter = new FilterStateAdapter(mActivity, stateObjs, this,0);
        recyclerView.setAdapter(selectStateAdapter);
    }

    private void callDistrictsApi() {
        if (Utils.isInternetAvailable(mActivity)) {
            Call<DistrictsResponse> call = ArtalentApplication.apiService.getDistricts(selectedStateObj.getStateId());
            call.enqueue(new Callback<DistrictsResponse>() {
                @Override
                public void onResponse(Call<DistrictsResponse> call, Response<DistrictsResponse> response) {
                    int statusCode = response.code();
                    if (response.body().getObj() != null && response.body().getObj().size() > 0) {
                        districtsObjs.clear();
                        districtsObjs = response.body().getObj();
                        for (DistrictsObj stateObj : districtsObjs) {
                            for (DistrictsObj districtsObj : selectedDistrictsList) {
                                if (stateObj.getDistrictId() == districtsObj.getDistrictId()) {
                                    stateObj.setCheck(true);
                                }
                            }
                        }
                        setDistrictsAdapter();
                    }


                }

                @Override
                public void onFailure(Call<DistrictsResponse> call, Throwable t) {
                    // Log error here since request failed
                    Log.e("TAG", t.toString());
                }
            });
        }

    }

    private void setDistrictsAdapter() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        selectDistrictsAdapter = new FilterDistrictsAdapter(mActivity, districtsObjs, this,0);
        recyclerView.setAdapter(selectDistrictsAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_filter_reset, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_add) {
            selectedInterestList.clear();
            selectedCountry = null;
            selectedStateObj = null;
            selectedDistrictsList.clear();
            Intent output = new Intent();
            output.putExtra(Constants.INTEREST_LIST, (Serializable) selectedInterestList);
            output.putExtra(Constants.COUNTRY_OBJ, selectedCountry);
            output.putExtra(Constants.STATE_OBJ, selectedStateObj);
            output.putExtra(Constants.DISTRICTS_LIST, (Serializable) selectedDistrictsList);
            setResult(RESULT_OK, output);
            finish();

        }
        return super.onOptionsItemSelected(item);
    }

}
