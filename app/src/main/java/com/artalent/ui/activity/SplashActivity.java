package com.artalent.ui.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.artalent.ArtalentApplication;
import com.artalent.R;
import com.artalent.Utils;
import com.artalent.ui.model.UserObj;
import com.artalent.ui.model.UserResponse;
import com.artalent.ui.utils.Constants;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Mahajan on 08-09-2019.
 */

public class SplashActivity extends BaseActivityOther {

    private static final int STORAGE_PERMISSION_CODE = 100;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        checkStoragePermission();
    }

    private void checkStoragePermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
        } else {
            enqueueDownload();
        }
    }

    private void enqueueDownload() {

        new Handler().postDelayed(() -> {
            if (ArtalentApplication.prefManager.getUserObj() != null && ArtalentApplication.prefManager.getUserObj().getId() > 0 && ArtalentApplication.prefManager.getAuthorizationToken() != null && !TextUtils.isEmpty(ArtalentApplication.prefManager.getAuthorizationToken())) {
                getUserData();
            } else {
                startActivity(new Intent(getApplicationContext(), WelcomeActivity.class));
                finish();
            }
        }, 1000);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == STORAGE_PERMISSION_CODE && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            enqueueDownload();
        } else {
            finish();
        }
    }

    private void getUserData() {
        if (Utils.isInternetAvailable(mActivity)) {
            ArtalentApplication.apiService.getOtherUserDetails(ArtalentApplication.prefManager.getUserObj().getId()).enqueue(new Callback<UserResponse>() {
                @Override
                public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                    int statusCode = response.code();
                    if (response.body() != null && response.body().getObj() != null) {
                        UserObj userObj = response.body().getObj();
                        ArtalentApplication.prefManager.setUserObj(userObj);
                        goToNext();
                    } else {
                        startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                        finish();
                    }
                }

                @Override
                public void onFailure(Call<UserResponse> call, Throwable t) {
                    // Log error here since request failed
                    Log.e("TAG", t.toString());
                }
            });
        } else {
            Intent intent = new Intent(getApplicationContext(), NoInternetActivity.class);
            startActivityForResult(intent, Constants.NO_INTERNET);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.NO_INTERNET) {
            if (resultCode == Activity.RESULT_OK) {
                getUserData();
            }
        }
    }

    private void goToNext() {
        //  startActivity(new Intent(getApplicationContext(), MainActivity.class));
        //  finish();
        UserObj userObj = ArtalentApplication.prefManager.getUserObj();
        if (userObj != null) {
            if (userObj.getUserName() != null && !TextUtils.isEmpty(userObj.getUserName())) {
                if (userObj.getInterest() != null && userObj.getInterest().size() > 0) {
                    startActivity(new Intent(getApplicationContext(), MainActivity.class));
                    finish();
                } else {
                    startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                    finish();
                }
            } else {
                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                finish();
            }

        } else {
            startActivity(new Intent(getApplicationContext(), WelcomeActivity.class));
            finish();
        }


    }


}
