package com.artalent.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;

import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.view.View;

import com.artalent.R;
import com.artalent.ui.adapter.AdvertisePanelTabAdapter;
import com.artalent.ui.fragment.ActiveAdvertiseFragment;
import com.artalent.ui.fragment.ExpiredAdvertiseFragment;

import butterknife.BindView;

public class AdvertisePanelActivity extends BaseActivityOther implements View.OnClickListener {

    @BindView(R.id.add_advertise_fab)
    FloatingActionButton addAdvertise;

    AdvertisePanelTabAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_advertise_panel);
        addAdvertise.setOnClickListener(this);
        setupToolbarBack(getString(R.string.advertise_panel));

        ViewPager viewPager = findViewById(R.id.viewPager);
        TabLayout tabLayout = findViewById(R.id.tabLayout);
        adapter = new AdvertisePanelTabAdapter(getSupportFragmentManager(), FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        adapter.addFragment(new ActiveAdvertiseFragment(), "Active");
        adapter.addFragment(new ExpiredAdvertiseFragment(), "Expired");
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.add_advertise_fab){
            Intent intent = new Intent(getApplicationContext(), AddAdvertiseActivity.class);
            startActivity(intent);
        }
    }
}
