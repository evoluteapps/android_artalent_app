package com.artalent.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.View;
import android.widget.Button;

import com.artalent.R;
import com.artalent.Utils;

import butterknife.BindView;

public class NoInternetActivity extends BaseActivityOther implements View.OnClickListener {

    @BindView(R.id.btn_retry)
    Button btnRetry;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_no_internet);
        btnRetry.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_retry) {
            if (!Utils.isInternetAvailable(getApplicationContext())) {
                Utils.showToast(NoInternetActivity.this, "Please check your Internet connection");
            } else {
                Intent output = new Intent();
                setResult(RESULT_OK, output);
                finish();
            }
        }
    }
}
