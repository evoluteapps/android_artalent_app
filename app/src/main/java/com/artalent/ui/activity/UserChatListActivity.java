package com.artalent.ui.activity;

import android.content.Intent;
import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;

import com.artalent.R;
import com.artalent.ui.adapter.FeedItemAnimator;
import com.artalent.ui.adapter.InboxChatAdapter;
import com.artalent.ui.chatscreen.ChatActivity;
import com.artalent.ui.view.FeedContextMenuManager;

import butterknife.BindView;

/**
 * Created by Miroslaw Stanek on 14.01.15.
 */
public class UserChatListActivity extends BaseActivityOther implements InboxChatAdapter.OnInboxChatClickListener {


    @BindView(R.id.rvFeed)
    RecyclerView rvFeed;

    private InboxChatAdapter feedAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_inbox);
        setupToolbarBack(getString(R.string.chat_inbox));
        setupFeed();
        feedAdapter.updateItems(false);
    }

    protected void setupFeed() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mActivity) {
            @Override
            protected int getExtraLayoutSpace(RecyclerView.State state) {
                return 300;
            }
        };
        rvFeed.setLayoutManager(linearLayoutManager);
        feedAdapter = new InboxChatAdapter(mActivity);
        feedAdapter.setOnFeedItemClickListener(this);
        rvFeed.setAdapter(feedAdapter);
        rvFeed.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                FeedContextMenuManager.getInstance().onScrolled(recyclerView, dx, dy);
            }
        });
        rvFeed.setItemAnimator(new FeedItemAnimator());
    }


    @Override
    public void onCommentsClick(View v, int position) {

    }

    @Override
    public void onMoreClick(View v, int position) {

    }

    @Override
    public void onProfileClick(View v) {
        Intent intent = new Intent(mActivity, ChatActivity.class);
        startActivity(intent);

    }
}
