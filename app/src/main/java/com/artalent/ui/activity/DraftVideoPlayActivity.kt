package com.artalent.ui.activity

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import com.artalent.ArtalentApplication
import com.artalent.R
import com.artalent.Utils
import com.artalent.autoplayrecyclervideoplayer.adapter.AutoPlayVideoFragment
import com.artalent.ui.fragment.BottomSheetFragment
import com.artalent.ui.fragment.DraftAutoPlayVideoFragment
import com.artalent.ui.fragment.DraftEditFragment
import com.artalent.ui.model.*
import com.artalent.ui.model.room.ContestVideoDraft
import com.artalent.ui.utils.Constants
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.Serializable
import java.util.*

private const val TAG = "DraftVideoPlayActivity"

class DraftVideoPlayActivity : BaseActivityOther() {

    private lateinit var contestVideoDraft: ContestVideoDraft
    private var isFromRegisterActivity = false

    private val specialContests: MutableList<SpecialContestObj> = mutableListOf()
    private val registeredSpecialContestObj: MutableList<SpecialContestObj> = mutableListOf()
    private val contestObjs: MutableList<ContestObj> = mutableListOf()
    private val registeredContestList: MutableList<ContestObj> = mutableListOf()
    private val registeredContestObj = RegisteredContestObj()
    var bottomSheetFragment: BottomSheetFragment? = null

    override fun onResume() {
        callSpecialContestApi()
        callRunningContestApi()
        super.onResume()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_draft_video_play)

//        callSpecialContestApi()
//        callRunningContestApi()

        setupToolbarBack(
            "Preview",
            R.drawable.ic_vector_arrow_back_black,
            R.color.primary,
            R.color.white
        )

        if (intent != null) {
            contestVideoDraft = intent.getSerializableExtra("DraftObject") as ContestVideoDraft
            isFromRegisterActivity = intent.getBooleanExtra("isFromRegisterActivity", false)

            Log.d(
                "DraftVideoPlayActivity",
                "onCreate: received data from previous activity $contestVideoDraft"
            )
            Log.d(
                "DraftVideoPlayActivity",
                "onCreate: received data from ContentRegisterUpdateActivity activity $isFromRegisterActivity"
            )
        }


        val autoPlayVideoFragment = DraftAutoPlayVideoFragment()
        val bundle = Bundle()
        bundle.putSerializable("draft", contestVideoDraft)
        autoPlayVideoFragment.arguments = bundle
        supportFragmentManager.beginTransaction()
            .replace(R.id.root_container, autoPlayVideoFragment)
//            .addToBackStack(null)
            .commit()

        if (isFromRegisterActivity) {
            Log.d(TAG, "onCreate: is from register activity : $isFromRegisterActivity")
            goToBottomSheetOrInterestListActivity()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_next, menu)

        for (i in 0 until menu!!.size()) {
            val item = menu.getItem(i)
            val spanString = SpannableString(menu.getItem(i).title.toString())
            spanString.setSpan(
                ForegroundColorSpan(Color.BLACK),
                0,
                spanString.length,
                0
            ) //fix the color to white
            item.title = spanString
        }
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        if (item.itemId == R.id.action_next) {
//        Intent intent = new Intent(this, DraftVideoAutoPlayActivity.class);
//        intent.putExtra("DraftObject", contestVideoDraft);
//        startActivity(intent);

//            val intent = Intent(this, AddPostActivity::class.java)
//            intent.putExtra("DraftObject", contestVideoDraft)
//            startActivity(intent)

            goToBottomSheetOrInterestListActivity()
        }

        if (item.itemId == R.id.action_edit) {
            val draftEditFragment = DraftEditFragment()

            supportFragmentManager.beginTransaction()
                .replace(R.id.root_container, draftEditFragment)
                .addToBackStack(null)
                .commit()
        }

        return super.onOptionsItemSelected(item)
    }

    private fun goToBottomSheetOrInterestListActivity() {
        Log.d(TAG, "goToBottomSheetOrInterestListActivity: 1")
        //TO-DO Append this list to current list.
        val registeredSpecialContestObj: List<SpecialContestObj> = registeredSpecialContestObj
        val registeredContestObj: RegisteredContestObj = registeredContestObj

        val objectContestList: MutableList<Any> = ArrayList()

        objectContestList.addAll(registeredSpecialContestObj)
        if (registeredContestObj.contestObjList != null && registeredContestObj.contestObjList.size > 0) {
            objectContestList.addAll(registeredContestObj.contestObjList)
        }

        if (objectContestList.size > 0) {
//        if (isFromRegisterActivity) {
            Log.d(TAG, "goToBottomSheetOrInterestListActivity: 2")
            bottomSheetFragment = BottomSheetFragment()
            val bundle = Bundle()
            bundle.putSerializable("RegisteredContest", objectContestList as Serializable)
            bundle.putSerializable("SelectedDraft", contestVideoDraft as Serializable)
            bottomSheetFragment!!.arguments = bundle
            bottomSheetFragment!!.show(supportFragmentManager, bottomSheetFragment!!.tag)
        } else {
            Log.d(TAG, "goToBottomSheetOrInterestListActivity: 3")
            Utils.showToast(this, "please register in a talent for contest post")
            val intent = Intent(this, InterestListActivity::class.java)
            intent.putExtra("isFromDraftActivity", true)
            intent.putExtra("SelectedDraft", contestVideoDraft)
            startActivity(intent)
        }
    }


    private fun callSpecialContestApi() {
        Log.d(TAG, "callSpecialContestApi: called")
        if (Utils.isInternetAvailable(mActivity)) {
            val call = ArtalentApplication.apiService.getAdminContest(
                ArtalentApplication.prefManager.userObj.id,
                1,
                0
            )
            call.enqueue(object : Callback<SpecialContestResponse?> {
                override fun onResponse(
                    call: Call<SpecialContestResponse?>,
                    response: Response<SpecialContestResponse?>
                ) {
                    specialContests.clear()
                    registeredSpecialContestObj.clear()
                    if (response.body() != null && response.body()!!.obj != null) {
                        if (response.body()!!.obj.size > 0) {
                            specialContests.addAll(response.body()!!.obj)
                        }
                        for (specialContestObj in specialContests) {
                            if (specialContestObj.isRegistered == 1) {
                                registeredSpecialContestObj.add(specialContestObj)
                            }
                        }
                    }
                }

                override fun onFailure(call: Call<SpecialContestResponse?>, t: Throwable) {
                    hideProgress()
                }
            })
        } else {
            hideProgress()
            val intent = Intent(
                mActivity,
                NoInternetActivity::class.java
            )
            startActivityForResult(intent, Constants.NO_INTERNET)
        }
    }


    private fun callRunningContestApi() {
        Log.d(TAG, "callRunningContestApi: called")
        if (Utils.isInternetAvailable(mActivity)) {
            val call =
                ArtalentApplication.apiService.getContestRegistration(ArtalentApplication.prefManager.userObj.id)
            call.enqueue(object : Callback<ContestResponse?> {
                override fun onResponse(
                    call: Call<ContestResponse?>,
                    response: Response<ContestResponse?>
                ) {
                    contestObjs.clear()
                    registeredContestList.clear()
                    if (response.body() != null && response.body()!!.obj != null && response.body()!!
                            .obj.size > 0
                    ) {
                        for (contestObj in response.body()!!.obj) {
                            var isAvailable = false
                            for (contestRegistrationObj in contestObj.registrationDetails) {
                                if (ArtalentApplication.prefManager.userObj != null && contestRegistrationObj != null && contestRegistrationObj.user != null) {
                                    val userObj = ArtalentApplication.prefManager.userObj
                                    if (contestRegistrationObj.user.id == userObj.id) {
                                        isAvailable = true
                                    }
                                }
                            }
                            if (isAvailable) {
                                contestObjs.add(contestObj)
                                registeredContestList.add(contestObj)
                            }
                        }
                        for (contestObj in response.body()!!.obj) {
                            var isAvailable = false
                            for (contestRegistrationObj in contestObj.registrationDetails) {
                                if (ArtalentApplication.prefManager.userObj != null && contestRegistrationObj != null && contestRegistrationObj.user != null) {
                                    val userObj = ArtalentApplication.prefManager.userObj
                                    if (contestRegistrationObj.user.id == userObj.id) {
                                        isAvailable = true
                                    }
                                }
                            }
                            if (!isAvailable) {
                                contestObjs.add(contestObj)
                            }
                        }
                    }
                    registeredContestObj.contestObjList = registeredContestList
                }

                override fun onFailure(call: Call<ContestResponse?>, t: Throwable) {
                    // Log error here since request failed
                    hideProgress()
                }
            })
        } else {
            val intent = Intent(mActivity, NoInternetActivity::class.java)
            startActivityForResult(intent, Constants.NO_INTERNET)
        }
    }
}