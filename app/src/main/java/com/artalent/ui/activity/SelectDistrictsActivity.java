package com.artalent.ui.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.artalent.ArtalentApplication;
import com.artalent.R;
import com.artalent.Utils;
import com.artalent.ui.adapter.FilterDistrictsAdapter;
import com.artalent.ui.model.BaseResponse;
import com.artalent.ui.model.DistrictsObj;
import com.artalent.ui.model.DistrictsResponse;
import com.artalent.ui.model.request.DistrictReq;
import com.artalent.ui.utils.Constants;
import com.artalent.ui.utils.RecyclerTouchListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by Mahajan on 16-09-2019.
 */

public class SelectDistrictsActivity extends BaseActivityOther implements View.OnClickListener {

    RecyclerView recyclerView;
    FloatingActionButton save, addFab;
    FilterDistrictsAdapter selectStateAdapter;
    List<DistrictsObj> countries = new ArrayList<>();
    private DistrictsObj selectedDistrictsObj = null;
    private int selectedStateId = 0;
    private int fromData;
    private TextView tvNoListItem;

    private RecyclerTouchListener touchListener;

    public static void startActivity(Activity mActivity, DistrictsObj districtsObj, int stateId, int requestCode) {
        Intent intent = new Intent(mActivity, SelectDistrictsActivity.class);
        intent.putExtra(Constants.ID, stateId);
        intent.putExtra(Constants.STATE_OBJ, districtsObj);
        mActivity.startActivityForResult(intent, requestCode);
    }

    public static void startActivity(Activity mActivity, DistrictsObj districtsObj, int stateId, int requestCode, int from) {
        Intent intent = new Intent(mActivity, SelectDistrictsActivity.class);
        intent.putExtra(Constants.ID, stateId);
        intent.putExtra(Constants.STATE_OBJ, districtsObj);
        intent.putExtra(Constants.FROM, from);
        mActivity.startActivityForResult(intent, requestCode);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (fromData == 1) {
            recyclerView.addOnItemTouchListener(touchListener);
        }
    }

    private void initializeRecyclerTouchListener() {
        touchListener = new RecyclerTouchListener(this, recyclerView);
        touchListener
                .setClickable(new RecyclerTouchListener.OnRowClickListener() {
                    @Override
                    public void onRowClicked(int position) {
                        Utils.showToast(mActivity, "Clicked District" + countries.get(position).getDistrict());
                    }

                    @Override
                    public void onIndependentViewClicked(int independentViewID, int position) {

                    }
                })
                .setSwipeOptionViews(R.id.delete_task, R.id.edit_task)
                .setSwipeable(R.id.rowFG, R.id.rowBG, (viewID, position) -> {
                    switch (viewID) {
                        case R.id.delete_task:
                            callDeleteDistrictApi(countries.get(position), position);
                            break;
                        case R.id.edit_task:
                            showDialogData(countries.get(position), true);
                            break;

                    }
                });
    }


    private void callDeleteDistrictApi(DistrictsObj districtsObj, int position) {
        if (Utils.isInternetAvailable(mActivity)) {
            showProgress();
            Call<BaseResponse> call = ArtalentApplication.apiService.deleteDistrict(ArtalentApplication.prefManager.getUserObj().getId(), districtsObj.getDistrictId());
            call.enqueue(new Callback<BaseResponse>() {
                @Override
                public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                    int statusCode = response.code();
                    String mess = response.body().getMessage();
                    if (statusCode == 200) {
                        hideProgress();
                        countries.remove(position);
                        selectStateAdapter.setTaskList(countries);
                        Utils.showToast(mActivity, mess);
                    } else {
                        hideProgress();
                        Utils.showToast(mActivity, mess);
                    }
                }

                @Override
                public void onFailure(Call<BaseResponse> call, Throwable t) {
                    // Log error here since request failed
                    dialog.dismiss();
                    hideProgress();
                    Utils.showToast(mActivity, "onFailure : " + t.toString());
                }
            });
        }
    }

    private void callEditDistrictApi(DistrictsObj districtsObj) {
        if (Utils.isInternetAvailable(mActivity)) {
            showProgress();
            Call<BaseResponse> call = ArtalentApplication.apiService.editDistrict(ArtalentApplication.prefManager.getUserObj().getId(), districtsObj.getDistrictId(), fillDataDistrict(districtsObj.getDistrict(), districtsObj.getStateId()));
            call.enqueue(new Callback<BaseResponse>() {
                @Override
                public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                    int statusCode = response.code();
                    String mess = response.body().getMessage();
                    if (statusCode == 200) {
                        hideProgress();
                        dialog.dismiss();
                        Utils.showToast(mActivity, mess);
                        callDistrictApi();
                    } else {
                        hideProgress();
                        dialog.dismiss();
                        Utils.showToast(mActivity, mess);
                    }
                }

                @Override
                public void onFailure(Call<BaseResponse> call, Throwable t) {
                    // Log error here since request failed
                    dialog.dismiss();
                    hideProgress();
                    Utils.showToast(mActivity, "onFailure : " + t.toString());
                }
            });
        }
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_countery);
        init1();
        setupToolbarBack(getString(R.string.manage_district));
        initializeUi();
        if (fromData == 1) {
            initializeRecyclerTouchListener();
        }
        callDistrictApi();
    }

    private void callDistrictApi() {
        if (Utils.isInternetAvailable(mActivity)) {
            showProgress();
            Call<DistrictsResponse> call = ArtalentApplication.apiService.getDistricts(selectedStateId);
            call.enqueue(new Callback<DistrictsResponse>() {
                @Override
                public void onResponse(Call<DistrictsResponse> call, Response<DistrictsResponse> response) {
                    int statusCode = response.code();
                    if (response.body().getObj() != null && response.body().getObj().size() > 0) {
                        tvNoListItem.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                        hideProgress();
                        countries.clear();
                        countries.addAll(response.body().getObj());
                        for (DistrictsObj districtsObj : countries) {
                            if (selectedDistrictsObj != null) {
                                if (districtsObj.getDistrictId() == selectedDistrictsObj.getDistrictId()) {
                                    districtsObj.setCheck(true);
                                }
                            }
                        }
                        setAdapter();
                    } else {
                        tvNoListItem.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.GONE);
                        hideProgress();
                    }
                }

                @Override
                public void onFailure(Call<DistrictsResponse> call, Throwable t) {
                    // Log error here since request failed
                }
            });
        }
    }

    private void setAdapter() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        selectStateAdapter = new FilterDistrictsAdapter(mActivity, countries, this, fromData);
        selectStateAdapter.setTaskList(countries);
        recyclerView.setAdapter(selectStateAdapter);
    }

    private void init1() {
        selectedStateId = getIntent().getIntExtra(Constants.ID, 0);
        selectedDistrictsObj = (DistrictsObj) getIntent().getSerializableExtra(Constants.STATE_OBJ);
        fromData = getIntent().getIntExtra(Constants.FROM, 0);
    }

    @SuppressLint("RestrictedApi")
    public void initializeUi() {
        tvNoListItem = findViewById(R.id.tv_no_any_item);
        tvNoListItem.setText(R.string.no_any_districts);
        recyclerView = findViewById(R.id.recyclerView);
        save = findViewById(R.id.save);
        addFab = findViewById(R.id.addFab);
        save.setOnClickListener(this);
        addFab.setOnClickListener(this);
        if (fromData == 1) {
            addFab.setVisibility(View.VISIBLE);
            save.setVisibility(View.GONE);

        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.addFab:
                showDialogData(null, false);
                break;
            case R.id.save:
                if (selectedDistrictsObj != null) {
                    Intent output = new Intent();
                    output.putExtra(Constants.STATE_OBJ, selectedDistrictsObj);
                    setResult(RESULT_OK, output);
                    finish();
                } else {
                    Utils.showToast(mActivity, mActivity.getString(R.string.please_select_state));
                }

                break;
            case R.id.llMain:
                if (fromData != 1 && v.getTag() instanceof DistrictsObj) {
                    DistrictsObj stateObj = (DistrictsObj) v.getTag();
                    if (stateObj != null) {
                        for (DistrictsObj stateObj1 : countries) {
                            if (stateObj1.getDistrictId() == stateObj.getDistrictId()) {
                                stateObj1.setCheck(!stateObj1.isCheck());
                                if (stateObj1.isCheck()) {
                                    selectedDistrictsObj = stateObj;
                                } else {
                                    selectedDistrictsObj = null;
                                }
                            } else {
                                stateObj1.setCheck(false);
                            }
                        }
                        selectStateAdapter.notifyDataSetChanged();
                    }
                }
                break;
        }
    }

    Dialog dialog;

    private void showDialogData(DistrictsObj districtsObj, boolean isEdit) {
        dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_add_data);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setTitle("Title...");
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        // set the custom dialog components - text, image and button
        TextView titleTextView = dialog.findViewById(R.id.titleTextView);
        TextInputEditText name_et = dialog.findViewById(R.id.name_et);
        if (isEdit) {
            titleTextView.setText(R.string.edit_district);
            name_et.setText(districtsObj.getDistrict());
        } else {
            titleTextView.setText(R.string.add_district);
        }
        name_et.setHint("Enter district name");
        Button downloadButton = dialog.findViewById(R.id.downloadButton);
        Button cancelButton = dialog.findViewById(R.id.cancelButton);
        downloadButton.setOnClickListener(v -> {
            if (!TextUtils.isEmpty(name_et.getText().toString())) {
                if (isEdit) {
                    districtsObj.setDistrict(name_et.getText().toString());
                    callEditDistrictApi(districtsObj);
                } else {
                    callAddStateApi(name_et.getText().toString());
                }
            } else {
                Utils.showToast(SelectDistrictsActivity.this, "Please enter district name");
            }
        });
        cancelButton.setOnClickListener(v -> dialog.dismiss());
        dialog.show();
    }

    private DistrictReq fillDataDistrict(String districtName, int stateId) {
        DistrictReq districtReq = new DistrictReq();
        districtReq.setDistrictName(districtName);
        districtReq.setStateId(stateId);
        return districtReq;
    }

    private void callAddStateApi(String districtName) {
        if (Utils.isInternetAvailable(mActivity)) {
            hideProgress();
            Call<BaseResponse> call = ArtalentApplication.apiService.addDistrict(ArtalentApplication.prefManager.getUserObj().getId(), fillDataDistrict(districtName, selectedStateId));
            call.enqueue(new Callback<BaseResponse>() {
                @Override
                public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                    int statusCode = response.code();
                    String mess = response.body().getMessage();
                    if (statusCode == 200) {
                        callDistrictApi();
                        hideProgress();
                        dialog.dismiss();
                        Utils.showToast(mActivity, mess);
                    } else {
                        hideProgress();
                        dialog.dismiss();
                        Utils.showToast(mActivity, mess);
                    }
                }

                @Override
                public void onFailure(Call<BaseResponse> call, Throwable t) {
                    // Log error here since request failed
                    dialog.dismiss();
                    hideProgress();
                    Utils.showToast(mActivity, "onFailure : " + t.toString());
                }
            });
        }

    }
}
