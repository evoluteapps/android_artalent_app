package com.artalent.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.artalent.ArtalentApplication;
import com.artalent.R;
import com.artalent.Utils;
import com.artalent.ui.model.OTPResponse;
import com.artalent.ui.model.UserObj;
import com.artalent.ui.model.request.CreateReq;
import com.artalent.ui.otpview.OnOtpCompletionListener;
import com.artalent.ui.otpview.OtpView;
import com.artalent.ui.utils.Constants;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OTPActivity extends BaseActivityOther implements View.OnClickListener,
        OnOtpCompletionListener {
    private Button validateButton;
    private TextView lblNumber;
    private OtpView otpView;
    private String sMobileNumber = "";
    private static String randomOTP;

    public static void startActivity(Activity mActivity, String mobile, String OTP) {
        Intent intent = new Intent(mActivity, OTPActivity.class);
        randomOTP = OTP;
        intent.putExtra(Constants.MOBILE_NUMBER, mobile);
        mActivity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        init();
        initializeUi();
        setListeners();
        lblNumber.setText(String.format(getString(R.string.verification_code_subtext), sMobileNumber));
    }

    private void init() {
        sMobileNumber = getIntent().getStringExtra(Constants.MOBILE_NUMBER);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.validate_button) {
            // do Stuff
            if (otpView.getText().length() >= 4) {
                if (otpView.getText().toString().equalsIgnoreCase(randomOTP)) {
                    callVerifyOTPApi();
                } else {
                    Utils.showToastForOtp(mActivity, mActivity.getString(R.string.enter_correct_otp));
                }
            } else {
                Utils.showToastForOtp(mActivity, mActivity.getString(R.string.please_enter_otp));
            }
        }
    }

    private boolean isNextView = false;

    private void startHomeActivity() {
//        if (!isNextView) {
//            isNextView = true;
//            Intent intent = new Intent(OTPActivity.this, RegistrationActivity.class);
//            startActivity(intent);
//            finishAffinity();
//        }

        UserObj userObj = ArtalentApplication.prefManager.getUserObj();

        if (userObj != null) {
            if (userObj.getUserName() != null && !TextUtils.isEmpty(userObj.getUserName())) {
                if (userObj.getInterest() != null && userObj.getInterest().size() > 0) {
                    startActivity(new Intent(mActivity, MainActivity.class));
                    finishAffinity();
                } else {
                    startActivity(new Intent(mActivity, RegistrationActivity.class));
                    finishAffinity();
                }
            } else {
                startActivity(new Intent(mActivity, RegistrationActivity.class));
                finishAffinity();
            }
        }
    }

    private void initializeUi() {
        otpView = findViewById(R.id.otp_view);
        validateButton = findViewById(R.id.validate_button);
        lblNumber = findViewById(R.id.lblNumber);
    }

    private void setListeners() {
        validateButton.setOnClickListener(this);
        otpView.setOtpCompletionListener(this);
    }

    @Override
    public void onOtpCompleted(String otp) {
        // do Stuff
        if (otpView.getText().length() >= 4) {
            if (otpView.getText().toString().equalsIgnoreCase(randomOTP)) {
                callVerifyOTPApi();
            } else {
                Utils.showToastForOtp(mActivity, mActivity.getString(R.string.enter_correct_otp));
            }
        } else {
            Utils.showToastForOtp(mActivity, mActivity.getString(R.string.please_enter_otp));
        }

    }

    private boolean isCalling = false;

    private void callVerifyOTPApi() {
        if (Utils.isInternetAvailable(mActivity)) {
            if (!isCalling) {
                isCalling = true;
                ArtalentApplication.apiService.verifyOTP(sMobileNumber, fillData())
                        .enqueue(new Callback<OTPResponse>() {
                            @Override
                            public void onResponse(Call<OTPResponse> call, Response<OTPResponse> response) {
                                int statusCode = response.code();
                                if (response.body().getToken() != null && !TextUtils.isEmpty(response.body().getToken())) {
                                    ArtalentApplication.prefManager.setAuthorizationToken(response.body().getToken());
                                    if (response.body().getUserInfo() != null) {
                                        ArtalentApplication.prefManager.setUserObj(response.body().getUserInfo());
                                    }
                                    isCalling = false;
                                    startHomeActivity();
                                } else {
                                    isCalling = false;
                                    Utils.showToastForOtp(mActivity, mActivity.getString(R.string.please_try_again));
                                }
                            }

                            @Override
                            public void onFailure(Call<OTPResponse> call, Throwable t) {
                                // Log error here since request failed
                                Log.e("TAGArtalent", t.toString());
                                Utils.showToastForOtp(mActivity, "onFailure : " + t.toString());
                            }
                        });
            }

        } else {
            Utils.showToastForOtp(mActivity, getString(R.string.no_internet_connection_available));
        }
    }

    private CreateReq fillData() {
        CreateReq createReq = new CreateReq();
        createReq.setOtp(Constants.BACKEND_OTP_VALUE);
        return createReq;
    }
}
