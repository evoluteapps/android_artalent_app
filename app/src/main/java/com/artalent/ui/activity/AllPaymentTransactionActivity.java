package com.artalent.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.artalent.ArtalentApplication;
import com.artalent.R;
import com.artalent.Utils;
import com.artalent.ui.adapter.PaymentTransactionAdapter;
import com.artalent.ui.model.WalletTransactionListObj;
import com.artalent.ui.model.request.WalletResponseObj;
import com.artalent.ui.utils.Constants;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AllPaymentTransactionActivity extends BaseActivityOther {

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.pullToRefresh)
    SwipeRefreshLayout pullToRefresh;

    @BindView(R.id.no_payment_transaction_tv)
    TextView noPaymentTransactionTV;


    private PaymentTransactionAdapter feedAdapter;
    private List<WalletTransactionListObj> userObjList = new ArrayList<>();
    private int offset = 0;

    private LinearLayoutManager mLayoutManager;
    private boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_payment_transaction);
        setupToolbarBack(getString(R.string.all_transaction));
        setupFeed();
        refresh();
        initScrollListener();
        pullToRefresh.setOnRefreshListener(this::refresh);
    }

    private void initScrollListener() {
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NotNull RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();
                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            if (offset != -1) {
                                loading = false;
                                Log.e("mLayoutManager", "Last Item Wow !");
                                callHomePostApi();
                            }
                        }
                    }
                }
            }
        });
    }

    protected void setupFeed() {
        mLayoutManager = new LinearLayoutManager(mActivity);
        recyclerView.setLayoutManager(mLayoutManager);
        feedAdapter = new PaymentTransactionAdapter(userObjList, true, this);
        recyclerView.setAdapter(feedAdapter);
    }

    private void refresh() {
        userObjList.clear();
        offset = 0;
        callHomePostApi();
    }

    private void callHomePostApi() {
        if (Utils.isInternetAvailable(mActivity)) {
            Call<WalletResponseObj> call = ArtalentApplication.apiService.getWalletTransaction(ArtalentApplication.prefManager.getUserObj().getId(), offset);
            call.enqueue(new Callback<WalletResponseObj>() {
                @Override
                public void onResponse(Call<WalletResponseObj> call, Response<WalletResponseObj> response) {
                    pullToRefresh.setRefreshing(false);
                    loading = true;
                    if (response.body() != null &&
                            response.body().getObj() != null && response.body().getObj().getTransactionListObjs() != null) {
                        offset = response.body().getIndex();
                        if (response.body().getObj().getTransactionListObjs().size() > 0) {
                            userObjList.addAll(response.body().getObj().getTransactionListObjs());
                        }
                        feedAdapter.notifyDataSetChanged();
                    }
                    if (userObjList.size() == 0) {
                        noPaymentTransactionTV.setVisibility(View.VISIBLE);
                    } else {
                        noPaymentTransactionTV.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onFailure(Call<WalletResponseObj> call, Throwable t) {
                    // Log error here since request failed
                    loading = true;
                    Log.e("TAG", t.toString());
                    pullToRefresh.setRefreshing(false);
                }
            });
        } else {
            pullToRefresh.setRefreshing(false);
            Intent intent = new Intent(mActivity, NoInternetActivity.class);
            startActivityForResult(intent, Constants.NO_INTERNET);
        }
    }
}
