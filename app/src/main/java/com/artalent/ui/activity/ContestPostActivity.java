package com.artalent.ui.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextSwitcher;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.artalent.ArtalentApplication;
import com.artalent.R;
import com.artalent.Utils;
import com.artalent.autoplayrecyclervideoplayer.view.CenterLayoutManager;
import com.artalent.exo.DemoApp;
import com.artalent.ui.model.ContestObj;
import com.artalent.ui.model.InterestObj;
import com.artalent.ui.model.PostContestObj;
import com.artalent.ui.model.PostContestResponse;
import com.artalent.ui.model.VoteResponse;
import com.artalent.ui.model.request.ContestGetReq;
import com.artalent.ui.utils.Constants;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import im.ene.toro.CacheManager;
import im.ene.toro.widget.Container;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import com.artalent.exo.NewContestPostAdapter;

@SuppressLint("SourceLockedOrientationActivity")
public class ContestPostActivity extends BaseActivityOther implements View.OnClickListener, NewContestPostAdapter.OnFeedItemClickListener {

    public static final String TAG = "RecyclerViewFragment";

    @BindView(R.id.talent_label)
    TextView tvTalentLabel;

    @BindView(R.id.pullToRefresh)
    SwipeRefreshLayout pullToRefresh;

    @BindView(R.id.llNoPost)
    LinearLayout llNoPost;

    @BindView(R.id.container)
    public Container container;


    NewContestPostAdapter mAdapter;

    private boolean loading = true;
    private int offset = 0;
    List<PostContestObj> postObjs = new ArrayList<>();

    private ContestObj contestObj = null;
    private String location = "";
    private String contestTitle = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contest_post);
        DemoApp.Companion.setHelper(null);
        init1();
        initView();
        refresh();
        initScrollListener();
        pullToRefresh.setOnRefreshListener(this::refresh);
    }

    private void initView() {
        int pos = -1;
        for (int i = 0; i < postObjs.size(); i++) {
            if (postObjs.get(i) == null) {
                pos = i;
            }
        }
        if (pos != -1) {
            postObjs.remove(pos);
        }
        postObjs.add(null);
        mAdapter = new NewContestPostAdapter(mActivity, postObjs);
        mAdapter.setOnFeedItemClickListener(this);
        container.setLayoutManager(new CenterLayoutManager(mActivity));
        container.setAdapter(mAdapter);
        container.setCacheManager(CacheManager.DEFAULT);
    }


    private void initScrollListener() {
        container.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (!recyclerView.canScrollVertically(1) && newState == RecyclerView.SCROLL_STATE_IDLE) {

                    if (loading) {
                        if (offset != -1) {
                            loading = false;
                            callHomePostApi();
                        }
                    }
                }
            }
        });


    }

    private void refresh() {
        postObjs.clear();
        offset = 0;
        callHomePostApi();
    }

    private void init1() {
        contestObj = (ContestObj) getIntent().getSerializableExtra(Constants.DATA);
        String category = "";
        if (contestObj != null && contestObj.getInterest() != null && !TextUtils.isEmpty(contestObj.getInterest().getInterest())) {
            category = contestObj.getInterest().getInterest();
        } else {
            category = "";
        }

        if (contestObj != null) {
            if (contestObj.getFlag() == 1) {
                location = contestObj.getDistrictEntity().getDistrict();
                contestTitle = category + " " + "contest for " + location;
            } else if (contestObj.getFlag() == 2) {
                location = contestObj.getStateEntity().getState();
                contestTitle = category + " " + "contest for " + location;
            } else {
                location = contestObj.getCountryEntity().getCountryName();
                contestTitle = category + " " + "contest for " + location;
            }
        }

        tvTalentLabel.setText(location + " 's Trending Talent");
        setupToolbarBack(contestTitle, R.drawable.ic_arrow_back_white_24dp, R.color.primary, R.color.black);
    }

    private void callHomePostApi() {
        if (Utils.isInternetAvailable(mActivity)) {
            int[] intArray = new int[contestObj.getRegistrationDetails().size()];
            for (int i = 0; i < contestObj.getRegistrationDetails().size(); i++) {
                intArray[i] = contestObj.getRegistrationDetails().get(i).getRegisterationId();
            }
            ContestGetReq contestGetReq = new ContestGetReq();
            contestGetReq.setRegistrationIds(intArray);
            Call<PostContestResponse> call = ArtalentApplication.apiService.getPostContest(ArtalentApplication.prefManager.getUserObj().getId(), offset, contestGetReq);
            call.enqueue(new Callback<PostContestResponse>() {
                @Override
                public void onResponse(Call<PostContestResponse> call, Response<PostContestResponse> response) {
                    int statusCode = response.code();
                    pullToRefresh.setRefreshing(false);
                    loading = true;
                    if (response.body() != null && response.body().getObj() != null) {

                        if (response.body().getObj().size() > 0) {
                            postObjs.addAll(response.body().getObj());
                        }
                        mAdapter.notifyDataSetChanged();
                        offset = response.body().getIndex();
                    }
                    noResultFount(postObjs.size());
                }

                @Override
                public void onFailure(Call<PostContestResponse> call, Throwable t) {
                    // Log error here since request failed
                    loading = true;
                    pullToRefresh.setRefreshing(false);
                    noResultFount(postObjs.size());
                }
            });
        } else {
            pullToRefresh.setRefreshing(false);
        }

    }


    public void noResultFount(int size) {
        if (size == 0) {
            llNoPost.setVisibility(View.VISIBLE);
        } else {
            llNoPost.setVisibility(View.GONE);
        }
    }


    @Override
    public void onCommentsClick(View v, int position) {

    }

    @Override
    public void onVolumeClick(ImageView btnVolume, int position) {

    }

    @Override
    public void onRegisterClick(int position) {
        if (contestObj != null && contestObj.getInterest() != null) {
            InterestObj interestObj = contestObj.getInterest();
            if (interestObj != null) {
                Intent intent = new Intent(mActivity, ContestRegisterUpdateActivity.class);
                intent.putExtra(Constants.INTEREST_OBJ, interestObj);
                startActivity(intent);
            }
        }
    }

    @Override
    public void onProfileClick(View v) {

    }

    @Override
    public void onLikeClick(ImageView btnLike, int position, TextSwitcher tsLikesCounter) {

        PostContestObj postObj = postObjs.get(position);
        if (postObj != null && postObj.getContestPostId() > 0) {
            callVoteApi(btnLike, position, postObj, tsLikesCounter);
        }
    }

    private void callVoteApi(ImageView btnLike, int position, PostContestObj postObj, TextSwitcher tsLikesCounter) {
        if (Utils.isInternetAvailable(getApplicationContext())) {
            Call<VoteResponse> call = ArtalentApplication.apiService.callVote(ArtalentApplication.prefManager.getUserObj().getId(), postObj.getContestPostId());
            call.enqueue(new Callback<VoteResponse>() {
                @Override
                public void onResponse(Call<VoteResponse> call, Response<VoteResponse> response) {
                    int statusCode = response.code();
                    if (response.body() != null) {
                        if (postObj.getIsVoted().equals("0")) {
                            postObj.setVoteCount(postObj.getVoteCount() + 1);
                            btnLike.setImageResource(R.drawable.voted_colored);
                        }
                        tsLikesCounter.setCurrentText(getResources().getQuantityString(
                                R.plurals.votes_count, postObj.getVoteCount(), postObj.getVoteCount()
                        ));

                        Utils.showToast(ContestPostActivity.this, response.body().getMessage());
                    }
                }

                @Override
                public void onFailure(Call<VoteResponse> call, Throwable t) {
                    // Log error here since request failed
                }
            });
        }
    }


    @Override
    public void onClick(View v) {

    }

    @Override
    public void onDeleteClick(@NotNull PostContestObj postContestObj, int position) {

    }
}
