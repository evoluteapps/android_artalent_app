package com.artalent.ui.activity

import android.content.Context
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.viewpager.widget.PagerAdapter
import com.squareup.picasso.Picasso
import java.io.File
import java.util.*

class SelectedImageAdapterKotlin(private val context: Context, mList: ArrayList<String>) :
    PagerAdapter() {
    val TAG = "SelectedImageActivityKt"
    private var list = ArrayList<String>()

    init {
        Log.d(TAG, "SelectedImageAdapterKotlin init block - mList -> $mList")
        this.list = mList
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {

        val imageView = ImageView(context)
        imageView.maxHeight = 420
        val file = File(list[position])
        Picasso.get()
            .load(file)
            .into(imageView)
        container.addView(imageView)
        /*if (position == 0) {
            file?.let { imageFile ->
                (context as SelectedImageActivityKotlin).lifecycleScope.launch {
                }
            }
        }*/
        return imageView
    }

    private fun setCompressedImage(compressedImage: File?) {
        compressedImage?.let {
            // compressedImageView.setImageBitmap(BitmapFactory.decodeFile(it.absolutePath))
            //compressedSizeTextView.text = String.format("Size : %s", getReadableFileSize(it.length()))
            // Toast.makeText(context, "Compressed image save in " + it.path, Toast.LENGTH_LONG).show()
            Log.d("Compressor", "Compressed image save in " + it.path)
        }
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    override fun getCount(): Int {
        return list.size
    }

    override fun isViewFromObject(view: View, o: Any): Boolean {
        return view === o

    }
}
