package com.artalent.ui.activity;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;

import com.artalent.ui.adapter.AddWinnerAdapter;
import com.artalent.ui.model.ContestWinnerObj;
import com.artalent.ui.model.SpecialContestObj;
import com.google.android.material.textfield.TextInputEditText;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.provider.MediaStore;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.artalent.ArtalentApplication;
import com.artalent.R;
import com.artalent.Utils;
import com.artalent.ui.adapter.SelectedPostAdapter;
import com.artalent.ui.model.BaseResponse;
import com.artalent.ui.model.Country;
import com.artalent.ui.model.DistrictsObj;
import com.artalent.ui.model.InterestObj;
import com.artalent.ui.model.StateObj;
import com.artalent.ui.utils.Constants;
import com.artalent.ui.utils.ImageQuality;
import com.artalent.ui.utils.Options;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddSpecialContestActivity extends BaseActivityOther implements View.OnClickListener {

    private static final String TAG = "AddSpecialContestActivity";

    @BindView(R.id.et_company_name)
    TextInputEditText etCompanyName;

    @BindView(R.id.et_advertise_desc)
    EditText etAdvertiseDesc;


    @BindView(R.id.et_expiry_date)
    TextInputEditText etStarttDate;

    @BindView(R.id.et_reg_end_date)
    TextInputEditText etRegEndDate;

    @BindView(R.id.et_reg_fee)
    EditText etRegFee;


    @BindView(R.id.et_max_reg_count)
    TextInputEditText etMaxRegCount;


    @BindView(R.id.et_end_date)
    TextInputEditText etEndDate;

    @BindView(R.id.edtCountry)
    TextInputEditText edtCountry;

    @BindView(R.id.et_state)
    TextInputEditText edtState;

    @BindView(R.id.et_district)
    TextInputEditText edtDistrict;

    @BindView(R.id.edtInterest)
    TextInputEditText edtInterest;


    private InterestObj selectedInterest = null;
    private Country selectedCountry = null;
    private StateObj selectedStateObj = null;

    private DistrictsObj selectedDistrictsObj = null;
    public static final int REQUEST_COUNTRY = 200;
    public static final int REQUEST_STATE = 300;
    public static final int REQUEST_DISTRICT = 400;
    public static final int REQUEST_INTEREST = 500;

    @BindView(R.id.recyclerViewMyPost)
    RecyclerView recyclerViewMyPost;

    @BindView(R.id.recyclerAddWinner)
    RecyclerView recyclerAddWinner;


    @BindView(R.id.rlPhoto)
    RelativeLayout selectBanerImage;

    @BindView(R.id.ivAddWinner)
    ImageView ivAddWinner;


    String companyName, advertiseDescription, expiryDate;

    private boolean isClick = false;

    private Options options;
    ArrayList<String> returnValue = new ArrayList<>();

    ArrayList<ContestWinnerObj> winnerAddList = new ArrayList<>();
    private SpecialContestObj specialContestObj;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        setContentView(R.layout.activity_add_special_contest);
        specialContestObj = (SpecialContestObj) getIntent().getSerializableExtra(Constants.DATA);
        if (specialContestObj != null && specialContestObj.getAdminContestId() > 0) {
            setupToolbarBack(mActivity.getString(R.string.edit_special_contest));
        } else {
            setupToolbarBack(mActivity.getString(R.string.add_special_contest));
        }

        OnClickView();
        options = Options.init()
                .setRequestCode(100)
                .setCount(3)
                .setFrontfacing(false)
                .setImageQuality(ImageQuality.LOW)
                .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)
                .setPath("/akshay/new");
        setEditContestData();

        ContestWinnerObj contestWinnerObj = new ContestWinnerObj();
        winnerAddList.add(contestWinnerObj);
        adapterWinner = new AddWinnerAdapter(mActivity, winnerAddList, this, new AddWinnerAdapter.OnEditTextChanged() {
            @Override
            public void onTextChangedTitle(int position, String charSeq) {
                Log.e("charSeq", "TITLE : " + position + " charSeq : " + charSeq);
                winnerAddList.get(position).setWinner_title(charSeq);
            }

            @Override
            public void onTextChangedPrice(int position, String charSeq) {
                Log.e("charSeq", "PRICE : " + position + " charSeq : " + charSeq);
                winnerAddList.get(position).setPrice(charSeq);
            }

            @Override
            public void onTextChangedRegUpto(int position, String charSeq) {
                Log.e("charSeq", "UPTO : " + position + " charSeq : " + charSeq);
                try {
                    int count = Integer.parseInt(charSeq);
                    winnerAddList.get(position).setReg_count_upto(count);
                } catch (NumberFormatException e) {
                    winnerAddList.get(position).setReg_count_upto(0);
                    e.printStackTrace();
                }
            }
        });
        recyclerAddWinner.setLayoutManager(new LinearLayoutManager(this));
        recyclerAddWinner.setItemAnimator(new DefaultItemAnimator());
        recyclerAddWinner.setAdapter(adapterWinner);

    }

    AddWinnerAdapter adapterWinner;

    private void setEditContestData() {
        if (specialContestObj != null && specialContestObj.getAdminContestId() > 0) {
            if (!TextUtils.isEmpty(specialContestObj.getTittle()))
                etCompanyName.setText(specialContestObj.getTittle());

            if (!TextUtils.isEmpty(specialContestObj.getDescription()))
                etAdvertiseDesc.setText(specialContestObj.getDescription());

            if (!TextUtils.isEmpty(specialContestObj.getStartDate()))
                etStarttDate.setText(specialContestObj.getStartDate());

            if (!TextUtils.isEmpty(specialContestObj.getEndDate()))
                etEndDate.setText(specialContestObj.getEndDate());

            if (specialContestObj.getInterestObj() != null && !TextUtils.isEmpty(specialContestObj.getInterestObj().getInterest())) {
                edtInterest.setText(specialContestObj.getInterestObj().getInterest());
                selectedInterest = specialContestObj.getInterestObj();
            }
            /*
            start
             */

            if (specialContestObj.getCountry() != null && !TextUtils.isEmpty(specialContestObj.getCountry().getCountryName())) {
                edtCountry.setText(specialContestObj.getCountry().getCountryName());
                selectedCountry = specialContestObj.getCountry();
            }

            if (specialContestObj.getState() != null && !TextUtils.isEmpty(specialContestObj.getState().getState())) {
                edtState.setText(specialContestObj.getState().getState());
                selectedStateObj = specialContestObj.getState();
            }

            if (specialContestObj.getDistricts() != null && !TextUtils.isEmpty(specialContestObj.getDistricts().getDistrict())) {
                edtDistrict.setText(specialContestObj.getDistricts().getDistrict());
                selectedDistrictsObj = specialContestObj.getDistricts();
            }

        }
    }

    private void OnClickView() {
        selectBanerImage.setOnClickListener(this);
        etStarttDate.setOnClickListener(this);
        etRegEndDate.setOnClickListener(this);
        etEndDate.setOnClickListener(this);
        edtCountry.setOnClickListener(this);
        edtState.setOnClickListener(this);
        edtDistrict.setOnClickListener(this);
        edtInterest.setOnClickListener(this);
        ivAddWinner.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_post, menu);
        return true;
    }

    public boolean validateEditText() {
        boolean isEmpty = false;
        if (TextUtils.isEmpty(Objects.requireNonNull(etCompanyName.getText()).toString().trim())) {
            isEmpty = true;
            Utils.showToast(this, "Enter Title");
        } else if (TextUtils.isEmpty(Objects.requireNonNull(etRegFee.getText()).toString().trim())) {
            isEmpty = true;
            Utils.showToast(this, "Enter Reg Fee");
        } else if (TextUtils.isEmpty(Objects.requireNonNull(etMaxRegCount.getText()).toString().trim())) {
            isEmpty = true;
            Utils.showToast(this, "Please Enter Max. Registration Limit");
        } else if (TextUtils.isEmpty(Objects.requireNonNull(etRegEndDate.getText()).toString().trim())) {
            isEmpty = true;
            Utils.showToast(this, "Set Contest Reg. End date");
        } else if (TextUtils.isEmpty(Objects.requireNonNull(etStarttDate.getText()).toString().trim())) {
            isEmpty = true;
            Utils.showToast(this, "Set Contest Start date");
        } else if (TextUtils.isEmpty(Objects.requireNonNull(etEndDate.getText()).toString().trim())) {
            isEmpty = true;
            Utils.showToast(this, "Set Contest End date");
        } else if (selectedInterest == null) {
            isEmpty = true;
            Utils.showToast(this, getString(R.string.select_interest));
        } else if (selectedCountry == null) {
            isEmpty = true;
            Utils.showToast(this, getString(R.string.select_country));
        } else if (returnValue != null && !(returnValue.size() > 0)) {
            if (specialContestObj != null && specialContestObj.getAdminContestId() > 0) {

            } else {
                isEmpty = true;
                Utils.showToast(this, "Please select contest image");
            }

        }
        return isEmpty;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_add) {
            if (!validateEditText()) {
                if (isWinnerCount()) {
                    if (specialContestObj != null && specialContestObj.getAdminContestId() > 0) {
                        if (returnValue.size() == 0) {
                            callEditAdvertiseWithoutImage();
                        } else {
                            callEditAdvertise();
                        }
                    } else {
                        callAddAdvertise();
                    }

                }
            }

        }
        return super.onOptionsItemSelected(item);
    }

    private boolean isWinnerCount() {
        if (winnerAddList.size() == 1) {
            ContestWinnerObj contestWinnerObj = winnerAddList.get(0);
            if (!TextUtils.isEmpty(contestWinnerObj.getWinner_title())) {
                if (!TextUtils.isEmpty(contestWinnerObj.getPrice())) {
                    if (contestWinnerObj.getReg_count_upto() > 0) {
                        return true;
                    } else {
                        Utils.showToast(mActivity, "Please enter winner Reg. Count Upto");
                        return false;
                    }
                } else {
                    Utils.showToast(mActivity, "Please enter winner price");
                    return false;
                }

            } else {
                Utils.showToast(mActivity, "Please add winner title");
                return false;
            }

        } else {
            boolean check = false;
            for (int i = 0; i < winnerAddList.size(); i++) {
                ContestWinnerObj contestWinnerObj = winnerAddList.get(i);
                if (!TextUtils.isEmpty(contestWinnerObj.getWinner_title())) {
                    if (!TextUtils.isEmpty(contestWinnerObj.getPrice())) {
                        if (contestWinnerObj.getReg_count_upto() > 0) {
                            if (i != 0) {
                                ContestWinnerObj contestWinnerObjOLD = winnerAddList.get(i - 1);
                                if (contestWinnerObj.getReg_count_upto() > contestWinnerObjOLD.getReg_count_upto()) {
                                    check = true;
                                } else {
                                    check = false;
                                }
                            } else {
                                check = true;
                            }

                        } else {
                            check = false;
                        }
                    } else {
                        check = false;
                    }

                } else {
                    check = false;
                }
            }
            if (!check) {
                Utils.showToast(mActivity, "please add winner data and reg. Upto count greater than previous");
            }
            return check;

        }


    }


    public void callAddAdvertise() {
        if (!isClick) {
            if (Utils.isInternetAvailable(AddSpecialContestActivity.this)) {
                String sWinnerData = "";
                JSONArray jsonArray = new JSONArray();
                for (int i = 0; i < winnerAddList.size(); i++) {
                    ContestWinnerObj contestWinnerObj = winnerAddList.get(i);
                    try {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("winner_title", contestWinnerObj.getWinner_title());
                        jsonObject.put("winner_type", "" + (i + 1));
                        jsonObject.put("price", "" + contestWinnerObj.getPrice());
                        jsonObject.put("reg_count_upto", "" + contestWinnerObj.getReg_count_upto());
                        jsonArray.put(jsonObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                sWinnerData = jsonArray.toString();
                showProgress();
                isClick = true;
                File file = new File(returnValue.get(0));
                RequestBody postBody = RequestBody.create(MediaType.parse("image/*"), file);
                MultipartBody.Part advertiseImagePart = MultipartBody.Part.createFormData("file", file.getName(), postBody);

                RequestBody tittle = RequestBody.create(MediaType.parse("multipart/form-data"), etCompanyName.getText().toString());
                RequestBody description = RequestBody.create(MediaType.parse("multipart/form-data"), etAdvertiseDesc.getText().toString());
                RequestBody interestId = RequestBody.create(MediaType.parse("multipart/form-data"), "" + selectedInterest.getInterestMasterId());
                RequestBody countryId = RequestBody.create(MediaType.parse("multipart/form-data"), "" + selectedCountry.getCountryId());
                RequestBody stateId = RequestBody.create(MediaType.parse("multipart/form-data"), "" + selectedStateObj.getStateId());
                RequestBody districtId = RequestBody.create(MediaType.parse("multipart/form-data"), "" + selectedDistrictsObj.getDistrictId());
                RequestBody userId = RequestBody.create(MediaType.parse("multipart/form-data"), "" + ArtalentApplication.prefManager.getUserObj().getId());
                RequestBody startDate = RequestBody.create(MediaType.parse("multipart/form-data"), etStarttDate.getText().toString());
                RequestBody endDate = RequestBody.create(MediaType.parse("multipart/form-data"), etEndDate.getText().toString());
                RequestBody regEndDate = RequestBody.create(MediaType.parse("multipart/form-data"), etRegEndDate.getText().toString());
                RequestBody maxRegCount = RequestBody.create(MediaType.parse("multipart/form-data"), etMaxRegCount.getText().toString());
                RequestBody winnerData = RequestBody.create(MediaType.parse("multipart/form-data"), sWinnerData);
                RequestBody regFee = RequestBody.create(MediaType.parse("multipart/form-data"), etRegFee.getText().toString());


                ArtalentApplication.apiServicePost.addAdminContest(ArtalentApplication.prefManager.getUserObj().getId(), advertiseImagePart,
                        tittle, description, interestId, countryId, stateId, districtId, userId, startDate, endDate, winnerData, regEndDate, maxRegCount, regFee)
                        .enqueue(new Callback<BaseResponse>() {
                            @Override
                            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                                int statusCode = response.code();
                                hideProgress();
                                String mess = response.body().getMessage();
                                if (statusCode == 200) {
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            hideProgress();
                                            isClick = false;
                                            finish();
                                        }
                                    }, 2000);

                                    Utils.showToast(AddSpecialContestActivity.this, mess);
                                } else {
                                    hideProgress();
                                    isClick = false;
                                }
                            }

                            @Override
                            public void onFailure(Call<BaseResponse> call, Throwable t) {
                                // Log error here since request failed
                                Log.e("TAGArtalent", t.toString());
                                hideProgress();
                                isClick = false;
                                Utils.showToast(AddSpecialContestActivity.this, "onFailure : " + t.toString());
                            }
                        });
            }
        }
    }


    public void callEditAdvertise() {
        if (!isClick) {
            if (Utils.isInternetAvailable(AddSpecialContestActivity.this)) {
                showProgress();
                isClick = true;
                File file = new File(returnValue.get(0));
                RequestBody postBody = RequestBody.create(MediaType.parse("image/*"), file);
                MultipartBody.Part advertiseImagePart = MultipartBody.Part.createFormData("file", file.getName(), postBody);

                RequestBody tittle = RequestBody.create(MediaType.parse("multipart/form-data"), etCompanyName.getText().toString());
                RequestBody description = RequestBody.create(MediaType.parse("multipart/form-data"), etAdvertiseDesc.getText().toString());
                RequestBody interestId = RequestBody.create(MediaType.parse("multipart/form-data"), "" + selectedInterest.getInterestMasterId());
                RequestBody countryId = RequestBody.create(MediaType.parse("multipart/form-data"), "" + selectedCountry.getCountryId());
                RequestBody stateId = RequestBody.create(MediaType.parse("multipart/form-data"), "" + selectedStateObj.getStateId());
                RequestBody districtId = RequestBody.create(MediaType.parse("multipart/form-data"), "" + selectedDistrictsObj.getDistrictId());
                RequestBody userId = RequestBody.create(MediaType.parse("multipart/form-data"), "" + ArtalentApplication.prefManager.getUserObj().getId());
                RequestBody startDate = RequestBody.create(MediaType.parse("multipart/form-data"), etStarttDate.getText().toString());
                RequestBody endDate = RequestBody.create(MediaType.parse("multipart/form-data"), etEndDate.getText().toString());

                ArtalentApplication.apiServicePost.editSpecialContest(ArtalentApplication.prefManager.getUserObj().getId(), specialContestObj.getAdminContestId(), advertiseImagePart, tittle, description, interestId, countryId, stateId, districtId, userId, startDate, endDate)
                        .enqueue(new Callback<BaseResponse>() {
                            @Override
                            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                                int statusCode = response.code();
                                hideProgress();
                                String mess = response.body().getMessage();
                                if (statusCode == 200) {
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            hideProgress();
                                            isClick = false;
                                            finish();
                                        }
                                    }, 2000);

                                    Utils.showToast(AddSpecialContestActivity.this, mess);
                                } else {
                                    hideProgress();
                                    isClick = false;
                                }
                            }

                            @Override
                            public void onFailure(Call<BaseResponse> call, Throwable t) {
                                // Log error here since request failed
                                Log.e("TAGArtalent", t.toString());
                                hideProgress();
                                isClick = false;
                                Utils.showToast(AddSpecialContestActivity.this, "onFailure : " + t.toString());
                            }
                        });
            }
        }
    }

    public void callEditAdvertiseWithoutImage() {
        if (!isClick) {
            if (Utils.isInternetAvailable(AddSpecialContestActivity.this)) {
                showProgress();
                isClick = true;
                RequestBody postBody = RequestBody.create(MediaType.parse("image/*"), "");
                MultipartBody.Part advertiseImagePart = MultipartBody.Part.createFormData("file", "", postBody);

                RequestBody tittle = RequestBody.create(MediaType.parse("multipart/form-data"), etCompanyName.getText().toString());
                RequestBody description = RequestBody.create(MediaType.parse("multipart/form-data"), etAdvertiseDesc.getText().toString());
                RequestBody interestId = RequestBody.create(MediaType.parse("multipart/form-data"), "" + selectedInterest.getInterestMasterId());
                RequestBody countryId = RequestBody.create(MediaType.parse("multipart/form-data"), "" + selectedCountry.getCountryId());
                RequestBody stateId = RequestBody.create(MediaType.parse("multipart/form-data"), "" + selectedStateObj.getStateId());
                RequestBody districtId = RequestBody.create(MediaType.parse("multipart/form-data"), "" + selectedDistrictsObj.getDistrictId());
                RequestBody userId = RequestBody.create(MediaType.parse("multipart/form-data"), "" + ArtalentApplication.prefManager.getUserObj().getId());
                RequestBody startDate = RequestBody.create(MediaType.parse("multipart/form-data"), etStarttDate.getText().toString());
                RequestBody endDate = RequestBody.create(MediaType.parse("multipart/form-data"), etEndDate.getText().toString());
                RequestBody regEndDate = RequestBody.create(MediaType.parse("multipart/form-data"), etRegEndDate.getText().toString());
                RequestBody maxRegCount = RequestBody.create(MediaType.parse("multipart/form-data"), etMaxRegCount.getText().toString());

                ArtalentApplication.apiServicePost.editSpecialContest(ArtalentApplication.prefManager.getUserObj().getId(), specialContestObj.getAdminContestId(), advertiseImagePart, tittle, description, interestId, countryId, stateId, districtId, userId, startDate, endDate)
                        .enqueue(new Callback<BaseResponse>() {
                            @Override
                            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                                int statusCode = response.code();
                                hideProgress();
                                String mess = response.body().getMessage();
                                if (statusCode == 200) {
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            hideProgress();
                                            isClick = false;
                                            finish();
                                        }
                                    }, 2000);

                                    Utils.showToast(AddSpecialContestActivity.this, mess);
                                } else {
                                    hideProgress();
                                    isClick = false;
                                }
                            }

                            @Override
                            public void onFailure(Call<BaseResponse> call, Throwable t) {
                                // Log error here since request failed
                                Log.e("TAGArtalent", t.toString());
                                hideProgress();
                                isClick = false;
                                Utils.showToast(AddSpecialContestActivity.this, "onFailure : " + t.toString());
                            }
                        });
            }
        }
    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.ivClose) {
            int position = (int) v.getTag();
            if (position < returnValue.size()) {
                returnValue.remove(position);
                adapter.notifyDataSetChanged();
            }
        } else if (v.getId() == R.id.et_reg_end_date) {
            int mYear, mMonth, mDay;
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                    (view, year, monthOfYear, dayOfMonth) -> {
                        String month = "" + (monthOfYear + 1);
                        if ((monthOfYear + 1) < 10) {
                            month = "0" + month;
                        }
                        String day = "" + dayOfMonth;
                        if (dayOfMonth < 10) {
                            day = "0" + day;
                        }

                        etRegEndDate.setText(year + "-" + month + "-" + day);
                    }, mYear, mMonth, mDay);
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
            datePickerDialog.show();
        } else if (v.getId() == R.id.et_expiry_date) {
            int mYear, mMonth, mDay;
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                    (view, year, monthOfYear, dayOfMonth) -> {
                        String month = "" + (monthOfYear + 1);
                        if ((monthOfYear + 1) < 10) {
                            month = "0" + month;
                        }
                        String day = "" + dayOfMonth;
                        if (dayOfMonth < 10) {
                            day = "0" + day;
                        }

                        etStarttDate.setText(year + "-" + month + "-" + day);
                    }, mYear, mMonth, mDay);
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
            datePickerDialog.show();
        } else if (v.getId() == R.id.ivAddWinner) {
            winnerAddList.add(new ContestWinnerObj());
            adapterWinner.notifyDataSetChanged();
        } else if (v.getId() == R.id.ivDelete) {
            int position = (int) v.getTag();
            winnerAddList.remove(position);
            adapterWinner.notifyDataSetChanged();
        } else if (v.getId() == R.id.et_end_date) {
            int mYear, mMonth, mDay;
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                    (view, year, monthOfYear, dayOfMonth) -> {
                        String month = "" + (monthOfYear + 1);
                        if ((monthOfYear + 1) < 10) {
                            month = "0" + month;
                        }
                        String day = "" + dayOfMonth;
                        if (dayOfMonth < 10) {
                            day = "0" + day;
                        }
                        etEndDate.setText(year + "-" + month + "-" + day);
                    }, mYear, mMonth, mDay);
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
            datePickerDialog.show();
        } else if (v.getId() == R.id.rlPhoto) {
            openCameraGallery();
        } else if (v.getId() == R.id.edtCountry) {
            SelectCountryActivity.startActivity(mActivity, selectedCountry, REQUEST_COUNTRY);
        } else if (v.getId() == R.id.et_state) {
            if (selectedCountry != null && selectedCountry.getCountryId() > 0) {
                SelectStateActivity.startActivity(mActivity, selectedStateObj, selectedCountry.getCountryId(), REQUEST_STATE);
            } else {
                Utils.showToast(mActivity, mActivity.getString(R.string.select_country_first));
            }

        } else if (v.getId() == R.id.et_district) {
            if (selectedStateObj != null && selectedStateObj.getStateId() > 0) {
                SelectDistrictsActivity.startActivity(mActivity, selectedDistrictsObj, selectedStateObj.getStateId(), REQUEST_DISTRICT);
            } else {
                Utils.showToast(mActivity, mActivity.getString(R.string.select_state_first));
            }

        } else if (v.getId() == R.id.edtInterest) {
            SelectInterestNewActivity.startActivity(mActivity, selectedInterest, REQUEST_INTEREST);
        }

    }

    private void openCameraGallery(){
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            showImagePickerOptions();
                        }

                        if (report.isAnyPermissionPermanentlyDenied()) {
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();

    }
    private void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        builder.setTitle(getString(R.string.dialog_permission_title));
        builder.setMessage(getString(R.string.dialog_permission_message));
        builder.setPositiveButton(getString(R.string.go_to_settings), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                AddSpecialContestActivity.this.openSettings();
            }
        });
        builder.setNegativeButton(getString(android.R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();

    }

    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }


    private void showImagePickerOptions() {
        ImagePickerActivity.showImagePicker(this, new ImagePickerActivity.PickerOptionListener() {
            @Override
            public void onTakeCameraSelected() {
                launchCameraIntent();
            }

            @Override
            public void onChooseGallerySelected() {
                launchGalleryIntent();
            }

            @Override
            public void onDeleteImage() {

            }

        });
    }

    public static final int REQUEST_IMAGE = 100;
    private void launchCameraIntent() {
        Intent intent = new Intent(mActivity, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_IMAGE_CAPTURE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 16); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 9);

        /*// setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 1000);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 1000);*/

        startActivityForResult(intent, REQUEST_IMAGE);
    }

    private void launchGalleryIntent() {
        Intent intent = new Intent(mActivity, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_GALLERY_IMAGE);

  /*      // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 16); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 9);*/
        startActivityForResult(intent, REQUEST_IMAGE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_INTEREST) {
            if (resultCode == Activity.RESULT_OK) {
                selectedInterest = (InterestObj) data.getSerializableExtra(Constants.INTEREST_OBJ);
                if (selectedInterest != null && !TextUtils.isEmpty(selectedInterest.getInterest())) {
                    edtInterest.setText(selectedInterest.getInterest());
                }
            }
        } else if (requestCode == REQUEST_COUNTRY) {
            if (resultCode == Activity.RESULT_OK) {
                selectedCountry = (Country) data.getSerializableExtra(Constants.COUNTRY_OBJ);
                if (selectedCountry != null && !TextUtils.isEmpty(selectedCountry.getCountryName())) {
                    edtCountry.setText(selectedCountry.getCountryName());
                    selectedStateObj = null;
                    if (selectedStateObj != null && !TextUtils.isEmpty(selectedStateObj.getState())) {
                        edtState.setText(selectedStateObj.getState());
                    } else {
                        edtState.setText("");
                    }

                    selectedDistrictsObj = null;
                    if (selectedDistrictsObj != null && !TextUtils.isEmpty(selectedDistrictsObj.getDistrict())) {
                        edtDistrict.setText(selectedDistrictsObj.getDistrict());
                    } else {
                        edtDistrict.setText("");
                    }
                }
            }
        } else if (requestCode == REQUEST_STATE) {
            if (resultCode == Activity.RESULT_OK) {
                selectedStateObj = (StateObj) data.getSerializableExtra(Constants.STATE_OBJ);
                if (selectedStateObj != null && !TextUtils.isEmpty(selectedStateObj.getState())) {
                    edtState.setText(selectedStateObj.getState());
                }
                selectedDistrictsObj = null;
                if (selectedDistrictsObj != null && !TextUtils.isEmpty(selectedDistrictsObj.getDistrict())) {
                    edtDistrict.setText(selectedDistrictsObj.getDistrict());
                } else {
                    edtDistrict.setText("");
                }
            }
        } else if (requestCode == REQUEST_DISTRICT) {
            if (resultCode == Activity.RESULT_OK) {
                selectedDistrictsObj = (DistrictsObj) data.getSerializableExtra(Constants.STATE_OBJ);
                if (selectedDistrictsObj != null && !TextUtils.isEmpty(selectedDistrictsObj.getDistrict())) {
                    edtDistrict.setText(selectedDistrictsObj.getDistrict());
                }
            }
        } else if (requestCode == REQUEST_IMAGE) { // Please, use a final int instead of hardcoded int value
            if (resultCode == RESULT_OK) {
                try {
                    /*returnValue.addAll(data.getStringArrayListExtra(AppCameraActivity.IMAGE_RESULTS));
                    adapter = new SelectedPostAdapter(mActivity, returnValue, this);
                    recyclerViewMyPost.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
                    recyclerViewMyPost.setItemAnimator(new DefaultItemAnimator());
                    recyclerViewMyPost.setAdapter(adapter);*/
                    Uri uri = data.getParcelableExtra("path");
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
                    saveImageFile(bitmap);
                    returnValue.add(saveImageFile(bitmap));
                } catch (Exception e) {
                    Log.d(TAG, "onActivityResult: exception - " + e.getMessage());
                }
            }
        }
    }

    public String saveImageFile(Bitmap bitmap) {
        FileOutputStream out = null;
        String filename = getFilename();
        try {
            out = new FileOutputStream(filename);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
            Log.e("Picasso123", filename);
            //
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return filename;
    }

    private String getFilename() {
        File file = new File(Environment.getExternalStorageDirectory()
                .getPath(), "Artalent");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/art_"
                + System.currentTimeMillis() + ".jpg");
        return uriSting;
    }

    SelectedPostAdapter adapter;
}
