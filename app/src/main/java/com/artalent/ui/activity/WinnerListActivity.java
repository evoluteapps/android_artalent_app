package com.artalent.ui.activity;

import android.os.Bundle;

import com.artalent.ui.fragment.AllWinnerFragment;
import com.google.android.material.tabs.TabLayout;

import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.artalent.R;
import com.artalent.ui.adapter.AdvertisePanelTabAdapter;
import com.artalent.ui.fragment.LastWeekWinnerFragment;

public class WinnerListActivity extends BaseActivityOther {

    AdvertisePanelTabAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_winner_list);
        setupToolbarBack("Wall of Fame : The Winner List", R.drawable.ic_arrow_back_white_24dp, R.color.primary, R.color.black);
        ViewPager viewPager = findViewById(R.id.viewPager);
        TabLayout tabLayout = findViewById(R.id.tabLayout);
        adapter = new AdvertisePanelTabAdapter(getSupportFragmentManager(), FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        adapter.addFragment(new LastWeekWinnerFragment(), "Last Week");
        adapter.addFragment(new AllWinnerFragment(), "All Time");
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
    }
}
