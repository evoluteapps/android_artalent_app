package com.artalent.ui.activity;

import android.content.Intent;
import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.artalent.ArtalentApplication;
import com.artalent.R;
import com.artalent.Utils;
import com.artalent.ui.adapter.PaymentTransactionAdapter;
import com.artalent.ui.model.WalletTransactionListObj;
import com.artalent.ui.model.request.WalletResponseObj;
import com.artalent.ui.razorpay.RazorpayTestActivity;
import com.artalent.ui.utils.Constants;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ArtalentWalletActivity extends BaseActivityOther implements View.OnClickListener {

    @BindView(R.id.transaction_recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.show_all_transaction_btn)
    Button btnShowAll;

    @BindView(R.id.total_balance_amount)
    TextView totalBalanceAmount;

    @BindView(R.id.wallet_amount)
    TextView walletAmount;

    @BindView(R.id.referral_amount)
    TextView referralAmount;

    @BindView(R.id.btnAddBalance)
    Button btnAddBalance;


    @BindView(R.id.refer_earn_parent_rl)
    RelativeLayout referEarnParent;

    private List<WalletTransactionListObj> walletTransactionListObjs = new ArrayList<>();

    private PaymentTransactionAdapter paymentTransactionAdapter;
    private double dWalletBalance = 0.0;
    private double dReferralBalance = 0.0;
    private double dTotalWalletBalance = 0.0;
    private int offset = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_artalent_wallet);

        setupToolbarBack("My Wallet", R.drawable.ic_vector_arrow_back_black, R.color.primary, R.color.white);


//        Toolbar toolbar = findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
        btnShowAll.setOnClickListener(this);
        btnAddBalance.setOnClickListener(this);
        callWalletTransactionGetApi();
    }

    public void setTransactionAdapter() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        paymentTransactionAdapter = new PaymentTransactionAdapter(walletTransactionListObjs, false, this);
        recyclerView.setAdapter(paymentTransactionAdapter);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.show_all_transaction_btn) {
            Intent intent = new Intent(getApplicationContext(), AllPaymentTransactionActivity.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable("walletTransactionList", (Serializable) walletTransactionListObjs);
            intent.putExtras(bundle);
            startActivity(intent);
        } else if (view.getId() == R.id.btnAddBalance) {
            if (dTotalWalletBalance >= 100) {
                Intent intent1 = new Intent(mActivity, RazorpayTestActivity.class);
                intent1.putExtra(Constants.DATA, dTotalWalletBalance);
                startActivity(intent1);
                finish();
            } else {
                Utils.showToast(mActivity, "you don't have enough funds, withdrawal amount at least 100 rupees");
            }

        }
    }

    @OnClick(R.id.refer_earn_card)
    public void openReferEarnActivity() {
        Intent intent = new Intent(mActivity, ReferEarnActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.how_to_earn_txt)
    public void openReferEarn() {
        Intent intent = new Intent(mActivity, ReferEarnActivity.class);
        startActivity(intent);
    }

    private void callWalletTransactionGetApi() {
        if (Utils.isInternetAvailable(mActivity)) {
            Call<WalletResponseObj> call = ArtalentApplication.apiService.getWalletTransaction(ArtalentApplication.prefManager.getUserObj().getId(), offset);
            call.enqueue(new Callback<WalletResponseObj>() {
                @Override
                public void onResponse(Call<WalletResponseObj> call, Response<WalletResponseObj> response) {

                    if (response.body() != null && response.body().getObj() != null && response.body().getObj() != null) {
                        dReferralBalance = response.body().getObj().getReferralBalance();
                        dWalletBalance = response.body().getObj().getWalletBalance();
                        dTotalWalletBalance = dReferralBalance + dWalletBalance;
                        referralAmount.setText(getResources().getString(R.string.rupee) + " " + dReferralBalance);
                        walletAmount.setText(getResources().getString(R.string.rupee) + " " + dWalletBalance);
                        totalBalanceAmount.setText(getResources().getString(R.string.rupee) + " " + dTotalWalletBalance);
                        if (response.body().getObj().getTransactionListObjs().size() > 0) {
                            walletTransactionListObjs.addAll(response.body().getObj().getTransactionListObjs());
                        }
                        setTransactionAdapter();
                    }
                }

                @Override
                public void onFailure(Call<WalletResponseObj> call, Throwable t) {
                    // Log error here since request failed
                    Log.e("TAG", t.toString());
                }
            });
        } else {
            Intent intent = new Intent(mActivity, NoInternetActivity.class);
            startActivityForResult(intent, Constants.NO_INTERNET);
        }
    }

}
