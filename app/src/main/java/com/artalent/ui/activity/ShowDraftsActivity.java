package com.artalent.ui.activity;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.artalent.R;
import com.artalent.Utils;
import com.artalent.ui.adapter.DraftAdapter;
import com.artalent.ui.contestcamera.CameraActivity;
import com.artalent.ui.model.room.ContestVideoDraft;
import com.artalent.ui.roomdb.viewmodel.DraftViewModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class ShowDraftsActivity extends BaseActivityOther implements DraftAdapter.DraftClickInterface {

    private DraftViewModel draftViewModel;

    @BindView(R.id.draft_recycler_view)
    RecyclerView draftRecyclerView;

    @BindView(R.id.no_draft_found_txt)
    TextView noDraftFoundTxt;

    @BindView(R.id.create_a_new_draft_fab)
    FloatingActionButton createANewDraftFab;

    @BindView(R.id.parent_show_draft_activity)
    ConstraintLayout parentShowDraftActivity;

    DraftAdapter draftAdapter;
    private List<ContestVideoDraft> savedDraftsInDb = new ArrayList<ContestVideoDraft>();
    private ContestVideoDraft deletedDraft;
    private static final String TAG = "ShowDraftActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_drafts);

        setupToolbarBack("Saved Drafts", R.drawable.ic_vector_arrow_back_black, R.color.black, R.color.white);

        draftViewModel = new ViewModelProvider(this).get(DraftViewModel.class);

        //Setting up recyclerview
        draftAdapter = new DraftAdapter(ContestVideoDraft.itemCallback, this);
        draftRecyclerView.setAdapter(draftAdapter);

        draftViewModel.getSavedDraft().observe(this, new Observer<List<ContestVideoDraft>>() {
            @Override
            public void onChanged(List<ContestVideoDraft> contestVideoDrafts) {

                if (contestVideoDrafts.size() < 1) {
                    noDraftFoundTxt.setVisibility(View.VISIBLE);
                } else {
                    for (ContestVideoDraft draft : contestVideoDrafts) {
                        if (new File(draft.getStoragePath()).exists()) {
                            Log.d(TAG, "Is it available in storage -> : YES");
                        } else {
                            Log.d(TAG, "Is it available in storage -> : No");
                            draftViewModel.delete(draft);
                        }
                    }
                    noDraftFoundTxt.setVisibility(View.GONE);
                }
                savedDraftsInDb = contestVideoDrafts;
                draftAdapter.submitList(contestVideoDrafts);
            }
        });

        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                //Remove swiped item from list and notify the RecyclerView
                int position = viewHolder.getAdapterPosition();
//                deletedDraft = savedDraftsInDb.get(position);
//                draftViewModel.delete(savedDraftsInDb.get(position));

                ContestVideoDraft swipedItem = savedDraftsInDb.get(position);

                Snackbar snackbar = Snackbar.make(parentShowDraftActivity, "Do you want to delete it?", Snackbar.LENGTH_LONG);
                snackbar.setActionTextColor(Color.WHITE);
                snackbar.setAction("Yes", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        draftViewModel.delete(swipedItem);
                        try {
                            new File(swipedItem.getStoragePath()).delete();
                        } catch (Exception e) {
                            Log.d(TAG, "onClick: delete exception -> " + e.getMessage());
                        }
                        draftAdapter.notifyDataSetChanged();
                    }
                });
                snackbar.show();
                draftAdapter.notifyDataSetChanged();
            }
        };

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(draftRecyclerView);
    }

    @Override
    public void onClick(ContestVideoDraft contestVideoDraft) {
        Log.d("Draft", "ShowDraftsActivity : onClick: " + contestVideoDraft.toString());
//        Intent intent = new Intent(this, DraftVideoAutoPlayActivity.class);
//        intent.putExtra("DraftObject", contestVideoDraft);
//        startActivity(intent);

        if (new File(contestVideoDraft.getStoragePath()).exists()) {
            Log.d(TAG, "Is it available in storage -> : YES");
            Intent intent = new Intent(this, DraftVideoPlayActivity.class);
            intent.putExtra("DraftObject", contestVideoDraft);
            startActivity(intent);
        } else {
            Log.d(TAG, "Is it available in storage -> : No");
            Utils.showToast(mActivity, "This Video is not available");
            draftViewModel.delete(contestVideoDraft);
        }
    }

    @OnClick(R.id.create_a_new_draft_fab)
    public void onCreateANewDraftFabClick() {
        Intent intent = new Intent(this, CameraActivity.class);
        intent.putExtra("IsOnlyForSavingDraft", true);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }
}