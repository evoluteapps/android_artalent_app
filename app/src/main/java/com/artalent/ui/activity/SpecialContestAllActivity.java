package com.artalent.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.artalent.R;
import com.artalent.ui.adapter.AdvertisePanelTabAdapter;
import com.artalent.ui.fragment.AllSpecialContestFragment;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;

import butterknife.BindView;

public class SpecialContestAllActivity extends BaseActivityOther implements View.OnClickListener {

    @BindView(R.id.add_advertise_fab)
    FloatingActionButton addAdvertise;

    AdvertisePanelTabAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_special_all_contest);
        addAdvertise.setOnClickListener(this);
        setupToolbarBack(getString(R.string.special_contest));

        ViewPager viewPager = findViewById(R.id.viewPager);
        TabLayout tabLayout = findViewById(R.id.tabLayout);

        AllSpecialContestFragment runingRuningContestFragment = new AllSpecialContestFragment();
        adapter = new AdvertisePanelTabAdapter(getSupportFragmentManager(), FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        adapter.addFragment(runingRuningContestFragment, "Running");
        runingRuningContestFragment.setType(1);
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.add_advertise_fab) {
            Intent intent = new Intent(getApplicationContext(), AddSpecialContestActivity.class);
            startActivity(intent);
        }
    }
}
