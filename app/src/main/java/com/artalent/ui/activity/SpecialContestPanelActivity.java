package com.artalent.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;

import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.view.View;

import com.artalent.R;
import com.artalent.ui.adapter.AdvertisePanelTabAdapter;
import com.artalent.ui.fragment.RuningContestFragment;

import butterknife.BindView;

public class SpecialContestPanelActivity extends BaseActivityOther implements View.OnClickListener {

    @BindView(R.id.add_advertise_fab)
    FloatingActionButton addAdvertise;

    AdvertisePanelTabAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_advertise_panel);
        addAdvertise.setOnClickListener(this);
        setupToolbarBack(getString(R.string.special_contest));

        ViewPager viewPager = findViewById(R.id.viewPager);
        TabLayout tabLayout = findViewById(R.id.tabLayout);

        RuningContestFragment runingRuningContestFragment = new RuningContestFragment();
        RuningContestFragment pastRuningContestFragment = new RuningContestFragment();
        adapter = new AdvertisePanelTabAdapter(getSupportFragmentManager(), FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        adapter.addFragment(runingRuningContestFragment, "Running");
        runingRuningContestFragment.setType(1);
        adapter.addFragment(pastRuningContestFragment, "Past");
        pastRuningContestFragment.setType(0);
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.add_advertise_fab) {
            Intent intent = new Intent(getApplicationContext(), AddSpecialContestActivity.class);
            startActivity(intent);
        }
    }
}
