package com.artalent.ui.activity;

import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.artalent.R;
import com.artalent.ui.adapter.ContestUserAdapter;
import com.artalent.ui.model.ContestObj;
import com.artalent.ui.model.ContestRegistrationObj;

import java.util.List;

import butterknife.BindView;

public class ContestUserActivity extends BaseActivityOther {

    @BindView(R.id.recycler_view)
    RecyclerView userRecyclerView;

    ContestObj contestObj;
    private List<ContestRegistrationObj> contestRegistrationObjList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contest_user);
        setupToolbarBack(getString(R.string.contest_participants), R.drawable.ic_arrow_back_white_24dp, R.color.primary, R.color.black);
        Bundle contestObjExtras = getIntent().getExtras();
        if (contestObjExtras != null) {
            contestObj = (ContestObj) contestObjExtras.getSerializable("contestObj");
            if (contestObj != null) {
                contestRegistrationObjList = contestObj.getRegistrationDetails();
                setAdapter();
            }
        }
    }

    private void setAdapter() {
        userRecyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        ContestUserAdapter contestUserAdapter = new ContestUserAdapter(contestRegistrationObjList, mActivity);
        userRecyclerView.setAdapter(contestUserAdapter);
    }
}
