package com.artalent.ui.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.artalent.ArtalentApplication;
import com.artalent.Data;
import com.artalent.R;
import com.artalent.SingleDownloadActivity;
import com.artalent.Utils;
import com.artalent.audiocutter.AudioTrimmerActivity;
import com.artalent.ui.adapter.MusicListAdapter;
import com.artalent.ui.dialog.AudioDialog;
import com.artalent.ui.dialog.RateDialog;
import com.artalent.ui.model.AudioObj;
import com.artalent.ui.model.AudioResponse;
import com.artalent.ui.model.BaseResponse;
import com.artalent.ui.utils.Constants;
import com.artalent.ui.utils.Enums;
import com.example.jean.jcplayer.JcPlayerManagerListener;
import com.example.jean.jcplayer.general.JcStatus;
import com.example.jean.jcplayer.general.errors.OnInvalidPathListener;
import com.example.jean.jcplayer.model.JcAudio;
import com.example.jean.jcplayer.view.JcPlayerView;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.otaliastudios.cameraview.CameraView.PERMISSION_REQUEST_CODE;

public class MusicLibraryActivity extends BaseActivityOther implements OnInvalidPathListener, JcPlayerManagerListener {

    private static final String TAG = "MusicLibraryActivity";
    @BindView(R.id.recycler_view)
    RecyclerView songRecyclerView;

    @BindView(R.id.pullToRefresh)
    SwipeRefreshLayout pullToRefresh;

    @BindView(R.id.jcplayer)
    JcPlayerView jcPlayerView;

    @BindView(R.id.tvNoResult)
    TextView noResultTV;

    private LinearLayoutManager mLayoutManager;
    private boolean loading = true;
    private MusicListAdapter musicListAdapter;
    private int pastVisiblesItems, visibleItemCount, totalItemCount;
    private int offset = 0;
    ArrayList<AudioObj> songNameList = new ArrayList<>();

    public static boolean checkGo = false;

    boolean from = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_music_library);
        from = getIntent().getBooleanExtra(Constants.FROM, false);
        if (ArtalentApplication.prefManager.isFirstTimeMusicLibraryLaunch()) {
            ArtalentApplication.prefManager.setFirstTimeMusicLibraryLaunch();
            RateDialog rateDialog = new RateDialog(this, getResources().getString(R.string.music_library_dialog_title),
                    getResources().getString(R.string.music_library_dialog_description), true);
            rateDialog.show();
        }
        checkGo = false;
        setupToolbarBack(getString(R.string.music_library));
        setContestWinningAdapter();
        initScrollListener();

        String[] SONG_TYPE = new String[]{"All", "Song", "Music", "Dialog", "My Playlist"};

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this,
                R.layout.dropdown_menu_popup_item,
                SONG_TYPE);

        Spinner spinner =
                findViewById(R.id.spinner1);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        refresh(String.valueOf(Enums.MusicType.ALL), false);
                        break;
                    case 1:
                        refresh(String.valueOf(Enums.MusicType.SONG), false);
                        break;
                    case 2:
                        refresh(String.valueOf(Enums.MusicType.MUSIC), false);
                        break;

                    case 3:
                        refresh(String.valueOf(Enums.MusicType.DIALOG), false);
                        break;

                    case 4:
                        refresh(String.valueOf(Enums.MusicType.MY_PLAYLIST), true);
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }


    private void initScrollListener() {
        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refresh(String.valueOf(Enums.MusicType.ALL), false);
            }
        });
        songRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NotNull RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();
                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            if (offset != -1) {
                                loading = false;
                                Log.e("mLayoutManager", "Last Item Wow !");
                                callWalletTransactionGetApi(String.valueOf(Enums.MusicType.ALL), false);
                            }

                            //Do pagination.. i.e. fetch new data
                        }
                    }
                }
            }
        });
    }


    protected void setContestWinningAdapter() {
        mLayoutManager = new LinearLayoutManager(mActivity);
        songRecyclerView.setLayoutManager(mLayoutManager);
        Log.d(TAG, "setContestWinningAdapter: songNameList -> " + songNameList.toString());
        musicListAdapter = new MusicListAdapter(mActivity, jcAudios, songNameList);

        musicListAdapter.setOnItemClickListener(new MusicListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                jcPlayerView.playAudio(jcPlayerView.getMyPlaylist().get(position));
            }

            @Override
            public void onDownloadImage(AudioObj audioObj) {
                /*DownloadDialog downloadDialog = new DownloadDialog(MusicLibraryActivity.this, audioObj);
                downloadDialog.show();*/
                if (audioObj != null && !TextUtils.isEmpty(audioObj.getAudioUrl())) {
                    jcPlayerView.pause();
                    Intent intent = new Intent(MusicLibraryActivity.this, SingleDownloadActivity.class);
                    intent.putExtra(Constants.ID, audioObj.getAudioUrl());
                    startActivityForResult(intent, REQUEST_STATE);
                }
            }

            @Override
            public void onDownloadedBtnClicked() {
                Utils.showToast(mActivity, "This Audio file is already downloaded");
            }

            @Override
            public void onDeleteClick(AudioObj postObj, int position) {
                open(postObj, position);
            }

            @Override
            public void onSongItemDeleteClicked(int position) {
            }

/*
            @Override
            public void onSongItemDeleteClicked(int position) {
                Toast.makeText(mActivity, "Delete song at position " + position,
                        Toast.LENGTH_SHORT).show();
//                if(player.getCurrentPlayedAudio() != null) {
//                    Toast.makeText(MainActivity.this, "Current audio = " + player.getCurrentPlayedAudio().getPath(),
//                            Toast.LENGTH_SHORT).show();
//                }
                removeItem(position);
            }
*/
        });

        songRecyclerView.setAdapter(musicListAdapter);
    }


    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onStop() {
        super.onStop();
        jcPlayerView.createNotification(R.drawable.final_logo);
    }

    @Override
    protected void onPause() {
        super.onPause();
        jcPlayerView.createNotification(R.drawable.final_logo);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        jcPlayerView.kill();
    }

    private void updateProgress(final JcStatus jcStatus) {
        Log.d("MusicListFragment", "Song duration = " + jcStatus.getDuration()
                + "\n song position = " + jcStatus.getCurrentPosition());

        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // calculate progress
                float progress = (float) (jcStatus.getDuration() - jcStatus.getCurrentPosition())
                        / (float) jcStatus.getDuration();
                progress = 1.0f - progress;
                musicListAdapter.updateProgress(jcStatus.getJcAudio(), progress);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_music_library, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_add) {
            if (Build.VERSION.SDK_INT >= 24) {
                if (checkPermission1()) {
                    selectDialog();
                } else {
                    requestPermission(); // Code for permission
                }
            } else {
                selectDialog();
            }
            return true;
        } else if (item.getItemId() == R.id.action_next) {
            if (songNameList.size() > 0) {
                if (musicListAdapter.getSelected() != null) {
                    AudioObj audioObj = musicListAdapter.getSelected();
                    if (audioObj != null) {
                        String filePath = Data.getSaveDir() + "ARTalentAudio/" + Data.getNameFromUrl(audioObj.getAudioUrl());
                        File file = new File(filePath);
                        if (file.exists()) {
                            Intent output = new Intent();
                            output.putExtra(Constants.DATA, filePath);
                            setResult(RESULT_OK, output);
                            finish();
                        } else
                            Utils.showToast(mActivity, "Please download your selected Music/Song");
                    }
                } else Utils.showToast(mActivity, "No Selection");
            } else Utils.showToast(mActivity, "No songs in list, add any song in your library");
        }
        return super.onOptionsItemSelected(item);
    }

    private ArrayList<JcAudio> jcAudios = new ArrayList<>();


    private void callWalletTransactionGetApi(String type, boolean isUserPlaylist) {
        if (Utils.isInternetAvailable(mActivity)) {
            showProgress();
            Call<AudioResponse> call;
            int userId = ArtalentApplication.prefManager.getUserObj().getId();
            if (!isUserPlaylist) {
                call = ArtalentApplication.apiService.getAudioList(userId, offset, type);
            } else {
                call = ArtalentApplication.apiService.getUsersAudioList(userId, offset, userId);
            }
            call.enqueue(new Callback<AudioResponse>() {
                @Override
                public void onResponse(Call<AudioResponse> call, Response<AudioResponse> response) {
                    loading = true;
                    pullToRefresh.setRefreshing(false);
                    if (response.body() != null && response.body().getObj() != null && response.body().getObj() != null) {
                        hideProgress();
                        noResultTV.setVisibility(View.GONE);
                        pullToRefresh.setVisibility(View.VISIBLE);
                        jcPlayerView.setVisibility(View.VISIBLE);
                        songNameList.addAll(response.body().getObj());

                        Log.d(TAG, "onResponse: From API -> " + response.body().toString());

                        Collections.reverse(songNameList);
                        for (AudioObj audioObj : songNameList) {
                            String titleWithAppName = "ARTalent: " + audioObj.getSongName();
//                            jcAudios.add(JcAudio.createFromURL(audioObj.getSongName(), audioObj.getAudioUrl()));
                            jcAudios.add(JcAudio.createFromURL(titleWithAppName, audioObj.getAudioUrl()));
                        }
                        jcPlayerView.initPlaylist(jcAudios, MusicLibraryActivity.this);
                        jcPlayerView.createNotification(R.drawable.final_logo); // default icon
                        musicListAdapter.notifyDataSetChanged();
                    } else {
                        hideProgress();
                        noResultTV.setVisibility(View.VISIBLE);
                        jcPlayerView.setVisibility(View.GONE);
                        pullToRefresh.setVisibility(View.GONE);
                    }
                    if (response.body() != null) {
                        offset = response.body().getIndex();
                    }
                }

                @Override
                public void onFailure(Call<AudioResponse> call, Throwable t) {
                    loading = true;
                    hideProgress();
                    pullToRefresh.setRefreshing(false);
                    // Log error here since request failed
                    Log.e("TAG", t.toString());
                }
            });
        } else {
            Intent intent = new Intent(mActivity, NoInternetActivity.class);
            startActivityForResult(intent, Constants.NO_INTERNET);
        }
    }


    private void refresh(String type, boolean isUserPlaylist) {
        songNameList.clear();
        offset = 0;
        callWalletTransactionGetApi(type, isUserPlaylist);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (MusicLibraryActivity.checkGo) {
            MusicLibraryActivity.checkGo = false;
            refresh(String.valueOf(Enums.MusicType.ALL), false);
        }
    }

    private void selectDialog() {
        AudioDialog audioDialog = new AudioDialog(this);
        audioDialog.show();
    }

    public static final int REQUEST_STATE = 300;
    private static final int ADD_AUDIO = 1001;
    private static final int AUDIO_OBJ = 884;
    private static final int UPLOAD_AUDIO_RESPONSE = 1000;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                //the selected audio.
                Uri uri = data.getData();
                Log.d(TAG, "onActivityResult: Music file selected from storage -> "
                        + uri.getPath() + "---" + uri.toString());
                checkGo = true;
                Intent sendIntent = new Intent(getApplicationContext(), AudioTrimmerActivity.class);
                sendIntent.setData(uri);
                startActivityForResult(sendIntent, AUDIO_OBJ);
            }
        } else if (requestCode == AUDIO_OBJ) {
            if (resultCode == Activity.RESULT_OK) {
                Bundle bundle = data.getExtras();
                Log.d(TAG, "onActivityResult: getting data from audioTrimmerActivity -> "
                        + bundle.getString("INTENT_AUDIO_FILE_PATH"));
                Intent intent = new Intent(getApplicationContext(), UploadAudioActivity.class);
                if (bundle != null) {
                    intent.putExtras(bundle);
                    startActivityForResult(intent, UPLOAD_AUDIO_RESPONSE);
                }

            }
        } else if (requestCode == UPLOAD_AUDIO_RESPONSE) {
            if (resultCode == RESULT_OK) {
                Log.d(TAG, "onActivityResult: Music file uploaded...");
                refresh(String.valueOf(Enums.MusicType.ALL), false);
            }
        } else if (requestCode == REQUEST_STATE) {
            if (resultCode == Activity.RESULT_OK) {
                String selectedStateObj = data.getStringExtra(Constants.STATE_OBJ);
                Utils.showToast(mActivity, selectedStateObj);
                refresh(String.valueOf(Enums.MusicType.ALL), false);

            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    private boolean checkPermission1() {
        int result = ContextCompat.checkSelfPermission(MusicLibraryActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(MusicLibraryActivity.this,
                new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NotNull String[] permissions, @NotNull int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                selectDialog();
            } else {
            }
        }
    }

    @Override
    public void onCompletedAudio() {

    }

    @Override
    public void onContinueAudio(@NotNull JcStatus jcStatus) {

    }

    @Override
    public void onJcpError(@NotNull Throwable throwable) {

    }

    @Override
    public void onPaused(@NotNull JcStatus jcStatus) {

    }

    @Override
    public void onPlaying(@NotNull JcStatus jcStatus) {

    }

    @Override
    public void onPreparedAudio(@NotNull JcStatus jcStatus) {

    }

    @Override
    public void onStopped(@NotNull JcStatus jcStatus) {

    }

    @Override
    public void onTimeChanged(@NotNull JcStatus jcStatus) {
        updateProgress(jcStatus);
    }

    @Override
    public void onPathError(@NotNull JcAudio jcAudio) {

    }

    public void open(AudioObj postObj, int position) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mActivity);
        alertDialogBuilder.setMessage("Are you sure, You want to delete Music?");
        alertDialogBuilder.setPositiveButton("yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        arg0.dismiss();
                        deleteInterest(postObj, position);
                    }
                });

        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void deleteInterest(AudioObj postObj, int position) {
        if (Utils.isInternetAvailable(mActivity)) {
            ArtalentApplication.apiService.deleteMusic(ArtalentApplication.prefManager.getUserObj().getId(), postObj.getAudioid())
                    .enqueue(new Callback<BaseResponse>() {
                        @Override
                        public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                            //callInterestApi();
                            if (response.body() != null) {
                                songNameList.remove(position);
                                musicListAdapter.notifyDataSetChanged();
                            }
                        }

                        @Override
                        public void onFailure(Call<BaseResponse> call, Throwable t) {

                        }
                    });
        }
    }
}
