package com.artalent.ui.activity;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.Settings;

import androidx.annotation.Nullable;

import com.google.android.material.textfield.TextInputEditText;

import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;

import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.artalent.ArtalentApplication;
import com.artalent.R;
import com.artalent.Utils;
import com.artalent.ui.model.BaseResponse;
import com.artalent.ui.model.Country;
import com.artalent.ui.model.DistrictsObj;
import com.artalent.ui.model.InterestListObj;
import com.artalent.ui.model.InterestObj;
import com.artalent.ui.model.StateObj;
import com.artalent.ui.model.UserObj;
import com.artalent.ui.model.UserResponse;
import com.artalent.ui.model.request.CreateReq;
import com.artalent.ui.utils.CircleTransformation;
import com.artalent.ui.utils.Constants;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Miroslaw Stanek on 14.01.15.
 */
public class EditProfileActivity extends BaseActivityOther implements View.OnClickListener {

    private static final String TAG = "EditProfileActivity";

    @BindView(R.id.ivUserProfilePhoto)
    ImageView ivUserProfilePhoto;

    @BindView(R.id.llProfile)
    LinearLayout llProfile;

    @BindView(R.id.ivDelete)
    ImageView ivDelete;

    @BindView(R.id.et_first_name)
    TextInputEditText etFirstName;

    @BindView(R.id.et_last_name)
    TextInputEditText etLastName;

    @BindView(R.id.et_talent_list)
    EditText etTalentList;

    @BindView(R.id.et_bio)
    TextInputEditText etBio;

    @BindView(R.id.et_website)
    TextInputEditText etWebsite;

    @BindView(R.id.et_user_name)
    TextInputEditText etUserName;

    @BindView(R.id.et_email_address)
    TextInputEditText etEmailAddress;

    @BindView(R.id.et_mobile_number)
    TextInputEditText etMobileNumber;

    @BindView(R.id.et_pin_code)
    TextInputEditText et_pin_code;


    @BindView(R.id.et_country)
    EditText etCountry;

    @BindView(R.id.et_state)
    EditText etState;

    @BindView(R.id.et_district)
    EditText etDistrict;

    public static final int REQUEST_IMAGE = 100;
    public static final int REQUEST_COUNTRY = 200;
    public static final int REQUEST_STATE = 300;
    public static final int REQUEST_DISTRICT = 400;
    public static final int REQUEST_INTEREST = 500;

    private Country selectedCountry = null;
    private StateObj selectedStateObj = null;
    private DistrictsObj selectedDistrictsObj = null;
    private InterestListObj selectedInterestListObj = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        setupToolbarBack(getString(R.string.edit));
        etCountry.setOnClickListener(this);
        etState.setOnClickListener(this);
        etDistrict.setOnClickListener(this);
        etTalentList.setOnClickListener(this);
        setUserData();
        ImagePickerActivity.clearCache(this);
    }

    private void setUserData() {

        UserObj userObj = ArtalentApplication.prefManager.getUserObj();
        if (userObj != null) {
            selectedInterestListObj = new InterestListObj();
            selectedInterestListObj.setInterestObjList(userObj.getInterest());
            if (selectedInterestListObj != null) {
                etTalentList.setText(getUserTalent(selectedInterestListObj));
            }
            if (!TextUtils.isEmpty(userObj.getProfileImage())) {
                ivDelete.setVisibility(View.GONE);
            } else {
                ivDelete.setVisibility(View.GONE);
            }

            if (!TextUtils.isEmpty(userObj.getProfileImage())) {
                Picasso.get()
                        .load(userObj.getProfileImage())
                        .placeholder(R.drawable.person_placeholder)
                        .resize(getApplicationContext().getResources().getDimensionPixelSize(R.dimen.dimen_90), getApplicationContext().getResources().getDimensionPixelSize(R.dimen.dimen_90))
                        .centerCrop()
                        .transform(new CircleTransformation())
                        .into(ivUserProfilePhoto);
            } else {
                ivUserProfilePhoto.setImageResource(R.drawable.person_placeholder);
            }

            if (!TextUtils.isEmpty(userObj.getFirstName())) {
                etFirstName.setText(userObj.getFirstName());
            }
            if (!TextUtils.isEmpty(userObj.getLastName())) {
                etLastName.setText(userObj.getLastName());
            }
            if (!TextUtils.isEmpty(userObj.getUserName())) {
                etUserName.setText(userObj.getUserName());
            }

            if (userObj.getCountry() != null && userObj.getCountry().getCountryId() > 0) {
                selectedCountry = userObj.getCountry();
                etCountry.setText(selectedCountry.getCountryName());
            }
            if (userObj.getState() != null && userObj.getState().getStateId() > 0) {
                selectedStateObj = userObj.getState();
                etState.setText(selectedStateObj.getState());
            }
            if (userObj.getDistrict() != null && userObj.getDistrict().getDistrictId() > 0) {
                selectedDistrictsObj = userObj.getDistrict();
                etDistrict.setText(selectedDistrictsObj.getDistrict());
            }
        }
    }

    public UserObj getUserData() {
        return ArtalentApplication.prefManager.getUserObj();
    }


    @OnClick(R.id.ivDelete)
    public void onDeleteClick() {
        open();
    }

    @OnClick(R.id.llProfile)
    public void onllProfileClick() {


        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            showImagePickerOptions();
                        }

                        if (report.isAnyPermissionPermanentlyDenied()) {
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_save, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:
                callLoginApi();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public String getRealPathFromURI(Uri contentUri) {

        String[] proj = {MediaStore.Images.Media.DATA};
        try (Cursor cursor = getContentResolver().query(contentUri,
                proj, // Which columns to return
                null,       // WHERE clause; which rows to return (all rows)
                null,       // WHERE clause selection arguments (none)
                null)) {
            int column_index = 0;
            if (cursor != null) {
                column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();
                return cursor.getString(column_index);
            }
            return "";
        } // Order-by clause (ascending by name)
    }

    private void callUserProfileApi(String getSelectedPath) {
        if (Utils.isInternetAvailable(mActivity)) {
            showProgress();
            File file = new File(getSelectedPath);
            RequestBody postBody = RequestBody.create(MediaType.parse("image/*"), file);
            MultipartBody.Part postImagesParts = MultipartBody.Part.createFormData("file", file.getName(), postBody);
            ArtalentApplication.apiServicePost.updateUserProfile(ArtalentApplication.prefManager.getUserObj().getId(), postImagesParts)
                    .enqueue(new Callback<BaseResponse>() {
                        @Override
                        public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                            hideProgress();
                            int statusCode = response.code();
                            if (statusCode == 200) {
                                String mess = response.body().getMessage();
                                Utils.showToast(mActivity, mess);
                                getUserData11();
                            }
                        }

                        @Override
                        public void onFailure(Call<BaseResponse> call, Throwable t) {
                            hideProgress();
                            // Log error here since request failed
                            Log.e("TAGArtalent", t.toString());
                            Utils.showToast(mActivity, "onFailure : " + t.toString());
                        }
                    });
        }

    }

    private void getUserData11() {
        if (Utils.isInternetAvailable(mActivity)) {
            ArtalentApplication.apiService.getOtherUserDetails(ArtalentApplication.prefManager.getUserObj().getId()).enqueue(new Callback<UserResponse>() {
                @Override
                public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                    int statusCode = response.code();
                    if (response.body() != null && response.body().getObj() != null) {
                        UserObj userObj = response.body().getObj();
                        Log.d(TAG, "onResponse: userObj from API" + userObj.toString());
                        ArtalentApplication.prefManager.setUserObj(userObj);
                        setUserData();
                    } else {
                        startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                        finish();

                        Utils.showToast(mActivity, mActivity.getString(R.string.no_internet_connection_available));
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                finish();
                            }
                        }, 2000);
                    }
                }

                @Override
                public void onFailure(Call<UserResponse> call, Throwable t) {
                    // Log error here since request failed
                    Log.e("TAG", t.toString());
                }
            });
        }
    }

    private void loadProfile(Uri url) {
        Picasso.get().load(url)
                .into(ivUserProfilePhoto);
        ivUserProfilePhoto.setColorFilter(ContextCompat.getColor(this, android.R.color.transparent));
        new Handler().postDelayed(() -> {
            Bitmap bm = ((BitmapDrawable) ivUserProfilePhoto.getDrawable()).getBitmap();
            saveImageFile(bm);
        }, 1500);
    }

    String selectedFileName = "";

    public String saveImageFile(Bitmap bitmap) {
        FileOutputStream out = null;
        String filename = getFilename();
        try {
            out = new FileOutputStream(filename);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
            Log.e("Picasso123", filename);
            selectedFileName = filename;
            //
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return filename;
    }

    private String getFilename() {
        File file = new File(Environment.getExternalStorageDirectory()
                .getPath(), "Artalent");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/art_"
                + System.currentTimeMillis() + ".jpg");
        return uriSting;
    }

    private void showImagePickerOptions() {
        ImagePickerActivity.showImagePickerOptions(this, new ImagePickerActivity.PickerOptionListener() {
            @Override
            public void onTakeCameraSelected() {
                launchCameraIntent();
            }

            @Override
            public void onChooseGallerySelected() {
                launchGalleryIntent();
            }

            @Override
            public void onDeleteImage() {
                open();
            }
        }, true);
    }

    public void open() {
        android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(mActivity);
        alertDialogBuilder.setMessage("Are you sure, You wanted to delete own Profile Photo?");
        alertDialogBuilder.setPositiveButton("yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        arg0.dismiss();
                        deleteInterest();
                    }
                });

        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        android.app.AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void deleteInterest() {
        if (Utils.isInternetAvailable(mActivity)) {
            ArtalentApplication.apiService.deleteOwnProfile(ArtalentApplication.prefManager.getUserObj().getId())
                    .enqueue(new Callback<BaseResponse>() {
                        @Override
                        public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                            //callInterestApi();
                            if (response.body() != null) {
                                getUserData11();
                            }
                        }

                        @Override
                        public void onFailure(Call<BaseResponse> call, Throwable t) {

                        }
                    });
        }
    }

    private void launchCameraIntent() {
        Intent intent = new Intent(mActivity, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_IMAGE_CAPTURE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);

        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 1000);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 1000);

        startActivityForResult(intent, REQUEST_IMAGE);
    }

    private void launchGalleryIntent() {
        Intent intent = new Intent(mActivity, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_GALLERY_IMAGE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);
        startActivityForResult(intent, REQUEST_IMAGE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                Uri uri = data.getParcelableExtra("path");

                loadProfile(uri);
            }
        } else if (requestCode == REQUEST_COUNTRY) {
            if (resultCode == Activity.RESULT_OK) {
                selectedCountry = (Country) data.getSerializableExtra(Constants.COUNTRY_OBJ);
                if (selectedCountry != null && !TextUtils.isEmpty(selectedCountry.getCountryName())) {
                    etCountry.setText(selectedCountry.getCountryName());
                    selectedStateObj = null;
                    if (selectedStateObj != null && !TextUtils.isEmpty(selectedStateObj.getState())) {
                        etState.setText(selectedStateObj.getState());
                    } else {
                        etState.setText("");
                    }
                }

            }
        } else if (requestCode == REQUEST_STATE) {
            if (resultCode == Activity.RESULT_OK) {
                selectedStateObj = (StateObj) data.getSerializableExtra(Constants.STATE_OBJ);
                if (selectedStateObj != null && !TextUtils.isEmpty(selectedStateObj.getState())) {
                    etState.setText(selectedStateObj.getState());
                }

            }
        } else if (requestCode == REQUEST_DISTRICT) {
            if (resultCode == Activity.RESULT_OK) {
                selectedDistrictsObj = (DistrictsObj) data.getSerializableExtra(Constants.STATE_OBJ);
                if (selectedDistrictsObj != null && !TextUtils.isEmpty(selectedDistrictsObj.getDistrict())) {
                    etDistrict.setText(selectedDistrictsObj.getDistrict());
                }
            }
        } else if (requestCode == REQUEST_INTEREST) {
            if (resultCode == Activity.RESULT_OK) {
                selectedInterestListObj = (InterestListObj) data.getSerializableExtra(Constants.INTEREST_OBJ);
                if (selectedInterestListObj != null) {
                    etTalentList.setText(getUserTalent(selectedInterestListObj));
                }
            }
        }
    }

    private String getUserTalent(InterestListObj interestObjList) {
        StringBuilder interestBuilder = new StringBuilder();
        int i = 0;
        for (InterestObj interestObj : interestObjList.getInterestObjList()) {
            if (i++ == interestObjList.getInterestObjList().size() - 1) {
                // Last iteration
                interestBuilder.append(interestObj.getInterest());
            } else {
                interestBuilder.append(interestObj.getInterest()).append(", ");
            }
        }
        return interestBuilder.toString();
    }

    /**
     * Showing Alert Dialog with Settings option
     * Navigates user to app settings
     * NOTE: Keep proper title and message depending on your app
     */
    private void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        builder.setTitle(getString(R.string.dialog_permission_title));
        builder.setMessage(getString(R.string.dialog_permission_message));
        builder.setPositiveButton(getString(R.string.go_to_settings), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                EditProfileActivity.this.openSettings();
            }
        });
        builder.setNegativeButton(getString(android.R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();

    }

    // navigating user to app settings
    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.et_country:
                SelectCountryActivity.startActivity(mActivity, selectedCountry, REQUEST_COUNTRY);
                break;
            case R.id.et_state:
                if (selectedCountry != null && selectedCountry.getCountryId() > 0) {
                    SelectStateActivity.startActivity(mActivity, selectedStateObj, selectedCountry.getCountryId(), REQUEST_STATE);
                } else {
                    Utils.showToast(mActivity, mActivity.getString(R.string.select_country_first));
                }
                break;
            case R.id.et_district:
                if (selectedStateObj != null && selectedStateObj.getStateId() > 0) {
                    SelectDistrictsActivity.startActivity(mActivity, selectedDistrictsObj, selectedStateObj.getStateId(), REQUEST_DISTRICT);
                } else {
                    Utils.showToast(mActivity, mActivity.getString(R.string.select_state_first));
                }
                break;

            case R.id.et_talent_list:
                SelectInterestActivity.startActivityFromEdit(mActivity, selectedInterestListObj, REQUEST_INTEREST, false);
                break;
        }
    }

    private void callLoginApi() {
        if (Utils.isInternetAvailable(mActivity)) {
            showProgress();
            ArtalentApplication.apiService.updateUser(ArtalentApplication.prefManager.getUserObj().getId(), fillData())
                    .enqueue(new Callback<UserResponse>() {
                        @Override
                        public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                            hideProgress();
                            int statusCode = response.code();
                            UserObj userObj = response.body().getObj();
                            if (userObj != null && !TextUtils.isEmpty(userObj.getMobile())) {
                                ArtalentApplication.prefManager.setUserObj(userObj);
                                if (!TextUtils.isEmpty(selectedFileName)) {
                                    callUserProfileApi(selectedFileName);
                                } else {
                                    Utils.showToast(mActivity, "Profile Updated Successfully");
                                }
                            } else {
                                Utils.showToast(mActivity, mActivity.getString(R.string.please_try_again));
                            }
                        }

                        @Override
                        public void onFailure(Call<UserResponse> call, Throwable t) {
                            hideProgress();
                            // Log error here since request failed
                            Log.e("TAGArtalent", t.toString());
                            Utils.showToast(mActivity, "onFailure : " + t.toString());
                        }
                    });
        }
    }


    private CreateReq fillData() {
        CreateReq createReq = new CreateReq();
        createReq.setMobile(ArtalentApplication.prefManager.getUserObj().getMobile());
        createReq.setId(ArtalentApplication.prefManager.getUserObj().getId());
        if (etFirstName.getText() != null)
            createReq.setFirstName(etFirstName.getText().toString().trim());
        if (etLastName.getText() != null)
            createReq.setLastName(etLastName.getText().toString().trim());
        if (etUserName.getText() != null)
            createReq.setUserName(etUserName.getText().toString().trim());
        if (etDistrict.getText() != null)
            createReq.setDistrictId(selectedDistrictsObj.getDistrictId());
        return createReq;
    }

}
