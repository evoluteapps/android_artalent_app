package com.artalent.ui.activity;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.artalent.R;
import com.artalent.ui.progressbar.ChromeFloatingCirclesDrawable;
import com.artalent.ui.progressbar.FoldingCirclesDrawable;
import com.artalent.ui.progressbar.GoogleMusicDicesDrawable;
import com.artalent.ui.progressbar.NexusRotationCrossDrawable;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by Miroslaw Stanek on 19.01.15.
 */
public class BaseActivityOther extends AppCompatActivity {

    @Nullable
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Nullable
    @BindView(R.id.ivLogo)
    TextView ivLogo;

    @Nullable
    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @Nullable
    @BindView(R.id.tvSpecialTitle)
    TextView tvSpecialTitle;
    public Activity mActivity;


    ProgressBar mProgressBar;
    RelativeLayout rlProgress;

    private static final int FOLDING_CIRCLES = 0;
    private static final int MUSIC_DICES = 1;
    private static final int NEXUS_CROSS_ROTATION = 2;
    private static final int CHROME_FLOATING_CIRCLES = 3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = this;
    }


    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        bindViews();
    }

    protected void bindViews() {
        ButterKnife.bind(this);
        setupToolbar();
    }

    public void setContentViewWithoutInject(int layoutResId) {
        super.setContentView(layoutResId);
    }

    protected void setupToolbar() {
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_menu_white);
        }
    }

    protected void setupToolbarBack(String title) {
        if (toolbar != null && tvTitle != null) {
            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayShowTitleEnabled(false);
                tvTitle.setText(title);
                toolbar.setNavigationIcon(R.drawable.icons_left);
            }
        }
    }

    protected void setupToolbarBack(String title, int drawable, int textColor, int toolbarBackground) {
        if (toolbar != null && tvTitle != null) {
            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                tvTitle.setVisibility(View.VISIBLE);
                getSupportActionBar().setDisplayShowTitleEnabled(false);
                tvTitle.setText(title);
                tvTitle.setTextColor(getResources().getColor(textColor));
                toolbar.setBackgroundColor(getResources().getColor(toolbarBackground));
                toolbar.setNavigationIcon(getResources().getDrawable(drawable));
            }
        }
    }


    protected void setupToolbarSpecialBack(String title, int drawable, int toolbarBackground) {
        if (toolbar != null && tvTitle != null && tvSpecialTitle != null) {
            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayShowTitleEnabled(false);
                tvSpecialTitle.setVisibility(View.VISIBLE);
                tvTitle.setVisibility(View.GONE);
                tvSpecialTitle.setText(title);
                toolbar.setBackgroundColor(getResources().getColor(toolbarBackground));
                toolbar.setNavigationIcon(getResources().getDrawable(drawable));
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    public Toolbar getToolbar() {
        return toolbar;
    }


    /*
  start progress
   */
    public void showProgress() {
        if (mProgressBar == null) {
            mProgressBar = findViewById(R.id.google_progress);
        }
        if (rlProgress == null) {
            rlProgress = findViewById(R.id.rlProgress);
        }
        if (mProgressBar != null) {
            Rect bounds = mProgressBar.getIndeterminateDrawable().getBounds();
            mProgressBar.setIndeterminateDrawable(getProgressDrawable());
            mProgressBar.getIndeterminateDrawable().setBounds(bounds);
            if (rlProgress != null) {
                rlProgress.setVisibility(View.VISIBLE);
            }
        }

    }

    public void hideProgress() {
        if (rlProgress != null) {
            rlProgress.setVisibility(View.GONE);
        }
    }

    private Drawable getProgressDrawable() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        int value = Integer.parseInt(prefs.getString(getString(R.string.progressBar_pref_key), getString(R.string.progressBar_pref_defValue)));
        Drawable progressDrawable = null;
        switch (value) {
            case FOLDING_CIRCLES:
                progressDrawable = new FoldingCirclesDrawable.Builder(this)
                        .colors(getProgressDrawableColors())
                        .build();
                break;

            case MUSIC_DICES:
                progressDrawable = new GoogleMusicDicesDrawable.Builder().build();
                break;

            case NEXUS_CROSS_ROTATION:
                progressDrawable = new NexusRotationCrossDrawable.Builder(this)
                        .colors(getProgressDrawableColors())
                        .build();
                break;

            case CHROME_FLOATING_CIRCLES:
                progressDrawable = new ChromeFloatingCirclesDrawable.Builder(this)
                        .colors(getProgressDrawableColors())
                        .build();
                break;
        }

        return progressDrawable;
    }

    private int[] getProgressDrawableColors() {
        int[] colors = new int[4];
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        colors[0] = prefs.getInt(getString(R.string.firstcolor_pref_key), getResources().getColor(R.color.red));
        colors[1] = prefs.getInt(getString(R.string.secondcolor_pref_key), getResources().getColor(R.color.blue));
        colors[2] = prefs.getInt(getString(R.string.thirdcolor_pref_key), getResources().getColor(R.color.yellow));
        colors[3] = prefs.getInt(getString(R.string.fourthcolor_pref_key), getResources().getColor(R.color.green));
        return colors;
    }

    /*
    end progress
     */


}
