package com.artalent.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;

import com.artalent.ArtalentApplication;
import com.artalent.R;
import com.artalent.Utils;
import com.artalent.ui.adapter.FilterInterestAdapter;
import com.artalent.ui.model.InterestObj;
import com.artalent.ui.model.InterestResponse;
import com.artalent.ui.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by Mahajan on 16-09-2019.
 */

public class SelectInterestNewActivity extends BaseActivityOther implements View.OnClickListener {

    RecyclerView recyclerView;
    FloatingActionButton save;
    FilterInterestAdapter selectCountryAdapter;
    List<InterestObj> interestObjs = new ArrayList<>();
    private InterestObj selectedInterest = null;

    public static void startActivity(Activity mActivity, InterestObj interestObj, int requestCode) {
        Intent intent = new Intent(mActivity, SelectInterestNewActivity.class);
        intent.putExtra(Constants.INTEREST_OBJ, interestObj);
        mActivity.startActivityForResult(intent, requestCode);
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_countery);
        init1();
        setupToolbarBack(getString(R.string.select_interest));
        initializeUi();

        callCountryApi();


    }

    private void callCountryApi() {
        if (Utils.isInternetAvailable(mActivity)) {
            Call<InterestResponse> call = ArtalentApplication.apiService.getInterest();
            call.enqueue(new Callback<InterestResponse>() {
                @Override
                public void onResponse(Call<InterestResponse> call, Response<InterestResponse> response) {
                    int statusCode = response.code();
                    if (response.body().getObj() != null && response.body().getObj().size() > 0) {
                        interestObjs.clear();
                        interestObjs = response.body().getObj();
                        for (InterestObj country : interestObjs) {
                            if (selectedInterest != null) {
                                if (country.getInterestMasterId() == selectedInterest.getInterestMasterId()) {
                                    country.setCheck(true);
                                }

                            }

                        }
                        setAdapter();
                    }


                }

                @Override
                public void onFailure(Call<InterestResponse> call, Throwable t) {
                    // Log error here since request failed
                    Log.e("TAG", t.toString());
                }
            });
        }

    }

    private void setAdapter() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        selectCountryAdapter = new FilterInterestAdapter(mActivity, interestObjs, this);
        recyclerView.setAdapter(selectCountryAdapter);
    }

    private void init1() {
        selectedInterest = (InterestObj) getIntent().getSerializableExtra(Constants.COUNTRY_OBJ);
    }


    public void initializeUi() {
        recyclerView = findViewById(R.id.recyclerView);
        save = findViewById(R.id.save);
        save.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.save:
                if (selectedInterest != null) {
                    Intent output = new Intent();
                    output.putExtra(Constants.INTEREST_OBJ, selectedInterest);
                    setResult(RESULT_OK, output);
                    finish();
                } else {
                    Utils.showToast(mActivity, "Please Select Interest");
                }

                break;
            case R.id.llMain:
                if (v.getTag() instanceof InterestObj) {
                    InterestObj interestObj = (InterestObj) v.getTag();
                    if (interestObj != null) {
                        for (InterestObj interestObj1 : interestObjs) {
                            if (interestObj1.getInterestMasterId() == interestObj.getInterestMasterId()) {
                                interestObj1.setCheck(!interestObj1.isCheck());
                                if (interestObj1.isCheck()) {
                                    selectedInterest = interestObj;
                                } else {
                                    selectedInterest = null;
                                }
                            } else {
                                interestObj1.setCheck(false);
                            }
                        }
                        selectCountryAdapter.notifyDataSetChanged();
                    }
                }

                break;
        }
    }
}
