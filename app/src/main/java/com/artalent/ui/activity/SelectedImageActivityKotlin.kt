package com.artalent.ui.activity

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.FrameLayout
import androidx.appcompat.widget.Toolbar
import androidx.viewpager.widget.ViewPager
import com.artalent.R
import com.artalent.ui.pageindicator.PageIndicator
import ja.burhanrashid52.photoeditor.PhotoFilter
import java.io.File
import java.util.*
import kotlin.collections.ArrayList

/**
 * Created by Mahajan on 16-09-2019.
 */

class SelectedImageActivityKotlin : BaseActivityOther(), ViewPager.OnPageChangeListener {

    val TAG = "SelectedImageActivityKt"

    var viewPager: ViewPager? = null
    var topToolbar: Toolbar? = null
    var currentPosition: Int = 0

    var selectedImageAdapter: SelectedImageAdapterKotlin? = null
    var layoutBottomSheet: FrameLayout? = null

    var returnValue: ArrayList<String>? = ArrayList()
    var selectedReturnValue: ArrayList<String> = ArrayList()

    private var btnSave: Button? = null

    private var defaultAppliedFilter = PhotoFilter.NONE.name
    var appliedFiltersHashMap = HashMap<Int, String>()
    private var savedImagesInLocal: ArrayList<String> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_selected_image)

        Log.d(TAG, "onCreate: called from selectedImageActivityKotlin")
        returnValue?.clear()

        topToolbar = findViewById(R.id.toolbar)
        topToolbar?.setNavigationOnClickListener {
            deleteUnusedLocalSavedFile(savedImagesInLocal)
        }

        setupToolbarBack(
            "Selected Images",
            R.drawable.ic_arrow_back_white_24dp,
            R.color.white,
            R.color.primary
        )

        viewPager = findViewById(R.id.view_pager)

        layoutBottomSheet = findViewById(R.id.bottom_sheet)
        btnSave = findViewById(R.id.btnSave)

        val intent = intent
        returnValue = intent.getStringArrayListExtra(AppCameraActivity.IMAGE_RESULTS)
        Log.d(
            TAG,
            "onCreate: intent value from image select/camera activity -> " + intent.getStringArrayListExtra(
                AppCameraActivity.IMAGE_RESULTS
            ).toString()
        )

        copyItsValue()

        selectedImageAdapter = SelectedImageAdapterKotlin(this, returnValue!!)
        viewPager!!.adapter = selectedImageAdapter
        findViewById<PageIndicator>(R.id.pagerPageIndicator).attachTo(viewPager!!)
        viewPager!!.addOnPageChangeListener(this)

        btnSave!!.setOnClickListener {

            val garbage = savedImagesInLocal.minus(returnValue as java.util.ArrayList<String>)
            Log.d(TAG, "Useful array = ${returnValue.toString()}")
            Log.d(TAG, "Garbage array = ${garbage.toString()}")

            deleteUnusedLocalSavedFile(garbage)

            val resultIntent = Intent()
            resultIntent.putStringArrayListExtra(AppCameraActivity.IMAGE_RESULTS, returnValue)
            setResult(Activity.RESULT_OK, resultIntent)
            finish()
        }
    }

    fun deleteUnusedLocalSavedFile(paths: List<String>) {
        for (i in paths) {
            val file = File(i)
            val isDeleted = file.delete()
            Log.d(TAG, "Delete ho gaya $i? -> $isDeleted")
        }
    }

    private fun copyItsValue() {

        if (returnValue!!.size > 0) {
            for (i in 0..returnValue!!.size) {
                appliedFiltersHashMap[i] = defaultAppliedFilter
            }

            selectedReturnValue.clear()
            Log.d(TAG, "copyItsValue: called")
            returnValue!!.forEach { it -> selectedReturnValue.add(it) }
            Log.d(TAG, "onCreate: returnValue : " + returnValue.toString())
            Log.d(TAG, "onCreate: selectedReturnValue : $selectedReturnValue")

        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_filter, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
            R.id.action_filter -> {
                btnSave!!.visibility = View.VISIBLE
                openFilterActivity()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }


    private fun openFilterActivity() {
//        val intent = Intent(applicationContext, ImagePickerActivity::class.java)
        val intent = Intent(applicationContext, PostCropAndFilterActivity::class.java)

        Log.d(TAG, "openFilterActivity: returnValue : " + returnValue.toString())
        Log.d(TAG, "openFilterActivity: selectedReturnValue : $selectedReturnValue")
        Log.d(
            TAG,
            "openFilterActivity: send Image to crop&Filter activity ${selectedReturnValue[currentPosition]}"
        )

        intent.putExtra("imagePath", selectedReturnValue[currentPosition])
        intent.putExtra("appliedFilterForAnotherEdit", appliedFiltersHashMap[currentPosition])

        startActivityForResult(intent, 100)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 100) { // Please, use a final int instead of hardcoded int value
            if (resultCode == Activity.RESULT_OK) {

                val imagePath = data!!.getStringExtra("imagePath")
                savedImagesInLocal.add(imagePath!!)
                data.getStringExtra("appliedFilter")
                    .also { appliedFiltersHashMap[currentPosition] = it }

                returnValue!![currentPosition] = imagePath

                selectedImageAdapter = SelectedImageAdapterKotlin(this, returnValue!!)
                viewPager!!.adapter = selectedImageAdapter
                viewPager!!.addOnPageChangeListener(this)
                viewPager!!.setCurrentItem(currentPosition, true)
            }
        }
    }

    override fun onPageScrolled(i: Int, v: Float, i1: Int) {

    }

    override fun onPageSelected(i: Int) {
        this.currentPosition = i
        // Toast.makeText(SelectedImageActivity.this, "viewpager position " + i, Toast.LENGTH_LONG).show();
    }

    override fun onPageScrollStateChanged(i: Int) {

    }

    /* private fun saveImage(finalBitmap: Bitmap) {
         val root = Environment.getExternalStorageDirectory().absolutePath + "/ArtalentFilter"
         val myDir = File(root)
         myDir.mkdirs()

         val fname = "Artalent_" + System.currentTimeMillis() + ".jpg"
         val file = File(myDir, fname)
         if (file.exists()) file.delete()
         try {
             val out = FileOutputStream(file)
             finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out)
             out.flush()
             out.close()
             returnValue!![currentPosition] = "$root/$fname"
             selectedImageAdapter = SelectedImageAdapterKotlin(this, returnValue!!)
             viewPager!!.adapter = selectedImageAdapter
             viewPager!!.addOnPageChangeListener(this)
             viewPager!!.setCurrentItem(currentPosition, true)
         } catch (e: Exception) {
             e.printStackTrace()
         }

     } */

    override fun onBackPressed() {
        deleteUnusedLocalSavedFile(savedImagesInLocal)
        super.onBackPressed()
        finish()
    }

    companion object {
        init {
            System.loadLibrary("NativeImageProcessor")
        }

        var addedImage: Bitmap? = null
    }
}

