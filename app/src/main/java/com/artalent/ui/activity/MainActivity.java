package com.artalent.ui.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.viewpager.widget.ViewPager;

import com.artalent.ArtalentApplication;
import com.artalent.R;
import com.artalent.Utils;
import com.artalent.exo.DemoApp;
import com.artalent.fcm.Config;
import com.artalent.ui.adapter.FeedAdapter;
import com.artalent.ui.fragment.BottomSheetFragment;
import com.artalent.ui.fragment.ContestFragment;
import com.artalent.ui.fragment.HomeFragment;
import com.artalent.ui.fragment.ProfileFragment;
import com.artalent.ui.fragment.SearchUserFragment;
import com.artalent.ui.model.BaseResponse;
import com.artalent.ui.model.RegisteredContestObj;
import com.artalent.ui.model.SpecialContestObj;
import com.artalent.ui.model.request.FirebaseToken;
import com.artalent.ui.utils.Options;
import com.artalent.ui.view.FeedContextMenu;
import com.artalent.ui.view.FeedContextMenuManager;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.installations.FirebaseInstallations;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainActivity extends BaseDrawerActivity implements FeedAdapter.OnFeedItemClickListener,
        FeedContextMenu.OnFeedContextMenuItemClickListener, BottomNavigationView.OnNavigationItemSelectedListener {
    public static final String ACTION_SHOW_LOADING_ITEM = "action_show_loading_item";

    private Options options;

    @BindView(R.id.content)
    RelativeLayout clContent;

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @BindView(R.id.myProfleViewpager)
    ViewPager myProfleViewpager;

    private HomeFragment homeFragment;
    private ContestFragment contestHomeFragment;
    private ProfileFragment profileFragment;
    public BottomSheetFragment bottomSheetFragment;
    BottomNavigationView navigation;
    public static boolean isContestReg;
    public static boolean isContestPost;

    public static void startActivity(Activity mActivity) {
        Intent intent = new Intent(mActivity, MainActivity.class);
        mActivity.startActivity(intent);
    }

    private BroadcastReceiver mRegistrationBroadcastReceiver;


    private void addFirebaseToken(String refreshedToken) {
        if (Utils.isInternetAvailable(MainActivity.this)) {
            FirebaseToken firebaseToken = new FirebaseToken();
            firebaseToken.setFirebaseToken(refreshedToken);
            ArtalentApplication.apiService.addFirebaseToken(ArtalentApplication.prefManager.getUserObj().getId(), firebaseToken)
                    .enqueue(new Callback<BaseResponse>() {
                        @Override
                        public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                            int statusCode = response.code();
                            if (response.body() != null) {
                                ArtalentApplication.prefManager.setFCMToken(true);
                            }
                        }

                        @Override
                        public void onFailure(Call<BaseResponse> call, Throwable t) {
                        }
                    });
        }
    }

    private void getWinner() {
        if (Utils.isInternetAvailable(MainActivity.this)) {
            ArtalentApplication.apiService.getWinner()
                    .enqueue(new Callback<BaseResponse>() {
                        @Override
                        public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                            int statusCode = response.code();
                            if (response.body() != null) {
                            }
                        }

                        @Override
                        public void onFailure(Call<BaseResponse> call, Throwable t) {
                        }
                    });
        }
    }


    @SuppressLint("WrongConstant")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.e("getAuthorizationToken", "" + ArtalentApplication.prefManager.getUserObj().getId());
        registerFCMToken();

        setTitle();
        setupToolbarBack();
        setUserData();
        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(v -> drawerLayout.openDrawer(Gravity.START));
        }

        //getting bottom navigation view and attaching the listener
        navigation = findViewById(R.id.bottom_navigation);
        navigation.setOnNavigationItemSelectedListener(this);
        Log.e("Access_Token", ArtalentApplication.prefManager.getAuthorizationToken());
        Log.e("User Id", ArtalentApplication.prefManager.getUserObj().getId() + "");
        setupViewPager();
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(Config.PUSH_NOTIFICATION_POST_DONE)) {
                    if (contestHomeFragment != null) {
                        if (contestHomeFragment.autoPlayVideoFragment != null) {
                            Utils.showToast(MainActivity.this, "Contest Post Uploaded Successfully");
                            contestHomeFragment.autoPlayVideoFragment.refresh();
                        }
                    }
                    //PushNotification pushNotification = (PushNotification) intent.getSerializableExtra(Constants.KeyConstant.DATA);
                    //handleNotification1(pushNotification);
                }
            }
        };
        getWinner();

    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }


    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION_POST_DONE));
        setUserData();
    }

    private void registerFCMToken() {
        if (!ArtalentApplication.prefManager.isFCMToken()) {
            try {
//                String refreshedToken = FirebaseInstanceId.getInstance().getToken();
//                addFirebaseToken(refreshedToken);

                FirebaseInstallations.getInstance().getToken(true).addOnCompleteListener(task -> {
                    if (task.getResult() != null) {
                        String refreshedToken = task.getResult().getToken();
                        addFirebaseToken(refreshedToken);
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
        }
    }


    class ViewPagerAdapter1 extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        ViewPagerAdapter1(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    private void setupViewPager() {

        homeFragment = new HomeFragment();
        contestHomeFragment = new ContestFragment();
        profileFragment = new ProfileFragment();
        ViewPagerAdapter1 adapter = new ViewPagerAdapter1(getSupportFragmentManager());
        adapter.addFragment(contestHomeFragment, "");
        adapter.addFragment(homeFragment, "");
        adapter.addFragment(new SearchUserFragment(), "");
        adapter.addFragment(profileFragment, "");
        myProfleViewpager.setAdapter(adapter);
        myProfleViewpager.setOffscreenPageLimit(3);

        myProfleViewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            private int currentPage = 0;

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position != 0) {
                    if (DemoApp.Companion.getHelper() != null) {
                        DemoApp.Companion.getHelper().pause();
                    }
                }
                Fragment fragment = adapter.getItem(currentPage);

                if (fragment instanceof FragmentCallback &&
                        currentPage != position) {
                    ((FragmentCallback) adapter.getItem(currentPage)).onPageChanged();
                }
                currentPage = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    public interface FragmentCallback {
        void onPageChanged();
    }

    public void setTitle() {
        Typeface font = Typeface.createFromAsset(
                getAssets(),
                "fonts/aladin_regular.ttf");
        tvTitle.setTypeface(font);
        tvTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP, 22);
        tvTitle.setText(R.string.app_name);
        tvTitle.setTextColor(getResources().getColor(R.color.primary));
    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        return true;
    }*/

    @Override
    public void onCommentsClick(View v, int position) {
        final Intent intent = new Intent(this, CommentsActivity.class);
        int[] startingLocation = new int[2];
        v.getLocationOnScreen(startingLocation);
        intent.putExtra(CommentsActivity.ARG_DRAWING_START_LOCATION, startingLocation[1]);
        startActivity(intent);
        overridePendingTransition(0, 0);
    }

    @Override
    public void onMoreClick(View v, int itemPosition) {
        FeedContextMenuManager.getInstance().toggleContextMenuFromView(v, itemPosition, this);
    }

    @Override
    public void onProfileClick(View v) {
        Toast.makeText(MainActivity.this, "hello", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onReportClick(int feedItem) {
        FeedContextMenuManager.getInstance().hideContextMenu();
    }

    @Override
    public void onSharePhotoClick(int feedItem) {
        FeedContextMenuManager.getInstance().hideContextMenu();
    }

    @Override
    public void onCopyShareUrlClick(int feedItem) {
        FeedContextMenuManager.getInstance().hideContextMenu();
    }

    @Override
    public void onCancelClick(int feedItem) {
        FeedContextMenuManager.getInstance().hideContextMenu();
    }

    @OnClick(R.id.floatingActionButton)
    public void onAddPost() {
        if (myProfleViewpager.getCurrentItem() == 0) {
            //TO-DO Append this list to current list.
            List<SpecialContestObj> registeredSpecialContestObj = contestHomeFragment.getRegisteredSpecialContest();
            RegisteredContestObj registeredContestObj = contestHomeFragment.getRegisteredContest();

            List<Object> objectContestList = new ArrayList<>();
            if (registeredSpecialContestObj != null) {
                objectContestList.addAll(registeredSpecialContestObj);
            }
            if (registeredContestObj != null && registeredContestObj.getContestObjList() != null && registeredContestObj.getContestObjList().size() > 0) {
                objectContestList.addAll(registeredContestObj.getContestObjList());
            }
            if (objectContestList.size() > 0) {
                bottomSheetFragment = new BottomSheetFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable("RegisteredContest", (Serializable) objectContestList);
                bottomSheetFragment.setArguments(bundle);
                bottomSheetFragment.show(getSupportFragmentManager(), bottomSheetFragment.getTag());
            } else {
                Utils.showToast(MainActivity.this, "please register in a talent for contest post");
                Intent intent = new Intent(this, InterestListActivity.class);
                startActivity(intent);
            }
        } else {
            startActivity(new Intent(getApplicationContext(), AddPostActivity.class));
        }
    }

    public void showLikedSnackbar() {
        Snackbar.make(clContent, "Liked!", Snackbar.LENGTH_SHORT).show();
    }

    private int getSelectedItem(BottomNavigationView bottomNavigationView) {
        Menu menu = bottomNavigationView.getMenu();
        for (int i = 0; i < bottomNavigationView.getMenu().size(); i++) {
            MenuItem menuItem = menu.getItem(i);
            if (menuItem.isChecked()) {
                return menuItem.getItemId();
            }
        }
        return 0;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        Intent intent;
        switch (menuItem.getItemId()) {

            case R.id.bottomNavigationHomeMenuId:
                myProfleViewpager.setCurrentItem(0, false);
                if (profileFragment != null) {
                    profileFragment.stopVideo();
                }
                break;

            case R.id.bottomNavigationContestMenuId:
                myProfleViewpager.setCurrentItem(1, false);
                if (contestHomeFragment != null) {
                    contestHomeFragment.stopVideo();
                }
                if (profileFragment != null) {
                    profileFragment.stopVideo();
                }
                break;

           /* case R.id.bottomNavigationAddPostMenuId:
                navigation.setSelectedItemId(R.id.bottomNavigationContestMenuId);
                break;
*/


            case R.id.bottomNavigationSearchMenuId:
                myProfleViewpager.setCurrentItem(2, false);
                if (contestHomeFragment != null) {
                    contestHomeFragment.stopVideo();
                }
                if (profileFragment != null) {
                    profileFragment.stopVideo();
                }
                break;

            case R.id.bottomNavigationProfileMenuId:
                myProfleViewpager.setCurrentItem(3, false);
                if (contestHomeFragment != null) {
                    contestHomeFragment.stopVideo();
                }
                break;
        }
        return true;
    }


    boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, getString(R.string.again_to_exit), Toast.LENGTH_LONG).show();
        new Handler().postDelayed(() -> doubleBackToExitPressedOnce = false, 1000);
    }

    private String getFilename() {
        File file = new File(Environment.getExternalStorageDirectory()
                .getPath(), "Artalent");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/art_"
                + System.currentTimeMillis() + ".jpg");
        return uriSting;
    }

    ArrayList<String> returnValue = new ArrayList<>();

    public String saveImageFile(Bitmap bitmap) {
        FileOutputStream out = null;
        String filename = getFilename();
        try {
            out = new FileOutputStream(filename);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
            Log.e("Picasso123", filename);
            //
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return filename;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100) { // Please, use a final int instead of hardcoded int value
            if (resultCode == RESULT_OK) {
                if (data.getStringArrayListExtra(AppCameraActivity.IMAGE_RESULTS) != null && (data.getStringArrayListExtra(AppCameraActivity.IMAGE_RESULTS)).size() > 0) {
                    Uri uri = data.getParcelableExtra("path");
                    Bitmap bitmap = null;
                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
                        saveImageFile(bitmap);
                        returnValue.add(saveImageFile(bitmap));
                        if (returnValue.size() > 0) {
//                            callUserProfileApi();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private void callUserProfileApi() {
        if (Utils.isInternetAvailable(MainActivity.this)) {
            showProgress();
            File file = new File(returnValue.get(0));
            RequestBody postBody = RequestBody.create(MediaType.parse("image/*"), file);
            MultipartBody.Part postImagesParts = MultipartBody.Part.createFormData("file", file.getName(), postBody);

            RequestBody description = RequestBody.create(MediaType.parse("multipart/form-data"), "Hello, Artalent");
            ArtalentApplication.apiServicePost.addStory(ArtalentApplication.prefManager.getUserObj().getId(), postImagesParts, description)
                    .enqueue(new Callback<BaseResponse>() {
                        @Override
                        public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                            hideProgress();
                            returnValue.clear();
                            int statusCode = response.code();
                            String mess = "";
                            try {
                                mess = response.body().getMessage();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            if (statusCode == 200) {
                                Utils.showToast(MainActivity.this, mess);
                                if (homeFragment != null) {
                                    homeFragment.callGetStoryApi();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<BaseResponse> call, Throwable t) {
                            // Log error here since request failed
                            Log.e("TAGArtalent", t.toString());
                            hideProgress();
                            returnValue.clear();
                        }
                    });
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_notification, menu);
        //menuOpen = menu.findItem(R.id.action_delete);
        //menuOpen.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_delete) {
            NotificationListActivity.startActivity(this);
        }
        return super.onOptionsItemSelected(item);
    }


}