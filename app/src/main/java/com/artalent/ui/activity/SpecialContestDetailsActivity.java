package com.artalent.ui.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.artalent.R;
import com.artalent.Utils;
import com.artalent.ui.model.SpecialContestObj;
import com.artalent.ui.model.SpecialContestWinnerObj;
import com.squareup.picasso.Picasso;

import butterknife.BindView;

/**
 * Created by Miroslaw Stanek on 14.01.15.
 */
public class SpecialContestDetailsActivity extends BaseActivityOther {

    private static final String TAG = "SpecialContestDetailsActivity";

    @BindView(R.id.image)
    ImageView image;

    @BindView(R.id.tvStartDate)
    TextView tvStartDate;

    @BindView(R.id.tvEndDate)
    TextView tvEndDate;

    @BindView(R.id.special_contest_title)
    TextView special_contest_title;

    @BindView(R.id.special_contest_description)
    TextView special_contest_description;

    @BindView(R.id.special_winner)
    TextView special_winner;

    @BindView(R.id.special_contest_total_slot)
    TextView specialContestTotalSlot;

    @BindView(R.id.special_contest_available_slot)
    TextView specialContestAvailableSlot;

    @BindView(R.id.special_contest_registration_fee)
    TextView specialContestRegistrationFee;

    @BindView(R.id.special_contest_country_txt)
    TextView specialContestCountryTxt;

    @BindView(R.id.special_contest_state_txt)
    TextView specialContestStateTxt;

    @BindView(R.id.special_contest_district_txt)
    TextView specialContestDistrictTxt;

    private SpecialContestObj specialContestObj = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contest_details);
        setupToolbarBack(getString(R.string.special_contest_details));
        specialContestObj = (SpecialContestObj) getIntent().getSerializableExtra("contest_item");
        Log.d(TAG, "onCreate: specialContestObj =  " + specialContestObj.toString());
        setUserData();
    }

    private void setUserData() {

        Picasso picasso = Picasso.get();
        picasso.load(specialContestObj.getImage())
                .placeholder(R.drawable.chat_background)
                .into(image);

        if (specialContestObj != null && specialContestObj.getStartDate() != null && !TextUtils.isEmpty(specialContestObj.getStartDate())) {
            tvStartDate.setText("Registration Started On " + specialContestObj.getStartDate());
        } else {
            tvStartDate.setText("");
        }

        if (specialContestObj != null && specialContestObj.getRegEndDate() != null && !TextUtils.isEmpty(specialContestObj.getRegEndDate())) {
            String sCurrentDate = Utils.getCurrentDate();
            String sContestRegEndDate = specialContestObj.getRegEndDate();
            if (Utils.getCurrentDiffDate(sCurrentDate, sContestRegEndDate) >= 0) {
                tvEndDate.setText("Registration Ends On " + specialContestObj.getRegEndDate());
            } else {
                if (specialContestObj != null && specialContestObj.getEndDate() != null && !TextUtils.isEmpty(specialContestObj.getEndDate())) {
                    tvEndDate.setText("Contest Ends On " + specialContestObj.getEndDate());
                }
            }
        } else {
            if (specialContestObj != null && specialContestObj.getEndDate() != null && !TextUtils.isEmpty(specialContestObj.getEndDate())) {
                tvEndDate.setText("Contest Ends On " + specialContestObj.getEndDate());
            }
        }

        if (specialContestObj != null && specialContestObj.getTittle() != null && !TextUtils.isEmpty(specialContestObj.getTittle())) {
            special_contest_title.setText(specialContestObj.getTittle());
        } else {
            special_contest_title.setText("");
        }

        if (specialContestObj != null && specialContestObj.getDescription() != null && !TextUtils.isEmpty(specialContestObj.getDescription())) {
            special_contest_description.setText(specialContestObj.getDescription());
        } else {
            special_contest_description.setText("");
        }

        if (specialContestObj != null && specialContestObj.getWinnerContestWinnerList() != null && specialContestObj.getWinnerContestWinnerList().size() > 0) {
            String winner = "";
            for (int i = 0; i < specialContestObj.getWinnerContestWinnerList().size(); i++) {
                SpecialContestWinnerObj specialContestWinnerObj = specialContestObj.getWinnerContestWinnerList().get(i);
                if (TextUtils.isEmpty(winner)) {
                    winner = "" + (i + 1) + " " + specialContestWinnerObj.getWinnerTitle() + " Winner Prize ₹ " + specialContestWinnerObj.getPrice();
                } else {
                    winner = winner + "\n" + (i + 1) + " " + specialContestWinnerObj.getWinnerTitle() + " Winner Prize ₹ " + specialContestWinnerObj.getPrice();
                }

                //for (SpecialContestWinnerObj specialContestWinnerObj : specialContestObj.getWinnerContestWinnerList()) {
            }
            special_winner.setText(winner);
        } else {
            special_winner.setText("-");
        }

        if (specialContestObj != null && String.valueOf(specialContestObj.getMaxRegCount()) != null && !TextUtils.isEmpty(String.valueOf(specialContestObj.getMaxRegCount()))) {
            specialContestTotalSlot.setText(String.valueOf(specialContestObj.getMaxRegCount()));
        } else {
            specialContestTotalSlot.setText("");
        }

        if (specialContestObj != null && String.valueOf(specialContestObj.getRegisteredCount()) != null && !TextUtils.isEmpty(String.valueOf(specialContestObj.getRegisteredCount()))) {
            specialContestAvailableSlot.setText(String.valueOf(specialContestObj.getMaxRegCount() - specialContestObj.getRegisteredCount()));
        } else {
            specialContestAvailableSlot.setText("");
        }

        if (specialContestObj != null && specialContestObj.getRegFee() != null && !TextUtils.isEmpty(specialContestObj.getRegFee())) {
            specialContestRegistrationFee.setText(specialContestObj.getRegFee() + " Rs.");
        } else {
            specialContestRegistrationFee.setText("");
        }

        if (specialContestObj != null && specialContestObj.getCountry().getCountryName() != null && !TextUtils.isEmpty(specialContestObj.getCountry().getCountryName())) {
            specialContestCountryTxt.setText(specialContestObj.getCountry().getCountryName());
        } else {
            specialContestCountryTxt.setText("");
        }

        if (specialContestObj != null && specialContestObj.getState().getState() != null && !TextUtils.isEmpty(specialContestObj.getState().getState())) {
            specialContestStateTxt.setText(specialContestObj.getState().getState());
        } else {
            specialContestStateTxt.setText("");
        }

        if (specialContestObj != null && specialContestObj.getDistricts().getDistrict() != null && !TextUtils.isEmpty(specialContestObj.getDistricts().getDistrict())) {
            specialContestDistrictTxt.setText(specialContestObj.getDistricts().getDistrict());
        } else {
            specialContestDistrictTxt.setText("");
        }
    }
}
