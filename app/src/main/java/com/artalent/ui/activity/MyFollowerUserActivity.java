package com.artalent.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.artalent.ArtalentApplication;
import com.artalent.R;
import com.artalent.Utils;
import com.artalent.ui.adapter.SearchUserAdapter;
import com.artalent.ui.model.BaseResponse;
import com.artalent.ui.model.Country;
import com.artalent.ui.model.DistrictsObj;
import com.artalent.ui.model.InterestObj;
import com.artalent.ui.model.StateObj;
import com.artalent.ui.model.UserObj;
import com.artalent.ui.model.UserResponse;
import com.artalent.ui.model.request.ContestRegistrationReq;
import com.artalent.ui.model.request.FollowerObjResponse;
import com.artalent.ui.model.request.FollowingReq;
import com.artalent.ui.utils.Constants;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyFollowerUserActivity extends BaseActivityOther implements View.OnClickListener {

    public static void startActivity(Activity mActivity, int userID) {
        Intent intent = new Intent(mActivity, MyFollowerUserActivity.class);
        intent.putExtra(Constants.ID, userID);
        mActivity.startActivity(intent);
    }

    private List<UserObj> userObjList = new ArrayList<>();
    private RecyclerView recyclerView;

    @BindView(R.id.pullToRefresh)
    SwipeRefreshLayout pullToRefresh;

   /* @BindView(R.id.ivFilter)
    TextView ivFilter;*/


    private SearchUserAdapter feedAdapter;
    private int offset = 0;

    private LinearLayoutManager mLayoutManager;
    private boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;

    public static final int REQUEST_FILTER = 400;
    private Country selectedCountry = null;
    List<InterestObj> selectedInterestList = new ArrayList<>();
    private StateObj selectedStateObj = null;
    List<DistrictsObj> selectedDistrictsList = new ArrayList<>();
    private int userID = 0;

    @BindView(R.id.tvNoResult)
    TextView tvNoResult;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_following_user);
        userID = getIntent().getIntExtra(Constants.ID, 0);
        setupToolbarBack(getString(R.string.my_followers));
        ButterKnife.bind(this);
        tvNoResult.setText(getString(R.string.no_follower_yet));
        recyclerView = findViewById(R.id.recycler_view);
        setupFeed();
        refresh();
        initScrollListener();
        pullToRefresh.setOnRefreshListener(this::refresh);
        //ivFilter.setOnClickListener(this);
    }


    private void initScrollListener() {
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NotNull RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();
                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            if (offset != -1) {
                                loading = false;
                                Log.e("mLayoutManager", "Last Item Wow !");
                                callHomePostApi();
                            }
                        }
                    }
                }
            }
        });
    }

    protected void setupFeed() {
        mLayoutManager = new LinearLayoutManager(mActivity);
        recyclerView.setLayoutManager(mLayoutManager);
        feedAdapter = new SearchUserAdapter(userObjList, mActivity, this);
        recyclerView.setAdapter(feedAdapter);
    }

    private void refresh() {
        userObjList.clear();
        offset = 0;
        callHomePostApi();
    }

    private void callHomePostApi() {
        if (Utils.isInternetAvailable(mActivity)) {
            Call<FollowerObjResponse> call = ArtalentApplication.apiService.getFollowersUser(ArtalentApplication.prefManager.getUserObj().getId(), offset, userID);
            call.enqueue(new Callback<FollowerObjResponse>() {
                @Override
                public void onResponse(Call<FollowerObjResponse> call, Response<FollowerObjResponse> response) {
                    pullToRefresh.setRefreshing(false);
                    loading = true;
                    if (response.body() != null &&
                            response.body().getObj() != null && response.body().getObj().getFollowers() != null) {
                        offset = response.body().getIndex();
                        if (response.body().getObj().getFollowers().size() > 0) {
                           /* for (UserObj userObj : response.body().getFollowers()) {
                                userObjList.add(userObj);
                            }*/
                            userObjList.addAll(response.body().getObj().getFollowers());
                        }
                        feedAdapter.notifyDataSetChanged();
                    }
                    if (userObjList.size() == 0) {
                        tvNoResult.setVisibility(View.VISIBLE);
                    } else {
                        tvNoResult.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onFailure(Call<FollowerObjResponse> call, Throwable t) {
                    // Log error here since request failed
                    loading = true;
                    Log.e("TAG", t.toString());
                    pullToRefresh.setRefreshing(false);
                }
            });
        } else {
            pullToRefresh.setRefreshing(false);
            Intent intent = new Intent(mActivity, NoInternetActivity.class);
            startActivityForResult(intent, Constants.NO_INTERNET);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.NO_INTERNET) {
            if (resultCode == RESULT_OK) {
                refresh();
            }
        }
        if (requestCode == REQUEST_FILTER) {
            if (resultCode == RESULT_OK) {
                selectedInterestList = (ArrayList<InterestObj>) data.getSerializableExtra(Constants.INTEREST_LIST);
                selectedCountry = (Country) data.getSerializableExtra(Constants.COUNTRY_OBJ);
                selectedStateObj = (StateObj) data.getSerializableExtra(Constants.STATE_OBJ);
                selectedDistrictsList = (ArrayList<DistrictsObj>) data.getSerializableExtra(Constants.DISTRICTS_LIST);
                refresh();
            }
        }
    }

    private ContestRegistrationReq fillData() {
        ContestRegistrationReq createReq = new ContestRegistrationReq();
        int countFilter = 0;
        String sDistrictIds = "";
        if (selectedDistrictsList.size() > 0) {
            countFilter = countFilter + selectedDistrictsList.size();
            for (DistrictsObj districtsObj : selectedDistrictsList) {
                if (TextUtils.isEmpty(sDistrictIds)) {
                    sDistrictIds = "" + districtsObj.getDistrictId();
                } else {
                    sDistrictIds = sDistrictIds + "," + districtsObj.getDistrictId();
                }

            }
        }

        String sInterestIds = "";
        if (selectedInterestList.size() > 0) {
            countFilter = countFilter + selectedInterestList.size();
            for (InterestObj interestObj : selectedInterestList) {
                if (TextUtils.isEmpty(sInterestIds)) {
                    sInterestIds = "" + interestObj.getInterestMasterId();
                } else {
                    sInterestIds = sInterestIds + "," + interestObj.getInterestMasterId();
                }

            }
        }

        if (selectedCountry != null) {
            countFilter = countFilter + 1;
        }
        if (selectedStateObj != null) {
            countFilter = countFilter + 1;
        }

        if (countFilter == 0) {
//            ivFilter.setText(getString(R.string.select_filter));
            //ivFilter.setTextColor(ContextCompat.getColor(mActivity, R.color.primary));
        } else {
            //ivFilter.setText(" (" + countFilter + ")");
            //ivFilter.setTextColor(ContextCompat.getColor(mActivity, R.color.primary));
        }
        createReq.setDistrictId(sDistrictIds);
        createReq.setInterestId(sInterestIds);
        return createReq;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnFollow:
                int position = (int) v.getTag();
                UserObj userObj = userObjList.get(position);
                if (userObj != null) {
                    if (userObj.getIsFollowed() == 1) {
                        postFollowDelete(userObj, position);
                    } else {
                        postFollow(userObj, position);
                    }
                }
                break;
            case R.id.rlTop:
                UserObj userObj1 = (UserObj) v.getTag();
                if (userObj1 != null) {
                    Intent intent = new Intent(mActivity, OtherUserProfileActivity.class);
                    intent.putExtra(Constants.DATA, userObj1);
                    startActivity(intent);
                }
                break;
            case R.id.ivFilter:
                Intent intent = new Intent(mActivity, FilterActivity.class);
                intent.putExtra(Constants.INTEREST_LIST, (Serializable) selectedInterestList);
                intent.putExtra(Constants.COUNTRY_OBJ, selectedCountry);
                intent.putExtra(Constants.STATE_OBJ, selectedStateObj);
                intent.putExtra(Constants.DISTRICTS_LIST, (Serializable) selectedDistrictsList);
                startActivityForResult(intent, REQUEST_FILTER);
                break;

        }
    }

    private void postFollowDelete(UserObj userObj, int position) {
        if (Utils.isInternetAvailable(mActivity)) {
            ArtalentApplication.apiService.postFollowDelete(ArtalentApplication.prefManager.getUserObj().getId(), (userObj.getId()))
                    .enqueue(new Callback<BaseResponse>() {
                        @Override
                        public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                            int statusCode = response.code();
                            if (response.body() != null) {
                                userObjList.get(position).setIsFollowed(0);
                                feedAdapter.notifyItemChanged(position);
                                getUserData();
                            }
                        }

                        @Override
                        public void onFailure(Call<BaseResponse> call, Throwable t) {
                            // Log error here since request failed
                            Log.e("TAGArtalent", t.toString());
                        }
                    });
        } else {
            Utils.showToast(mActivity, getResources().getString(R.string.please_check_your_internet));
        }
    }

    private void postFollow(UserObj userObj, int position) {
        if (Utils.isInternetAvailable(mActivity)) {
            ArtalentApplication.apiService.postFollow(ArtalentApplication.prefManager.getUserObj().getId(), fillDataFollow(userObj.getId()))
                    .enqueue(new Callback<BaseResponse>() {
                        @Override
                        public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                            int statusCode = response.code();
                            if (response.body() != null) {
                                userObjList.get(position).setIsFollowed(1);
                                feedAdapter.notifyItemChanged(position);
                                getUserData();
                            }
                        }

                        @Override
                        public void onFailure(Call<BaseResponse> call, Throwable t) {
                            // Log error here since request failed
                            Log.e("TAGArtalent", t.toString());
                        }
                    });
        } else {
            Utils.showToast(mActivity, getResources().getString(R.string.please_check_your_internet));
        }
    }

    private FollowingReq fillDataFollow(int id) {
        FollowingReq createReq = new FollowingReq();
        createReq.setFollowingId(id);
        return createReq;
    }

    private void getUserData() {
        if (Utils.isInternetAvailable(mActivity)) {
            ArtalentApplication.apiService.getOtherUserDetails(ArtalentApplication.prefManager.getUserObj().getId()).enqueue(new Callback<UserResponse>() {
                @Override
                public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                    int statusCode = response.code();
                    if (response.body() != null && response.body().getObj() != null) {
                        UserObj userObj = response.body().getObj();
                    }
                }

                @Override
                public void onFailure(Call<UserResponse> call, Throwable t) {
                    // Log error here since request failed
                    Log.e("TAG", t.toString());
                }
            });
        } else {
            Intent intent = new Intent(mActivity, NoInternetActivity.class);
            startActivityForResult(intent, Constants.NO_INTERNET);
        }
    }

}
