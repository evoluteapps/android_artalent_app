package com.artalent.ui.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.artalent.ArtalentApplication;
import com.artalent.R;
import com.artalent.Utils;
import com.artalent.ui.adapter.FilterStateAdapter;
import com.artalent.ui.model.BaseResponse;
import com.artalent.ui.model.StateObj;
import com.artalent.ui.model.StateResponse;
import com.artalent.ui.model.request.StateReq;
import com.artalent.ui.utils.Constants;
import com.artalent.ui.utils.RecyclerTouchListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by Mahajan on 16-09-2019.
 */

public class SelectStateActivity extends BaseActivityOther implements View.OnClickListener {

    RecyclerView recyclerView;
    FloatingActionButton save, addFab;
    FilterStateAdapter selectStateAdapter;
    List<StateObj> countries = new ArrayList<>();
    private StateObj selectedStateObj = null;
    private int selectedCountryId = 0;
    private int fromData;
    private TextView tvNoListItem;

    private RecyclerTouchListener touchListener;

    public static void startActivity(Activity mActivity, StateObj stateObj, int countryId, int requestCode) {
        Intent intent = new Intent(mActivity, SelectStateActivity.class);
        intent.putExtra(Constants.ID, countryId);
        intent.putExtra(Constants.STATE_OBJ, stateObj);
        mActivity.startActivityForResult(intent, requestCode);
    }

    public static void startActivity(Activity mActivity, StateObj stateObj, int countryId, int requestCode, int from) {
        Intent intent = new Intent(mActivity, SelectStateActivity.class);
        intent.putExtra(Constants.ID, countryId);
        intent.putExtra(Constants.STATE_OBJ, stateObj);
        intent.putExtra(Constants.FROM, from);
        mActivity.startActivityForResult(intent, requestCode);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_countery);
        init1();
        setupToolbarBack(getString(R.string.manage_state));
        initializeUi();
        if (fromData == 1) {
            initializeRecyclerTouchListener();
        }
        callStateApi();
    }

    private void initializeRecyclerTouchListener() {
        touchListener = new RecyclerTouchListener(this, recyclerView);
        touchListener
                .setClickable(new RecyclerTouchListener.OnRowClickListener() {
                    @Override
                    public void onRowClicked(int position) {
                        StateObj country = countries.get(position);
                        SelectDistrictsActivity.startActivity(mActivity, null, country.getStateId(), 0, fromData);
                    }

                    @Override
                    public void onIndependentViewClicked(int independentViewID, int position) {

                    }
                })
                .setSwipeOptionViews(R.id.delete_task, R.id.edit_task)
                .setSwipeable(R.id.rowFG, R.id.rowBG, (viewID, position) -> {
                    switch (viewID) {
                        case R.id.delete_task:
                            callDeleteStateApi(countries.get(position), position);
                            break;
                        case R.id.edit_task:
                            showDialogData(countries.get(position), true);
                            break;

                    }
                });
    }

    private void callStateApi() {
        if (Utils.isInternetAvailable(mActivity)) {
            showProgress();
            Call<StateResponse> call = ArtalentApplication.apiService.getStates(selectedCountryId);
            call.enqueue(new Callback<StateResponse>() {
                @Override
                public void onResponse(Call<StateResponse> call, Response<StateResponse> response) {
                    int statusCode = response.code();
                    if (response.body() != null && response.body().getObj() != null && response.body().getObj().size() > 0) {
                        hideProgress();
                        tvNoListItem.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                        countries.clear();
                        countries.addAll(response.body().getObj());
                        for (StateObj stateObj : countries) {
                            if (selectedStateObj != null) {
                                if (stateObj.getStateId() == selectedStateObj.getStateId()) {
                                    stateObj.setCheck(true);
                                }
                            }
                        }
                        setAdapter();
                    } else {
                        tvNoListItem.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.GONE);
                        hideProgress();
                    }
                }

                @Override
                public void onFailure(Call<StateResponse> call, Throwable t) {
                    // Log error here since request failed
                    Log.e("TAG", t.toString());
                }
            });
        }
    }


    private void callDeleteStateApi(StateObj stateObj, int position) {
        if (Utils.isInternetAvailable(mActivity)) {
            showProgress();
            Call<BaseResponse> call = ArtalentApplication.apiService.deleteState(ArtalentApplication.prefManager.getUserObj().getId(), stateObj.getCountryId());
            call.enqueue(new Callback<BaseResponse>() {
                @Override
                public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                    int statusCode = response.code();
                    String mess = response.body().getMessage();
                    if (statusCode == 200) {
                        hideProgress();
                        countries.remove(position);
                        selectStateAdapter.setTaskList(countries);
                        Utils.showToast(mActivity, mess);
                    } else {
                        hideProgress();
                        Utils.showToast(mActivity, mess);
                    }
                }

                @Override
                public void onFailure(Call<BaseResponse> call, Throwable t) {
                    // Log error here since request failed
                    dialog.dismiss();
                    hideProgress();
                    Utils.showToast(mActivity, "onFailure : " + t.toString());
                }
            });
        }
    }

    private void callEditStateApi(StateObj stateObj) {
        if (Utils.isInternetAvailable(mActivity)) {
            showProgress();
            Call<BaseResponse> call = ArtalentApplication.apiService.editState(ArtalentApplication.prefManager.getUserObj().getId(), stateObj.getStateId(), fillDataState(stateObj.getState(), stateObj.getCountryId()));
            call.enqueue(new Callback<BaseResponse>() {
                @Override
                public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                    int statusCode = response.code();
                    String mess = response.body().getMessage();
                    if (statusCode == 200) {
                        hideProgress();
                        dialog.dismiss();
                        Utils.showToast(mActivity, mess);
                        callStateApi();
                    } else {
                        hideProgress();
                        dialog.dismiss();
                        Utils.showToast(mActivity, mess);
                    }
                }

                @Override
                public void onFailure(Call<BaseResponse> call, Throwable t) {
                    // Log error here since request failed
                    dialog.dismiss();
                    hideProgress();
                    Utils.showToast(mActivity, "onFailure : " + t.toString());
                }
            });
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (fromData == 1) {
            recyclerView.addOnItemTouchListener(touchListener);
        }
    }


    private void setAdapter() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        selectStateAdapter = new FilterStateAdapter(mActivity, countries, this, fromData);
        selectStateAdapter.setTaskList(countries);
        recyclerView.setAdapter(selectStateAdapter);
    }

    private void init1() {
        selectedCountryId = getIntent().getIntExtra(Constants.ID, 0);
        selectedStateObj = (StateObj) getIntent().getSerializableExtra(Constants.STATE_OBJ);
        fromData = getIntent().getIntExtra(Constants.FROM, 0);
    }


    @SuppressLint("RestrictedApi")
    public void initializeUi() {
        tvNoListItem = findViewById(R.id.tv_no_any_item);
        recyclerView = findViewById(R.id.recyclerView);
        save = findViewById(R.id.save);
        addFab = findViewById(R.id.addFab);
        save.setOnClickListener(this);
        addFab.setOnClickListener(this);
        if (fromData == 1) {
            addFab.setVisibility(View.VISIBLE);
            save.setVisibility(View.GONE);

        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.addFab:
                showDialogData(null, false);
                break;
            case R.id.save:
                if (selectedStateObj != null) {
                    Intent output = new Intent();
                    output.putExtra(Constants.STATE_OBJ, selectedStateObj);
                    setResult(RESULT_OK, output);
                    finish();
                } else {
                    Utils.showToast(mActivity, mActivity.getString(R.string.please_select_state));
                }

                break;
            case R.id.llMain:
                if (v.getTag() instanceof StateObj) {
                    StateObj stateObj = (StateObj) v.getTag();
                    if (stateObj != null) {
                        for (StateObj stateObj1 : countries) {
                            if (stateObj1.getStateId() == stateObj.getStateId()) {
                                stateObj1.setCheck(!stateObj1.isCheck());
                                if (stateObj1.isCheck()) {
                                    selectedStateObj = stateObj;
                                } else {
                                    selectedStateObj = null;
                                }
                            } else {
                                stateObj1.setCheck(false);
                            }
                        }
                        selectStateAdapter.notifyDataSetChanged();
                    }
                }
                break;
        }
    }

    Dialog dialog;

    private void showDialogData(StateObj stateObj, boolean isEdit) {
        dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_add_data);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setTitle("Title...");
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        // set the custom dialog components - text, image and button
        TextView titleTextView = dialog.findViewById(R.id.titleTextView);
        TextInputEditText name_et = dialog.findViewById(R.id.name_et);
        if (isEdit) {
            titleTextView.setText(R.string.edit_state);
            name_et.setText(stateObj.getState());
        } else {
            titleTextView.setText(R.string.add_state);
        }
        name_et.setHint("Enter state name");
        Button downloadButton = dialog.findViewById(R.id.downloadButton);
        Button cancelButton = dialog.findViewById(R.id.cancelButton);
        downloadButton.setOnClickListener(v -> {
            if (!TextUtils.isEmpty(name_et.getText().toString())) {
                if (isEdit) {
                    stateObj.setState(name_et.getText().toString());
                    callEditStateApi(stateObj);
                } else {
                    callAddStateApi(name_et.getText().toString());
                }
            } else {
                Utils.showToast(SelectStateActivity.this, "Please enter state name");
            }
        });
        cancelButton.setOnClickListener(v -> dialog.dismiss());
        dialog.show();
    }

    private StateReq fillDataState(String stateName, int countryId) {
        StateReq stateReq = new StateReq();
        stateReq.setStateName(stateName);
        stateReq.setCountryId(countryId);
        return stateReq;
    }

    private void callAddStateApi(String stateName) {
        if (Utils.isInternetAvailable(mActivity)) {
            hideProgress();
            Call<BaseResponse> call = ArtalentApplication.apiService.addState(ArtalentApplication.prefManager.getUserObj().getId(), fillDataState(stateName, selectedCountryId));
            call.enqueue(new Callback<BaseResponse>() {
                @Override
                public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                    int statusCode = response.code();
                    String mess = response.body().getMessage();
                    if (statusCode == 200) {
                        callStateApi();
                        hideProgress();
                        dialog.dismiss();
                        Utils.showToast(mActivity, mess);
                    } else {
                        hideProgress();
                        dialog.dismiss();
                        Utils.showToast(mActivity, mess);
                    }
                }

                @Override
                public void onFailure(Call<BaseResponse> call, Throwable t) {
                    // Log error here since request failed
                    dialog.dismiss();
                    hideProgress();
                    Utils.showToast(mActivity, "onFailure : " + t.toString());
                    Log.e("TAG", t.toString());
                }
            });
        }

    }

}
