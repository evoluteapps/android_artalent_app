package com.artalent.ui.activity;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import com.google.android.material.textfield.TextInputEditText;
import android.text.TextUtils;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.artalent.ArtalentApplication;
import com.artalent.R;
import com.artalent.Utils;
import com.artalent.ui.model.UserObj;

import java.net.URLEncoder;

import butterknife.BindView;
import butterknife.OnClick;

public class ReferEarnActivity extends BaseActivityOther {


    @Nullable
    @BindView(R.id.etReferalCode)
    TextInputEditText etReferalCode;

    @Nullable
    @BindView(R.id.tvShare)
    TextView tvShare;

    @Nullable
    @BindView(R.id.rlWhatsApp)
    RelativeLayout rlWhatsApp;

    @BindView(R.id.refer_earn)
    TextView referEarnTV;

    private String mShare = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_refer_earn);
        setupToolbarBack(getString(R.string.refer_earn));
        initializeUi();
    }


    public void initializeUi() {
        UserObj userObj = ArtalentApplication.prefManager.getUserObj();
        if (userObj != null && !TextUtils.isEmpty(userObj.getReferalCode())) {
            etReferalCode.setText(getString(R.string.copy_refferal_code) + userObj.getReferalCode());
            getStringShare(userObj.getReferalCode());
        }

    }

    private void getStringShare(String referalCode) {
        mShare = "Hey! Check out the Artalent app.\n" +
                "\n" +
                "Don't forget to use my Referral code \"" + referalCode + "\" during sign up! \n" +
                "\n" +
                "\n" +
                "Download the app at :\n" +
                "http://www.fusionik.in";
    }

    @OnClick(R.id.etReferalCode)
    public void onAddPost() {
        copiedToClipboard();
    }

    @OnClick(R.id.tvShare)
    public void onCardShare() {
        openShare();
    }

    @OnClick(R.id.rlWhatsApp)
    public void onWhatsApp() {
        openShareWhatsApp();
    }

    @OnClick(R.id.refer_earn)
    public void openWalletScreen(){
        referEarnTV.setAlpha(0.5F);
        new Handler().postDelayed(() -> {
            referEarnTV.setAlpha(1F);
        }, 500);
        startActivity(new Intent(mActivity, ArtalentWalletActivity.class));
    }

    private void copiedToClipboard() {
        UserObj userObj = ArtalentApplication.prefManager.getUserObj();
        if (userObj != null && !TextUtils.isEmpty(userObj.getReferalCode())) {
            Toast.makeText(mActivity, mActivity.getString(R.string.copied_to_clipboard), Toast.LENGTH_LONG).show();
            ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText("label", userObj.getReferalCode());
            clipboard.setPrimaryClip(clip);
            etReferalCode.setAlpha(0.5F);
            new Handler().postDelayed(() -> {
                etReferalCode.setAlpha(1F);
            }, 1000);
        }
    }

    private void openShare() {
        UserObj userObj = ArtalentApplication.prefManager.getUserObj();
        if (userObj != null && !TextUtils.isEmpty(userObj.getReferalCode())) {
            // etReferalCode.setText(getString(R.string.copy_refferal_code) + userObj.getReferalCode());

            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, mShare);
            sendIntent.setType("text/plain");
            startActivity(Intent.createChooser(sendIntent, "Share via"));
        }

    }

    private void openShareWhatsApp() {
        PackageManager packageManager = getPackageManager();
        Intent i = new Intent(Intent.ACTION_VIEW);
        try {
            String url = "https://api.whatsapp.com/send?phone=" + "" + "&text=" + URLEncoder.encode(mShare, "UTF-8");
            i.setPackage("com.whatsapp");
            i.setData(Uri.parse(url));
            if (i.resolveActivity(packageManager) != null) {
                startActivity(i);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Utils.showToast(mActivity, mActivity.getString(R.string.whatsapp_did_not_find_in_my_phone));
        }
    }

}
