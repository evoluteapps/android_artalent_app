package com.artalent.ui.activity;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.Settings;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.artalent.ArtalentApplication;
import com.artalent.R;
import com.artalent.Utils;
import com.artalent.ui.model.BaseResponse;
import com.artalent.ui.model.Country;
import com.artalent.ui.model.DistrictsObj;
import com.artalent.ui.model.StateObj;
import com.artalent.ui.model.UserNameCountResponse;
import com.artalent.ui.model.UserObj;
import com.artalent.ui.model.UserResponse;
import com.artalent.ui.model.request.CreateReq;
import com.artalent.ui.utils.Constants;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by Mahajan on 10-09-2019.
 */

public class RegistrationActivity extends BaseActivityOther implements View.OnClickListener {


    @BindView(R.id.first_name_et)
    EditText firstName;

    @BindView(R.id.last_name_et)
    EditText lastName;

    @BindView(R.id.user_name_et)
    EditText userName;

    @BindView(R.id.country_et)
    TextView country;

    @BindView(R.id.state_et)
    TextView state;

    @BindView(R.id.district_et)
    TextView district;

    @BindView(R.id.pin_code_et)
    EditText pinCodeEt;



   /* @BindView(R.id.submit_btn)
    Button submit;*/

    @BindView(R.id.userProfileImage)
    ImageView userProfileImage;

    @BindView(R.id.llProfile)
    LinearLayout llProfile;

    private int avatarSize;
    private String profilePhoto;
    public static final int REQUEST_IMAGE = 100;
    public static final int REQUEST_COUNTRY = 200;
    public static final int REQUEST_STATE = 300;
    public static final int REQUEST_DISTRICT = 400;

    private Country selectedCountry = null;
    private StateObj selectedStateObj = null;
    private DistrictsObj selectedDistrictsObj = null;


    TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (s.toString().length() == 1 && s.toString().startsWith("0")) {
                s.clear();
            }
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        setupToolbarBack(getString(R.string.add_details));
        country.setOnClickListener(this);
        state.setOnClickListener(this);
        district.setOnClickListener(this);
        pinCodeEt.addTextChangedListener(textWatcher);
        this.avatarSize = getResources().getDimensionPixelSize(R.dimen.dimen_90);
        this.profilePhoto = getString(R.string.user_profile_photo);
        ImagePickerActivity.clearCache(this);
        setData();
    }


    private void setData() {
        UserObj userObj = ArtalentApplication.prefManager.getUserObj();
        if (userObj != null) {
            if (!TextUtils.isEmpty(userObj.getProfileImage())) {
                Glide.with(mActivity)
                        .load(userObj.getProfileImage()).apply(new RequestOptions()
                        .centerCrop()
                        .placeholder(R.drawable.interest_categories)
                        .transform(new RoundedCorners(100))
                        .error(R.drawable.interest_categories)).into(userProfileImage);
            }
            if (!TextUtils.isEmpty(userObj.getFirstName())) {
                firstName.setText(userObj.getFirstName());
            }
            if (!TextUtils.isEmpty(userObj.getLastName())) {
                lastName.setText(userObj.getLastName());
            }
            if (!TextUtils.isEmpty(userObj.getUserName())) {
                userName.setText(userObj.getUserName());
            }
            if (!TextUtils.isEmpty(userObj.getPincode())) {
                pinCodeEt.setText(userObj.getPincode());
            }

            if (userObj.getCountry() != null && userObj.getCountry().getCountryId() > 0) {
                selectedCountry = userObj.getCountry();
                country.setText(selectedCountry.getCountryName());
            }
            if (userObj.getState() != null && userObj.getState().getStateId() > 0) {
                selectedStateObj = userObj.getState();
                state.setText(selectedStateObj.getState());
            }
            if (userObj.getDistrict() != null && userObj.getDistrict().getDistrictId() > 0) {
                selectedDistrictsObj = userObj.getDistrict();
                district.setText(selectedDistrictsObj.getDistrict());
            }
        }
    }


    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.country_et:
                SelectCountryActivity.startActivity(mActivity, selectedCountry, REQUEST_COUNTRY);
                break;
            case R.id.state_et:
                if (selectedCountry != null && selectedCountry.getCountryId() > 0) {
                    SelectStateActivity.startActivity(mActivity, selectedStateObj, selectedCountry.getCountryId(), REQUEST_STATE);
                } else {
                    Utils.showToast(mActivity, mActivity.getString(R.string.select_country_first));
                }

                break;
            case R.id.district_et:
                if (selectedStateObj != null && selectedStateObj.getStateId() > 0) {
                    SelectDistrictsActivity.startActivity(mActivity, selectedDistrictsObj, selectedStateObj.getStateId(), REQUEST_DISTRICT);
                } else {
                    Utils.showToast(mActivity, mActivity.getString(R.string.select_state_first));
                }

                break;

           /* case R.id.submit_btn:
                intent = new Intent(getApplicationContext(), ListActivity.class);
                intent.putExtra("listName", Constants.INTEREST_LIST_NAME);
                startActivity(intent);
                finish();
                break;*/
        }
    }

    @OnClick(R.id.llProfile)
    public void onllProfileClick() {


        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            showImagePickerOptions();
                        }

                        if (report.isAnyPermissionPermanentlyDenied()) {
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    private void showImagePickerOptions() {
        ImagePickerActivity.showImagePickerOptions(this, new ImagePickerActivity.PickerOptionListener() {
            @Override
            public void onTakeCameraSelected() {
                launchCameraIntent();
            }

            @Override
            public void onChooseGallerySelected() {
                launchGalleryIntent();
            }

            @Override
            public void onDeleteImage() {

            }
        });
    }

    private void launchCameraIntent() {
        Intent intent = new Intent(this, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_IMAGE_CAPTURE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);

        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 1000);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 1000);

        startActivityForResult(intent, REQUEST_IMAGE);
    }

    private void launchGalleryIntent() {
        Intent intent = new Intent(this, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_GALLERY_IMAGE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);
        startActivityForResult(intent, REQUEST_IMAGE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                Uri uri = data.getParcelableExtra("path");
                loadProfile(uri);
            }
        } else if (requestCode == REQUEST_COUNTRY) {
            if (resultCode == Activity.RESULT_OK) {
                selectedCountry = (Country) data.getSerializableExtra(Constants.COUNTRY_OBJ);
                if (selectedCountry != null && !TextUtils.isEmpty(selectedCountry.getCountryName())) {
                    country.setText(selectedCountry.getCountryName());
                    selectedStateObj = null;
                    if (selectedStateObj != null && !TextUtils.isEmpty(selectedStateObj.getState())) {
                        state.setText(selectedStateObj.getState());
                    } else {
                        state.setText("");
                    }
                }

            }
        } else if (requestCode == REQUEST_STATE) {
            if (resultCode == Activity.RESULT_OK) {
                selectedStateObj = (StateObj) data.getSerializableExtra(Constants.STATE_OBJ);
                if (selectedStateObj != null && !TextUtils.isEmpty(selectedStateObj.getState())) {
                    state.setText(selectedStateObj.getState());
                }

            }
        } else if (requestCode == REQUEST_DISTRICT) {
            if (resultCode == Activity.RESULT_OK) {
                selectedDistrictsObj = (DistrictsObj) data.getSerializableExtra(Constants.STATE_OBJ);
                if (selectedDistrictsObj != null && !TextUtils.isEmpty(selectedDistrictsObj.getDistrict())) {
                    district.setText(selectedDistrictsObj.getDistrict());
                }

            }
        }
    }

    private void loadProfile(Uri url) {
        /*Picasso.get().load(url)
                .into(userProfileImage);*/
        Glide.with(mActivity)
                .load(url).apply(new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.interest_categories)
                .transform(new RoundedCorners(100))
                .error(R.drawable.interest_categories)).into(userProfileImage);
        userProfileImage.setColorFilter(ContextCompat.getColor(this, android.R.color.transparent));
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Bitmap bm = ((BitmapDrawable) userProfileImage.getDrawable()).getBitmap();
                saveImageFile(bm);
            }
        }, 1500);
    }

    public void saveImageFile(Bitmap bitmap) {
        FileOutputStream out = null;
        String filename = getFilename();
        try {
            out = new FileOutputStream(filename);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
            Log.e("Picasso123", filename);
            callUserProfileApi(filename);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private String getFilename() {
        File file = new File(Environment.getExternalStorageDirectory()
                .getPath(), "Artalent/ArtalentUserProfile");
        if (!file.exists()) file.mkdirs();
        String uriSting = (file.getAbsolutePath() + "/art_"
                + System.currentTimeMillis() + ".jpg");
        return uriSting;
    }

    /**
     * Showing Alert Dialog with Settings option
     * Navigates user to app settings
     * NOTE: Keep proper title and message depending on your app
     */
    private void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.dialog_permission_title));
        builder.setMessage(getString(R.string.dialog_permission_message));
        builder.setPositiveButton(getString(R.string.go_to_settings), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                RegistrationActivity.this.openSettings();
            }
        });
        builder.setNegativeButton(getString(android.R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();

    }

    // navigating user to app settings
    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_submit_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_submit:
                if (isValidate()) {
                    getUserName();

                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean isValidate() {
        if (!TextUtils.isEmpty(firstName.getText().toString().trim())) {
            if ((firstName.getText().toString().trim()).length() > 1) {
                if (!TextUtils.isEmpty(lastName.getText().toString().trim())) {
                    if ((lastName.getText().toString().trim()).length() > 1) {
                        if (!TextUtils.isEmpty(userName.getText().toString().trim())) {
                            if (!TextUtils.isEmpty(country.getText().toString().trim())) {
                                if (!TextUtils.isEmpty(state.getText().toString().trim())) {
                                    if (!TextUtils.isEmpty(district.getText().toString().trim())) {
                                        return true;
                                    } else {
                                        Utils.showToast(mActivity, "Please select districts");
                                        return false;
                                    }
                                } else {
                                    Utils.showToast(mActivity, "Please select state");
                                    return false;
                                }
                            } else {
                                Utils.showToast(mActivity, "Please select country");
                                return false;
                            }
                        } else {
                            Utils.showToast(mActivity, "Please enter username");
                            return false;
                        }
                    } else {
                        Utils.showToast(mActivity, "last name should be more then one char.");
                        return false;
                    }
                } else {
                    Utils.showToast(mActivity, "Please enter last name");
                    return false;
                }
            } else {
                Utils.showToast(mActivity, "first name should be more then one char.");
                return false;
            }
        } else {
            Utils.showToast(mActivity, "Please enter first name");
            return false;
        }
    }


    private void getUserName() {
        if (Utils.isInternetAvailable(mActivity)) {
            ArtalentApplication.apiService.verifyUserName(ArtalentApplication.prefManager.getUserObj().getId(), userName.getText().toString().trim()).enqueue(new Callback<UserNameCountResponse>() {
                @Override
                public void onResponse(Call<UserNameCountResponse> call, Response<UserNameCountResponse> response) {
                    int statusCode = response.code();
                    if (response.body() != null && response.body().getObj() != null) {
                        int count = -1;
                        try {
                            count = Integer.parseInt((response.body().getObj().getMessage()));
                        } catch (NumberFormatException e) {
                            e.printStackTrace();
                        }
                        if (count == 0) {
                            callLoginApi();
                        } else {
                            Utils.showToast(mActivity, mActivity.getString(R.string.usernamr_exit));
                        }
                    } else {
                        Utils.showToast(mActivity, mActivity.getString(R.string.please_try_again));
                    }
                }

                @Override
                public void onFailure(Call<UserNameCountResponse> call, Throwable t) {
                }
            });
        }
    }


    private void callLoginApi() {
        if (Utils.isInternetAvailable(mActivity)) {
            ArtalentApplication.apiService.updateUser(ArtalentApplication.prefManager.getUserObj().getId(), fillData())
                    .enqueue(new Callback<UserResponse>() {
                        @Override
                        public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                            int statusCode = response.code();
                            if (response.body() != null && response.body().getObj() != null) {
                                UserObj userObj = response.body().getObj();
                                if (userObj != null && !TextUtils.isEmpty(userObj.getMobile())) {
                                    ArtalentApplication.prefManager.setUserObj(userObj);
                                    SelectInterestActivity.startActivity(mActivity, false, true);
                                    finishAffinity();
                                } else {
                                    Utils.showToast(mActivity, mActivity.getString(R.string.please_try_again));
                                }
                            } else {
                                Utils.showToast(mActivity, mActivity.getString(R.string.please_try_again));
                            }

                        }

                        @Override
                        public void onFailure(Call<UserResponse> call, Throwable t) {
                            // Log error here since request failed
                            Log.e("TAGArtalent", t.toString());
                            Utils.showToast(mActivity, "onFailure : " + t.toString());
                        }
                    });
        }
    }

    public String getRealPathFromURI(Uri contentUri) {

        String[] proj = {MediaStore.Images.Media.DATA};
        try (Cursor cursor = getContentResolver().query(contentUri,
                proj, // Which columns to return
                null,       // WHERE clause; which rows to return (all rows)
                null,       // WHERE clause selection arguments (none)
                null)) {
            int column_index = 0;
            if (cursor != null) {
                column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();
                return cursor.getString(column_index);
            }
            return "";
        } // Order-by clause (ascending by name)
    }

    private void callUserProfileApi(String getSelectedPath) {
        if (Utils.isInternetAvailable(mActivity)) {
            File file = new File(getSelectedPath);
            RequestBody postBody = RequestBody.create(MediaType.parse("image/*"), file);
            MultipartBody.Part postImagesParts = MultipartBody.Part.createFormData("file", file.getName(), postBody);
            ArtalentApplication.apiServicePost.updateUserProfile(ArtalentApplication.prefManager.getUserObj().getId(), postImagesParts)
                    .enqueue(new Callback<BaseResponse>() {
                        @Override
                        public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                            int statusCode = response.code();
                            if (statusCode == 200) {
                                String mess = response.body().getMessage();
                                Utils.showToast(mActivity, mess);
                                getUserData11();
                            }
                        }

                        @Override
                        public void onFailure(Call<BaseResponse> call, Throwable t) {
                            // Log error here since request failed
                            Log.e("TAGArtalent", t.toString());
                            Utils.showToast(mActivity, "onFailure : " + t.toString());
                        }
                    });
        }

    }

    private void getUserData11() {
        if (Utils.isInternetAvailable(mActivity)) {
            ArtalentApplication.apiService.getOtherUserDetails(ArtalentApplication.prefManager.getUserObj().getId()).enqueue(new Callback<UserResponse>() {
                @Override
                public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                    int statusCode = response.code();
                    if (response.body() != null && response.body().getObj() != null) {
                        UserObj userObj = response.body().getObj();
                        ArtalentApplication.prefManager.setUserObj(userObj);
                        setData();
                    } else {
                        startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                        finish();

                        Utils.showToast(mActivity, mActivity.getString(R.string.no_internet_connection_available));
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                finish();
                            }
                        }, 2000);
                    }
                }

                @Override
                public void onFailure(Call<UserResponse> call, Throwable t) {
                    // Log error here since request failed
                    Log.e("TAG", t.toString());
                }
            });
        }
    }

    private CreateReq fillData() {
        CreateReq createReq = new CreateReq();
        createReq.setMobile(ArtalentApplication.prefManager.getUserObj().getMobile());
        createReq.setId(ArtalentApplication.prefManager.getUserObj().getId());
        createReq.setFirstName(firstName.getText().toString().trim());
        createReq.setLastName(lastName.getText().toString().trim());
        createReq.setUserName(userName.getText().toString().trim());
        createReq.setDistrictId(selectedDistrictsObj.getDistrictId());
        createReq.setPincode(pinCodeEt.getText().toString().trim());
        return createReq;
    }
}
