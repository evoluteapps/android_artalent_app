package com.artalent.ui.activity;

import android.os.Bundle;

import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.artalent.R;
import com.artalent.ui.adapter.AdvertisePanelTabAdapter;
import com.artalent.ui.fragment.SPWinnerFragment;
import com.google.android.material.tabs.TabLayout;

public class SPWinnerListActivity extends BaseActivityOther {

    AdvertisePanelTabAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_winner_list);
        setupToolbarBack("The Special Contest Winners", R.drawable.ic_arrow_back_white_24dp, R.color.primary, R.color.black);
        ViewPager viewPager = findViewById(R.id.viewPager);
        TabLayout tabLayout = findViewById(R.id.tabLayout);
        adapter = new AdvertisePanelTabAdapter(getSupportFragmentManager(), FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        adapter.addFragment(new SPWinnerFragment(), "Last Week");
        adapter.addFragment(new SPWinnerFragment(), "All Time");
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
    }
}
