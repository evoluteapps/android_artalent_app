package com.artalent.ui.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.viewpager.widget.ViewPager;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextSwitcher;
import android.widget.TextView;

import com.artalent.ArtalentApplication;
import com.artalent.R;
import com.artalent.Utils;
import com.artalent.ui.adapter.CommentPostAdapter;
import com.artalent.ui.adapter.MyPagerAdapter;
import com.artalent.ui.model.BaseResponse;
import com.artalent.ui.model.CommentObj;
import com.artalent.ui.model.CommentResponse;
import com.artalent.ui.model.PostObj;
import com.artalent.ui.model.request.CommentReq;
import com.artalent.ui.utils.CircleTransformation;
import com.artalent.ui.utils.Constants;
import com.artalent.ui.pageindicator.PageIndicator;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PostCommentActivity extends BaseActivityOther implements View.OnClickListener {
    private List<CommentObj> userObjList = new ArrayList<>();
    private RecyclerView recyclerView;
    private EditText editText;
    private PostObj selectedPostObj = null;
    private ViewPager pager;
    private PageIndicator pageIndicator;
    private MyPagerAdapter myPagerAdapter;
    private FrameLayout vImageRoot;
    private ImageView ivUserProfile;
    private TextView tvUserName;
    private TextView tvFeedBottom;

    private ImageView ivDelete;
    private int commentCount = 0;

    private TextSwitcher tsLikesCounter, tsCommentCounter;
    private FloatingActionButton floatingButton;
    private NestedScrollView nestedScrollView;
    private CommentPostAdapter feedAdapter;
    private int offset = 0;
    private LinearLayoutManager mLayoutManager;
    private boolean loading = true;

    public static void startActivity(Context activity, PostObj postObjs) {
        Intent intent = new Intent(activity, PostCommentActivity.class);
        intent.putExtra(Constants.DATA, postObjs);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_comment);
        init1();
        initView();
        setupToolbarBack(mActivity.getString(R.string.view_comments));
        setData();
        setupFeed();
        refresh();

        //commentCount = selectedPostObj.getCommentCount();
        //            tsCommentCounter.setCurrentText(getResources().getQuantityString(
        //                    R.plurals.comments_count, commentCount, commentCount
        //            ));
        ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDelete(selectedPostObj);
            }
        });
        if (selectedPostObj.getUserInfo().getId() == ArtalentApplication.prefManager.getUserObj().getId()) {
            ivDelete.setVisibility(View.VISIBLE);

        }


    }

    protected void setupFeed() {
        mLayoutManager = new LinearLayoutManager(mActivity);
        recyclerView.setLayoutManager(mLayoutManager);
        feedAdapter = new CommentPostAdapter(userObjList, mActivity, this);
        recyclerView.setAdapter(feedAdapter);

        if (nestedScrollView != null) {
            nestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
                @Override
                public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                    if (scrollY > oldScrollY) {
                        Log.e("mLayoutManager", "Scroll DOWN");
                    }
                    if (scrollY < oldScrollY) {
                        Log.e("mLayoutManager", "Scroll UP");
                    }

                    if (scrollY == 0) {
                        Log.e("mLayoutManager", "TOP SCROLL");
                    }

                    if (scrollY == (v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight())) {
                        Log.e("mLayoutManager", "BOTTOM SCROLL");
                        if (loading) {
                            if (offset != -1) {
                                loading = false;
                                Log.e("mLayoutManager", "Last Item Wow !");
                                callHomePostApi();
                            }
                        }
                    }
                }
            });
        }
    }

    private void refresh() {
        userObjList.clear();
        offset = 0;
        callHomePostApi();
    }

    private void setData() {
        if (selectedPostObj != null && selectedPostObj.getMediaUrls() != null && selectedPostObj.getMediaUrls().size() > 0) {
            vImageRoot.setVisibility(View.VISIBLE);
            Picasso picasso = Picasso.get();
            myPagerAdapter = new MyPagerAdapter(picasso, selectedPostObj.getMediaUrls());
            pager.setAdapter(myPagerAdapter);
            pageIndicator.attachTo(pager);

            if (selectedPostObj.getMediaUrls().size() > 1) {
                pageIndicator.setVisibility(View.VISIBLE);
            } else {
                pageIndicator.setVisibility(View.GONE);
            }

        } else {
            vImageRoot.setVisibility(View.GONE);
        }

        if (selectedPostObj != null && selectedPostObj.getUserInfo() != null) {
            tsLikesCounter.setCurrentText(getResources().getQuantityString(
                    R.plurals.likes_count, selectedPostObj.getLikeCount(), selectedPostObj.getLikeCount()
            ));
            commentCount = selectedPostObj.getCommentCount();
            tsCommentCounter.setCurrentText(getResources().getQuantityString(
                    R.plurals.comments_count, commentCount, commentCount
            ));
            tvUserName.setText(Utils.getFullName(selectedPostObj.getUserInfo()));

            if (selectedPostObj != null && !TextUtils.isEmpty(selectedPostObj.getDescription()) && !selectedPostObj.getDescription().equals("-")) {
                tvFeedBottom.setText(selectedPostObj.getDescription());
            } else {
                tvFeedBottom.setVisibility(View.GONE);
            }

        } else {
            tvUserName.setText("");
        }


        if (selectedPostObj != null && selectedPostObj.getUserInfo() != null && !TextUtils.isEmpty(selectedPostObj.getUserInfo().getProfileImage())) {
            Picasso.get()
                    .load(selectedPostObj.getUserInfo().getProfileImage())
                    .placeholder(R.drawable.user_outline)
                    .resize(getResources().getDimensionPixelSize(R.dimen.dimen_90), getResources().getDimensionPixelSize(R.dimen.dimen_90))
                    .centerCrop()
                    .transform(new CircleTransformation())
                    .into(ivUserProfile);
        } else {
            Picasso.get()
                    .load(R.drawable.man)
                    .placeholder(R.drawable.user_outline)
                    .resize(getResources().getDimensionPixelSize(R.dimen.dimen_90), getResources().getDimensionPixelSize(R.dimen.dimen_90))
                    .centerCrop()
                    .transform(new CircleTransformation())
                    .into(ivUserProfile);
        }
    }

    private void init1() {
        selectedPostObj = (PostObj) getIntent().getSerializableExtra(Constants.DATA);

    }

    private void initView() {
        ivDelete = findViewById(R.id.ivDelete);
        ivDelete.setVisibility(View.GONE);
        tsLikesCounter = findViewById(R.id.tsLikesCounter);
        tsCommentCounter = findViewById(R.id.tsCommentCounter);
        tvUserName = findViewById(R.id.tvUserName);
        tvFeedBottom = findViewById(R.id.tvFeedBottom);
        ivUserProfile = findViewById(R.id.ivUserProfile);
        floatingButton = findViewById(R.id.floatingButton);
        recyclerView = findViewById(R.id.recycler_view);
        pager = findViewById(R.id.pager);
        editText = findViewById(R.id.editText);
        pageIndicator = findViewById(R.id.pagerPageIndicator);
        vImageRoot = findViewById(R.id.vImageRoot);
        nestedScrollView = findViewById(R.id.nestedScrollView);
        floatingButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.floatingButton:
                // Do something
                if (!TextUtils.isEmpty(editText.getText().toString().trim())) {
                    callLikeApi(editText.getText().toString().trim());
                } else {
                    Utils.showToast(mActivity, mActivity.getString(R.string.please_enter_comment));
                }

                break;
            case R.id.ivDelete:
                int position = (int) view.getTag();
                if (userObjList.size() > position) {
                    CommentObj advertiseObj = userObjList.get(position);
                    if (advertiseObj != null && advertiseObj.getPostCommentedById() > 0) {
                        open(advertiseObj, position);
                    }
                }
                break;
        }
    }


    public void open(CommentObj postObj, int position) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mActivity);
        alertDialogBuilder.setMessage("Are you sure, You want to delete comment?");
        alertDialogBuilder.setPositiveButton("yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        arg0.dismiss();
                        deleteInterest(postObj, position);
                    }
                });

        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void deleteInterest(CommentObj postObj, int position) {
        if (Utils.isInternetAvailable(mActivity)) {
            ArtalentApplication.apiService.deletePostCommentedBy(ArtalentApplication.prefManager.getUserObj().getId(), postObj.getPostCommentedById())
                    .enqueue(new Callback<BaseResponse>() {
                        @Override
                        public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                            //callInterestApi();
                            if (response.body() != null) {
                                userObjList.remove(position);
                                feedAdapter.notifyDataSetChanged();
                            }
                        }

                        @Override
                        public void onFailure(Call<BaseResponse> call, Throwable t) {

                        }
                    });
        }
    }


    private void callLikeApi(String trim) {
        if (Utils.isInternetAvailable(mActivity)) {
            CommentReq commentReq = new CommentReq();
            commentReq.setComment(trim);

            Call<BaseResponse> call = ArtalentApplication.apiService.callPostComment(ArtalentApplication.prefManager.getUserObj().getId(), selectedPostObj.getPostId(), commentReq);
            call.enqueue(new Callback<BaseResponse>() {
                @Override
                public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                    int statusCode = response.code();
                    if (response.body() != null) {
                        Utils.showToast(mActivity, response.body().getMessage());
                        Utils.hideSoftKeyboard(mActivity, editText);
                        editText.setText("");
                        commentCount = commentCount + 1;
                        tsCommentCounter.setCurrentText(getResources().getQuantityString(
                                R.plurals.comments_count, commentCount, commentCount
                        ));
                        refresh();
                    }


                }

                @Override
                public void onFailure(Call<BaseResponse> call, Throwable t) {
                    // Log error here since request failed
                    Log.e("TAG", t.toString());
                }
            });
        }

    }

    private void callHomePostApi() {
        if (Utils.isInternetAvailable(mActivity)) {
            Call<CommentResponse> call = ArtalentApplication.apiService.getCommentUser(ArtalentApplication.prefManager.getUserObj().getId(), selectedPostObj.getPostId(), offset);
            call.enqueue(new Callback<CommentResponse>() {
                @Override
                public void onResponse(Call<CommentResponse> call, Response<CommentResponse> response) {
                    //pullToRefresh.setRefreshing(false);
                    loading = true;
                    if (response.body() != null && response.body().getObj() != null) {
                        offset = response.body().getIndex();
                        if (response.body().getObj().size() > 0) {
                            userObjList.addAll(response.body().getObj());
                        }
                        feedAdapter.notifyDataSetChanged();

                    }


                }

                @Override
                public void onFailure(Call<CommentResponse> call, Throwable t) {
                    // Log error here since request failed
                    loading = true;
                    Log.e("TAG", t.toString());
                    // pullToRefresh.setRefreshing(false);
                }
            });
        } else {
            // pullToRefresh.setRefreshing(false);
        }

    }


    public void openDelete(PostObj postObj) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mActivity);
        alertDialogBuilder.setMessage("Are you sure, You want to delete post?");
        alertDialogBuilder.setPositiveButton("yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        arg0.dismiss();
                        deletePost(postObj);
                    }
                });

        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void deletePost(PostObj postObj) {
        if (Utils.isInternetAvailable(mActivity)) {
            ArtalentApplication.apiService.deletePost(ArtalentApplication.prefManager.getUserObj().getId(), postObj.getPostId())
                    .enqueue(new Callback<BaseResponse>() {
                        @Override
                        public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                            if (response.body() != null) {
                                finish();
                            }
                        }

                        @Override
                        public void onFailure(Call<BaseResponse> call, Throwable t) {

                        }
                    });
        }
    }

}
