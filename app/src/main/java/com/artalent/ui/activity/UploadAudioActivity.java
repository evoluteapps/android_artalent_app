package com.artalent.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.artalent.ArtalentApplication;
import com.artalent.R;
import com.artalent.Utils;
import com.artalent.ui.model.BaseResponse;

import java.io.File;

import butterknife.BindView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UploadAudioActivity extends BaseActivityOther implements View.OnClickListener {

    @BindView(R.id.et_audio_name)
    EditText audioNameET;


    @BindView(R.id.et_audio_duration)
    EditText audioDurationET;

    @BindView(R.id.radioGroup)
    RadioGroup radioGroup;


    @BindView(R.id.btnLogin)
    Button btnLogin;

    private File file;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_audio);
        setupToolbarBack("Upload Audio", R.drawable.ic_arrow_back_white_24dp, R.color.white, R.color.primary);
        Bundle extras = getIntent().getExtras();
        btnLogin.setOnClickListener(this);
        if (extras != null) {
            String audioRealPath = extras.getString("INTENT_AUDIO_FILE_PATH");
            String audioName = extras.getString("INTENT_FILE_NAME");
            String audioDuration = extras.getString("INTENT_FILE_DURATION");
            if (audioRealPath != null) {
                file = new File(audioRealPath);
                audioNameET.setText(file.getName());
                audioDurationET.setText(audioDuration);
            }
            /*

             */

            Log.e("audioFileName", audioName);
            Log.e("audioRealPath", "" + audioRealPath);
//        audioDurationET.setText(audioDuration);
        }
    }

    RadioButton radioSexButton;

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnLogin) {
            String songType = "";
            try {
                int selectedId = radioGroup.getCheckedRadioButtonId();
                radioSexButton = (RadioButton) findViewById(selectedId);
                songType = radioSexButton.getText().toString().toUpperCase();
            } catch (Exception e) {
                e.printStackTrace();
            }
            String name = audioNameET.getText().toString().trim();
            String duration = audioDurationET.getText().toString().trim();
            if (TextUtils.isEmpty(name)) {
                Utils.showToast(mActivity, "Please enter song name.");
            } else if (TextUtils.isEmpty(duration)) {
                Utils.showToast(mActivity, "Please enter song duration in sec.");
            } else if (TextUtils.isEmpty(songType)) {
                Utils.showToast(mActivity, "Please enter song type.");
            } else {
                callUserProfileApi(songType, duration, name);
            }
        }
    }

    private void callUserProfileApi(String sType, String sDuration, String sSongName) {
        if (Utils.isInternetAvailable(mActivity)) {
            showProgress();
            // String realPath = getRealPathFromUri(UploadAudioActivity.this, audioUri);
            //File file1 = new File(realPath);
            MultipartBody.Part postImagesParts = null;
            if (file != null) {
                RequestBody postBody = RequestBody.create(MediaType.parse("audio/*"), file);
                postImagesParts = MultipartBody.Part.createFormData("file", sSongName, postBody);
            }

            RequestBody type = RequestBody.create(MediaType.parse("multipart/form-data"), sType);
            RequestBody duration = RequestBody.create(MediaType.parse("multipart/form-data"), sDuration);
            RequestBody songName = RequestBody.create(MediaType.parse("multipart/form-data"), sSongName);

            ArtalentApplication.apiServicePost.uploadAudio(ArtalentApplication.prefManager.getUserObj().getId(), postImagesParts,
                    type, duration, songName)
                    .enqueue(new Callback<BaseResponse>() {
                        @Override
                        public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                            hideProgress();
                            int statusCode = response.code();
                            if (statusCode == 200) {
                                Intent intent = new Intent();
                                setResult(RESULT_OK, intent);
                                finish();
                            }
                        }

                        @Override
                        public void onFailure(Call<BaseResponse> call, Throwable t) {
                            hideProgress();
                            // Log error here since request failed
                            Log.e("TAGArtalent", t.toString());
                            Utils.showToast(mActivity, "onFailure : " + t.toString());
                        }
                    });
        }

    }

}
