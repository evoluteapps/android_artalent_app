package com.artalent.ui.activity

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.artalent.R
import com.artalent.ui.adapter.FilterListener
import com.artalent.ui.adapter.FilterViewAdapter
import com.artalent.ui.utils.BitmapUtilsCropper
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.fenchtose.nocropper.CropperView
import ja.burhanrashid52.photoeditor.OnSaveBitmap
import ja.burhanrashid52.photoeditor.PhotoEditor
import ja.burhanrashid52.photoeditor.PhotoEditorView
import ja.burhanrashid52.photoeditor.PhotoFilter
import kotlinx.android.synthetic.main.activity_post_crop_and_filter.*
import java.io.File
import java.io.FileOutputStream


private const val TAG = "PostCropAndFilterActivity"

class PostCropAndFilterActivity : BaseActivityOther(), FilterListener {

    private var isCropperRunning = true
    private lateinit var cropperView: CropperView
    private lateinit var snapBtn: ImageView
    private lateinit var rotateBtn: ImageView
    private lateinit var cropperFrameLayout: FrameLayout
    var mPhotoEditor: PhotoEditor? = null
    private var mPhotoEditorView: PhotoEditorView? = null
    private var mRvFilters: RecyclerView? = null
    private val mFilterViewAdapter: FilterViewAdapter = FilterViewAdapter(this)

    private var rotationCount = 0
    private var isSnappedToCenter = false

    private var imagePath: String? = null
    var originalImage: Bitmap? = null

    var appliedFilter: PhotoFilter = PhotoFilter.NONE

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_post_crop_and_filter)

        if (intent != null) {
            appliedFilter =
                PhotoFilter.valueOf(intent.getStringExtra("appliedFilterForAnotherEdit"))
        }
        init()
    }

    private fun init() {

        val intent = intent;
        if (intent != null) {
            imagePath = intent.getStringExtra("imagePath")
            setSelectedImage()
        }

        setupToolbarBack(
            "Crop",
            R.drawable.ic_arrow_back_white_24dp,
            R.color.white,
            R.color.primary
        )

        cropperView = findViewById(R.id.crop_view)
        cropperView.isMakeSquare = false
        snapBtn = findViewById(R.id.snap_btn)
        rotateBtn = findViewById(R.id.rotate_btn)
        cropperFrameLayout = findViewById(R.id.cropper_frame_layout)

        mPhotoEditorView = findViewById(R.id.photoEditorView)
        mRvFilters = findViewById(R.id.rvFilterView)
    }

    private fun setSelectedImage() {

        Glide.with(this)
            .asBitmap()
            .load(imagePath)
            .into(object : CustomTarget<Bitmap?>() {
                override fun onResourceReady(
                    resource: Bitmap,
                    transition: Transition<in Bitmap?>?
                ) {
                    originalImage = resource
                    cropperView.setImageBitmap(resource)
                }

                override fun onLoadCleared(placeholder: Drawable?) {

                }
            })
    }

    fun onSnapImageClick(view: View) {
        if (isSnappedToCenter) {
            cropperView.cropToCenter()
        } else {
            cropperView.fitToCenter()
        }

        isSnappedToCenter = !isSnappedToCenter
    }

    fun onRotateImageClick(view: View) {
        if (originalImage == null) {
            Log.e(TAG, "bitmap is not loaded yet...")
            return
        }
        originalImage = BitmapUtilsCropper.rotateBitmap(originalImage, 90f)
        cropperView.setImageBitmap(originalImage)
        rotationCount++
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        if (isCropperRunning)
            menuInflater.inflate(R.menu.filter_menu, menu)
        else
            menuInflater.inflate(R.menu.save_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_filter -> {
                Log.d(TAG, "onOptionsItemSelected: ${item.itemId}")
                isCropperRunning = false
                cropperFrameLayout.visibility = View.GONE
                invalidateOptionsMenu()
                setupToolbarBack(
                    "Filter",
                    R.drawable.ic_arrow_back_white_24dp,
                    R.color.white,
                    R.color.primary
                )
                loadFilterContent()
            }
            R.id.action_save -> {
                Log.d(TAG, "onOptionsItemSelected: ${item.itemId}")

                mPhotoEditor?.saveAsBitmap(object : OnSaveBitmap {
                    override fun onBitmapReady(saveBitmap: Bitmap?) {
                        Log.d(TAG, "onBitmapReady: called")
                        saveBitmap?.let {
                            saveImage(it)
                        }
                    }

                    override fun onFailure(e: java.lang.Exception?) {
                        Log.d(TAG, "onFailure: called ${e?.message}")
                    }

                })
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun loadFilterContent() {
        rvFilterView.visibility = View.VISIBLE
        photoEditorView.visibility = View.VISIBLE

        mPhotoEditor = PhotoEditor.Builder(this, mPhotoEditorView).build()

        photoEditorView.source.setImageBitmap(cropperView.croppedBitmap.bitmap)

        photoEditorView.source.scaleType = ImageView.ScaleType.CENTER_CROP

        Log.d(TAG, "loadFilterContent: ${cropperView.croppedBitmap.bitmap.toString()}")

        mFilterViewAdapter.getBitmap(cropperView.croppedBitmap.bitmap)
        mRvFilters?.recycledViewPool?.setMaxRecycledViews(0, 0)

        mRvFilters?.adapter = mFilterViewAdapter

        mPhotoEditor?.setFilterEffect(appliedFilter)
    }

    override fun onFilterSelected(photoFilter: PhotoFilter?) {
        if (photoFilter != null) {
            appliedFilter = photoFilter
            mPhotoEditor!!.setFilterEffect(photoFilter)
        }
    }

    private fun saveImage(finalBitmap: Bitmap) {
        val root = Environment.getExternalStorageDirectory().absolutePath + "/ArtalentFilter"
        val myDir = File(root)
        myDir.mkdirs()
        val fname = "Artalent_" + System.currentTimeMillis() + ".jpg"
        val file = File(myDir, fname)
        if (file.exists()) file.delete()
        try {
            val out = FileOutputStream(file)
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out)
            out.flush()
            out.close()
            val resultIntent = Intent()
            resultIntent.putExtra("imagePath", "$root/$fname")
            resultIntent.putExtra("appliedFilter", appliedFilter.name)
            setResult(RESULT_OK, resultIntent)
            finish()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}