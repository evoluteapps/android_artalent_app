package com.artalent.ui.activity;

import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.artalent.R;
import com.artalent.ui.fragment.ProfileFragment;
import com.artalent.ui.model.UserObj;
import com.artalent.ui.utils.Constants;

/**
 * Created by Mahajan on 08-09-2019.
 */

public class OtherUserProfileActivity extends BaseActivityOther {
    ProfileFragment fragment = null;
    private UserObj selectedUserObj = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_other_user);
        fragment = new ProfileFragment();
        setFragment();
    }

    private void init1() {
        selectedUserObj = (UserObj) getIntent().getSerializableExtra(Constants.DATA);
        if (selectedUserObj != null) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    fragment.setUserData(selectedUserObj);
                }
            }, 100);
        }
    }

    protected void setFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction =
                fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, fragment);
        fragmentTransaction.commit();
        init1();
    }
}
