package com.artalent.ui.activity;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.LinearLayout;

import com.artalent.R;

import butterknife.BindView;
import butterknife.OnClick;

public class AboutUsActivity extends BaseActivityOther {

    @BindView(R.id.llMail)
    LinearLayout llMail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about_us);
        setupToolbarBack(getString(R.string.about_us));
    }

    @OnClick(R.id.llMail)
    public void onOpenMailClick() {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:akshay@gmail.com")); // only email apps should handle this
        intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    @OnClick(R.id.llFB)
    public void onOpenFBClick() {
        startActivity(getOpenFacebookIntent());
    }

    public Intent getOpenFacebookIntent() {
        try {
            getPackageManager().getPackageInfo("com.facebook.katana", 0);
            return new Intent(Intent.ACTION_VIEW, Uri.parse("fb://page/1650883534968511"));
        } catch (Exception e) {
            return new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/Super-Kirana-Thikri-1650883534968511/"));
        }
    }

    @OnClick(R.id.llInsta)
    public void onOpenInstaClick() {
        Uri uri = Uri.parse("https://instagram.com/fusion_of_talents?igshid=vdzbxvjeziba");
        Intent likeIng = new Intent(Intent.ACTION_VIEW, uri);

        likeIng.setPackage("com.instagram.android");

        try {
            startActivity(likeIng);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://instagram.com/fusion_of_talents?igshid=vdzbxvjeziba")));
        }
    }

    @OnClick(R.id.llYouTube)
    public void onOpenYouTubeClick() {
        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:VlzVXW65-jo"));
        startActivity(i);
    }

    @OnClick(R.id.llWebSite)
    public void onOpenWebClick() {
        String url = "http://www.google.com";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

    @OnClick(R.id.llPlayStore)
    public void onOpenPlayStoreClick() {
        String playStoreUrl =
                "https://play.google.com/store/apps/details?id=com.artalent&hl=en_US";
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        Intent sendIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(playStoreUrl));
        sendIntent.setPackage("com.android.vending");
        startActivity(sendIntent);
    }
}
