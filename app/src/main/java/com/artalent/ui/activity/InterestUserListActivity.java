package com.artalent.ui.activity;

import android.os.Bundle;
import android.util.Log;

import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.artalent.ArtalentApplication;
import com.artalent.R;
import com.artalent.Utils;
import com.artalent.ui.adapter.InterestUserListAdapter;
import com.artalent.ui.model.InterestObj;
import com.artalent.ui.model.InterestResponse;
import com.artalent.ui.model.UserObj;
import com.artalent.ui.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InterestUserListActivity extends BaseActivityOther {

    private static final String TAG = "InterestUserListActivity";
    RecyclerView recyclerView;
    private List<InterestObj> interestList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_interest_user);
        interestList = (ArrayList<InterestObj>) getIntent().getSerializableExtra(Constants.INTEREST_LIST);
        setupToolbarBack(getString(R.string.user_interest), R.drawable.ic_arrow_back_white_24dp, R.color.primary, R.color.black);
        recyclerView = findViewById(R.id.recycler_view);
        //callInterestApi();
        setAdapter();
    }

    protected void callInterestApi() {
        if (Utils.isInternetAvailable(mActivity)) {
            Call<InterestResponse> call = ArtalentApplication.apiService.getInterest();
            call.enqueue(new Callback<InterestResponse>() {
                @Override
                public void onResponse(Call<InterestResponse> call, Response<InterestResponse> response) {
                    int statusCode = response.code();
                    if (response.body().getObj() != null && response.body().getObj().size() > 0) {
                        interestList.clear();
                        interestList = response.body().getObj();
                        UserObj userObj = ArtalentApplication.prefManager.getUserObj();
                        if (userObj != null && userObj.getInterest() != null && userObj.getInterest().size() > 0) {
                            for (InterestObj interestListObj : interestList) {
                                for (InterestObj interestListObj1 : userObj.getInterest()) {
                                    if (interestListObj1.getInterestMasterId() == interestListObj.getInterestMasterId()) {
                                        interestListObj.setCheck(true);
                                    }
                                }
                            }
                        }
                        setAdapter();
                    }
                }

                @Override
                public void onFailure(Call<InterestResponse> call, Throwable t) {
                    // Log error here since request failed
                    Log.e("TAG", t.toString());
                }
            });
        }
    }

    private void setAdapter() {
        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        InterestUserListAdapter interestListAdapter = new InterestUserListAdapter(interestList, Constants.INTEREST_SHOW_ALL_LIST_NAME, mActivity);
        interestList.forEach(it ->
                Log.d(TAG, "setAdapter: " + it.toString())
        );
        recyclerView.setAdapter(interestListAdapter);
    }
}
