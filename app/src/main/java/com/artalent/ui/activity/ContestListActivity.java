package com.artalent.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.artalent.ArtalentApplication;
import com.artalent.R;
import com.artalent.Utils;
import com.artalent.ui.adapter.ContestListAdapter;
import com.artalent.ui.model.ContestObj;
import com.artalent.ui.model.ContestRegistrationObj;
import com.artalent.ui.model.ContestResponse;
import com.artalent.ui.model.UserObj;
import com.artalent.ui.utils.Constants;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ContestListActivity extends BaseActivityOther {

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.pullToRefresh)
    SwipeRefreshLayout pullToRefresh;

    List<ContestObj> contestObjs = new ArrayList<>();

    public static void startActivity(Activity mActivity) {
        Intent intent = new Intent(mActivity, ContestListActivity.class);
        mActivity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contest_list);
        setupToolbarBack(getString(R.string.contests), R.drawable.ic_arrow_back_white_24dp, R.color.primary, R.color.black);
        callContestRegistrationApi();
        pullToRefresh.setOnRefreshListener(this::callContestRegistrationApi);
    }

    protected void callContestRegistrationApi() {
        if (Utils.isInternetAvailable(Objects.requireNonNull(mActivity))) {
            Call<ContestResponse> call = ArtalentApplication.apiService.getContestRegistration(ArtalentApplication.prefManager.getUserObj().getId());
            call.enqueue(new Callback<ContestResponse>() {
                @Override
                public void onResponse(Call<ContestResponse> call, Response<ContestResponse> response) {
                    int statusCode = response.code();
                    contestObjs.clear();
                    if (response.body() != null && response.body().getObj() != null && response.body().getObj().size() > 0) {
                        for (ContestObj contestObj : response.body().getObj()) {
                            boolean isAvailable = false;
                            for (ContestRegistrationObj contestRegistrationObj : contestObj.getRegistrationDetails()) {
                                if (ArtalentApplication.prefManager.getUserObj() != null && contestRegistrationObj != null && contestRegistrationObj.getUser() != null) {
                                    UserObj userObj = ArtalentApplication.prefManager.getUserObj();
                                    if (contestRegistrationObj.getUser().getId() == userObj.getId()) {
                                        isAvailable = true;
                                    }
                                }
                            }
                            if (isAvailable) {
                                contestObjs.add(contestObj);
                            }
                        }

                        for (ContestObj contestObj : response.body().getObj()) {
                            boolean isAvailable = false;
                            for (ContestRegistrationObj contestRegistrationObj : contestObj.getRegistrationDetails()) {
                                if (ArtalentApplication.prefManager.getUserObj() != null && contestRegistrationObj != null && contestRegistrationObj.getUser() != null) {
                                    UserObj userObj = ArtalentApplication.prefManager.getUserObj();
                                    if (contestRegistrationObj.getUser().getId() == userObj.getId()) {
                                        isAvailable = true;
                                    }
                                }
                            }
                            if (!isAvailable) {
                                contestObjs.add(contestObj);
                            }
                        }


                        // contestObjs = response.body().getObj();
                    }
                    pullToRefresh.setRefreshing(false);
                    setAdapter();
                }

                @Override
                public void onFailure(Call<ContestResponse> call, Throwable t) {
                    // Log error here since request failed
                    pullToRefresh.setRefreshing(false);
                }
            });
        } else {
            pullToRefresh.setRefreshing(false);
            Intent intent = new Intent(mActivity, NoInternetActivity.class);
            startActivityForResult(intent, Constants.NO_INTERNET);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.NO_INTERNET) {
            if (resultCode == Activity.RESULT_OK) {
                callContestRegistrationApi();
            }
        }
    }

    private void setAdapter() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        ContestListAdapter contestListAdapter = new ContestListAdapter(mActivity, contestObjs);
        recyclerView.setAdapter(contestListAdapter);
    }

}
