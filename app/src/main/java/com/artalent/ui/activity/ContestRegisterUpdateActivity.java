package com.artalent.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.artalent.ArtalentApplication;
import com.artalent.R;
import com.artalent.Utils;
import com.artalent.ui.model.BaseResponse;
import com.artalent.ui.model.Country;
import com.artalent.ui.model.DistrictsObj;
import com.artalent.ui.model.InterestObj;
import com.artalent.ui.model.StateObj;
import com.artalent.ui.model.UserObj;
import com.artalent.ui.model.UserResponse;
import com.artalent.ui.model.request.ContestRegistrationReq;
import com.artalent.ui.model.room.ContestVideoDraft;
import com.artalent.ui.utils.Constants;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ContestRegisterUpdateActivity extends BaseActivityOther implements View.OnClickListener, PaymentResultListener {

    @BindView(R.id.tvInterest)
    TextView tvInterest;

    @BindView(R.id.etTitle)
    EditText etTitle;

    @BindView(R.id.et_contest_description)
    EditText et_contest_description;

    @BindView(R.id.edtCountry)
    EditText edtCountry;

    @BindView(R.id.edtState)
    EditText edtState;

    @BindView(R.id.edtDistrict)
    EditText edtDistrict;

    @BindView(R.id.btnLogin)
    Button btnLogin;

    @BindView(R.id.btnRazorpay)
    Button btnRazorpay;

    @BindView(R.id.referral_debit_parent)
    RelativeLayout referralDebitParent;

    @BindView(R.id.wallet_debit_parent)
    RelativeLayout walletDebitParent;

    @BindView(R.id.contest_end_date)
    TextView contestEndDate;

    @BindView(R.id.referral_debit_checkbox)
    CheckBox referralDebitCheckBox;

    @BindView(R.id.wallet_debit_checkbox)
    CheckBox walletDebitCheckBox;

    @BindView(R.id.interest_image)
    ImageView ivInterest;

    private InterestObj selectedInterestObj = null;

    private Country selectedCountry = null;
    private StateObj selectedStateObj = null;
    private DistrictsObj selectedDistrictsObj = null;
    public static final int REQUEST_COUNTRY = 200;
    public static final int REQUEST_STATE = 300;
    public static final int REQUEST_DISTRICT = 400;
    private int selectedInterestMasterId = 0;
    private boolean isDone = false;
    private double dRegistrationAmount = 0.0;
    private double dWalletBalanceAmount = 0.0;
    private double dReferralBalanceAmount = 0.0;

    private double referralDebitAmt = 0.0;
    private double walletDebitAmt = 0.0;

    private String transactionId = "";

    private RequestOptions options;

    private boolean isFromDraftActivity = false;
    private ContestVideoDraft contestVideoDraft = null;
    private static final String TAG = "ContestRegisterUpdateActivity";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_register_contest);

        if (getIntent().getExtras() != null) {
            try {
                isFromDraftActivity = getIntent().getBooleanExtra("isFromDraft", false);
                contestVideoDraft = (ContestVideoDraft) getIntent().getSerializableExtra("draft");

                Log.d(TAG, "onCreate: isFromDraftActivity " + isFromDraftActivity);
                Log.d(TAG, "onCreate: isFromDraftActivity " + contestVideoDraft.toString());
            } catch (Exception e) {
                isFromDraftActivity = false;
                contestVideoDraft = null;
            }
        }


        MainActivity.isContestReg = true;
        options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.interest_categories)
                .error(R.drawable.interest_categories);
        Checkout.preload(getApplicationContext());
        setupToolbarBack(getString(R.string.contest_registration), R.drawable.ic_arrow_back_white_24dp, R.color.primary, R.color.black);
        init1();
        setDataView();
        getContestFeeAmount();


        referralDebitCheckBox.setOnCheckedChangeListener(
                new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        /*if (isChecked) {
                            if (walletDebitCheckBox.isChecked()) {
                                btnLogin.setText(getResources().getString(R.string.pay) + " " + (dRegistrationAmount - referralDebitAmt - walletDebitAmt) + " INR");
                            } else {
                                btnLogin.setText(getResources().getString(R.string.pay) + " " + (dRegistrationAmount - referralDebitAmt) + " INR");

                            }
                        } else {
                            if (walletDebitCheckBox.isChecked()) {
                                btnLogin.setText(getResources().getString(R.string.pay) + " " + (dRegistrationAmount - walletDebitAmt) + " INR");
                            } else {
                                btnLogin.setText(getResources().getString(R.string.pay) + " " + (dRegistrationAmount) + " INR");
                            }
                        }*/
                        double payAmount = getPayAmount();
                    }
                }
        );
        walletDebitCheckBox.setOnCheckedChangeListener(
                new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        double flag = getPayAmount();

                    }
                }
        );
    }

    private double getPayAmount() {
        double amount = 0.0;
        if (walletDebitCheckBox.isChecked()) {
            if (referralDebitCheckBox.isChecked()) {
                amount = (dRegistrationAmount - referralDebitAmt - walletDebitAmt);
            } else {
                amount = (dRegistrationAmount - walletDebitAmt);
            }
        } else {
            if (referralDebitCheckBox.isChecked()) {
                amount = (dRegistrationAmount - referralDebitAmt);
            } else {
                amount = (dRegistrationAmount);

            }
        }
        btnLogin.setText(getResources().getString(R.string.pay) + " " + (amount) + " INR");
        return amount;
    }

    private void getContestFeeAmount() {
        referralDebitParent.setVisibility(View.GONE);
        walletDebitParent.setVisibility(View.GONE);
        if (Utils.isInternetAvailable(mActivity)) {
            ArtalentApplication.apiService.getReferalAmount(ArtalentApplication.prefManager.getUserObj().getId()).enqueue(new Callback<UserResponse>() {
                @Override
                public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                    int statusCode = response.code();
                    if (response.body() != null && response.body().getObj() != null) {
                        UserObj userObj = response.body().getObj();
                        if (userObj.getAmount() > 0.0) {
                            dRegistrationAmount = userObj.getAmount();
                            dReferralBalanceAmount = userObj.getReferralBalance();
                            dWalletBalanceAmount = userObj.getWalletBalance();
                            double registrationAmount20Percent = (dRegistrationAmount * 20) / 100;
                            if (dReferralBalanceAmount >= registrationAmount20Percent) {
                                referralDebitAmt = registrationAmount20Percent;
                            }

                            if (dWalletBalanceAmount > 0) {
                                if (dWalletBalanceAmount >= (dRegistrationAmount - referralDebitAmt)) {
                                    walletDebitAmt = dRegistrationAmount - referralDebitAmt;
                                } else {
                                    walletDebitAmt = dWalletBalanceAmount;
                                }
                            }

                            btnLogin.setText(getResources().getString(R.string.pay) + " " + (dRegistrationAmount - referralDebitAmt - walletDebitAmt) + " INR");
                        } else {
                            btnLogin.setText(mActivity.getString(R.string.free_registration));
                            dRegistrationAmount = 0.0;
                        }

                        if (referralDebitAmt > 0) {
                            referralDebitCheckBox.setText(referralDebitAmt + " INR");
                            referralDebitParent.setVisibility(View.VISIBLE);
                        } else {
                            referralDebitParent.setVisibility(View.GONE);
                        }

                        if (walletDebitAmt > 0) {
                            walletDebitCheckBox.setText(walletDebitAmt + " INR");
                            walletDebitParent.setVisibility(View.VISIBLE);
                        } else {
                            walletDebitParent.setVisibility(View.GONE);
                        }


                    } else {
                        finish();
                    }
                }

                @Override
                public void onFailure(Call<UserResponse> call, Throwable t) {
                    // Log error here since request failed
                    Log.e("TAG", t.toString());
                    referralDebitParent.setVisibility(View.GONE);
                    walletDebitParent.setVisibility(View.GONE);
                    walletDebitAmt = 0.0;
                    referralDebitAmt = 0.0;
                }
            });
        } else {
            Intent intent = new Intent(getApplicationContext(), NoInternetActivity.class);
            startActivityForResult(intent, Constants.NO_INTERNET);
            finish();
        }
    }


    private void setDataView() {
        edtCountry.setOnClickListener(this);
        edtState.setOnClickListener(this);
        edtDistrict.setOnClickListener(this);
        btnLogin.setOnClickListener(this);
        btnRazorpay.setOnClickListener(this);
        contestEndDate.setText(getUpcomingSaturdaysDate());
        if (selectedInterestObj != null) {
            if (!TextUtils.isEmpty(selectedInterestObj.getInterest())) {
                tvInterest.setText(selectedInterestObj.getInterest());
                etTitle.setText(selectedInterestObj.getInterest());
                et_contest_description.setText(selectedInterestObj.getInterest());
            }
        }
        UserObj userObj = ArtalentApplication.prefManager.getUserObj();
        if (userObj.getCountry() != null && userObj.getCountry().getCountryId() > 0) {
            selectedCountry = userObj.getCountry();
            edtCountry.setText(selectedCountry.getCountryName());
        }
        if (userObj.getState() != null && userObj.getState().getStateId() > 0) {
            selectedStateObj = userObj.getState();
            edtState.setText(selectedStateObj.getState());
        }
        if (userObj.getDistrict() != null && userObj.getDistrict().getDistrictId() > 0) {
            selectedDistrictsObj = userObj.getDistrict();
            edtDistrict.setText(selectedDistrictsObj.getDistrict());
        }
    }

    private void init1() {
        selectedInterestObj = (InterestObj) getIntent().getSerializableExtra(Constants.INTEREST_OBJ);
        if (selectedInterestObj != null && selectedInterestObj.getInterestMasterId() > 0) {
            selectedInterestMasterId = selectedInterestObj.getInterestMasterId();
            Utils.setImageRounded(mActivity, selectedInterestObj.getBaner(), ivInterest);
        }
    }

    public String getUpcomingSaturdaysDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE yyyy-MMM-dd");

        Calendar c = Calendar.getInstance();
        c.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.add(Calendar.DATE, 7);

        Date currentTime = c.getTime();
        return dateFormat.format(currentTime);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
           /* case R.id.btnRazorpay:
                startPayment();
                break;*/
            case R.id.btnLogin:
                if (isValidate()) {
                    if (!isDone) {

                        isDone = true;
                        btnLogin.setAlpha(0.5f);
                        callContestRegisterCheckApi();

                       /* if (dPaymentAmount < 1) {
                            callContestRegisterApi();//1
                        } else if (dRegistrationAmount > checkZeroValue) {

                        } else {
                            callContestRegisterApi();//2
                        }*/
                    }
                }
                break;
            case R.id.edtCountry:
                SelectCountryActivity.startActivity(mActivity, selectedCountry, REQUEST_COUNTRY);
                break;
            case R.id.edtState:
                if (selectedCountry != null && selectedCountry.getCountryId() > 0) {
                    SelectStateActivity.startActivity(mActivity, selectedStateObj, selectedCountry.getCountryId(), REQUEST_STATE);
                } else {
                    Utils.showToast(mActivity, mActivity.getString(R.string.select_country_first));
                }
                break;
            case R.id.edtDistrict:
                if (selectedStateObj != null && selectedStateObj.getStateId() > 0) {
                    SelectDistrictsActivity.startActivity(mActivity, selectedDistrictsObj, selectedStateObj.getStateId(), REQUEST_DISTRICT);
                } else {
                    Utils.showToast(mActivity, mActivity.getString(R.string.select_state_first));
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_COUNTRY) {
            if (resultCode == Activity.RESULT_OK) {
                selectedCountry = (Country) data.getSerializableExtra(Constants.COUNTRY_OBJ);
                if (selectedCountry != null && !TextUtils.isEmpty(selectedCountry.getCountryName())) {
                    edtCountry.setText(selectedCountry.getCountryName());
                    selectedStateObj = null;
                    if (selectedStateObj != null && !TextUtils.isEmpty(selectedStateObj.getState())) {
                        edtState.setText(selectedStateObj.getState());
                    } else {
                        edtState.setText("");
                    }

                    selectedDistrictsObj = null;
                    if (selectedDistrictsObj != null && !TextUtils.isEmpty(selectedDistrictsObj.getDistrict())) {
                        edtDistrict.setText(selectedDistrictsObj.getDistrict());
                    } else {
                        edtDistrict.setText("");
                    }
                }
            }
        } else if (requestCode == REQUEST_STATE) {
            if (resultCode == Activity.RESULT_OK) {
                selectedStateObj = (StateObj) data.getSerializableExtra(Constants.STATE_OBJ);
                if (selectedStateObj != null && !TextUtils.isEmpty(selectedStateObj.getState())) {
                    edtState.setText(selectedStateObj.getState());
                }
                selectedDistrictsObj = null;
                if (selectedDistrictsObj != null && !TextUtils.isEmpty(selectedDistrictsObj.getDistrict())) {
                    edtDistrict.setText(selectedDistrictsObj.getDistrict());
                } else {
                    edtDistrict.setText("");
                }
            }
        } else if (requestCode == REQUEST_DISTRICT) {
            if (resultCode == Activity.RESULT_OK) {
                selectedDistrictsObj = (DistrictsObj) data.getSerializableExtra(Constants.STATE_OBJ);
                if (selectedDistrictsObj != null && !TextUtils.isEmpty(selectedDistrictsObj.getDistrict())) {
                    edtDistrict.setText(selectedDistrictsObj.getDistrict());
                }
            }
        }
    }

    private boolean isValidate() {
        if (!TextUtils.isEmpty(edtCountry.getText().toString().trim())) {
            if (!TextUtils.isEmpty(edtState.getText().toString().trim())) {
                if (!TextUtils.isEmpty(edtDistrict.getText().toString().trim())) {
                    return true;
                } else {
                    Utils.showToast(mActivity, "Please select districts");
                    return false;
                }
            } else {
                Utils.showToast(mActivity, "Please select state");
                return false;
            }
        } else {
            Utils.showToast(mActivity, "Please select country");
            return false;
        }
    }

    private void callContestRegisterApi() {
        if (Utils.isInternetAvailable(mActivity)) {
            showProgress();
            ArtalentApplication.apiService.contestRegistration(ArtalentApplication.prefManager.getUserObj().getId(), fillData1())
                    .enqueue(new Callback<BaseResponse>() {
                        @Override
                        public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                            int statusCode = response.code();
                            if (response.body() != null) {
                                Utils.showToast(mActivity, response.body().getMessage());
                                if (statusCode == 202) {
                                    new Handler().postDelayed(() -> {
                                        hideProgress();
                                        isDone = false;
                                        btnLogin.setAlpha(1f);
                                        // Changing flow if you have draft obj
                                        if (isFromDraftActivity && contestVideoDraft != null) {
                                            Intent intent = new Intent(mActivity, DraftVideoPlayActivity.class);
                                            intent.putExtra("DraftObject", contestVideoDraft);
                                            intent.putExtra("isFromRegisterActivity", true);
                                            startActivity(intent);
                                            finish();
                                        } else {
                                            Intent output = new Intent();
                                            setResult(RESULT_OK, output);
                                            finish();
                                        }
                                    }, 2000);
                                } else {
                                    hideProgress();
                                    isDone = false;
                                    btnLogin.setAlpha(1f);
                                }
                            } else {
                                Log.e("registration_message", response.message());
                                hideProgress();
                                isDone = false;
                                btnLogin.setAlpha(1f);
                                Utils.showToast(mActivity, mActivity.getString(R.string.please_try_again));
                            }
                        }

                        @Override
                        public void onFailure(Call<BaseResponse> call, Throwable t) {
                            // Log error here since request failed
                            Log.e("TAGArtalent", t.toString());
                            Utils.showToast(mActivity, "onFailure : " + t.toString());
                            isDone = false;
                            btnLogin.setAlpha(1f);
                        }
                    });
        } else {
            isDone = false;
            btnLogin.setAlpha(1f);
            Utils.showToast(mActivity, getResources().getString(R.string.please_check_your_internet));
        }
    }

    private void callContestRegisterCheckApi() {
        if (Utils.isInternetAvailable(mActivity)) {
            showProgress();
            ArtalentApplication.apiService.contestRegistration(ArtalentApplication.prefManager.getUserObj().getId(), 1, fillData1())
                    .enqueue(new Callback<BaseResponse>() {
                        @Override
                        public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                            int statusCode = response.code();
                            if (response.body() != null) {
                                hideProgress();
                                isDone = false;
                                btnLogin.setAlpha(1f);
                                if (statusCode == 200) {
                                    double payAmount = getPayAmount();
                                    if (payAmount == 0) {
                                        callContestRegisterApi();//1
                                    } else {
                                        startPayment();
                                    }
                                    //
                                }
                            } else {
                                hideProgress();
                                isDone = false;
                                btnLogin.setAlpha(1f);
                                Utils.showToast(mActivity, mActivity.getString(R.string.please_try_again));
                            }
                        }

                        @Override
                        public void onFailure(Call<BaseResponse> call, Throwable t) {
                            // Log error here since request failed
                            Log.e("TAGArtalent", t.toString());
                            Utils.showToast(mActivity, "onFailure : " + t.toString());
                            isDone = false;
                            btnLogin.setAlpha(1f);
                        }
                    });
        } else {
            isDone = false;
            btnLogin.setAlpha(1f);
            Utils.showToast(mActivity, getResources().getString(R.string.please_check_your_internet));
        }
    }


    /*
    payment getway
     */
    public void startPayment() {
        /*
          You need to pass current activity in order to let Razorpay create CheckoutActivity
         */
        final Activity activity = this;

        final Checkout co = new Checkout();
        try {
            JSONObject options = new JSONObject();
            options.put("name", getString(R.string.app_name));
            options.put("description", getString(R.string.app_tag_line));
            //You can omit the image option to fetch the image from dashboard
            options.put("image", "https://artalentbucket.s3.ap-south-1.amazonaws.com/1577275123128-final_logo.png");
            options.put("currency", "INR");
            double payAmount = getPayAmount();
            options.put("amount", "" + (payAmount * 100));

            JSONObject preFill = new JSONObject();
            if (ArtalentApplication.prefManager.getUserObj() != null && ArtalentApplication.prefManager.getUserObj().getMobile() != null && !TextUtils.isEmpty(ArtalentApplication.prefManager.getUserObj().getMobile())) {
                preFill.put("contact", ArtalentApplication.prefManager.getUserObj().getMobile());
            }

            // preFill.put("email", "test@razorpay.com");


            options.put("prefill", preFill);
            co.setFullScreenDisable(true);
            co.open(activity, options);

        } catch (Exception e) {
            Toast.makeText(activity, "Error in payment: " + e.getMessage(), Toast.LENGTH_SHORT)
                    .show();
            e.printStackTrace();
        }
    }

    /**
     * The name of the function has to be
     * onPaymentSuccess
     * Wrap your code in try catch, as shown, to ensure that this method runs correctly
     */
    @SuppressWarnings("unused")
    @Override
    public void onPaymentSuccess(String razorpayPaymentID) {
        try {
            transactionId = razorpayPaymentID;
            callContestRegisterApi();//3
        } catch (Exception e) {
            Log.e("TAG", "Exception in onPaymentSuccess", e);
        }
    }

    /**
     * The name of the function has to be
     * onPaymentError
     * Wrap your code in try catch, as shown, to ensure that this method runs correctly
     */
    @SuppressWarnings("unused")
    @Override
    public void onPaymentError(int code, String response) {
        try {
            Toast.makeText(this, "Payment failed: " + code + " " + response, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Log.e("TAG", "Exception in onPaymentError", e);
        }
    }

    private ContestRegistrationReq fillData1() {
        ContestRegistrationReq createReq = new ContestRegistrationReq();
        createReq.setDistrictId("" + selectedDistrictsObj.getDistrictId());
        createReq.setInterestId("" + selectedInterestMasterId);
        createReq.setTransactionId(transactionId);

        if (walletDebitCheckBox.isChecked()) {
            createReq.setWalletDebit("" + walletDebitAmt);
        } else {
            createReq.setWalletDebit("0.0");
        }

        if (referralDebitCheckBox.isChecked()) {
            createReq.setReferalDebit("" + referralDebitAmt);
        } else {
            createReq.setReferalDebit("0.0");
        }
        if (dRegistrationAmount > 0) {
            createReq.setRegistrationAmount("" + dRegistrationAmount);
        } else {
            createReq.setRegistrationAmount("10.0");
        }
        Gson gson = new Gson();
        String jsonStr = gson.toJson(createReq);
        Log.e("createReqDaw", jsonStr);
        return createReq;
    }
}
