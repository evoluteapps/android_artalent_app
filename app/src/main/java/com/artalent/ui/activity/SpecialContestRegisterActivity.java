package com.artalent.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.artalent.ArtalentApplication;
import com.artalent.R;
import com.artalent.Utils;
import com.artalent.ui.model.BaseResponse;
import com.artalent.ui.model.Country;
import com.artalent.ui.model.DistrictsObj;
import com.artalent.ui.model.InterestObj;
import com.artalent.ui.model.SpecialContestObj;
import com.artalent.ui.model.StateObj;
import com.artalent.ui.model.UserObj;
import com.artalent.ui.model.UserResponse;
import com.artalent.ui.model.request.ContestRegistrationReq;
import com.artalent.ui.utils.Constants;
import com.artalent.ui.view.RoundedImageView;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SpecialContestRegisterActivity extends BaseActivityOther implements View.OnClickListener, PaymentResultListener {


    @BindView(R.id.tvInterest)
    TextView tvInterest;

    @BindView(R.id.contest_start_date)
    TextView tvStartDate;

    @BindView(R.id.contest_end_date)
    TextView tvEndDate;

    @BindView(R.id.special_contest_banner_image)
    RoundedImageView specialContestBanner;

    @BindView(R.id.etTitle)
    EditText etTitle;

    @BindView(R.id.et_contest_description)
    EditText et_contest_description;

    @BindView(R.id.edtCountry)
    EditText edtCountry;

    @BindView(R.id.edtState)
    EditText edtState;

    @BindView(R.id.edtDistrict)
    EditText edtDistrict;

    @BindView(R.id.btnLogin)
    Button btnLogin;

    @BindView(R.id.btnRazorpay)
    Button btnRazorpay;

    @BindView(R.id.referral_debit_parent)
    RelativeLayout referralDebitParent;

    @BindView(R.id.wallet_debit_parent)
    RelativeLayout walletDebitParent;

    @BindView(R.id.referral_debit_checkbox)
    CheckBox referralDebitCheckBox;

    @BindView(R.id.wallet_debit_checkbox)
    CheckBox walletDebitCheckBox;

    private SpecialContestObj specialContestObj = null;

    private Country selectedCountry = null;
    private StateObj selectedStateObj = null;
    private DistrictsObj selectedDistrictsObj = null;
    private InterestObj interestObj = null;
    private boolean isDone = false;
    private double dRegistrationAmount = 0.0;
    private double dWalletBalanceAmount = 0.0;
    private double dWalletDebitAmount = 0.0;
    private double dReferralDebitAMount = 0.0;
    private double dReferralBalanceAmount = 0.0;
    private final double checkZeroValue = 0.0;
    private double dPaymentAmount = 0.0;
    private String transactionId = "";
    private int specialContestId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_special_contest_register);
        setupToolbarSpecialBack(getString(R.string.special_contest_registration), R.drawable.ic_arrow_back_white_24dp, R.color.black);
        init1();
        setDataView();
        //getContestFeeAmount();
        if (specialContestObj != null && specialContestObj.getRegFee() != null && !TextUtils.isEmpty(specialContestObj.getRegFee())) {
            dRegistrationAmount = 0.0;
            try {
                dRegistrationAmount = Double.parseDouble(specialContestObj.getRegFee());
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
            if (dRegistrationAmount > 0) {
                btnLogin.setText(getResources().getString(R.string.pay) + " ₹ " + dRegistrationAmount);
            } else {
                btnLogin.setText(mActivity.getString(R.string.free_registration));
                dRegistrationAmount = 0.0;
            }
        } else {
            btnLogin.setText(mActivity.getString(R.string.free_registration));
            dRegistrationAmount = 0.0;
        }

    }

    private void init1() {

        specialContestObj = (SpecialContestObj) getIntent().getSerializableExtra("contest_item");
        interestObj = specialContestObj.getInterestObj();
        specialContestId = specialContestObj.getAdminContestId();

    }

    private void getContestFeeAmount() {
        if (Utils.isInternetAvailable(mActivity)) {
            ArtalentApplication.apiService.getReferalAmount(ArtalentApplication.prefManager.getUserObj().getId()).enqueue(new Callback<UserResponse>() {
                @Override
                public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                    if (response.body() != null && response.body().getObj() != null) {
                        UserObj userObj = response.body().getObj();
                        if (userObj.getAmount() > 0.0) {
                            dRegistrationAmount = userObj.getAmount();
                            dReferralBalanceAmount = userObj.getReferralBalance();
                            dWalletBalanceAmount = userObj.getWalletBalance();

                            // for Calculating 20% of Registration Amount
                            double registrationAmount20Percent = (dRegistrationAmount * 20) / 100;
                            if (dReferralBalanceAmount > checkZeroValue && registrationAmount20Percent > checkZeroValue) {
                                String referralDebitAMount;
                                if (dReferralBalanceAmount >= registrationAmount20Percent) {
                                    dPaymentAmount = dRegistrationAmount - registrationAmount20Percent;
                                    dReferralDebitAMount = registrationAmount20Percent;
                                    referralDebitAMount = registrationAmount20Percent + " INR";
                                } else {
                                    dPaymentAmount = dRegistrationAmount - dReferralBalanceAmount;
                                    dReferralDebitAMount = dReferralBalanceAmount;
                                    referralDebitAMount = dReferralBalanceAmount + " INR";
                                }
                                referralDebitCheckBox.setText(referralDebitAMount);
                                referralDebitParent.setVisibility(View.VISIBLE);
                            }
                            if (dWalletBalanceAmount > checkZeroValue) {
                                String walletDebitAMount;
                                if (dWalletBalanceAmount >= dRegistrationAmount) {
                                    dPaymentAmount = checkZeroValue;
                                    dWalletDebitAmount = dRegistrationAmount;
                                    walletDebitAMount = dRegistrationAmount + " INR";
                                } else {
                                    dPaymentAmount = dRegistrationAmount - dWalletBalanceAmount;
                                    dWalletDebitAmount = dWalletBalanceAmount;
                                    walletDebitAMount = dWalletBalanceAmount + " INR";
                                }
                                referralDebitCheckBox.setText(walletDebitAMount);
                                walletDebitParent.setVisibility(View.VISIBLE);
                            }
                            btnLogin.setText(getResources().getString(R.string.pay) + " " + dPaymentAmount + " INR");
                        } else {
                            btnLogin.setText(mActivity.getString(R.string.free_registration));
                            dRegistrationAmount = 0.0;
                        }
                    } else {
                        finish();
                    }
                }

                @Override
                public void onFailure(Call<UserResponse> call, Throwable t) {
                }
            });
        } else {
            Intent intent = new Intent(getApplicationContext(), NoInternetActivity.class);
            startActivityForResult(intent, Constants.NO_INTERNET);
            finish();
        }
    }

    private void setDataView() {

        btnLogin.setOnClickListener(this);
        btnRazorpay.setOnClickListener(this);

        if (specialContestObj != null) {
            if (!TextUtils.isEmpty(specialContestObj.getTittle())) {
                etTitle.setText(specialContestObj.getTittle());
            } else {
                etTitle.setText("");
            }
            if (!TextUtils.isEmpty(specialContestObj.getDescription())) {
                et_contest_description.setText(specialContestObj.getDescription());
            } else {
                et_contest_description.setText("");
            }

            if (specialContestObj.getCountry() != null && specialContestObj.getCountry().getCountryId() > 0) {
                selectedCountry = specialContestObj.getCountry();
                edtCountry.setText(selectedCountry.getCountryName());
            } else {
                edtCountry.setText("None");
            }
            if (specialContestObj.getState() != null && specialContestObj.getState().getStateId() > 0) {
                selectedStateObj = specialContestObj.getState();
                edtState.setText(selectedStateObj.getState());
            } else {
                edtState.setText("This is a Country level contest");
            }
            if (specialContestObj.getDistricts() != null && specialContestObj.getDistricts().getDistrictId() > 0) {
                selectedDistrictsObj = specialContestObj.getDistricts();
                edtDistrict.setText(selectedDistrictsObj.getDistrict());
            } else {
                if (specialContestObj.getState() != null) {
                    edtDistrict.setText("This is a State level contest");
                } else {
                    edtDistrict.setText("This is a Country level contest");
                }

            }

            Picasso.get()
                    .load(specialContestObj.getImage())
                    .placeholder(R.drawable.chat_background)
//                    .resize(0, 500)
//                    .centerInside()
                    .into(specialContestBanner);

            if (interestObj != null && interestObj.getInterestMasterId() > 0) {
                tvInterest.setText(interestObj.getInterest());
            }
            tvStartDate.setText(specialContestObj.getStartDate());
            tvEndDate.setText(specialContestObj.getEndDate());
        }


    }

    private ContestRegistrationReq fillDataSpecial() {
        ContestRegistrationReq createReq = new ContestRegistrationReq();
        createReq.setContestId("" + specialContestObj.getAdminContestId());
        createReq.setTransactionId(transactionId);
        createReq.setReferalDebit("" + dReferralDebitAMount);
        createReq.setWalletDebit("" + dWalletDebitAmount);
        createReq.setRegistrationAmount("" + dRegistrationAmount);
        return createReq;
    }

    private void callSpecialContestRegisterApi() {
        if (Utils.isInternetAvailable(mActivity)) {
            showProgress();
            ArtalentApplication.apiService.specialContestRegistration(ArtalentApplication.prefManager.getUserObj().getId(), fillDataSpecial())
                    .enqueue(new Callback<BaseResponse>() {
                        @Override
                        public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                            int statusCode = response.code();
                            if (response.body() != null) {
                                Utils.showToast(mActivity, response.body().getMessage() + statusCode);
                                if (statusCode == 200) {
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            hideProgress();
                                            isDone = false;
                                            btnLogin.setAlpha(1f);
                                            finish();
                                        }
                                    }, 2000);
                                } else {
                                    hideProgress();
                                    isDone = false;
                                    btnLogin.setAlpha(1f);
                                }
                            } else {
                                hideProgress();
                                isDone = false;
                                btnLogin.setAlpha(1f);
                                Utils.showToast(mActivity, mActivity.getString(R.string.please_try_again));
                            }
                        }

                        @Override
                        public void onFailure(Call<BaseResponse> call, Throwable t) {
                            // Log error here since request failed
                            Log.e("TAGArtalent", t.toString());
                            Utils.showToast(mActivity, "onFailure : " + t.toString());
                            isDone = false;
                            btnLogin.setAlpha(1f);
                        }
                    });
        } else {
            isDone = false;
            btnLogin.setAlpha(1f);
            Utils.showToast(mActivity, getResources().getString(R.string.please_check_your_internet));
        }
    }

    private void callSpecialContestRegisterCheckApi() {
        if (Utils.isInternetAvailable(mActivity)) {
            showProgress();
            ArtalentApplication.apiService.specialContestRegistrationVerify(ArtalentApplication.prefManager.getUserObj().getId(), specialContestId)
                    .enqueue(new Callback<BaseResponse>() {
                        @Override
                        public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                            int statusCode = response.code();
                            if (response.body() != null) {
                                Utils.showToast(mActivity, response.body().getMessage() + statusCode);
                                hideProgress();
                                isDone = false;
                                btnLogin.setAlpha(1f);
                                if (statusCode == 200) {
                                    startPayment();
                                }
                            } else {
                                hideProgress();
                                isDone = false;
                                btnLogin.setAlpha(1f);
                                Utils.showToast(mActivity, mActivity.getString(R.string.please_try_again));
                            }
                        }

                        @Override
                        public void onFailure(Call<BaseResponse> call, Throwable t) {
                            // Log error here since request failed
                            Log.e("TAGArtalent", t.toString());
                            Utils.showToast(mActivity, "onFailure : " + t.toString());
                            isDone = false;
                            btnLogin.setAlpha(1f);
                        }
                    });
        } else {
            isDone = false;
            btnLogin.setAlpha(1f);
            Utils.showToast(mActivity, getResources().getString(R.string.please_check_your_internet));
        }
    }

    public void startPayment() {
        /*
          You need to pass current activity in order to let Razorpay create CheckoutActivity
         */
        final Activity activity = this;

        final Checkout co = new Checkout();
        try {
            JSONObject options = new JSONObject();
            options.put("name", getString(R.string.app_name));
            options.put("description", getString(R.string.app_tag_line));
            //You can omit the image option to fetch the image from dashboard
            options.put("image", "https://artalentbucket.s3.ap-south-1.amazonaws.com/1577275123128-final_logo.png");
            options.put("currency", "INR");
            options.put("amount", "" + (dPaymentAmount * 100));

            JSONObject preFill = new JSONObject();
            if (ArtalentApplication.prefManager.getUserObj() != null && ArtalentApplication.prefManager.getUserObj().getMobile() != null && !TextUtils.isEmpty(ArtalentApplication.prefManager.getUserObj().getMobile())) {
                preFill.put("contact", ArtalentApplication.prefManager.getUserObj().getMobile());
            }
            options.put("prefill", preFill);
            co.setFullScreenDisable(true);
            co.open(activity, options);

        } catch (Exception e) {
            Toast.makeText(activity, "Error in payment: " + e.getMessage(), Toast.LENGTH_SHORT)
                    .show();
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnLogin:
                if (!isDone) {
                    if (specialContestObj != null) {
                        callSpecialContestRegisterApi();
                    } else {
                        isDone = true;
                        btnLogin.setAlpha(0.5f);
                        if (dRegistrationAmount > 0.0) {
                            callSpecialContestRegisterCheckApi();
                        } else {
                            callSpecialContestRegisterApi();
                        }
                    }
                }
                break;

        }
    }

    @Override
    public void onPaymentSuccess(String razorpayPaymentID) {
        try {
            transactionId = razorpayPaymentID;
            callSpecialContestRegisterApi();
        } catch (Exception e) {
        }
    }

    @Override
    public void onPaymentError(int code, String response) {
        try {
            Toast.makeText(this, "Payment failed: " + code + " " + response, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
        }
    }
}
