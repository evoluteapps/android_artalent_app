package com.artalent.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextSwitcher;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.artalent.ArtalentApplication;
import com.artalent.R;
import com.artalent.Utils;
import com.artalent.autoplayrecyclervideoplayer.view.CenterLayoutManager;
import com.artalent.ui.fragment.BaseFragment;
import com.artalent.ui.model.Country;
import com.artalent.ui.model.DistrictsObj;
import com.artalent.ui.model.InterestObj;
import com.artalent.ui.model.PostContestObj;
import com.artalent.ui.model.PostContestResponse;
import com.artalent.ui.model.StateObj;
import com.artalent.ui.model.UserObj;
import com.artalent.ui.model.request.ContestRegistrationReq;
import com.artalent.ui.utils.Constants;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import im.ene.toro.CacheManager;
import im.ene.toro.widget.Container;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import com.artalent.exo.NewContestPostAdapter;

import static android.app.Activity.RESULT_OK;

public class MyConPostFragment extends BaseFragment implements View.OnClickListener, NewContestPostAdapter.OnFeedItemClickListener {

    public static void startActivity(Activity mActivity, int userID) {
        Intent intent = new Intent(mActivity, MyConPostFragment.class);
        intent.putExtra(Constants.ID, userID);
        mActivity.startActivity(intent);
    }

    private List<UserObj> userObjList = new ArrayList<>();

    @BindView(R.id.container)
    public Container container;

    @BindView(R.id.pullToRefresh)
    SwipeRefreshLayout pullToRefresh;

   /* @BindView(R.id.ivFilter)
    TextView ivFilter;*/


    private NewContestPostAdapter mAdapter;

    private boolean loading = true;
    private int offset = 0;
    private List<PostContestObj> postObjs = new ArrayList<>();

    private LinearLayoutManager mLayoutManager;
    int pastVisiblesItems, visibleItemCount, totalItemCount;

    public static final int REQUEST_FILTER = 400;
    private Country selectedCountry = null;
    List<InterestObj> selectedInterestList = new ArrayList<>();
    private StateObj selectedStateObj = null;
    List<DistrictsObj> selectedDistrictsList = new ArrayList<>();

    @BindView(R.id.tvNoResult)
    TextView tvNoResult;

    private int userID = 0;


    /*@Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_contest_post);*/
    private View rootView;

    public void setType(int id) {
        this.userID = id;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.activity_my_contest_post, container, false);
        ButterKnife.bind(this, rootView);
        tvNoResult.setVisibility(View.GONE);
        tvNoResult.setText(getString(R.string.no_contest_post_yet));
        setupFeed();
        refresh();
        initScrollListener();
        pullToRefresh.setOnRefreshListener(this::refresh);
        return rootView;
        //ivFilter.setOnClickListener(this);
    }


    private void initScrollListener() {
        container.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (!recyclerView.canScrollVertically(1) && newState == RecyclerView.SCROLL_STATE_IDLE) {
                    if (loading) {
                        if (offset != -1) {
                            loading = false;
                            callHomePostApi();
                        }
                    }
                }
            }
        });
    }

    protected void setupFeed() {
        mAdapter = new NewContestPostAdapter(mActivity, postObjs);
        mAdapter.setOnFeedItemClickListener(this);
        container.setLayoutManager(new CenterLayoutManager(mActivity));
        container.setAdapter(mAdapter);
        container.setCacheManager(CacheManager.DEFAULT);
    }

    private void refresh() {
        userObjList.clear();
        offset = 0;
        callHomePostApi();
    }

    private void callHomePostApi() {
        if (Utils.isInternetAvailable(mActivity)) {
            Call<PostContestResponse> call = ArtalentApplication.apiService.getMyContestPost(ArtalentApplication.prefManager.getUserObj().getId(), offset, userID);
            call.enqueue(new Callback<PostContestResponse>() {
                @Override
                public void onResponse(Call<PostContestResponse> call, Response<PostContestResponse> response) {
//                    int statusCode = response.code();
                    pullToRefresh.setRefreshing(false);
                    loading = true;
                    if (response.body() != null && response.body().getObj() != null) {
                        if (response.body().getObj().size() > 0) {
                            postObjs.addAll(response.body().getObj());
                        }

                        mAdapter.notifyDataSetChanged();
                        offset = response.body().getIndex();
                    }
                    if (postObjs.size() > 0) {
                        tvNoResult.setVisibility(View.GONE);
                    } else {
                        tvNoResult.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onFailure(Call<PostContestResponse> call, Throwable t) {
                    // Log error here since request failed
                    loading = true;
                    pullToRefresh.setRefreshing(false);
                }
            });
        } else {
            pullToRefresh.setRefreshing(false);
            Intent intent = new Intent(mActivity, NoInternetActivity.class);
            startActivityForResult(intent, Constants.NO_INTERNET);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.NO_INTERNET) {
            if (resultCode == RESULT_OK) {
                refresh();
            }
        }
    }

    private ContestRegistrationReq fillData() {
        ContestRegistrationReq createReq = new ContestRegistrationReq();
        int countFilter = 0;
        String sDistrictIds = "";
        if (selectedDistrictsList.size() > 0) {
            countFilter = countFilter + selectedDistrictsList.size();
            for (DistrictsObj districtsObj : selectedDistrictsList) {
                if (TextUtils.isEmpty(sDistrictIds)) {
                    sDistrictIds = "" + districtsObj.getDistrictId();
                } else {
                    sDistrictIds = sDistrictIds + "," + districtsObj.getDistrictId();
                }

            }
        }

        String sInterestIds = "";
        if (selectedInterestList.size() > 0) {
            countFilter = countFilter + selectedInterestList.size();
            for (InterestObj interestObj : selectedInterestList) {
                if (TextUtils.isEmpty(sInterestIds)) {
                    sInterestIds = "" + interestObj.getInterestMasterId();
                } else {
                    sInterestIds = sInterestIds + "," + interestObj.getInterestMasterId();
                }

            }
        }

        if (selectedCountry != null) {
            countFilter = countFilter + 1;
        }
        if (selectedStateObj != null) {
            countFilter = countFilter + 1;
        }

        if (countFilter == 0) {
//            ivFilter.setText(getString(R.string.select_filter));
            //ivFilter.setTextColor(ContextCompat.getColor(mActivity, R.color.primary));
        } else {
            //ivFilter.setText(" (" + countFilter + ")");
            //ivFilter.setTextColor(ContextCompat.getColor(mActivity, R.color.primary));
        }
        createReq.setDistrictId(sDistrictIds);
        createReq.setInterestId(sInterestIds);
        return createReq;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnFollow:
                break;
        }
    }

    @Override
    public void onCommentsClick(View v, int position) {

    }

    @Override
    public void onVolumeClick(ImageView btnVolume, int position) {

    }

    @Override
    public void onRegisterClick(int position) {

    }

    @Override
    public void onProfileClick(View v) {

    }

    @Override
    public void onLikeClick(ImageView btnLike, int position, TextSwitcher tsLikesCounter) {

    }


    @Override
    public void onDeleteClick(@NotNull PostContestObj postContestObj, int position) {

    }

}
