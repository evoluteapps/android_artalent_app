package com.artalent.ui.activity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.google.android.material.textfield.TextInputEditText;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.PersistableBundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.artalent.ArtalentApplication;
import com.artalent.R;
import com.artalent.Utils;
import com.artalent.ui.adapter.SelectedPostAdapter;
import com.artalent.ui.model.BaseResponse;
import com.artalent.ui.utils.ImageQuality;
import com.artalent.ui.utils.Options;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Objects;

import butterknife.BindView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddAdvertiseActivity extends BaseActivityOther implements View.OnClickListener {

    private static final String TAG = "AddAdvertiseActivity";

    @BindView(R.id.et_company_name)
    TextInputEditText etCompanyName;

    @BindView(R.id.et_advertise_desc)
    TextInputEditText etAdvertiseDesc;

    @BindView(R.id.et_company_url)
    TextInputEditText etCompanyURL;

    @BindView(R.id.et_expiry_date)
    TextInputEditText etExpirytDate;

    @BindView(R.id.recyclerViewMyPost)
    RecyclerView recyclerViewMyPost;

    @BindView(R.id.rlPhoto)
    RelativeLayout selectBanerImage;

    String companyName, advertiseDescription, companyURL, expiryDate;

    private boolean isClick = false;

    private Options options;
    ArrayList<String> returnValue = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_advertise);
        setupToolbarBack(mActivity.getString(R.string.add_advertise));
        selectBanerImage.setOnClickListener(this);
        etExpirytDate.setOnClickListener(this);
        options = Options.init()
                .setRequestCode(100)
                .setCount(3)
                .setFrontfacing(false)
                .setImageQuality(ImageQuality.LOW)
                .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)
                .setPath("/akshay/new");

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                exitConfirmation();
                Log.d(TAG, "AddAdvertiseActivity: Toolbar back Pressed ");
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_post, menu);
        return true;
    }

    public boolean validateEditText() {
        boolean isEmpty = false;
        if (TextUtils.isEmpty(Objects.requireNonNull(etCompanyName.getText()).toString().trim())) {
            isEmpty = true;
            etCompanyName.setError("Enter company name");
        } else {
            companyName = etCompanyName.getText().toString();
        }
        if (TextUtils.isEmpty(Objects.requireNonNull(etAdvertiseDesc.getText()).toString().trim())) {
            isEmpty = true;
            etAdvertiseDesc.setError("Enter description");
        } else {
            advertiseDescription = etAdvertiseDesc.getText().toString();
        }

        if (TextUtils.isEmpty(Objects.requireNonNull(etCompanyURL.getText()).toString().trim())) {
            isEmpty = true;
            etCompanyURL.setError("Enter company URL");
        } else if (Utils.isValidUrl(etCompanyURL.getText().toString())) {
            etCompanyURL.setError("Invalid URL");
        } else {
            companyURL = etCompanyURL.getText().toString();
        }

        if (TextUtils.isEmpty(Objects.requireNonNull(etExpirytDate.getText()).toString().trim())) {
            isEmpty = true;
            etExpirytDate.setError("Set expiry date");
        } else {
            expiryDate = etExpirytDate.getText().toString();
        }
        if (returnValue != null && !(returnValue.size() > 0)) {
            isEmpty = true;
            Utils.showToast(this, "Please select advertise image");
        }
        return isEmpty;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_add) {
            if (!validateEditText()) callAddAdvertise();
        }
        return super.onOptionsItemSelected(item);
    }

    public void callAddAdvertise() {
        if (!isClick) {
            if (Utils.isInternetAvailable(AddAdvertiseActivity.this)) {
                showProgress();
                isClick = true;
                File file = new File(returnValue.get(0));
                RequestBody postBody = RequestBody.create(MediaType.parse("image/*"), file);
                MultipartBody.Part advertiseImagePart = MultipartBody.Part.createFormData("file", file.getName(), postBody);
                RequestBody advCompany = RequestBody.create(MediaType.parse("multipart/form-data"), companyName);
                RequestBody advDdescription = RequestBody.create(MediaType.parse("multipart/form-data"), advertiseDescription);
                RequestBody companyUrl = RequestBody.create(MediaType.parse("multipart/form-data"), companyURL);
                RequestBody advExpiryDate = RequestBody.create(MediaType.parse("multipart/form-data"), expiryDate);
                ArtalentApplication.apiServicePost.addAdvertise(ArtalentApplication.prefManager.getUserObj().getId(), advertiseImagePart, advCompany, advDdescription, companyUrl, advExpiryDate)
                        .enqueue(new Callback<BaseResponse>() {
                            @Override
                            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                                int statusCode = response.code();
                                hideProgress();
                                String mess = null;
                                if (response.body() != null) {
                                    mess = response.body().getMessage();
                                }
                                if (statusCode == 200) {
                                    new Handler().postDelayed(() -> {
                                        hideProgress();
                                        isClick = false;
                                        finish();
                                    }, 2000);

                                    Utils.showToast(AddAdvertiseActivity.this, mess);
                                } else {
                                    hideProgress();
                                    isClick = false;
                                }
                            }

                            @Override
                            public void onFailure(Call<BaseResponse> call, Throwable t) {
                                // Log error here since request failed
                                Log.e("TAGArtalent", t.toString());
                                hideProgress();
                                isClick = false;
                                Utils.showToast(AddAdvertiseActivity.this, "onFailure : " + t.toString());
                            }
                        });
            }
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.ivClose) {
            int position = (int) v.getTag();
            if (position < returnValue.size()) {
                returnValue.remove(position);
                adapter.notifyDataSetChanged();
            }
        } else if (v.getId() == R.id.et_expiry_date) {
            int mYear, mMonth, mDay;
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                    (view, year, monthOfYear, dayOfMonth) -> etExpirytDate.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth), mYear, mMonth, mDay);
            datePickerDialog.show();
        } else if (v.getId() == R.id.rlPhoto) {
            AppCameraActivity.start(AddAdvertiseActivity.this, options);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100) { // Please, use a final int instead of hardcoded int value
            try {
                if (resultCode == RESULT_OK) {
                    returnValue.addAll(data.getStringArrayListExtra(AppCameraActivity.IMAGE_RESULTS));
                    Toast.makeText(mActivity, "size : " + returnValue.size(), Toast.LENGTH_LONG).show();
                    adapter = new SelectedPostAdapter(mActivity, returnValue, this);
                    recyclerViewMyPost.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
                    recyclerViewMyPost.setItemAnimator(new DefaultItemAnimator());
                    recyclerViewMyPost.setAdapter(adapter);
                }
            } catch (Exception e) {
                Log.d(TAG, "onActivityResult: Exception : " + e.getMessage());
            }

        }
    }

    SelectedPostAdapter adapter;

    @Override
    public void onBackPressed() {
        exitConfirmation();
        Log.d(TAG, "onBackPressed: pressed");
    }


    public void exitConfirmation() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to exit?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }
}
