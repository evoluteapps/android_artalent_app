package com.artalent.ui.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import com.artalent.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import butterknife.BindView;

public class AudioTestActivity extends BaseActivityOther implements View.OnClickListener {

    @BindView(R.id.add_advertise_fab)
    FloatingActionButton addAdvertise;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_panel);
        addAdvertise.setOnClickListener(this);
        setupToolbarBack(getString(R.string.app_name));
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.add_advertise_fab) {
            Intent intent_upload = new Intent();
            intent_upload.setType("audio/*");
            intent_upload.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(intent_upload, 1);

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                //the selected audio.
                Uri uri = data.getData();

            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
