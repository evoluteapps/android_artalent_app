package com.artalent.ui.contestcamera;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Chronometer;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.artalent.ArtalentApplication;
import com.artalent.MergeAudioVideoActivity;
import com.artalent.R;
import com.artalent.Utils;
import com.artalent.ui.activity.MainActivity;
import com.artalent.ui.activity.MusicLibraryActivity;
import com.artalent.ui.adapter.VideoFilterAdapter;
import com.artalent.ui.utils.Constants;
import com.artalent.ui.video.VideoConstants;
import com.artalent.ui.video.VideoTrimmerActivity;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.otaliastudios.cameraview.CameraException;
import com.otaliastudios.cameraview.CameraListener;
import com.otaliastudios.cameraview.CameraLogger;
import com.otaliastudios.cameraview.CameraOptions;
import com.otaliastudios.cameraview.CameraView;
import com.otaliastudios.cameraview.VideoResult;
import com.otaliastudios.cameraview.controls.Audio;
import com.otaliastudios.cameraview.controls.Flash;
import com.otaliastudios.cameraview.controls.Preview;
import com.otaliastudios.cameraview.filter.Filters;
import com.otaliastudios.cameraview.frame.Frame;
import com.otaliastudios.cameraview.frame.FrameProcessor;
import com.uzairiqbal.circulartimerview.CircularTimerListener;
import com.uzairiqbal.circulartimerview.CircularTimerView;
import com.uzairiqbal.circulartimerview.TimeFormatEnum;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.Arrays;
import java.util.List;

import co.mobiwise.materialintro.animation.MaterialIntroListener;
import co.mobiwise.materialintro.prefs.PreferencesManager;
import co.mobiwise.materialintro.shape.Focus;
import co.mobiwise.materialintro.shape.FocusGravity;
import co.mobiwise.materialintro.shape.ShapeType;
import co.mobiwise.materialintro.view.MaterialIntroView;
import de.hdodenhof.circleimageview.CircleImageView;

public class CameraActivity extends AppCompatActivity implements View.OnClickListener, OptionView.Callback {

    private final static CameraLogger LOG = CameraLogger.create("DemoApp");
    private final static boolean USE_FRAME_PROCESSOR = false;
    private final static boolean DECODE_BITMAP = true;
    private final static String TAG = "CameraActivity";

    private CameraView cameraView;
    private ViewGroup controlPanel;

    private boolean isCaptureVideoClicked = false;

    private final Filters[] mAllFilters = Filters.values();
    private Chronometer chronometer;
    private CircleImageView circleImageView;
    private boolean isFilterShow = false;
    private TextView counter;
    private CircularTimerView circularTimerView;
    private long videoMaxDuresion = 60000;
    private MediaPlayer mp;
    ImageButton btnSwitch, btnAudioSwitch, btnMusicLibrary;

    private boolean isFlashOn = false;
    private boolean isAudioOn = true;

    private boolean isOnlyForSavingDraft = false;

    // For intro viewer
    PreferencesManager pref;


    public void audioPlayer(String path) {
        //set up MediaPlayer
        try {
            mp.setDataSource(path);
            mp.prepare();
            playAudioPlayer();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void pauseAudioPlayer() {
        if (mp.isPlaying())
            mp.pause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mp.release();
    }

    public void stopAudioPlayer() {
        if (mp.isPlaying())
            mp.stop();
    }

    public void playAudioPlayer() {
        mp.start();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!mp.isPlaying()) {
            playAudioPlayer();
        }
        // on resume turn on the flash
//        turnFlashlightOn();
    }

    private String selectedAudioPath = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        pref = new PreferencesManager(this);

        if (getIntent() != null) {
            isOnlyForSavingDraft = getIntent().getBooleanExtra("IsOnlyForSavingDraft", false);
        }
        setContentView(R.layout.activity_camera);
        mp = new MediaPlayer();
        CameraLogger.setLogLevel(CameraLogger.LEVEL_VERBOSE);
        final View watermark = findViewById(R.id.watermark);
        TextView tvUserName = findViewById(R.id.user_name);
        counter = findViewById(R.id.counter);
        circularTimerView = findViewById(R.id.progress_circular);
        tvUserName.setText(ArtalentApplication.prefManager.getUserObj().getUserName());

        btnMusicLibrary = findViewById(R.id.musicLibrary);
        btnMusicLibrary.setOnClickListener(v -> {
            Intent intent = new Intent(CameraActivity.this, MusicLibraryActivity.class);
            startActivityForResult(intent, 300);
        });
        chronometer = findViewById(R.id.chronometer);
        chronometer.setVisibility(View.GONE);
        cameraView = findViewById(R.id.camera);
        cameraView.setLifecycleOwner(this);
        cameraView.addCameraListener(new Listener());
        cameraView.setPreviewFrameRate(1F);
        // cameraView.setVideoBitRate(1);

        circularTimerView.setOnClickListener(v -> circularTimerView.startTimer());
        circularTimerView.setStrokeWidthDimension(0);
        circularTimerView.setCircularTimerListener(new CircularTimerListener() {
            @Override
            public String updateDataOnTick(long remainingTimeInMs) {
                String remainTime = String.valueOf((int) Math.ceil((remainingTimeInMs / 1000.f)));
                counter.setVisibility(View.VISIBLE);
                counter.setText(remainTime);
                return remainTime;
            }

            @Override
            public void onTimerFinished() {
                counter.setVisibility(View.GONE);
                circularTimerView.setText("");
                if (!isCaptureVideoClicked) {
                    isCaptureVideoClicked = true;
                    circleImageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_stop_white_24dp));
                    captureVideoSnapshot();
                }
            }
        }, 5, TimeFormatEnum.SECONDS, 1);

        // flash switch button
        btnSwitch = findViewById(R.id.btnSwitch);
        btnAudioSwitch = findViewById(R.id.btnAudioSwitch);

        btnAudioSwitch.setOnClickListener(v -> {
            toggleAudioButtonImage();
            if (isAudioOn) {
                Utils.showToast(this, "Recording audio turned OFF");
                isAudioOn = false;
                cameraView.setAudio(Audio.OFF);
            } else {
                Utils.showToast(this, "Recording audio turned ON");
                isAudioOn = true;
                cameraView.setAudio(Audio.ON);
            }
        });

        // displaying button image

        btnSwitch.setOnClickListener(v -> {
            toggleButtonImage();
//            circularTimerView.setProgress(0);

            if (isFlashOn) {
                // turn off flash
                isFlashOn = false;
                cameraView.setFlashState(Flash.OFF);


            } else {
                // turn on flash
                isFlashOn = true;
                cameraView.setFlashState(Flash.TORCH);
            }
        });

        if (USE_FRAME_PROCESSOR) {
            cameraView.addFrameProcessor(new FrameProcessor() {
                private long lastTime = System.currentTimeMillis();

                @Override
                public void process(@NonNull Frame frame) {
                    long newTime = frame.getTime();
                    long delay = newTime - lastTime;
                    lastTime = newTime;
                    LOG.e("Frame delayMillis:", delay, "FPS:", 1000 / delay);
                    if (DECODE_BITMAP) {
                        YuvImage yuvImage = new YuvImage(frame.getData(), ImageFormat.NV21,
                                frame.getSize().getWidth(),
                                frame.getSize().getHeight(),
                                null);
                        ByteArrayOutputStream jpegStream = new ByteArrayOutputStream();
                        yuvImage.compressToJpeg(new Rect(0, 0,
                                frame.getSize().getWidth(),
                                frame.getSize().getHeight()), 100, jpegStream);
                        byte[] jpegByteArray = jpegStream.toByteArray();
                        Bitmap bitmap = BitmapFactory.decodeByteArray(jpegByteArray, 0, jpegByteArray.length);
                        //noinspection ResultOfMethodCallIgnored
                        bitmap.toString();
                    }
                }
            });
        }

        findViewById(R.id.edit).setOnClickListener(this);
        findViewById(R.id.capturePicture).setOnClickListener(this);
        findViewById(R.id.capturePictureSnapshot).setOnClickListener(this);
        findViewById(R.id.captureVideo).setOnClickListener(this);
        circleImageView = findViewById(R.id.captureVideoSnapshotNew);
        circleImageView.setOnClickListener(this);
        findViewById(R.id.toggleCamera).setOnClickListener(this);
        findViewById(R.id.changeFilter).setOnClickListener(this);

        controlPanel = findViewById(R.id.controls);
        ViewGroup group = (ViewGroup) controlPanel.getChildAt(0);


        List<Option<?>> options = Arrays.asList(
                // Layout
                new Option.Width(), new Option.Height(),
                // Engine and preview
                new Option.Mode(), new Option.Engine(), new Option.Preview(),
                // Some controls
                new Option.Flash(), new Option.WhiteBalance(), new Option.Hdr(),
                new Option.PictureMetering(), new Option.PictureSnapshotMetering(),
                // Video recording
                new Option.PreviewFrameRate(), new Option.VideoCodec(), new Option.Audio(),
                // Gestures
                new Option.Pinch(), new Option.HorizontalScroll(), new Option.VerticalScroll(),
                new Option.Tap(), new Option.LongTap(),
                // Watermarks
                new Option.OverlayInPreview(watermark),
                new Option.OverlayInPictureSnapshot(watermark),
                new Option.OverlayInVideoSnapshot(watermark),
                // Other
                new Option.Grid(), new Option.GridColor(), new Option.UseDeviceOrientation()
        );
        List<Boolean> dividers = Arrays.asList(
                // Layout
                false, true,
                // Engine and preview
                false, false, true,
                // Some controls
                false, false, false, false, true,
                // Video recording
                false, false, true,
                // Gestures
                false, false, false, false, true,
                // Watermarks
                false, false, true,
                // Other
                false, false, true
        );
        for (int i = 0; i < options.size(); i++) {
            OptionView view = new OptionView(this);
            //noinspection unchecked
            view.setOption(options.get(i), this);
            view.setHasDivider(dividers.get(i));
            group.addView(view,
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
        }

        controlPanel.getViewTreeObserver().addOnGlobalLayoutListener(() -> {
            BottomSheetBehavior b = BottomSheetBehavior.from(controlPanel);
            b.setState(BottomSheetBehavior.STATE_HIDDEN);
        });


        // Showing demo for the first time:
//        pref.reset("intro_card");
//        pref.resetAll();

        /*
        1. Choose any music of your choice to create a video
        2. Choose a filter
        3. Swipe left & right to set filter's level
        4. Swipe up & down to adjust the exposure
         */

        //1
        firstIntroView();

    }

    private void firstIntroView() {
        new MaterialIntroView.Builder(this)
                .enableDotAnimation(true)
                .enableIcon(false)
                .setFocusGravity(FocusGravity.CENTER)
                .setFocusType(Focus.MINIMUM)
                .setDelayMillis(100)
                .enableFadeAnimation(true)
                .performClick(false)
                .setShape(ShapeType.CIRCLE)
                .setInfoText("Choose any music of your choice to create a video")
                .setTarget(findViewById(R.id.musicLibrary))
                .setUsageId("firstIntroView")
                .setListener(new MaterialIntroListener() {
                    @Override
                    public void onUserClicked(String materialIntroViewId) {
                        Log.d(TAG, "onUserClicked: Passed 1 intro view");
                        //2
                        secondIntroView();
                    }
                })
                .show();
    }

    private void secondIntroView() {
        new MaterialIntroView.Builder(CameraActivity.this)
                .enableDotAnimation(true)
                .enableIcon(false)
                .setFocusGravity(FocusGravity.CENTER)
                .setFocusType(Focus.MINIMUM)
                .setDelayMillis(100)
                .enableFadeAnimation(true)
                .performClick(true)
                .setShape(ShapeType.CIRCLE)
                .setInfoText("Choose a filter")
                .setTarget(findViewById(R.id.changeFilter))
                .setUsageId("secondIntroView")
                .setListener(new MaterialIntroListener() {
                    @Override
                    public void onUserClicked(String materialIntroViewId) {
                        Log.d(TAG, "onUserClicked: Passed 2 intro view");
                        //3
                        thirdIntroView();
                    }
                })
                .show();
    }

    private void thirdIntroView() {
        new MaterialIntroView.Builder(CameraActivity.this)
                .enableDotAnimation(true)
                .enableIcon(false)
                .setFocusGravity(FocusGravity.CENTER)
                .setFocusType(Focus.MINIMUM)
                .setDelayMillis(100)
                .enableFadeAnimation(true)
                .performClick(false)
                .setShape(ShapeType.RECTANGLE)
                .setInfoText("Swipe left & right to set filter's level")
                .setTarget(findViewById(R.id.filter_recycler_view))
                .setUsageId("thirdIntroView")
                .setListener(new MaterialIntroListener() {
                    @Override
                    public void onUserClicked(String materialIntroViewId) {
                        Log.d(TAG, "onUserClicked: Passed 3 intro view");
                        //4
                        forthIntroView();
                    }
                })
                .show();
    }

    private void forthIntroView() {
        new MaterialIntroView.Builder(CameraActivity.this)
                .enableDotAnimation(true)
                .enableIcon(false)
                .setFocusGravity(FocusGravity.RIGHT)
                .setFocusType(Focus.MINIMUM)
                .setDelayMillis(100)
                .enableFadeAnimation(true)
                .performClick(false)
                .setShape(ShapeType.CIRCLE)
                .setInfoText("Swipe up & down to adjust the exposure")
                .setTarget(findViewById(R.id.camera))
                .setUsageId("forthIntroView")
                .setListener(new MaterialIntroListener() {
                    @Override
                    public void onUserClicked(String materialIntroViewId) {
                        Log.d(TAG, "onUserClicked: Passed 4 intro view");

                    }
                })
                .show();
    }

    private boolean checkAudioPath(String selectedAudioPath) {
        return !TextUtils.isEmpty(selectedAudioPath);

    }

    /*
     * Toggle switch button images
     * changing image states to on / off
     * */
    private void toggleButtonImage() {
        if (isFlashOn) {
            btnSwitch.setImageResource(R.drawable.ic_flash_off_white_24dp);
        } else {
            btnSwitch.setImageResource(R.drawable.ic_flash_on_white_24dp);
        }
    }

    private void toggleAudioButtonImage() {
        if (isAudioOn) {
            btnAudioSwitch.setImageResource(R.drawable.ic_volume_off_black_24dp);
        } else {
            btnAudioSwitch.setImageResource(R.drawable.ic_volume_up_black_24dp);
        }
    }


    private void message(@NonNull String content, boolean important) {
        if (important) {
            LOG.w(content);
            Toast.makeText(this, content, Toast.LENGTH_LONG).show();
        } else {
            LOG.i(content);
            Toast.makeText(this, content, Toast.LENGTH_SHORT).show();
        }
    }

    private class Listener extends CameraListener {

        @Override
        public void onCameraOpened(@NonNull CameraOptions options) {
            ViewGroup group = (ViewGroup) controlPanel.getChildAt(0);
            for (int i = 0; i < group.getChildCount(); i++) {
                OptionView view = (OptionView) group.getChildAt(i);
                view.onCameraOpened(cameraView, options);
            }
        }

        @Override
        public void onCameraError(@NonNull CameraException exception) {
            super.onCameraError(exception);
//            message("Got CameraException #" + exception.getReason(), true);
            LOG.w("onCameraError!");
        }

        @Override
        public void onVideoTaken(@NonNull VideoResult result) {
            super.onVideoTaken(result);
            long elapsedVideoMillis = SystemClock.elapsedRealtime() - chronometer.getBase();
            chronometer.stop();
            chronometer.setVisibility(View.GONE);
            circularTimerView.setVisibility(View.VISIBLE);
            LOG.w("onVideoTaken called! Launching activity.");

            //selectedFinalFileVideo = result.getFile();
            //goTrimmerActivity();

            if (selectedAudioPath != null && !TextUtils.isEmpty(selectedAudioPath)) {
                File file = new File(selectedAudioPath);
                if (file.exists()) {
                    MergeAudioVideoActivity.setVideoResult(result.getFile(), file, elapsedVideoMillis, videoMaxDuresion, selectedAudioPath);
                    Intent intent = new Intent(CameraActivity.this, MergeAudioVideoActivity.class);
                    startActivityForResult(intent, 222);
                } else {
                    selectedFinalFileVideo = result.getFile();
                    goTrimmerActivity();
                }
            } else {
                selectedFinalFileVideo = result.getFile();
                goTrimmerActivity();
            }
        }

        @Override
        public void onVideoRecordingStart() {
            super.onVideoRecordingStart();
            LOG.w("onVideoRecordingStart!");
        }

        @Override
        public void onVideoRecordingEnd() {
            super.onVideoRecordingEnd();
            message("Video taken Processing...", false);
            LOG.w("onVideoRecordingEnd!");
        }

        @Override
        public void onExposureCorrectionChanged(float newValue, @NonNull float[] bounds, @Nullable PointF[] fingers) {
            super.onExposureCorrectionChanged(newValue, bounds, fingers);
//            message("Exposure correction:" + newValue, false);
        }

        @Override
        public void onZoomChanged(float newValue, @NonNull float[] bounds, @Nullable PointF[] fingers) {
            super.onZoomChanged(newValue, bounds, fingers);
//            message("Zoom:" + newValue, false);
        }
    }

    File selectedFinalFileVideo = null;

    private void goTrimmerActivity() {
        if (selectedFinalFileVideo != null && selectedFinalFileVideo.exists()) {
            VideoTrimmerActivity.setVideoResult(selectedFinalFileVideo);
            Intent intent = new Intent(CameraActivity.this, VideoTrimmerActivity.class);
            intent.putExtra("IsOnlyForSavingDraft", isOnlyForSavingDraft);
            startActivityForResult(intent, 100);
        }
    }

    String mergeVideoPath = "";

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100) { // Please, use a final int instead of hardcoded int value
            if (resultCode == RESULT_OK) {
               /* Uri videoURI = data.getData();
                Intent intent = new Intent();
                intent.setData(videoURI);
                setResult(RESULT_OK, intent);
                finish();*/
                String path = data.getStringExtra(VideoConstants.EXTRA_VIDEO_PATH);
                Intent intent = new Intent();
                intent.putExtra(VideoConstants.EXTRA_VIDEO_PATH, path);
                Log.d("Draft", "CameraActivity : onActivityResult: --> " + path);
                setResult(RESULT_OK, intent);
                finish();
            }
        } else if (requestCode == 222) { // Please, use a final int instead of hardcoded int value
            if (resultCode == RESULT_OK) {
                mergeVideoPath = data.getStringExtra(VideoConstants.EXTRA_VIDEO_PATH);
                if (mergeVideoPath != null) {
                    selectedFinalFileVideo = new File(mergeVideoPath);
                }
                goTrimmerActivity();
            }
        } else if (requestCode == 300) {
            if (resultCode == RESULT_OK) {
                selectedAudioPath = data.getStringExtra(Constants.DATA);
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.edit:
                edit();
                break;
            case R.id.capturePicture:
                break;
            case R.id.capturePictureSnapshot:
                break;
            case R.id.captureVideo:
                break;
            case R.id.captureVideoSnapshotNew:
                if (isCaptureVideoClicked) {
                    isCaptureVideoClicked = false;
                    circleImageView.setImageResource(R.color.primary);
                    stopVideo();
                    stopAudioPlayer();
//                    cameraView.close();
                } else {
                    isCaptureVideoClicked = true;
                    circleImageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_stop_white_24dp));
//                    cameraView.open();
                    captureVideoSnapshot();
                }
                break;
            case R.id.toggleCamera:
                toggleCamera();
                break;
            case R.id.changeFilter:
                changeCurrentFilter();
                break;
            case R.id.tvFilter:
                ivFiltre(view);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        Log.d("TAG", "onBackPressed: ");
        if (isOnlyForSavingDraft) {
            finish();
        } else
            startActivity(new Intent(this, MainActivity.class));


        BottomSheetBehavior b = BottomSheetBehavior.from(controlPanel);
        if (b.getState() != BottomSheetBehavior.STATE_HIDDEN) {
            b.setState(BottomSheetBehavior.STATE_HIDDEN);
            return;
        }
        super.onBackPressed();
    }

    private void edit() {
        BottomSheetBehavior b = BottomSheetBehavior.from(controlPanel);
        b.setState(BottomSheetBehavior.STATE_COLLAPSED);
    }

    private void stopVideo() {
        cameraView.stopVideo();
    }

    //start 11
    private void captureVideoSnapshot() {
        if (checkAudioPath(selectedAudioPath)) {
            MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
            mediaMetadataRetriever.setDataSource(selectedAudioPath);
            String duration = mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
            try {
                videoMaxDuresion = Integer.parseInt(duration);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
            audioPlayer(selectedAudioPath);
        }
        if (cameraView.isTakingVideo()) {
            message("Already taking video.", false);
            return;
        }
        if (cameraView.getPreview() != Preview.GL_SURFACE) {
            message("Video snapshots are only allowed with the GL_SURFACE preview.", true);
            return;
        }
        message("Recording Contest Video...", true);
        cameraView.takeVideoSnapshot(new File(getFilesDir(), "video.mp4"), (int) videoMaxDuresion);
        circularTimerView.setVisibility(View.GONE);
        chronometer.setBase(SystemClock.elapsedRealtime());
        chronometer.start();
        chronometer.setVisibility(View.VISIBLE);
    }

    private void toggleCamera() {
        if (cameraView.isTakingPicture() || cameraView.isTakingVideo()) return;
        switch (cameraView.toggleFacing()) {
            case BACK:
                message("Switched to back camera!", false);
                break;

            case FRONT:
                message("Switched to front camera!", false);
                break;
        }
    }

    private void ivFiltre(View view) {
        if (view.getTag() instanceof Filters) {
            Filters filter = (Filters) view.getTag();
//            message(filter.toString(), false);
            // Normal behavior:
            cameraView.setFilter(filter.newInstance());
            if (dialog != null && dialog.isShowing()) {
                dialog.dismiss();
            }
        }
    }

    private void changeCurrentFilter() {
        if (cameraView.getPreview() != Preview.GL_SURFACE) {
            message("Filters are supported only when preview is Preview.GL_SURFACE.", true);
            return;
        }

        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }

        showFilter();
    }

    private BottomSheetDialog dialog;

    private void showFilter() {
        RecyclerView videoFilterRecycler = findViewById(R.id.filter_recycler_view);
        if (!isFilterShow) {
            isFilterShow = true;
            videoFilterRecycler.setVisibility(View.VISIBLE);
            VideoFilterAdapter videoFilterAdapter = new VideoFilterAdapter(mAllFilters, this);
            videoFilterRecycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
            videoFilterRecycler.setAdapter(videoFilterAdapter);
        } else {
            isFilterShow = false;
            videoFilterRecycler.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        pauseAudioPlayer();
    }

    @Override
    protected void onStop() {
        super.onStop();
        isFlashOn = false;
        cameraView.setFlashState(Flash.OFF);
        stopAudioPlayer();
    }

    @Override
    public <T> boolean onValueChanged(@NonNull Option<T> option, @NonNull T value, @NonNull String name) {
        if ((option instanceof Option.Width || option instanceof Option.Height)) {
            Preview preview = cameraView.getPreview();
            boolean wrapContent = (Integer) value == ViewGroup.LayoutParams.WRAP_CONTENT;
            if (preview == Preview.SURFACE && !wrapContent) {
                message("The SurfaceView preview does not support width or height changes. " +
                        "The view will act as WRAP_CONTENT by default.", true);
                return false;
            }
        }

        option.set(cameraView, value);
        BottomSheetBehavior b = BottomSheetBehavior.from(controlPanel);
        b.setState(BottomSheetBehavior.STATE_HIDDEN);
//        message("Changed " + option.getName() + " to " + name, false);
        return true;
    }

    //region Permissions

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        boolean valid = true;
        for (int grantResult : grantResults) {
            valid = valid && grantResult == PackageManager.PERMISSION_GRANTED;
        }
        if (valid && !cameraView.isOpened()) {
            cameraView.open();
        }
    }

    //endregion
}
