package com.artalent.ui.rest;

import com.artalent.Utils;
import com.artalent.ui.model.AdvertiseResponse;
import com.artalent.ui.model.AllUserListResponse;
import com.artalent.ui.model.AudioResponse;
import com.artalent.ui.model.BaseResponse;
import com.artalent.ui.model.CheckAccountResponse;
import com.artalent.ui.model.CommentResponse;
import com.artalent.ui.model.ContestHistoryResponse;
import com.artalent.ui.model.ContestResponse;
import com.artalent.ui.model.CountryResponse;
import com.artalent.ui.model.DistrictsResponse;
import com.artalent.ui.model.FundAccountResponse;
import com.artalent.ui.model.InterestResponse;
import com.artalent.ui.model.LikeResponse;
import com.artalent.ui.model.NotificationListResponse;
import com.artalent.ui.model.OTPResponse;
import com.artalent.ui.model.OtpServiceResponse;
import com.artalent.ui.model.PayoutResponse;
import com.artalent.ui.model.PostAdminContestResponse;
import com.artalent.ui.model.PostContestResponse;
import com.artalent.ui.model.PostResponse;
import com.artalent.ui.model.RazorpayContactResponse;
import com.artalent.ui.model.SpecialContestResponse;
import com.artalent.ui.model.SpecialContestWinnerResponse;
import com.artalent.ui.model.StateResponse;
import com.artalent.ui.model.StoryResponse;
import com.artalent.ui.model.UserNameCountResponse;
import com.artalent.ui.model.UserResponse;
import com.artalent.ui.model.VoteResponse;
import com.artalent.ui.model.request.AccountFoundReq;
import com.artalent.ui.model.request.AmountCreditDebitReq;
import com.artalent.ui.model.request.CardFoundReq;
import com.artalent.ui.model.request.CheckAccountReq;
import com.artalent.ui.model.request.CommentReq;
import com.artalent.ui.model.request.ContestGetReq;
import com.artalent.ui.model.request.ContestRegistrationReq;
import com.artalent.ui.model.request.CountryReq;
import com.artalent.ui.model.request.CreateReq;
import com.artalent.ui.model.request.DistrictReq;
import com.artalent.ui.model.request.FirebaseToken;
import com.artalent.ui.model.request.FollowerObjResponse;
import com.artalent.ui.model.request.FollowingObjResponse;
import com.artalent.ui.model.request.FollowingReq;
import com.artalent.ui.model.request.InterestReq;
import com.artalent.ui.model.request.PayOutReq;
import com.artalent.ui.model.request.PostReq;
import com.artalent.ui.model.request.RazorpayContactReq;
import com.artalent.ui.model.request.StateReq;
import com.artalent.ui.model.request.VPAFoundContactReq;
import com.artalent.ui.model.request.WalletResponseObj;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;


public interface ApiInterface {

    @POST("register/")
    Call<UserResponse> createUser(@Body CreateReq createReq);

    @PUT("register/{mobile}")
    Call<OTPResponse> verifyOTP(@Path("mobile") String mobile,
                                @Body CreateReq createReq);

    @GET("userinfo")
    Call<AllUserListResponse> getAllUserDetails();

    @GET("userinfo/{id}")
    Call<UserResponse> getOtherUserDetails(@Path("id") int id);

    @GET("userinfo/{id}/verifyUserName/{userName}")
    Call<UserNameCountResponse> verifyUserName(@Path("id") int id, @Path("userName") String userName);

    @GET("countries/")
    Call<CountryResponse> getCountries();

    @GET("states/{id}")
    Call<StateResponse> getStates(@Path("id") int id);

    @GET("districts/{id}")
    Call<DistrictsResponse> getDistricts(@Path("id") int id);

    @POST("userinfo/{id}")
    Call<UserResponse> updateUser(@Path("id") int id,
                                  @Body CreateReq createReq);

    @GET("interest/")
    Call<InterestResponse> getInterest();

    @POST("users/{id}/interests")
    Call<BaseResponse> updateInterests(@Path("id") int id,
                                       @Body InterestReq createReq);

    @Multipart
    @POST("userinfo/{id}/post")
    Call<BaseResponse> createPost(@Path("id") int id,
                                  @Part MultipartBody.Part[] surveyImage,
                                  @Part("description") RequestBody description);

    @GET("userinfo/{id}/post")
    Call<PostResponse> getPost(@Path("id") int id,
                               @Query("offset") int offset);

    @POST("userinfo/{id}/post/{post_id}/like/")
    Call<LikeResponse> callLike(@Path("id") int id,
                                @Path("post_id") int post_id);

    @POST("userinfo/{id}/post/{post_id}/comment/")
    Call<BaseResponse> callPostComment(@Path("id") int id,
                                       @Path("post_id") int post_id,
                                       @Body CommentReq commentReq);

    @POST("userinfo/{id}/contestRegistration/")
    Call<BaseResponse> contestRegistration(@Path("id") int id,
                                           @Body ContestRegistrationReq contestRegistrationReq);

    @POST("admin/{id}/adminRegisterContest/")
    Call<BaseResponse> specialContestRegistration(@Path("id") int id,
                                                  @Body ContestRegistrationReq contestRegistrationReq);

    @POST("admin/{id}/adminRegisterContest/{contestId}")
    Call<BaseResponse> specialContestRegistrationVerify(@Path("id") int id,
                                                        @Path("contestId") int contestId);


    @GET("userinfo/{id}/contestRegistration/")
    Call<ContestResponse> getContestRegistration(@Path("id") int id);

    @Multipart
    @POST("userinfo/{id}/ContestPost/")
    Call<BaseResponse> createContestPost(@Utils.Chunked
                                         @Path("id") int id,
                                         @Part MultipartBody.Part[] contestPostImages,
                                         @Part("description") RequestBody contestDescription,
                                         @Part("registrationId") RequestBody registrationId);

    @Multipart
    @POST("admin/{id}/adminContestPost/")
    Call<BaseResponse> createSpecialContestPost(@Path("id") int id,
                                                @Part MultipartBody.Part[] contestPostImages,
                                                @Part("description") RequestBody contestDescription,
                                                @Part("registrationId") RequestBody registrationId);

    @POST("userinfo/{id}/fetchContestPost")
    Call<PostContestResponse> getPostContest(@Path("id") int id,
                                             @Query("offset") int offset,
                                             @Body ContestGetReq contestRegistrationReq);

    @POST("userinfo/{id}/contestRegistration")
    Call<BaseResponse> contestRegistration(@Path("id") int id,
                                           @Query("onlycheckIfAlreadyReg") int onlycheckIfAlreadyReg,
                                           @Body ContestRegistrationReq contestRegistrationReq);

    @POST("userinfo/{id}/contestPost/{contest_post_id}/vote/")
    Call<VoteResponse> callVote(@Path("id") int id, @Path("contest_post_id") int contest_post_id);

    @POST("admin/{id}/adminContestPostVotedBy")
    Call<VoteResponse> callVoteAdmin(@Path("id") int id, @Body PostReq contestRegistrationReq);

    @Multipart
    @POST("userinfo/{id}/profileImage")
    Call<BaseResponse> updateUserProfile(@Path("id") int id,
                                         @Part MultipartBody.Part userProfileImage);

    @GET("userinfo/{id}/ContestPost/")
    Call<PostContestResponse> getAllContestPost(@Path("id") int id,
                                                @Query("offset") int offset);

    @GET("admin/{id}/adminContestPost")
    Call<PostAdminContestResponse> getAllAdminContestPost(@Path("id") int id,
                                                          @Query("offset") int offset, @Query("userIdSearch") int userIdSearch);

    @GET("userinfo/{id}/post")
    Call<PostResponse> getMyPost(@Path("id") int id, @Query("offset") int offset,
                                 @Query("userIdSearch") int userIdSearch);

    @GET("userinfo/{id}/ContestPost/")
    Call<PostContestResponse> getMyContestPost(@Path("id") int id,
                                               @Query("offset") int offset,
                                               @Query("userIdSearch") int userIdSearch);

    @GET("userinfo/{id}/ContestPost/")
    Call<PostContestResponse> getMyVotedPost(@Path("id") int id,
                                             @Query("offset") int offset,
                                             @Query("voterId") int voterId);

    @POST("userinfo/{id}/searchUser")
    Call<AllUserListResponse> getSearchUser(@Path("id") int id,
                                            @Query("offset") int offset,
                                            @Body ContestRegistrationReq contestRegistrationReq);

    @GET("userinfo/{id}/post/{post_id}/comment")
    Call<CommentResponse> getCommentUser(@Path("id") int id,
                                         @Path("post_id") int post_id,
                                         @Query("offset") int offset);

    @POST("userinfo/{id}/following/")
    Call<BaseResponse> postFollow(@Path("id") int id,
                                  @Body FollowingReq followingReq);

    @DELETE("userinfo/{id}/following/{other_id}")
    Call<BaseResponse> postFollowDelete(@Path("id") int id,
                                        @Path("other_id") int other_id);

    @GET("userinfo/{id}/contestHistory/")
    Call<ContestHistoryResponse> contestHistory(@Path("id") int id,
                                                @Query("offset") int offset);


    @GET("userinfo/{id}/contestHistoryLastWeek/")
    Call<ContestHistoryResponse> contestHistoryLastWeek(@Path("id") int id,
                                                        @Query("offset") int offset);


    @Multipart
    @POST("userinfo/{id}/story")
    Call<BaseResponse> addStory(@Path("id") int id,
                                @Part MultipartBody.Part userProfileImage,
                                @Part("description") RequestBody contestDescription);

    @GET("userinfo/{id}/story")
    Call<StoryResponse> getStory(@Path("id") int id);

    @DELETE("userinfo/{userId}/Story/{storyId}")
    Call<BaseResponse> deleteStoryByStoryId(@Path("userId") int userId, @Path("storyId") int storyId);

    @Multipart
    @POST("userinfo/{id}/advertise")
    Call<BaseResponse> addAdvertise(@Path("id") int id,
                                    @Part MultipartBody.Part advertiseBanerImage,
                                    @Part("name") RequestBody name,
                                    @Part("description") RequestBody advertiseDescription,
                                    @Part("url") RequestBody advertiserCompanyUrl,
                                    @Part("expiryDate") RequestBody expiryDate);

    @GET("userinfo/{id}/advertise")
    Call<AdvertiseResponse> getAllAdvertise(@Path("id") int id,
                                            @Query("offset") int offset);

    @GET("userinfo/{id}/referalAmount/")
    Call<UserResponse> getReferalAmount(@Path("id") int id);

    @Multipart
    @POST("admin/{id}/adminContest")
    Call<BaseResponse> addAdminContest(@Path("id") int id,
                                       @Part MultipartBody.Part advertiseBanerImage,
                                       @Part("tittle") RequestBody tittle,
                                       @Part("description") RequestBody description,
                                       @Part("interestId") RequestBody interestId,
                                       @Part("countryId") RequestBody countryId,
                                       @Part("stateId") RequestBody stateId,
                                       @Part("districtId") RequestBody districtId,
                                       @Part("userId") RequestBody userId,
                                       @Part("startDate") RequestBody startDate,
                                       @Part("endDate") RequestBody endDate,
                                       @Part("winnerData") RequestBody winnerData,
                                       @Part("regEndDate") RequestBody regEndDate,
                                       @Part("maxRegCount") RequestBody maxRegCount,
                                       @Part("regFee") RequestBody regFee);

    @GET("admin/{id}/adminContest/isRunning/{type}")
    Call<SpecialContestResponse> getAdminContest(@Path("id") int id,
                                                 @Path("type") int type,
                                                 @Query("offset") int offset);

    @GET("admin/{id}/adminContest")
    Call<SpecialContestResponse> getAdminRunningContest(@Path("id") int id,
                                                        @Query("offset") int offset);

    //1
    @GET("userinfo/{id}/followers")
    Call<FollowerObjResponse> getFollowersUser(@Path("id") int id,
                                               @Query("offset") int offset, @Query("otherUserID") int otherUserID);

    //2
    @GET("userinfo/{id}/following")
    Call<FollowingObjResponse> getFollowingUser(@Path("id") int id,
                                                @Query("offset") int offset, @Query("otherUserID") int otherUserID);

    @GET("userinfo/{id}/wallet")
    Call<WalletResponseObj> getWalletTransaction(@Path("id") int id,
                                                 @Query("offset") int offset);

    @GET("{api_key}/BAL/SMS")
    Call<OtpServiceResponse> getOTPBalance(@Path("api_key") String api_key);

    @GET("{api_key}/SMS/{phone_number}/{otp}/{template_name}")
    Call<OtpServiceResponse> sendOTP(@Path("api_key") String api_key,
                                     @Path("phone_number") String phone_number,
                                     @Path("otp") String otp,
                                     @Path("template_name") String template_name);

    @GET
    Call<ResponseBody> fetchCaptcha(@Url String url);

    @POST("userinfo/{id}/notification")
    Call<BaseResponse> addFirebaseToken(@Path("id") int id,
                                        @Body FirebaseToken firebaseToken);

    @DELETE("userinfo/{id}/notification/{userId}")
    Call<BaseResponse> deleteFirebaseToken(@Path("id") int id,
                                           @Path("userId") int userId);


    //@POST("userinfo/{id}/audio")
    // Call<BaseResponse> uploadAudio(@Path("id") int id);

    @Multipart
    @POST("userinfo/{id}/audio")
    Call<BaseResponse> uploadAudio(@Path("id") int id,
                                   @Part MultipartBody.Part userProfileImage,
                                   @Part("type") RequestBody type,
                                   @Part("duration") RequestBody duration,
                                   @Part("songName") RequestBody songName);

    @GET("userinfo/{id}/audio")
    Call<AudioResponse> getAudioList(@Path("id") int id,
                                     @Query("offset") int offset, @Query("type") String type);

    @GET("userinfo/{id}/audio")
    Call<AudioResponse> getUsersAudioList(@Path("id") int id,
                                          @Query("offset") int offset, @Query("uploaderId") int uploaderId);

    @DELETE("userinfo/{id}/audio/{audioId}")
    Call<BaseResponse> deleteAudio(@Path("id") int id,
                                   @Path("audioId") int audioId);


    @Multipart
    @POST("userinfo/{id}/interest")
    Call<BaseResponse> addInterest(@Path("id") int id,
                                   @Part MultipartBody.Part interestIcon,
                                   @Part MultipartBody.Part interestBaner,
                                   @Part("interest") RequestBody interest);

    @POST("userinfo/{id}/countries")
    Call<BaseResponse> addCountry(@Path("id") int id,
                                  @Body CountryReq countryReq);

    @POST("userinfo/{id}/states")
    Call<BaseResponse> addState(@Path("id") int id,
                                @Body StateReq stateReq);


    @POST("userinfo/{id}/districts")
    Call<BaseResponse> addDistrict(@Path("id") int id,
                                   @Body DistrictReq districtReq);

    @Multipart
    @PUT("userinfo/{id}/interest/{interestId}")
    Call<BaseResponse> editInterest(@Path("id") int id,
                                    @Path("interestId") int interestId,
                                    @Part MultipartBody.Part interestIcon,
                                    @Part MultipartBody.Part interestBaner,
                                    @Part("interest") RequestBody interest);

    @PUT("userinfo/{id}/countries/{countryId}")
    Call<BaseResponse> editCountry(@Path("id") int id,
                                   @Path("countryId") int countryId,
                                   @Body CountryReq countryReq);

    @PUT("userinfo/{id}/states/{stateId}")
    Call<BaseResponse> editState(@Path("id") int id,
                                 @Path("stateId") int stateId,
                                 @Body StateReq stateReq);

    @PUT("userinfo/{id}/districts/{districtId}")
    Call<BaseResponse> editDistrict(@Path("id") int id,
                                    @Path("districtId") int district,
                                    @Body DistrictReq districtReq);


    @DELETE("userinfo/{id}/interest/{interestId}")
    Call<BaseResponse> deleteInterest(@Path("id") int id,
                                      @Path("interestId") int interestId);

    @DELETE("userinfo/{id}/countries/{countryId}")
    Call<BaseResponse> deleteCountry(@Path("id") int id,
                                     @Path("countryId") int countryId);


    @DELETE("userinfo/{id}/states/{stateId}")
    Call<BaseResponse> deleteState(@Path("id") int id,
                                   @Path("stateId") int stateId);


    @DELETE("userinfo/{id}/districts/{districtId}")
    Call<BaseResponse> deleteDistrict(@Path("id") int id,
                                      @Path("districtId") int districtId);


    @DELETE("admin/{id}/adminContest/{contestId}")
    Call<BaseResponse> deleteSpecialContest(@Path("id") int id,
                                            @Path("contestId") int specialContestId);

    @Multipart
    @PUT("admin/{id}/adminContest/{contestId}")
    Call<BaseResponse> editSpecialContest(@Path("id") int id,
                                          @Path("contestId") int specialContestId,
                                          @Part MultipartBody.Part advertiseBanerImage,
                                          @Part("tittle") RequestBody tittle,
                                          @Part("description") RequestBody description,
                                          @Part("interestId") RequestBody interestId,
                                          @Part("countryId") RequestBody countryId,
                                          @Part("stateId") RequestBody stateId,
                                          @Part("districtId") RequestBody districtId,
                                          @Part("userId") RequestBody userId,
                                          @Part("startDate") RequestBody startDate,
                                          @Part("endDate") RequestBody endDate);


    //Razorpay payout - create contacts on payout

    @POST("payments/validate/account")
    Call<CheckAccountResponse> validateAccount(@Query("key_id") String key_id, @Body CheckAccountReq checkAccountReq);

    @POST("contacts")
    Call<RazorpayContactResponse> createRazorpayContact(@Body RazorpayContactReq razorpayContactReq);

    @POST("fund_accounts")
    Call<FundAccountResponse> createRazorpayFoundVPA(@Body VPAFoundContactReq razorpayContactReq);

    @POST("fund_accounts")
    Call<FundAccountResponse> createRazorpayFoundVPA(@Body CardFoundReq razorpayContactReq);

    @POST("fund_accounts")
    Call<FundAccountResponse> createRazorpayFoundVPA(@Body AccountFoundReq razorpayContactReq);

    @POST("payouts")
    Call<PayoutResponse> createRazorpayPayouts(@Body PayOutReq razorpayContactReq);

    @DELETE("userinfo/{id}/post/{postId}")
    Call<BaseResponse> deletePost(@Path("id") int id,
                                  @Path("postId") int postId);

    @DELETE("userinfo/{id}/ContestPost/{postId}")
    Call<BaseResponse> deleteContestPost(@Path("id") int id,
                                         @Path("postId") int postId);

    @DELETE("userinfo/{id}/Audio/{postId}")
    Call<BaseResponse> deleteMusic(@Path("id") int id,
                                   @Path("postId") int postId);

    @DELETE("userinfo/{id}/AdminContestPost/{postId}")
    Call<BaseResponse> deleteAdminContestPost(@Path("id") int id,
                                              @Path("postId") int postId);

    @DELETE("userinfo/{id}/Advertise/{advertiseId}")
    Call<BaseResponse> deleteAdvertise(@Path("id") int id,
                                       @Path("advertiseId") int postId);

    @DELETE("userinfo/{id}/PostCommentedBy/{advertiseId}")
    Call<BaseResponse> deletePostCommentedBy(@Path("id") int id,
                                             @Path("advertiseId") int postId);

    @DELETE("userinfo/{id}/image")
    Call<BaseResponse> deleteOwnProfile(@Path("id") int id);

    @POST("userinfo/{id}/amountCreditDebit")
    Call<BaseResponse> amountCreditDebit(@Path("id") int id,
                                         @Body AmountCreditDebitReq districtReq);


    @GET("userinfo/{id}/notification_art/")
    Call<NotificationListResponse> getNotificationArt(@Path("id") int id,
                                                      @Query("offset") int offset);

    @GET("admin/adminContestHistory/")
    Call<BaseResponse> getWinner();


    @GET("admin/{id}/adminWinnerContest/")
    Call<SpecialContestWinnerResponse> contestAdminHistory(@Path("id") int id,
                                                           @Query("offset") int offset,
                                                           @Query("isOnlyLastWeek") int isOnlyLastWeek);



    /*
    1. Story  -- DELETE "/api/v1/userinfo/{userId}/Story/{storyId}"
    5. Contest Vote  -- DELETE "/api/v1/userinfo/{userId}/ContestPostVotedBy/{contestPostVotedById}"
    7. Special Contest post vote -- DELETE "/api/v1/admin/{adminId}/AdminContestPostVotedBy/{adminContestPostVotedById}"
     */
}
