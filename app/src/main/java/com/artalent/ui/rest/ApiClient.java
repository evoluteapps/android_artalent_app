package com.artalent.ui.rest;

import android.text.TextUtils;
import android.util.Log;

import com.artalent.ArtalentApplication;
import com.artalent.ui.utils.BasicAuthInterceptor;
import com.artalent.ui.utils.Constants;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;


public class ApiClient {

    //Digital Ocean VM
    public static final String BASE_URL = "http://165.22.220.193:8080/api/v1/";
    private static final String RAZORPAY_BASE_URL = "https://api.razorpay.com/v1/";
    private static final String OTP_SERVICE_BASE_URL = "https://2factor.in/API/V1/";
    private static Retrofit retrofit = null;
    private static Retrofit otpRetrofit = null;
    private static Retrofit razorpayRetrofit = null;
    private static Retrofit chunkRetrofit = null;


    /*public static Retrofit getChunkClient() {
        if (chunkRetrofit == null) {
            chunkRetrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(getHeader())
                    .build();
        }
        return chunkRetrofit;
    }*/


    public static Retrofit getClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(getHeader())
                    .build();
        }
        return retrofit;
    }

    public static Retrofit getRazorpayClient() {
        if (razorpayRetrofit == null) {

            OkHttpClient client = new OkHttpClient.Builder()
                    .addInterceptor(new BasicAuthInterceptor(Constants.RAZORPAY_TEST_API_KEY, Constants.RAZORPAY_TEST_API_SECRET))
                    .build();

            razorpayRetrofit = new Retrofit.Builder()
                    .baseUrl(RAZORPAY_BASE_URL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return razorpayRetrofit;
    }


    public static Retrofit getClientOTP() {
        if (otpRetrofit == null) {
            otpRetrofit = new Retrofit.Builder()
                    .baseUrl(OTP_SERVICE_BASE_URL)
                    .client(getOTPHeader())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return otpRetrofit;
    }

    public static Retrofit getClientPost() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(getHeaderPost())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

    public static OkHttpClient getHeader() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient okClient = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .addNetworkInterceptor(
                        chain -> {
                            Request request = null;
                            Request original = chain.request();
                            Request.Builder requestBuilder = original.newBuilder();
                            if (ArtalentApplication.prefManager.getAuthorizationToken() != null && !TextUtils.isEmpty(ArtalentApplication.prefManager.getAuthorizationToken())) {
                                Log.e("--Authorization-- ", ArtalentApplication.prefManager.getAuthorizationToken());
                                requestBuilder.addHeader("Authorization", "Bearer " + ArtalentApplication.prefManager.getAuthorizationToken());
                            }
                            requestBuilder.addHeader("Content-Type", "Application/json");
                            request = requestBuilder.build();

                            return chain.proceed(request);
                        })
                .build();
        return okClient;

    }

    public static OkHttpClient getOTPHeader() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient okClient = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .addNetworkInterceptor(
                        chain -> {
                            Request request = null;
                            Request original = chain.request();
                            Request.Builder requestBuilder = original.newBuilder();
                            requestBuilder.addHeader("Content-Type", "application/x-www-form-urlencoded");
                            request = requestBuilder.build();

                            return chain.proceed(request);
                        })
                .build();
        return okClient;

    }


    public static OkHttpClient getHeaderPost() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient okClient = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .connectTimeout(120, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .addNetworkInterceptor(
                        chain -> {
                            Request request = null;
                            Request original = chain.request();
                            Request.Builder requestBuilder = original.newBuilder();
                            if (ArtalentApplication.prefManager.getAuthorizationToken() != null && !TextUtils.isEmpty(ArtalentApplication.prefManager.getAuthorizationToken())) {
                                Log.e("--Authorization-- ", ArtalentApplication.prefManager.getAuthorizationToken());
                                requestBuilder.addHeader("Authorization", "Bearer " + ArtalentApplication.prefManager.getAuthorizationToken());
                            }
                            request = requestBuilder.build();

                            return chain.proceed(request);
                        })
                .build();
        return okClient;

    }
}
