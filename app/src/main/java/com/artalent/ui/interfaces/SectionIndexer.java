package com.artalent.ui.interfaces;

/**
 * Created by akshay on 07/05/18.
 */

public interface SectionIndexer {
    String getSectionText(int position);
}

