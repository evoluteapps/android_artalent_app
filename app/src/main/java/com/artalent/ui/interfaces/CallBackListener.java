package com.artalent.ui.interfaces;

import com.artalent.ui.model.ContestObj;

import java.util.List;

public interface CallBackListener {

    List<ContestObj> getRegisteredContest();
}
