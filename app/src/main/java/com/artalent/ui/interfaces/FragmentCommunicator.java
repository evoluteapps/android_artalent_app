package com.artalent.ui.interfaces;

public interface FragmentCommunicator {

    public void passData(String imagepath);
}