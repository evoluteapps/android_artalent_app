package com.artalent.ui.video;

/**
 * Created by Deep Patel
 */

public interface OnSnackbarActionListener {
    void onAction();
}
