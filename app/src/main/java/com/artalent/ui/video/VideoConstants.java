package com.artalent.ui.video;

/**
 * Created by Deep Patel
 * (Sr. Android Developer)
 * on 6/4/2018
 */
public class VideoConstants {
    public static final String EXTRA_VIDEO_PATH = "EXTRA_VIDEO_PATH";
    public static final String MERGE_VIDEO_PATH = "MERGE_VIDEO_PATH";

    static String croppedVideoURI;
}
