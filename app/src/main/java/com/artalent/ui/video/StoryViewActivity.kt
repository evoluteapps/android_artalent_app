package com.artalent.ui.video

import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.artalent.ArtalentApplication
import com.artalent.R
import com.artalent.Utils
import com.artalent.ui.fragment.HomeFragment
import com.artalent.ui.model.BaseResponse
import com.artalent.ui.model.StoryImagesObj
import com.artalent.ui.utils.CircleTransformation
import com.artalent.ui.utils.Constants
import com.bolaware.viewstimerstory.Momentz
import com.bolaware.viewstimerstory.MomentzCallback
import com.bolaware.viewstimerstory.MomentzView
import com.squareup.picasso.Callback
import com.squareup.picasso.MemoryPolicy
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_story_view.*
import kotlinx.android.synthetic.main.item_music.view.*
import retrofit2.Call
import retrofit2.Response


class StoryViewActivity : AppCompatActivity(), MomentzCallback {

    val TAG = "StoryViewActivity"

    var selectedStoryImageList: List<StoryImagesObj> = ArrayList()
    var selectedMomentzView: ArrayList<MomentzView> = ArrayList()
    var userName: String? = null
    var profileImageUrl: String? = null
    var storyUserProfileId: Int = 0
    var nextPosition: Int? = -1
    var currentAppUserId = ArtalentApplication.prefManager.userObj.id

    var currentStoryIndex = 1;

    lateinit var momentz: Momentz

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_story_view)

        selectedStoryImageList =
            intent.getSerializableExtra(Constants.INTEREST_LIST) as List<StoryImagesObj>
        nextPosition = intent.getIntExtra(Constants.POSITION, -1);
        userName = intent.getStringExtra(Constants.USER_NAME)
        profileImageUrl = intent.getStringExtra(Constants.PROFILE_URL)
        storyUserProfileId = intent.getIntExtra(Constants.PROFILE_ID_FOR_STORY, 0)

        Log.d(
            TAG, "onCreate: storyUserProfileId -> "
                    + storyUserProfileId
        )
        Log.d(
            TAG, "onCreate: currentAppUserId -> "
                    + currentAppUserId.toString()
        )

        // Setting value to imageView and textView
        if (!userName.isNullOrEmpty()) {
            findViewById<TextView>(R.id.tvUserName).also {
                it.text = userName
            }
        }
        if (!profileImageUrl.isNullOrEmpty()) {
            findViewById<ImageView>(R.id.ivProfilePic).also {
                Picasso.get()
                    .load(profileImageUrl)
                    .placeholder(R.drawable.feed_placeholder)
                    .transform(CircleTransformation())
                    .into(it)
            }
        }

        if (storyUserProfileId != 0) {
            findViewById<ImageButton>(R.id.storyDeleteBtn).also {
                if (storyUserProfileId == currentAppUserId) {
                    Log.d(TAG, "onCreate: THIS IS CURRENT USER")
                    it.visibility = View.VISIBLE

                } else {
                    Log.d(TAG, "onCreate: THIS IS NOT CURRENT USER")
                    it.visibility = View.GONE
                }
                it.setOnClickListener {
                    deletingStory(currentStoryIndex)
                }
            }
        }

        for (storyImagesObj in selectedStoryImageList) {
            if (storyImagesObj != null && !TextUtils.isEmpty(storyImagesObj.storyUrl)) {

                Log.d(TAG, "onCreate: storyUrl -> ${storyImagesObj.storyUrl}")

                //show a customView
                val customView = LayoutInflater.from(this).inflate(R.layout.custom_view_for_story_image, null)
                customView.tag = storyImagesObj.storyUrl

                selectedMomentzView.add(MomentzView(customView, 5))

//                val internetLoadedImageView = ImageView(this)
//                internetLoadedImageView.requestLayout()
//                internetLoadedImageView.layoutParams.height =
//                    RelativeLayout.LayoutParams.MATCH_PARENT
//                internetLoadedImageView.layoutParams.width =
//                    RelativeLayout.LayoutParams.MATCH_PARENT
//                internetLoadedImageView.adjustViewBounds = true
//                internetLoadedImageView.cropToPadding = false
//                internetLoadedImageView.setScaleType(ImageView.ScaleType.FIT_XY);
//                internetLoadedImageView.tag = storyImagesObj.storyUrl

//                selectedMomentzView.add(MomentzView(internetLoadedImageView, 5))
            }
        }

        momentz = Momentz(this, selectedMomentzView, container, this)
        momentz.start()
    }

    private fun deletingStory(_currentStoryIndex: Int) {

        momentz.pause(true)

        // setup the alert builder
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Confirm Delete")
        builder.setMessage("Are you sure you want to delete this story?")

        // add the buttons
        builder.setPositiveButton("Yes", DialogInterface.OnClickListener { dialogInterface, i ->

            Log.d(TAG, "deletingStory: called")
            try {
                if (Utils.isInternetAvailable(this)) {

                    Log.d(TAG, "deletingStory: Current app user Id : $currentAppUserId")
                    Log.d(
                        TAG,
                        "deletingStory: selected story Id : ${selectedStoryImageList[_currentStoryIndex].storyId}"
                    )

                    val call =
                        ArtalentApplication.apiService.deleteStoryByStoryId(
                            currentAppUserId,
                            selectedStoryImageList[_currentStoryIndex].storyId
                        )
                    call.enqueue(object : retrofit2.Callback<BaseResponse> {
                        override fun onResponse(
                            call: Call<BaseResponse>,
                            response: Response<BaseResponse>
                        ) {
                            if (response.isSuccessful) {
                                Log.d(TAG, "onResponse: response obj ${response.body()?.message}")
                                val output = Intent()
                                output.putExtra(Constants.IS_STORY_DELETED, true)
                                setResult(Activity.RESULT_OK, output)
                                finish()
                                done()
                            } else {
                                Log.d(TAG, "onResponse: Unsuccessful : ${response.message()}")
                            }
                        }

                        override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                            Log.d(TAG, "onFailure: called")
                        }
                    })
                }
            } catch (e: Exception) {
                Log.d(TAG, "deletingStory: catch ${e.message}")
                momentz.resume()
                dialogInterface.dismiss()
            }

        })
        builder.setNegativeButton("Cancel", DialogInterface.OnClickListener { dialogInterface, i ->
            momentz.resume()
            dialogInterface.dismiss()
        })

        // create and show the alert dialog
        val dialog = builder.create()
        dialog.show()
    }


    override fun onNextCalled(view: View, momentz: Momentz, index: Int) {
        Log.d(TAG, "onNextCalled: Id : $index")
        currentStoryIndex = index

        if (view is VideoView) {
            momentz.pause(true)
//            playVideo(view, index, momentz)
        } else if ((view is ImageView) && (view.drawable == null)) {
            if (view.tag is String) {
                momentz.pause(true)
                Picasso.get()
                    .load(view.tag as String)
                    .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                    .into(view, object : Callback {
                        override fun onSuccess() {
                            momentz.resume()
                            //Toast.makeText(this@StoryViewActivity, "Image loaded from the internet", Toast.LENGTH_LONG).show()
                        }

                        override fun onError(e: Exception?) {
                            Toast.makeText(
                                this@StoryViewActivity,
                                e?.localizedMessage,
                                Toast.LENGTH_LONG
                            ).show()
                            e?.printStackTrace()
                        }
                    })
            }
        }
    }

    override fun done() {
        val output = Intent()
        output.putExtra(Constants.POSITION, nextPosition)
        setResult(Activity.RESULT_OK, output)
        finish()
    }

    fun playVideo(videoView: VideoView, index: Int, momentz: Momentz) {
        val str =
            "https://images.all-free-download.com/footage_preview/mp4/triumphal_arch_paris_traffic_cars_326.mp4"
        val uri = Uri.parse(str)

        videoView.setVideoURI(uri)

        videoView.requestFocus()
        videoView.start()

        videoView.setOnInfoListener(object : MediaPlayer.OnInfoListener {
            override fun onInfo(mp: MediaPlayer?, what: Int, extra: Int): Boolean {
                if (what == MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START) {
                    // Here the video starts
                    momentz.editDurationAndResume(index, (videoView.duration) / 1000)
                    Toast.makeText(
                        this@StoryViewActivity,
                        "Video loaded from the internet",
                        Toast.LENGTH_LONG
                    ).show()
                    return true
                }
                return false
            }
        })
    }
}

