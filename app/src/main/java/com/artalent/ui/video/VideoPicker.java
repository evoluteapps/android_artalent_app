package com.artalent.ui.video;

import android.content.Context;
import android.os.Bundle;
import android.os.SystemClock;
import androidx.annotation.NonNull;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import android.view.View;

import com.artalent.R;


public abstract class VideoPicker extends BottomSheetDialog implements View.OnClickListener {
    protected long lastClickTime = 0;

    public VideoPicker(@NonNull Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_video_picker);

        (findViewById(R.id.camera)).setOnClickListener(this);
        (findViewById(R.id.gallery)).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        preventDoubleClick(view);
        dismiss();
        switch (view.getId()) {
            case R.id.camera:
                onCameraClicked();
                break;
            case R.id.gallery:
                onGalleryClicked();
                break;
        }
    }

    private void preventDoubleClick(View view) {
        /*// preventing double, using threshold of 1000 ms*/
        if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
            return;
        }
        lastClickTime = SystemClock.elapsedRealtime();
    }

    protected abstract void onCameraClicked();

    protected abstract void onGalleryClicked();
}