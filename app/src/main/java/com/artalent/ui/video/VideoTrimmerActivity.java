package com.artalent.ui.video;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.artalent.ArtalentApplication;
import com.artalent.R;
import com.artalent.ui.activity.ShowDraftsActivity;
import com.artalent.ui.model.room.ContestVideoDraft;
import com.artalent.ui.roomdb.viewmodel.DraftViewModel;
import com.deep.videotrimmer.DeepVideoTrimmer;
import com.deep.videotrimmer.interfaces.OnTrimVideoListener;
import com.deep.videotrimmer.view.RangeSeekBarView;

import java.io.File;
import java.util.Calendar;
import java.util.Date;

public class VideoTrimmerActivity extends VideoBaseActivity implements OnTrimVideoListener {
    private DeepVideoTrimmer mVideoTrimmer;
    TextView textSize, tvCroppingMessage;
    RangeSeekBarView timeLineBar;

    private static File videoResult;

    private DraftViewModel draftViewModel;
    private boolean isDraftSaveBtnClicked = false;
    private int savedDraftCount;
    private String titleForDb = "";
    private String noteForDb = "";

    private boolean isOnlyForSavingDraft = false;
    private boolean isTitleAndNoteEntered = false;

    private static final String TAG = "VideoTrimmerActivity";

    public static void setVideoResult(File result) {
        videoResult = result;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_trimmer);

        showToastWithDesign("Working in oncreate");

        if (getIntent() != null) {
            isOnlyForSavingDraft = getIntent().getBooleanExtra("IsOnlyForSavingDraft", false);
            Log.d(TAG, "onCreate: Inside getIntent check : isOnlyForSavingDraft : " + isOnlyForSavingDraft);
        }

        draftViewModel = new ViewModelProvider(this).get(DraftViewModel.class);

        draftViewModel.getSavedDraftCount().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                savedDraftCount = integer;
            }
        });

        final File result = videoResult;
        if (result == null) {
            finish();
            return;
        }
        Log.e("uriPath", result.toString());

        mVideoTrimmer = (findViewById(R.id.timeLine));
        timeLineBar = findViewById(R.id.timeLineBar);
        textSize = findViewById(R.id.textSize);
        tvCroppingMessage = findViewById(R.id.tvCroppingMessage);

        if (mVideoTrimmer != null) {
            mVideoTrimmer.setMaxDuration(100);
            mVideoTrimmer.setOnTrimVideoListener(this);
            mVideoTrimmer.setVideoURI(Uri.fromFile(result));
        } else {
            showToastLong(getString(R.string.toast_cannot_retrieve_selected_video));
        }
    }

    @Override
    public void getResult(final Uri uri) {

        Log.d(TAG, "VideoTrimmerActivity : getResult -> " + uri.toString());

        if (isOnlyForSavingDraft) {

            if (isTitleAndNoteEntered) {
                if (savedDraftCount < 5) {
                    Log.d(TAG, "VideoTrimmerActivity: getResult: save draft button clicked only for isOnlyForSavingDraft");

                    Date date = Calendar.getInstance().getTime();
                    ContestVideoDraft contestVideoDraft = new ContestVideoDraft(ArtalentApplication.prefManager.getUserObj().getId(), titleForDb, noteForDb, uri.toString(), false, date, date);
                    try {
                        draftViewModel.insert(contestVideoDraft);
                        runOnUiThread(() -> showToastWithDesign("Draft Saved Successfully"));
                    } catch (Exception e) {
                        showToastWithDesign("Try again!");
                    }

                    mVideoTrimmer.destroy();
                    runOnUiThread(() -> tvCroppingMessage.setVisibility(View.GONE));
                    startActivity(new Intent(mActivity, ShowDraftsActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                    Log.d(TAG, "getResult: Draft saved...");
                    finish();

                } else {
                    Log.d(TAG, "VideoTrimmerActivity: getResult: savedDraft =  " + savedDraftCount);
//                    showToastWithDesign("Draft limit is over");
                    runOnUiThread(() -> showToastWithDesign("Draft limit is over"));
                }
            } else {
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        openCustomAlertDialogForDb();
                    }
                });
            }
        } else if (isDraftSaveBtnClicked) {
            if (savedDraftCount < 5) {
                isDraftSaveBtnClicked = false;

                Log.d(TAG, "VideoTrimmerActivity: getResult: save draft button clicked");

                Date date = Calendar.getInstance().getTime();
                ContestVideoDraft contestVideoDraft = new ContestVideoDraft(ArtalentApplication.prefManager.getUserObj().getId(), titleForDb, noteForDb, uri.toString(), false, date, date);
                try {
                    draftViewModel.insert(contestVideoDraft);
//                    showToastWithDesign("Draft Saved Successfully");
                    runOnUiThread(() -> showToastShort("Draft Saved Successfully")
//                            showToastWithDesign("Draft Saved Successfully")
                    );
                    runOnUiThread(() -> showToastWithDesign("Draft Saved Successfully"));
                } catch (Exception e) {
//                    showToastWithDesign("Try again!");
                    runOnUiThread(() -> showToastWithDesign("Try again!"));
                }

                mVideoTrimmer.destroy();
                runOnUiThread(() -> tvCroppingMessage.setVisibility(View.GONE));

                finish();
            } else {
                Log.d(TAG, "VideoTrimmerActivity: getResult: savedDraft =  " + savedDraftCount);
//                showToastWithDesign("Draft limit is over");
                runOnUiThread(() -> showToastWithDesign("Draft limit is over"));
            }

        } else {
            /*runOnUiThread(() -> tvCroppingMessage.setVisibility(View.GONE));
        if (uri != null) {
            Log.e("UriCheckDemo", "" + uri.toString());
            VideoConstants.croppedVideoURI = uri.toString();
            Intent intent = new Intent();
            intent.setData(uri);
            setResult(RESULT_OK, intent);
            finish();
        }*/
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    tvCroppingMessage.setVisibility(View.GONE);
                }
            });
            //file:///storage/emulated/0/ARTalent/2020_04_19_19_26_49.mp4
            VideoConstants.croppedVideoURI = uri.toString();
            String uriPath = VideoConstants.croppedVideoURI;
            if (uriPath.startsWith("file:///")) {
            /*String newPath = uriPath.replaceAll("file:///storage", "/storage");
            Intent intent = new Intent();
            intent.putExtra(VideoConstants.EXTRA_VIDEO_PATH, newPath);
            setResult(RESULT_OK, intent);
            finish();*/
            } else {
                Intent intent = new Intent();
                intent.putExtra(VideoConstants.EXTRA_VIDEO_PATH, uriPath);
                setResult(RESULT_OK, intent);
                finish();
            }
        }
    }

    @Override
    public void cancelAction() {
        Log.d(TAG, "VideoTrimmerActivity : cancelAction");

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Are you sure?");
        builder.setMessage("If you go back now, your video will be discarded.");

        // add the buttons
        builder.setPositiveButton("Save Draft", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                openCustomAlertDialogForDb();
            }
        });

        builder.setNegativeButton("Discard", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                isDraftSaveBtnClicked = false;

                destroyThisActivity();
            }
        });

        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void openCustomAlertDialogForDb() {
        Log.d(TAG, "VideoTrimmerActivity : openCustomAlertDialogForDb");

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
//        AlertDialog.Builder builder = new AlertDialog.Builder(VideoTrimmerActivity.this, R.style.AlertDialog);

        builder.setTitle("Enter title and note for draft");

        // set the custom layout
        final View customLayout
                = getLayoutInflater()
                .inflate(
                        R.layout.db_custom_alert_dialog,
                        null);


//        final View customLayout = LayoutInflater.from(this)
//                .inflate(R.layout.db_custom_alert_dialog, null, false);

        builder.setView(customLayout);

        // add the buttons
        builder.setPositiveButton("Yes", (dialogInterface, i) -> {
            EditText titleEdt = customLayout
                    .findViewById(
                            R.id.title_editText);
            titleForDb = titleEdt.getText().toString().trim();

            EditText noteEdt = customLayout
                    .findViewById(
                            R.id.note_editText);
            noteForDb = noteEdt.getText().toString().trim();

            isDraftSaveBtnClicked = true;
            isTitleAndNoteEntered = true;
//            mVideoTrimmer.saveBtnClicked();
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                isDraftSaveBtnClicked = false;

                destroyThisActivity();
            }
        });

        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void destroyThisActivity() {
        mVideoTrimmer.destroy();
        runOnUiThread(() -> tvCroppingMessage.setVisibility(View.GONE));
        finish();
    }
}