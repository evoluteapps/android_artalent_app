/*
 * Copyright (c) 2018 Nam Nguyen, nam@ene.im
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.artalent.exo

import android.app.Activity
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextSwitcher
import androidx.recyclerview.widget.RecyclerView.Adapter
import com.artalent.R
import com.artalent.ui.model.PostContestObj
import com.bumptech.glide.request.RequestOptions

/**
 * @author eneim (2018/01/23).
 */

private const val TAG = "NewContestPostAdapter"

class NewContestPostAdapter(
    var mActivity: Context,
    var postObjs: List<PostContestObj>
) : Adapter<BaseViewHolder>() {
    var mPostObjs: List<PostContestObj> = postObjs
    var activity: Context = mActivity
    companion object {
        const val videoItem = "div > video"
        const val typeText = 1
        const val typeVideo = 2
    }

    private var mOnFeedItemClickListener: OnFeedItemClickListener? = null

    fun setOnFeedItemClickListener(onFeedItemClickListener: OnFeedItemClickListener) {
        mOnFeedItemClickListener = onFeedItemClickListener
    }

    interface OnFeedItemClickListener {
        fun onCommentsClick(v: View?, position: Int)
        fun onVolumeClick(btnVolume: ImageView?, position: Int)
        fun onRegisterClick(position: Int)
        fun onProfileClick(v: View?)
        fun onLikeClick(btnLike: ImageView?, position: Int, tsLikesCounter: TextSwitcher?)
        fun onDeleteClick(postContestObj: PostContestObj, position: Int)
    }

    private var inflater: LayoutInflater? = null
    private var options: RequestOptions? = null
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BaseViewHolder {
        options = RequestOptions()
            .centerCrop()

            .placeholder(R.drawable.interest_categories)
            .error(R.drawable.interest_categories)
        if (inflater === null || inflater!!.context !== parent.context) {
            inflater = LayoutInflater.from(parent.context)
        }

        return if (viewType == typeVideo) NewVideoViewHolder(
            inflater!!,
            parent,
            mOnFeedItemClickListener!!
        )
        else TextViewHolder(inflater!!, parent)
    }

    override fun getItemCount(): Int {
        Log.d(TAG, "getItemCount: postObj size -> ${this.mPostObjs.size}")
        Log.d(TAG, "getItemCount: postObj -> ${this.mPostObjs}")
        return this.mPostObjs.size
    }

    override fun onBindViewHolder(
        holder: BaseViewHolder,
        position: Int) {
        options?.let { holder.bind(mPostObjs[position], mActivity, it) }
    }

    override fun getItemViewType(position: Int): Int {
        val item = mPostObjs[position]

        return if (item != null) typeVideo else typeText
    }
}