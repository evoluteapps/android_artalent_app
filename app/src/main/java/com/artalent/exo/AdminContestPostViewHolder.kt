/*
 * Copyright (c) 2018 Nam Nguyen, nam@ene.im
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.artalent.exo

import android.content.Intent
import android.net.Uri
import android.os.Handler
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.artalent.ArtalentApplication
import com.artalent.R
import com.artalent.Utils
import com.artalent.autoplayrecyclervideoplayer.view.CameraAnimation
import com.artalent.ui.activity.OtherUserProfileActivity
import com.artalent.ui.model.PostAdminContestObj
import com.artalent.ui.model.UserObj
import com.artalent.ui.utils.CircleTransformation
import com.artalent.ui.utils.Constants
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout
import com.google.android.exoplayer2.ui.PlayerView
import com.squareup.picasso.Picasso
import im.ene.toro.ToroPlayer
import im.ene.toro.ToroPlayer.EventListener
import im.ene.toro.ToroUtil.visibleAreaOffset
import im.ene.toro.exoplayer.ExoPlayerViewHelper
import im.ene.toro.media.PlaybackInfo
import im.ene.toro.widget.Container
import java.util.regex.Pattern

/**
 * @author eneim (2018/01/23).
 */
internal class AdminContestPostViewHolder(
    inflater: LayoutInflater,
    parent: ViewGroup, onFeedItemClickListener: AdminContestPostAdapter.OnFeedItemClickListener
) :
    BaseViewHolder(inflater.inflate(R.layout.exo_article_part_video, parent, false)),
    ToroPlayer {

    companion object {
        val ratioRegex = Pattern.compile("(\\d)+\\.(\\d+)")!!
        const val defaultRatio = 200 * 165.78F / 360F // magic number.
    }
    private val TAG = "AdminContestPostViewHolder"

    val app = itemView.context.applicationContext as DemoApp
    private var mOnFeedItemClickListener: AdminContestPostAdapter.OnFeedItemClickListener? =
        onFeedItemClickListener
    //  private val playerFrame by lazy { itemView as AspectRatioFrameLayout }

    //private val ivThumb = itemView.findViewById(R.id.ivThumb) as ImageView

    private val userNameLL = itemView.findViewById(R.id.user_name_ll) as LinearLayout
    private val ivUserProfile = itemView.findViewById(R.id.ivUserProfile) as ImageView
    private val tvUserName = itemView.findViewById(R.id.tvUserName) as TextView
    private val tvContestName = itemView.findViewById(R.id.tvEdit) as TextView
    private val btnRegister = itemView.findViewById(R.id.btnRegister) as Button
    private val ivDelete = itemView.findViewById(R.id.ivDelete) as ImageView

    private val playerFrame =
        itemView.findViewById(R.id.aspectRatioFrameLayout) as AspectRatioFrameLayout
    private val player = itemView.findViewById(R.id.player) as PlayerView
    private val ivCameraAnimation = itemView.findViewById(R.id.ivCameraAnimation) as CameraAnimation

    private val tvFeedBottom = itemView.findViewById(R.id.ivFeedBottom) as TextView
    private val btnLike = itemView.findViewById(R.id.btnLike) as ImageView
    private val btnShare = itemView.findViewById(R.id.btnShare) as ImageView
    private val tsLikesCounter = itemView.findViewById(R.id.tsLikesCounter) as TextSwitcher

    private val status = itemView.findViewById(R.id.playerStatus) as TextView

    private var videoUri: Uri? = null

    var listener: EventListener? = null

    override fun bind(item: Any?) {
        super.bind(item)
        val postContestObj = (item as PostAdminContestObj)
        Log.d(TAG, "bind: postAdminContestObj -> $postContestObj")

        playerFrame.tag = false
        playerFrame.setOnClickListener(View.OnClickListener {
            it.tag = !(it.tag as Boolean)
            if (DemoApp.helper != null) {
                if (DemoApp.helper!!.isPlaying) {
                    Handler().postDelayed({
                        DemoApp.helper!!.pause()
                    }, 1)
                } else {
                    Handler().postDelayed({
                        DemoApp.helper!!.play()
                    }, 1)
                }
            }
            // Utils.showToast(it.context as MainActivity, "tag : " + (it.tag as Boolean))
        })

        var videoUrl = ""
        if (postContestObj.mediaUrls != null && postContestObj!!.mediaUrls.size > 0) {
            videoUrl = postContestObj.mediaUrls[0]
        }

        //  val videoUrl="https://artalent.nyc3.digitaloceanspaces.com/1591290960288-594_Bon_Jovi-Hallelujah_with_lyrics.m3u8"
        if (videoUrl !== null) videoUri = Uri.parse(videoUrl)

        val imageViewArtwork = playerView.findViewById<ImageView>(R.id.exo_artwork)
        playerView.resizeMode = AspectRatioFrameLayout.RESIZE_MODE_FILL
        imageViewArtwork.scaleType = ImageView.ScaleType.CENTER_CROP
        if (postContestObj != null && postContestObj.thumbImage != null && !TextUtils.isEmpty(
                postContestObj.thumbImage
            )
        ) {
            Picasso.get()
                .load(postContestObj.thumbImage)
                .into(imageViewArtwork)
        } else {
            imageViewArtwork.setImageResource(R.drawable.feed_placeholder)
        }
        playerFrame.setAspectRatio(16f / 22f)

        /* val requestOptions = RequestOptions()
         requestOptions.placeholder(R.drawable.feed_placeholder)
         requestOptions.error(R.drawable.feed_placeholder)


         Glide.with(itemView.context)
                 .load(videoUrl)
                 .apply(requestOptions)
                 .thumbnail(Glide.with(itemView.context).load(videoUrl))
                 .into(ivThumb)*/
        /* Glide.with(itemView.context)
                 .asBitmap()
                 .load(videoUrl)
                 .diskCacheStrategy(DiskCacheStrategy.DATA)
                 .into(ivThumb);*/


        //if (style !== null) {
        //   val match = ratioRegex.matcher(style)
        // var ratio = if (match.find()) match.group().toFloat() else null
        //}

        if (postContestObj != null && !TextUtils.isEmpty(postContestObj.getDescription())) {
            tvFeedBottom.setText(postContestObj.getDescription())
        } else {
            tvFeedBottom.setText("")
        }

        if (postContestObj != null && postContestObj.getUserInfo() != null) {
            tvUserName.setText(Utils.getFullName(postContestObj.getUserInfo()))
        } else {
            tvUserName.setText("")
        }

        if (postContestObj != null && postContestObj.getUserInfo() != null && postContestObj.getUserInfo()
                .getId() > 0 && ArtalentApplication.prefManager.userObj.id == postContestObj.getUserInfo()
                .getId()
        ) {
            btnRegister.setVisibility(View.GONE)
        } else {
            btnRegister.setVisibility(View.GONE)
        }


        if (postContestObj != null && postContestObj.getUserInfo() != null && !TextUtils.isEmpty(
                postContestObj.getUserInfo().getProfileImage()
            )
        ) {
            Picasso.get()
                .load(postContestObj.getUserInfo().getProfileImage())
                .placeholder(R.drawable.user_outline)
                .resize(
                    itemView.context.getResources().getDimensionPixelSize(R.dimen.dimen_90),
                    itemView.context.getResources().getDimensionPixelSize(R.dimen.dimen_90)
                )
                .centerCrop()
                .transform(CircleTransformation())
                .into(ivUserProfile)
        } else {
            Picasso.get()
                .load(R.drawable.person_placeholder)
                .placeholder(R.drawable.user_outline)
                .resize(
                    itemView.context.getResources().getDimensionPixelSize(R.dimen.dimen_90),
                    itemView.context.getResources().getDimensionPixelSize(R.dimen.dimen_90)
                )
                .centerCrop()
                .transform(CircleTransformation())
                .into(ivUserProfile)
        }

        if (postContestObj != null) {
            tvContestName.setText(postContestObj.getDescription())
            btnLike.setImageResource(if (postContestObj.getIsVoted() == "0") R.drawable.ic_voted_grey_outline else R.drawable.voted_colored)
            tsLikesCounter.setCurrentText(
                itemView.context.getResources().getQuantityString(
                    R.plurals.votes_count,
                    postContestObj.getVoteCount(),
                    postContestObj.getVoteCount()
                )
            )
        }

        if (postContestObj != null && postContestObj.getUserInfo() != null && postContestObj.getUserInfo()
                .getId() > 0 && postContestObj.getUserInfo()
                .getId() == ArtalentApplication.prefManager.userObj.id
        ) {
            btnLike.setVisibility(View.GONE)
        } else {
            btnLike.setVisibility(View.VISIBLE)
        }

        userNameLL.setOnClickListener {
            var userObj1: UserObj? = null
            if (postContestObj != null) {
                userObj1 = postContestObj.userInfo
            }
            if (userObj1 != null) {
                val intent = Intent(itemView.context, OtherUserProfileActivity::class.java)
                intent.putExtra(Constants.DATA, userObj1)
                itemView.context.startActivity(intent)
            }
        }

        ivDelete.visibility = View.GONE
        if (postContestObj.userInfo != null && postContestObj.userInfo.id == ArtalentApplication.prefManager.userObj.id) {
            ivDelete.visibility = View.VISIBLE
        }
        ivDelete.tag = postContestObj
        ivDelete.setOnClickListener { v: View? ->
            mOnFeedItemClickListener!!.onDeleteClick(
                v!!.tag as PostAdminContestObj,
                position
            )
        }


        btnLike.setOnClickListener {
            mOnFeedItemClickListener!!.onLikeClick(
                btnLike,
                position,
                tsLikesCounter
            )
        }
        ivUserProfile.setOnClickListener { mOnFeedItemClickListener!!.onProfileClick(itemView) }
        btnRegister.setOnClickListener { mOnFeedItemClickListener!!.onRegisterClick(position) }
    }

    override fun getPlayerView() = player

    override fun getCurrentPlaybackInfo() = DemoApp.helper?.latestPlaybackInfo ?: PlaybackInfo()

    override fun initialize(
        container: Container,
        playbackInfo: PlaybackInfo
    ) {
        if (DemoApp.helper == null) DemoApp.helper =
            ExoPlayerViewHelper(this, videoUri!!, null, app.config)
        if (listener == null) {
            listener = object : EventListener {
                override fun onFirstFrameRendered() {
                    ivCameraAnimation.start()
                }

                override fun onBuffering() {
                    ivCameraAnimation.start()
                }

                override fun onPlaying() {
                    Log.e("onPlaying", "onPlaying")
                    ivCameraAnimation.stop()
                }

                override fun onPaused() {
                    ivCameraAnimation.stop()
                }

                override fun onCompleted() {
                    ivCameraAnimation.stop()
                }

            }
            DemoApp.helper!!.addPlayerEventListener(listener!!)
        }
        DemoApp.helper!!.initialize(container, playbackInfo)
    }

    override fun play() {
        Handler().postDelayed({
            DemoApp.helper!!.play()
        }, 1)

    }

    override fun pause() {
        DemoApp.helper!!.pause()
    }

    override fun isPlaying() = DemoApp.helper?.isPlaying ?: false

    override fun release() {
        if (listener != null) {
            DemoApp.helper?.removePlayerEventListener(listener)
            listener = null
        }
        DemoApp.helper?.release()
        DemoApp.helper = null
    }

    override fun wantsToPlay() = visibleAreaOffset(this, itemView.parent) >= 0.65

    override fun getPlayerOrder() = adapterPosition

}