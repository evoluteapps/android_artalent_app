/*
 * Copyright (c) 2018 Nam Nguyen, nam@ene.im
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.artalent.exo

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Handler
import android.text.TextUtils
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.artalent.ArtalentApplication
import com.artalent.R
import com.artalent.Utils
import com.artalent.autoplayrecyclervideoplayer.view.CameraAnimation
import com.artalent.exo.NewContestPostAdapter.OnFeedItemClickListener
import com.artalent.ui.activity.OtherUserProfileActivity
import com.artalent.ui.model.PostContestObj
import com.artalent.ui.model.UserObj
import com.artalent.ui.utils.CircleTransformation
import com.artalent.ui.utils.Constants
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout
import com.google.android.exoplayer2.ui.PlayerView
import com.squareup.picasso.Picasso
import im.ene.toro.ToroPlayer
import im.ene.toro.ToroPlayer.EventListener
import im.ene.toro.ToroUtil.visibleAreaOffset
import im.ene.toro.exoplayer.ExoPlayerViewHelper
import im.ene.toro.media.PlaybackInfo
import im.ene.toro.widget.Container


/**
 * @author eneim (2018/01/23).
 */
private const val TAG = "NewVideoViewHolder"

internal class NewVideoViewHolder(
    inflater: LayoutInflater,
    parent: ViewGroup, onFeedItemClickListener: OnFeedItemClickListener
) : BaseViewHolder(inflater.inflate(R.layout.exo_article_part_video, parent, false)), ToroPlayer {

    val app = itemView.context.applicationContext as DemoApp
    private var mOnFeedItemClickListener: OnFeedItemClickListener? = onFeedItemClickListener
    //  private val playerFrame by lazy { itemView as AspectRatioFrameLayout }

    private val playerFrame =
        itemView.findViewById(R.id.aspectRatioFrameLayout) as AspectRatioFrameLayout

    //private val ivThumb = itemView.findViewById(R.id.ivThumb) as ImageView

    private val player = itemView.findViewById(R.id.player) as PlayerView
    private val status = itemView.findViewById(R.id.playerStatus) as TextView

    private val tvContestName = itemView.findViewById(R.id.tvEdit) as TextView
    private val ivDelete = itemView.findViewById(R.id.ivDelete) as ImageView
    private val ivCameraAnimation = itemView.findViewById(R.id.ivCameraAnimation) as CameraAnimation

    private val userNameLL = itemView.findViewById(R.id.user_name_ll) as LinearLayout
    private val tvUserName = itemView.findViewById(R.id.tvUserName) as TextView
    private val ivUserProfile = itemView.findViewById(R.id.ivUserProfile) as ImageView
    private val btnRegister = itemView.findViewById(R.id.btnRegister) as Button
    private val tvFeedBottom = itemView.findViewById(R.id.ivFeedBottom) as TextView
    private val btnLike = itemView.findViewById(R.id.btnLike) as ImageView
    private val btnShare = itemView.findViewById(R.id.btnShare) as ImageView
    private val tsLikesCounter = itemView.findViewById(R.id.tsLikesCounter) as TextSwitcher

    private val bottomParentLayout =
        itemView.findViewById(R.id.bottom_parent_layout) as RelativeLayout

    private var videoUri: Uri? = null
    private var mActivity: Context? = null
    var listener: EventListener? = null

    override fun bind(item: Any?, activity: Context?, options: RequestOptions?) {
        super.bind(item)
        val postContestObj = (item as PostContestObj)
        mActivity = activity
        Log.d(TAG, "bind: PostContestObj -> $postContestObj")
        var videoUrl = ""
        if (postContestObj.mediaUrls != null && postContestObj.mediaUrls.size > 0) {
            videoUrl = postContestObj.mediaUrls[0]
        }
        videoUri = Uri.parse(videoUrl)
        val imageViewArtwork = playerView.findViewById<ImageView>(R.id.exo_artwork)
        val layoutParams: ViewGroup.LayoutParams = imageViewArtwork.layoutParams
        layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT
        layoutParams.height = ViewGroup.LayoutParams.MATCH_PARENT
        imageViewArtwork.layoutParams = layoutParams
        imageViewArtwork.foregroundGravity = Gravity.CENTER

        playerView.resizeMode = AspectRatioFrameLayout.RESIZE_MODE_FIXED_WIDTH
        imageViewArtwork.scaleType = ImageView.ScaleType.FIT_CENTER

        mActivity?.let {
            Glide.with(it).asGif()
                .load(R.raw.loading_video)
                .into(imageViewArtwork)
        }

     /*   if (postContestObj.thumbImage != null && !TextUtils.isEmpty(postContestObj.thumbImage)) {
            Log.d("NewVideoViewHolder", "bind: " + postContestObj.thumbImage)
            Picasso.get()
                .load(thumb)
                .into(imageViewArtwork)
        } else {
            imageViewArtwork.setImageResource(R.drawable.feed_placeholder)
        }*/

        if (!TextUtils.isEmpty(postContestObj.description)) {
            tvFeedBottom.text = postContestObj.description
        } else {
            tvFeedBottom.text = ""
        }

        if (postContestObj.userInfo != null) {
            tvUserName.text = Utils.getFullName(postContestObj.userInfo)
        } else {
            tvUserName.text = ""
        }

        if (postContestObj.userInfo != null && postContestObj.userInfo.id > 0 && ArtalentApplication.prefManager.userObj.id == postContestObj.userInfo.id) {
            btnRegister.visibility = View.GONE
        } else {
            btnRegister.visibility = View.GONE
        }

        if (postContestObj.userInfo != null && !TextUtils.isEmpty(postContestObj.userInfo.profileImage)) {
            Picasso.get()
                .load(postContestObj.userInfo.profileImage)
                .placeholder(R.drawable.man)
                .resize(
                    itemView.context.getResources().getDimensionPixelSize(R.dimen.dimen_90),
                    itemView.context.getResources().getDimensionPixelSize(R.dimen.dimen_90)
                )
                .centerCrop()
                .transform(CircleTransformation())
                .into(ivUserProfile)
        } else {
            Picasso.get()
                .load(R.drawable.man)
                .placeholder(R.drawable.man)
                .resize(
                    itemView.context.getResources().getDimensionPixelSize(R.dimen.dimen_90),
                    itemView.context.resources.getDimensionPixelSize(R.dimen.dimen_90)
                )
                .centerCrop()
                .transform(CircleTransformation())
                .into(ivUserProfile)
        }

        tvContestName.text = postContestObj.interest.interest
        btnLike.setImageResource(if (postContestObj.isVoted == "0") R.drawable.ic_thumb_up else R.drawable.voted_colored)
        tsLikesCounter.setCurrentText(
            itemView.context.resources.getQuantityString(
                R.plurals.votes_count, postContestObj.voteCount, postContestObj.voteCount
            )
        )

        if (postContestObj.userInfo != null && postContestObj.userInfo.id > 0 && postContestObj.userInfo.id == ArtalentApplication.prefManager.userObj.id) {
            btnLike.visibility = View.GONE
        } else {
            btnLike.visibility = View.VISIBLE
        }

        userNameLL.setOnClickListener {
            val userObj1: UserObj? = postContestObj.userInfo
            if (userObj1 != null) {
                val intent = Intent(itemView.context, OtherUserProfileActivity::class.java)
                intent.putExtra(Constants.DATA, userObj1)
                itemView.context.startActivity(intent)
            }
        }

        ivDelete.visibility = View.GONE
        if (postContestObj.userInfo != null && postContestObj.userInfo.id == ArtalentApplication.prefManager.userObj.id) {
            ivDelete.visibility = View.VISIBLE
        }
        ivDelete.tag = postContestObj
        ivDelete.setOnClickListener { v: View? ->
            mOnFeedItemClickListener!!.onDeleteClick(
                v!!.tag as PostContestObj,
                position
            )
        }

        btnLike.setOnClickListener {
            mOnFeedItemClickListener!!.onLikeClick(
                btnLike,
                position,
                tsLikesCounter
            )
        }

        btnShare.setOnClickListener {
            Log.d(TAG, "bind: btnShare btn clicked")

            val mShare = """
                Hey,I have participated in a contest of '${postContestObj.interest.interest}'
                Please vote to make me the winner of this contest.

                Download the app at :
                https://play.google.com/store/apps/details?id=com.artalent
                """.trimIndent()

            val sendIntent = Intent()
            sendIntent.action = Intent.ACTION_SEND
            sendIntent.putExtra(Intent.EXTRA_TEXT, mShare)
            sendIntent.type = "text/plain"
            it.context.startActivity(Intent.createChooser(sendIntent, "Share via"))

        }


//        ivUserProfile.setOnClickListener {
//            Log.d(TAG, "bind: NewVideoViewHolder -> onProfileClick")
//            mOnFeedItemClickListener!!.onProfileClick(itemView)
//        }
        btnRegister.setOnClickListener { mOnFeedItemClickListener!!.onRegisterClick(position) }

        Log.d("TAGs", "bind: $videoUrl")

        playerFrame.tag = false
        playerFrame.setOnClickListener {
            it.tag = !(it.tag as Boolean)
            if (DemoApp.helper != null) {
                if (DemoApp.helper!!.isPlaying) {
                    Handler().postDelayed({
                        DemoApp.helper!!.pause()
                    }, 1)
                } else {
                    Handler().postDelayed({
                        DemoApp.helper!!.play()
                    }, 1)
                }
            }
        }
        playerFrame.setAspectRatio(16f / 22f)
    }

    override fun getPlayerView() = player

    override fun getCurrentPlaybackInfo() = DemoApp.helper?.latestPlaybackInfo ?: PlaybackInfo()

    override fun initialize(
        container: Container,
        playbackInfo: PlaybackInfo
    ) {
        if (DemoApp.helper == null) DemoApp.helper =
            ExoPlayerViewHelper(this, videoUri!!, null, app.config)
        if (listener == null) {
            listener = object : EventListener {
                override fun onFirstFrameRendered() {
                    ivCameraAnimation.start()
                }

                override fun onBuffering() {
                    ivCameraAnimation.start()
                }

                override fun onPlaying() {
                    Log.e("onPlaying", "onPlaying")
                    ivCameraAnimation.stop()
                }

                override fun onPaused() {
                    ivCameraAnimation.stop()
                }

                override fun onCompleted() {
                    ivCameraAnimation.stop()
                }
            }
            DemoApp.helper!!.addPlayerEventListener(listener!!)
        }
        DemoApp.helper!!.initialize(container, playbackInfo)
    }

    override fun play() {
        Handler().postDelayed({
            DemoApp.helper!!.play()
        }, 1)
    }

    override fun pause() {
        DemoApp.helper!!.pause()
    }

    override fun isPlaying() = DemoApp.helper?.isPlaying ?: false

    override fun release() {
        if (listener != null) {
            DemoApp.helper?.removePlayerEventListener(listener)
            listener = null
        }
        DemoApp.helper?.release()
        DemoApp.helper = null
    }

    override fun wantsToPlay() = visibleAreaOffset(this, itemView.parent) >= 0.65

    override fun getPlayerOrder() = adapterPosition

}