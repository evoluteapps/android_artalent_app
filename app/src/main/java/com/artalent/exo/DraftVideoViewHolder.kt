package com.artalent.exo

import android.net.Uri
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.*
import com.artalent.R
import com.artalent.autoplayrecyclervideoplayer.view.CameraAnimation
import com.artalent.ui.model.room.ContestVideoDraft
import com.bumptech.glide.Glide
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout
import com.google.android.exoplayer2.ui.PlayerView
import im.ene.toro.ToroPlayer
import im.ene.toro.ToroUtil
import im.ene.toro.exoplayer.ExoPlayerViewHelper
import im.ene.toro.media.PlaybackInfo
import im.ene.toro.widget.Container
import java.io.File

private const val TAG = "DraftVideoViewHolder"

internal class DraftVideoViewHolder(
    inflater: LayoutInflater,
    parent: ViewGroup,
    onDraftFeedItemClickListener: DraftContestPostAdapter.OnDraftFeedItemClickListener
) : BaseViewHolder(inflater.inflate(R.layout.draft_exo_part_video, parent, false)),
    ToroPlayer {

    val app = itemView.context.applicationContext as DemoApp
    private var mOnDraftFeedItemClickListener: DraftContestPostAdapter.OnDraftFeedItemClickListener? =
        onDraftFeedItemClickListener

    private val playerFrame =
        itemView.findViewById(R.id.aspectRatioFrameLayout) as AspectRatioFrameLayout
    private val player = itemView.findViewById(R.id.player) as PlayerView
    private val status = itemView.findViewById(R.id.playerStatus) as TextView
    private val tvEdit = itemView.findViewById(R.id.tvEdit) as TextView
    private val ivCameraAnimation = itemView.findViewById(R.id.ivCameraAnimation) as CameraAnimation
    private val userNameLL = itemView.findViewById(R.id.user_name_ll) as LinearLayout
    private val tvUserName = itemView.findViewById(R.id.tvUserName) as TextView
    private val ivUserProfile = itemView.findViewById(R.id.ivUserProfile) as ImageView

    private var videoUri: Uri? = null
    var listener: ToroPlayer.EventListener? = null

    override fun bind(item: Any?) {
        super.bind(item)

        val draftObj = (item as ContestVideoDraft)

        var videoUrl = ""

        videoUri = Uri.fromFile(File(draftObj.storagePath))

        Glide.with(itemView.context)
            .load(Uri.fromFile(File(draftObj.storagePath)))
            .into(ivUserProfile)

        tvUserName.text = draftObj.title
        tvEdit.setOnClickListener {
            mOnDraftFeedItemClickListener?.onEditBtnClick(draftObj)
        }


        val imageViewArtwork = playerView.findViewById<ImageView>(R.id.exo_artwork)
        playerView.resizeMode = AspectRatioFrameLayout.RESIZE_MODE_FILL
        imageViewArtwork.scaleType = ImageView.ScaleType.CENTER_CROP
        Glide.with(itemView.context)
            .load(Uri.fromFile(File(draftObj.storagePath)))
            .placeholder(R.drawable.feed_placeholder)
            .error(R.drawable.feed_placeholder)
            .into(imageViewArtwork)

        Log.d("TAGs", "bind: $videoUrl")

        playerFrame.tag = false
        playerFrame.setOnClickListener {
            it.tag = !(it.tag as Boolean)
            if (DemoApp.helper != null) {
                if (DemoApp.helper!!.isPlaying) {
                    Handler().postDelayed({
                        DemoApp.helper!!.pause()
                    }, 1)
                } else {
                    Handler().postDelayed({
                        DemoApp.helper!!.play()
                    }, 1)
                }
            }
        }

        playerFrame.setAspectRatio(16f / 22f)
    }

    override fun getPlayerView() = player

    override fun getCurrentPlaybackInfo() = DemoApp.helper?.latestPlaybackInfo ?: PlaybackInfo()

    override fun initialize(
        container: Container,
        playbackInfo: PlaybackInfo
    ) {
        DemoApp.helper = null
        if (DemoApp.helper == null) {
            DemoApp.helper = ExoPlayerViewHelper(this, videoUri!!, null, app.config)
            Log.d(TAG, "initialize: $videoUri")
        }
        if (listener == null) {
            listener = object : ToroPlayer.EventListener {
                override fun onFirstFrameRendered() {
                    ivCameraAnimation.start()
                }

                override fun onBuffering() {
                    ivCameraAnimation.start()
                }

                override fun onPlaying() {
                    Log.e("onPlaying", "onPlaying")
                    ivCameraAnimation.stop()
                }

                override fun onPaused() {
                    ivCameraAnimation.stop()
                }

                override fun onCompleted() {
                    ivCameraAnimation.stop()
                }
            }
            DemoApp.helper!!.addPlayerEventListener(listener!!)
        }
        DemoApp.helper!!.initialize(container, playbackInfo)
    }

    override fun play() {
        Handler().postDelayed({
            DemoApp.helper!!.play()
        }, 1)
    }

    override fun pause() {
        DemoApp.helper!!.pause()
    }

    override fun isPlaying() = DemoApp.helper?.isPlaying ?: false

    override fun release() {
        if (listener != null) {
            DemoApp.helper?.removePlayerEventListener(listener)
            listener = null
        }
        DemoApp.helper?.release()
        DemoApp.helper = null
    }

    override fun wantsToPlay() = ToroUtil.visibleAreaOffset(this, itemView.parent) >= 0.65

    override fun getPlayerOrder() = adapterPosition

}