/*
 * Copyright (c) 2018 Nam Nguyen, nam@ene.im
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.artalent.exo

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView.LayoutManager
import com.artalent.R
import im.ene.toro.CacheManager
import kotlinx.android.synthetic.main.activity_basic_list.toolbar
import kotlinx.android.synthetic.main.activity_basic_list.toolbar_layout
import kotlinx.android.synthetic.main.content_basic_list.container

class BasicListActivity : AppCompatActivity() {

    private val adapter = SectionContentAdapter()

    // private val disposable = CompositeDisposable()
    var arrayList = ArrayList<String>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_basic_list)
        setSupportActionBar(toolbar)
        toolbar_layout.title = "Material Motion"
        arrayList.add("https://artalent.nyc3.digitaloceanspaces.com/1589214755285-VID_20200511_220207.mp4")//Adding object in arraylist
        arrayList.add("https://storage.googleapis.com/material-design/publish/material_v_12/assets/0B14F_FSUCc01a05pM2FXWEN0b0U/responsive-01-durations-v1.mp4")
        arrayList.add("https://storage.googleapis.com/material-design/publish/material_v_12/assets/0B14F_FSUCc01YVB4OXVzV3NQR3M/responsive-02-feedback-v2.mp4")
        arrayList.add("")

        container.apply {
            adapter = this@BasicListActivity.adapter
            layoutManager = LinearLayoutManager(this@BasicListActivity) as LayoutManager?
            cacheManager = CacheManager.DEFAULT
        }
        adapter.updateElements(arrayList)
        /* disposable.add(Motion.contents()
             .subscribeOn(io()).observeOn(mainThread())
             .doOnNext { adapter.updateElements(arrayList) }
             .subscribe()
         )*/
    }

    override fun onDestroy() {
        super.onDestroy()
        // disposable.clear()
    }
}
