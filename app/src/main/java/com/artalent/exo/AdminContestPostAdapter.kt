/*
 * Copyright (c) 2018 Nam Nguyen, nam@ene.im
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.artalent.exo

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextSwitcher
import androidx.recyclerview.widget.RecyclerView.Adapter
import com.artalent.ui.model.PostAdminContestObj

/**
 * @author eneim (2018/01/23).
 */
class AdminContestPostAdapter(mActivity: Activity, var postObjs: List<PostAdminContestObj>) :
    Adapter<BaseViewHolder>() {
    var mPostObjs: List<PostAdminContestObj> = postObjs

    companion object {
        const val videoItem = "div > video"
        const val typeText = 1
        const val typeVideo = 2
    }

    fun setOnFeedItemClickListener(onFeedItemClickListener: OnFeedItemClickListener) {
        mOnFeedItemClickListener = onFeedItemClickListener
    }

    interface OnFeedItemClickListener {
        fun onCommentsClick(v: View?, position: Int)
        fun onVolumeClick(btnVolume: ImageView?, position: Int)
        fun onRegisterClick(position: Int)
        fun onProfileClick(v: View?)
        fun onLikeClick(btnLike: ImageView?, position: Int, tsLikesCounter: TextSwitcher?)
        fun onDeleteClick(postContestObj: PostAdminContestObj, position: Int)
    }

    private var mOnFeedItemClickListener: OnFeedItemClickListener? = null

    // private val elements = Elements()


    private var inflater: LayoutInflater? = null

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BaseViewHolder {
        if (inflater === null || inflater!!.context !== parent.context) {
            inflater = LayoutInflater.from(parent.context)
        }

        return if (viewType == typeVideo) AdminContestPostViewHolder(
            inflater!!,
            parent,
            mOnFeedItemClickListener!!
        )
        else TextViewHolder(inflater!!, parent)
    }

    override fun getItemCount() = this.mPostObjs.size

    override fun onBindViewHolder(
        holder: BaseViewHolder,
        position: Int
    ) {
        holder.bind(mPostObjs[position])
    }

    override fun getItemViewType(position: Int): Int {
        val item = mPostObjs[position]

        return if (item != null) typeVideo else typeText
    }
}