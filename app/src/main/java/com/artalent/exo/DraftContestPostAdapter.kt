package com.artalent.exo

import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.artalent.ui.model.room.ContestVideoDraft

class DraftContestPostAdapter(
    mActivity: Activity,
    private var draftObj: List<ContestVideoDraft>
) : RecyclerView.Adapter<BaseViewHolder>() {

    companion object {
        const val videoItem = "div > video"
        const val typeText = 1
        const val typeVideo = 2
    }

    interface OnDraftFeedItemClickListener {
        fun onEditBtnClick(draftObj: ContestVideoDraft)
    }

    private var mOnDraftFeedItemClickListener: OnDraftFeedItemClickListener? = null

    fun setOnFeedItemClickListener(onDraftFeedItemClickListener: OnDraftFeedItemClickListener) {
        mOnDraftFeedItemClickListener = onDraftFeedItemClickListener
    }

    private var inflater: LayoutInflater? = null

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BaseViewHolder {
        if (inflater === null || inflater!!.context !== parent.context) {
            inflater = LayoutInflater.from(parent.context)
        }
        return if (viewType == typeVideo) DraftVideoViewHolder(
            inflater!!,
            parent,
            mOnDraftFeedItemClickListener!!
        )
        else TextViewHolder(inflater!!, parent)
    }

    override fun getItemCount() = draftObj.size

    override fun onBindViewHolder(
        holder: BaseViewHolder,
        position: Int
    ) {
        holder.bind(draftObj[position])
    }

    override fun getItemViewType(position: Int): Int {
        val item = draftObj[position]
        return if (item != null) typeVideo else typeText

    }
}