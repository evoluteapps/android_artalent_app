package com.artalent

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

object DateFormatter {
    var DATE_FORMAT_3 = "yyyy-MM-dd HH:mm:ss"
    val TIME_ZONE_UTC = "UTC"
    val dd_MM_yyyy_DASH = "dd-MM-yyyy"
    val yyyy_MM_dd_DASH = "yyyy-MM-dd"
    var dd_MM_yyyy_hh_mm_a_SLASH = "dd/MM/yyyy, hh:mm a"
    var hh_mm_a_SLASH = "hh:mm a"
    val yyyy_MM_DASH = "yyyy-MM"

    val timeStamp: String
        get() {
            return System.currentTimeMillis().toString()
        }

    fun getSimpleDateFormat(format: String, timeZone: TimeZone): SimpleDateFormat {
        val simpleDateFormat = SimpleDateFormat(format, Locale.ENGLISH)
        simpleDateFormat.timeZone = timeZone
        return simpleDateFormat
    }

    fun getFormattedByString(inFormat: String, outFormat: String, strDate: String): String {
        var sdf = SimpleDateFormat(inFormat, Locale.ENGLISH)
        sdf.timeZone = TimeZone.getTimeZone(TIME_ZONE_UTC)
        return try {
            val date = sdf.parse(strDate)
            sdf = SimpleDateFormat(outFormat, Locale.ENGLISH)
            sdf.timeZone = TimeZone.getDefault()
            sdf.format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
            " "
        }
    }

    fun convertDateToCalendarFormat(strDate: String?): IntArray? {
        val arr = IntArray(3)
        val cal = Calendar.getInstance()
        val sdf = SimpleDateFormat(yyyy_MM_dd_DASH, Locale.ENGLISH)
        try {
            cal.time = sdf.parse(strDate)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        arr[0] = cal[Calendar.YEAR]
        arr[1] = cal[Calendar.MONTH]
        arr[2] = cal[Calendar.DAY_OF_MONTH]
        return arr
    }

//    fun getFormattedByStringNormal(inFormat: String, outFormat: String, strDate: String): String {
//        var sdf = SimpleDateFormat(inFormat, Locale.ENGLISH)
//        return try {
//            val date = sdf.parse(strDate)
//            sdf = SimpleDateFormat(outFormat, Locale.ENGLISH)
//            sdf.format(date)
//        } catch (e: ParseException) {
//            e.printStackTrace()
//            " "
//        }
//    }

    fun getFormattedByStringTimestamp(dateFormat: String, timestamp: Long, timeZone: TimeZone): String? {
        var outDate: String? = null
        try {
            val dateFormatter =
                    SimpleDateFormat(dateFormat, Locale.ENGLISH)
            dateFormatter.timeZone = timeZone
            outDate = dateFormatter.format(Date(timestamp))
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return outDate
    }

    fun getDateByString(dateFormat: String, stringDate: String): Date? {
        val timeZone = TimeZone.getTimeZone("UTC")
        var date: Date? = null
        try {
            date = getSimpleDateFormat(dateFormat, timeZone).parse(stringDate)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return date
    }

    fun timeAgo(date: Date): String {
        val SECOND_MILLIS = 1000
        val MINUTE_MILLIS = 60 * SECOND_MILLIS
        val HOUR_MILLIS = 60 * MINUTE_MILLIS
        val DAY_MILLIS = 24 * HOUR_MILLIS

        val today = Calendar.getInstance()
        val cmgDay = Calendar.getInstance()
        cmgDay.time = date
        var time = cmgDay.timeInMillis

        if (time < 1000000000000L) {
            time *= 1000
        }
        val now = System.currentTimeMillis()

        val diff = now - time
        if (diff < MINUTE_MILLIS) {
            return "just now"
        } else if (diff < 2 * MINUTE_MILLIS) {
            return (diff / MINUTE_MILLIS).toString() + " min"
        } else if (diff < 50 * MINUTE_MILLIS) {
            return (diff / MINUTE_MILLIS).toString() + " mins"
        } else if (diff < 90 * MINUTE_MILLIS) {
            return "1hr"
        } else if (diff < 24 * HOUR_MILLIS) {
            return (diff / HOUR_MILLIS).toString() + " hrs"
        } else if (diff < 48 * HOUR_MILLIS) {
            return "1 day"
        } else {
            if (diff / DAY_MILLIS > 7) {
                val showFormatter =
                        SimpleDateFormat(dd_MM_yyyy_hh_mm_a_SLASH)

                return showFormatter.format(date)
            } else {
                return (diff / DAY_MILLIS).toString() + " days"
            }
        }
    }

    fun timeAgoTest(date: Date): String {
        val SECOND_MILLIS = 1000
        val MINUTE_MILLIS = 60 * SECOND_MILLIS
        val HOUR_MILLIS = 60 * MINUTE_MILLIS
        val DAY_MILLIS = 24 * HOUR_MILLIS

        val today = Calendar.getInstance()
        val cmgDay = Calendar.getInstance()
        cmgDay.time = date
        var time = cmgDay.timeInMillis

        if (time < 1000000000000L) {
            time *= 1000
        }
        val now = System.currentTimeMillis()

        val diff = now - time
        if (diff < MINUTE_MILLIS) {
            return "just now"
        } else if (diff < 2 * MINUTE_MILLIS) {
            return (diff / MINUTE_MILLIS).toString() + " min ago"
        } else if (diff < 50 * MINUTE_MILLIS) {
            return (diff / MINUTE_MILLIS).toString() + " mins ago"
        } else if (diff < 90 * MINUTE_MILLIS) {
            return "1hr ago"
        } else if (diff < 24 * HOUR_MILLIS) {
            return (diff / HOUR_MILLIS).toString() + " hrs ago"
        } else if (diff < 48 * HOUR_MILLIS) {
            return "1 day ago"
        } else {
            //  if (diff / DAY_MILLIS > 7) {
            // val showFormatter =
            //    SimpleDateFormat(dd_MM_yyyy_hh_mm_a_SLASH)

            //  return showFormatter.format(date)
            //} else {
            return (diff / DAY_MILLIS).toString() + " days ago"
            //}
        }
    }

    fun timeAgoSec(date: Date): Long {
        val SECOND_MILLIS = 1000
        val MINUTE_MILLIS = 60 * SECOND_MILLIS
        val HOUR_MILLIS = 60 * MINUTE_MILLIS
        val DAY_MILLIS = 24 * HOUR_MILLIS

        val today = Calendar.getInstance()
        val cmgDay = Calendar.getInstance()
        cmgDay.time = date
        var time = cmgDay.timeInMillis

        if (time < 1000000000000L) {
            time *= 1000
        }
        val now = System.currentTimeMillis()

        val diff = now - time
        return (diff / 1000)
    }
}
