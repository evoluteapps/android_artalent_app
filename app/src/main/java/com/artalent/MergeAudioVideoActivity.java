/*
 * Copyright (c) 2018 Taner Sener
 *
 * This file is part of MobileFFmpeg.
 *
 * MobileFFmpeg is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MobileFFmpeg is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with MobileFFmpeg.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.artalent;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;

import com.artalent.ui.activity.BaseActivityOther;
import com.artalent.ui.video.VideoConstants;
import com.coremedia.iso.boxes.Container;
import com.googlecode.mp4parser.authoring.Movie;
import com.googlecode.mp4parser.authoring.Track;
import com.googlecode.mp4parser.authoring.builder.DefaultMp4Builder;
import com.googlecode.mp4parser.authoring.container.mp4.MovieCreator;
/*import com.arthenica.mobileffmpeg.Config;
import com.arthenica.mobileffmpeg.FFmpeg;
import com.arthenica.mobileffmpeg.LogCallback;
import com.arthenica.mobileffmpeg.LogMessage;*/

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Queue;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.TimeUnit;

public class MergeAudioVideoActivity extends BaseActivityOther {
    private String outputPath = "";
    File fileVideo = null;
    File fileAudio = null;
    //FFmpeg ffmpeg;

    private static File videoResult;
    private static File audioResult;
    private static long elapsedVideoDur;
    private static long maxVideoDur;
    private static String selectedAudioPath;

    public static void setVideoResult(@Nullable File result, @Nullable File resultAudio, long elapsedVideoDuration,
                                      long maxVideoDuration, String audioPath) {
        videoResult = result;
        audioResult = resultAudio;
        elapsedVideoDur = elapsedVideoDuration;
        maxVideoDur = maxVideoDuration;
        selectedAudioPath = audioPath;

    }

    @SuppressLint("WrongConstant")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_merge_audio_video);
        fileVideo = videoResult;
        fileAudio = audioResult;
        if (fileAudio == null) {
            finish();
            return;
        }


        //enableLogCallback();
        /*new Handler().postDelayed(() -> {
            check = false;
            waitForUIAction();
            ;
        }, 1000);*/
        //ffmpeg = FFmpeg.getInstance(MergeAudioVideoActivity.this);
        /*try {
            ffmpeg.loadBinary(new LoadBinaryResponseHandler() {

                @Override
                public void onStart() {
                }

                @Override
                public void onFailure() {
                }

                @Override
                public void onSuccess() {
                }

                @Override
                public void onFinish() {
                }
            });
        } catch (FFmpegNotSupportedException e) {
            // Handle if FFmpeg is not supported by device
        }*/
        runFFmpeg();

        View runFFmpegButton = findViewById(R.id.runFFmpegButton);
        runFFmpegButton.setVisibility(View.GONE);
        runFFmpegButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // check = false;
                // waitForUIAction();
                // runFFmpeg();

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        //  setActive();
    }

   /* public void enableLogCallback() {
        Config.enableLogCallback(new LogCallback() {
            @Override
            public void apply(final LogMessage message) {
                addUIAction(new Callable() {
                    @Override
                    public Object call() {
                        appendLog(message.getText());
                        return null;
                    }
                });
            }
        });
    }*/


    public void runFFmpeg() {
        clearLog();
        //File fileVideo = new File("/storage/emulated/0/dawcheck/video_art.mp4");
        //File fileAudio = new File("/storage/emulated/0/dawcheck/audio.mp3");
        DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
        Date date = new Date();
        String fileName = dateFormat.format(date);
        outputPath = "";
        String artalentDirectory = Data.getSaveDir();
        outputPath = artalentDirectory + fileName + ".mp4";
        /*if(elapsedVideoDur != maxVideoDur){
            String croppedAudioOutPath = artalentDirectory + fileName + ".mp3";
            String endTime = convertMilliToTimeFormat(elapsedVideoDur);
            String[] commandStr = new String[]{"-y", "-i", "/storage/emulated/0/shweta.mp3", "-strict", "experimental", "-acodec", "copy", "-ss", "00:00:00", "-t", endTime, croppedAudioOutPath};
            int result = FFmpeg.execute(commandStr);//1
            if (result != 0) {
                Utils.showToast(MergeAudioVideoActivity.this, "Command failed. Please check output for the details.");
            }else {
                String[] complexCommand = new String[]{"-y", "-i", fileVideo.toString(), "-i", croppedAudioOutPath, "-c", "copy", "-map", "0:v:0", "-map", "1:a:0", outputPath};
                int res = FFmpeg.execute(complexCommand);//1
                if (res != 0) {
                    Utils.showToast( .this, "Command failed. Please check output for the details.");
                }
            }
        } else {*/
//        String[] command = new String[]{"-y", "-i", "/storage/emulated/0/Artalent/2020_04_06_17_44_24.mp4", "-filter_complex", "[0:v]setpts=2.0*PTS[v];[0:a]atempo=0.5[a]", "-map", "[v]", "-map", "[a]", "-b:v", "2097k", "-r", "60", "-vcodec", "mpeg4", "/storage/emulated/0/Artalent/slow_motion_test.mp4"};

        /*String[] complexCommand = new String[]{"-y", "-i", fileVideo.toString(), "-i", fileAudio.toString(), "-c", "copy", "-map", "0:v:0", "-map", "1:a:0", outputPath};
            int result = FFmpeg.execute(complexCommand);//1
            if (result != 0) {
                Utils.showToast(MergeAudioVideoActivity.this, "Command failed. Please check output for the details.");
        }*/
        String[] complexCommand = new String[]{"-y", "-i", fileVideo.toString(), "-i", fileAudio.toString(), "-c", "copy", "-map", "0:v:0", "-map", "1:a:0", outputPath};


        File sdCard = Environment.getDataDirectory();

        String videoFilePath = fileVideo.toString();
        String audioFilePath = fileAudio.toString();
        mux(videoFilePath, audioFilePath, outputPath);



        /*try {
            // to execute "ffmpeg -version" command you just need to pass "-version"
            ffmpeg.execute(complexCommand, new ExecuteBinaryResponseHandler() {

                @Override
                public void onStart() {
                    Log.e("onFailure", "start");
                }

                @Override
                public void onProgress(String message) {
                    Log.e("onFailure", message);
                }

                @Override
                public void onFailure(String message) {
                    Log.e("onFailure", message);
                }

                @Override
                public void onSuccess(String message) {
                    Log.e("onFailure", message);
                    File file = new File(outputPath);
                    if (file.exists()) {
                        Intent intent = new Intent();
                        intent.putExtra(VideoConstants.EXTRA_VIDEO_PATH, outputPath);
                        setResult(RESULT_OK, intent);
                        finish();

                    }

                }

                @Override
                public void onFinish() {
                    Log.e("onFailure", "onFinish");
                    File file = new File(outputPath);
                    if (file.exists()) {
                        Intent intent = new Intent();
                        intent.putExtra(VideoConstants.EXTRA_VIDEO_PATH, outputPath);
                        setResult(RESULT_OK, intent);
                        finish();

                    }

                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {
            Log.e("onFailure", "FFmpegCommandAlreadyRunningException" + e.getMessage());
        }*/


    }


    private String convertMilliToTimeFormat(long millis) {
        return String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis),
                TimeUnit.MILLISECONDS.toMinutes(millis) % TimeUnit.HOURS.toMinutes(1),
                TimeUnit.MILLISECONDS.toSeconds(millis) % TimeUnit.MINUTES.toSeconds(1));
    }


    public void appendLog(final String logMessage) {
        if (!check) {
            try {
                File file = new File(outputPath);
                if (file.exists()) {
                    check = true;
                    //FFmpeg.cancel();
                    Intent intent = new Intent();
                    intent.putExtra(VideoConstants.EXTRA_VIDEO_PATH, outputPath);
                    setResult(RESULT_OK, intent);
                    finish();

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void clearLog() {
    }

    /*
   ffmpag
    */

    /*public static void waitForUIAction() {
        handler.postDelayed(runnable, 250);
    }*/

    public static void addUIAction(final Callable callable) {
        actionQueue.add(callable);
    }

    protected static final Queue<Callable> actionQueue = new ConcurrentLinkedQueue<>();
    //protected static final Handler handler = new Handler();
    static boolean check = false;
  /*  protected static final Runnable runnable = new Runnable() {
        @Override
        public void run() {
            Callable callable;
            do {
                callable = actionQueue.poll();
                if (callable != null) {
                    try {
                        callable.call();
                    } catch (final Exception e) {
                    }
                }
            } while (callable != null);
            if (!check) {
                handler.postDelayed(this, 250);
            }
        }
    };*/

    public boolean mux(String videoFile, String audioFile, String outputFile) {
        Movie video;
        try {
            video = new MovieCreator().build(videoFile);

        } catch (RuntimeException e) {
            e.printStackTrace();

            return false;
        } catch (IOException e) {
            e.printStackTrace();

            return false;
        }

        Movie audio;
        try {

            audio = new MovieCreator().build(audioFile);

        } catch (IOException e) {
            e.printStackTrace();

            return false;
        } catch (NullPointerException e) {
            e.printStackTrace();

            return false;
        }

        Track audioTrack = audio.getTracks().get(0);
        video.addTrack(audioTrack);

        Container out = new DefaultMp4Builder().build(video);

        FileOutputStream fos;
        try {
            fos = new FileOutputStream(outputFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        }
        BufferedWritableFileByteChannel byteBufferByteChannel = new BufferedWritableFileByteChannel(fos);
        try {

            out.writeContainer(byteBufferByteChannel);
            byteBufferByteChannel.close();
            Log.e("Audio Video", "11");
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        File file = new File(outputPath);
        if (file.exists()) {
            Intent intent = new Intent();
            intent.putExtra(VideoConstants.EXTRA_VIDEO_PATH, outputPath);
            setResult(RESULT_OK, intent);
            finish();

        }
        return true;
    }

    private static class BufferedWritableFileByteChannel implements WritableByteChannel {
        //    private static final int BUFFER_CAPACITY = 1000000;
        private static final int BUFFER_CAPACITY = 10000000;

        private boolean isOpen = true;
        private final OutputStream outputStream;
        private final ByteBuffer byteBuffer;
        private final byte[] rawBuffer = new byte[BUFFER_CAPACITY];

        private BufferedWritableFileByteChannel(OutputStream outputStream) {
            this.outputStream = outputStream;
            this.byteBuffer = ByteBuffer.wrap(rawBuffer);
            Log.e("Audio Video", "13");
        }

        @Override
        public int write(ByteBuffer inputBuffer) throws IOException {
            int inputBytes = inputBuffer.remaining();

            if (inputBytes > byteBuffer.remaining()) {
                Log.e("Size ok ", "song size is ok");
                dumpToFile();
                byteBuffer.clear();

                if (inputBytes > byteBuffer.remaining()) {
                    Log.e("Size ok ", "song size is not okssss ok");
                    throw new BufferOverflowException();
                }
            }

            byteBuffer.put(inputBuffer);

            return inputBytes;
        }

        @Override
        public boolean isOpen() {
            return isOpen;
        }

        @Override
        public void close() throws IOException {
            dumpToFile();
            isOpen = false;
        }

        private void dumpToFile() {
            try {
                outputStream.write(rawBuffer, 0, byteBuffer.position());
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

}
