package com.artalent;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.os.Build;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.Log;
import android.util.Patterns;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.URLUtil;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.artalent.ui.model.SpecialContestWinnerObj;
import com.artalent.ui.model.UserObj;
import com.artalent.ui.utils.CircleTransformation;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.squareup.picasso.Picasso;

import org.aviran.cookiebar2.CookieBar;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import static android.content.Context.INPUT_METHOD_SERVICE;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

public class Utils {
    private static int screenWidth = 0;
    private static int screenHeight = 0;

    public static void hideSoftKeyboard(Activity activity, View editText) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }

    public static void setImage(Context activity, String path, ImageView imageView) {
        Glide.with(activity)
                .load(path).apply(new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.interest_categories)
                .error(R.drawable.interest_categories)).into(imageView);
    }

    public static void setImageRounded(Context activity, String path, ImageView imageView) {
        Glide.with(activity)
                .load(path).apply(new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.interest_categories)
                .transform(new RoundedCorners(120))
                .error(R.drawable.interest_categories)).into(imageView);
    }

    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static int getScreenHeight(Context c) {
        if (screenHeight == 0) {
            WindowManager wm = (WindowManager) c.getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            screenHeight = size.y;
        }
        return screenHeight;
    }


    public static void updateToolbar(Activity activity, int color, boolean isHide) {
        Objects.requireNonNull(((AppCompatActivity) activity).getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(activity.getResources().getColor(color)));
        if (isHide)
            Objects.requireNonNull(((AppCompatActivity) activity).getSupportActionBar()).hide();
        else Objects.requireNonNull(((AppCompatActivity) activity).getSupportActionBar()).show();
    }


    public static int getScreenWidth(Context c) {
        if (screenWidth == 0) {
            WindowManager wm = (WindowManager) c.getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            screenWidth = size.x;
        }
        return screenWidth;
    }

    public static boolean isAndroid5() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
    }


    public static String getCurrentDate() {
        Date d = new Date();
        CharSequence s = DateFormat.format("yyyy-MM-dd,  ", d.getTime());
        return s.toString();
    }

    public static long getCurrentDiffDate(String sCurrentDate, String sContestRegEndDate) {
        long days = -1;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date dateCurrent = sdf.parse(sCurrentDate);
            Date dateRegEndDate = sdf.parse(sContestRegEndDate);
            long diff = dateRegEndDate.getTime() - dateCurrent.getTime();
            long seconds = diff / 1000;
            long minutes = seconds / 60;
            long hours = minutes / 60;
            days = (hours / 24);
            Log.d("days", "" + days);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return days;
    }

    public static void showToastForOtp(Activity context, String msg) {
        try {
            CookieBar.build(context)
                    .setTitle(R.string.app_name)
                    .setBackgroundColor(R.color.white)
                    .setTitleColor(R.color.red)
                    .setMessage(msg)
                    .setMessageColor(R.color.red)
                    .setIcon(R.drawable.ic_info_red_24dp)
                    .setDuration(1000)
                    .setCookieListener(i -> {
                    })
                    .show();
        } catch (Exception e) {
            Log.d("Toast", "showToast: issue -> " + e.getMessage());
            e.printStackTrace();
        }
    }

    public static void showToast(Activity context, String msg) {
        try {
            CookieBar.build(context)
                    .setTitle(R.string.app_name)
                    .setBackgroundColor(R.color.primary)
                    .setTitleColor(R.color.white)
                    .setMessage(msg)
                    .setMessageColor(R.color.white)
                    .setIcon(R.drawable.ic_info_white_24dp)
                    .setDuration(1000)
                    .setCookieListener(i -> {
                    })
                    .show();
        } catch (Exception e) {
            Log.d("Toast", "showToast: issue -> " + e.getMessage());
            e.printStackTrace();
        }
    }

    public static void showToastWithTitle(Activity context, String msg, String title) {
        try {
            CookieBar.build(context)
                    .setTitle(title)
                    .setBackgroundColor(R.color.primary)
                    .setTitleColor(R.color.white)
                    .setMessage(msg)
                    .setMessageColor(R.color.white)
                    .setIcon(R.drawable.ic_info_white_24dp)
                    .setDuration(1000)
                    .setCookieListener(i -> {
                    })
                    .show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void setImageFromURL(String URL, int avatarSize, ImageView viewID) {
        if (!TextUtils.isEmpty(URL)) {
            Picasso.get()
                    .load(URL)
                    .placeholder(R.drawable.person_placeholder)
                    .resize(avatarSize, avatarSize)
                    .centerCrop()
                    .transform(new CircleTransformation())
                    .into(viewID);
        } else {
            Picasso.get()
                    .load(R.drawable.person_placeholder)
                    .placeholder(R.drawable.person_placeholder)
                    .resize(avatarSize, avatarSize)
                    .centerCrop()
                    .transform(new CircleTransformation())
                    .into(viewID);
        }
    }

    public static boolean isInternetAvailable(Context context) {
        ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

    public static String getFullName(UserObj userObj) {
        String fullName = "";
        if (userObj != null) {
            if (!TextUtils.isEmpty(userObj.getFirstName())) {
                String str = userObj.getFirstName();
                fullName = str.substring(0, 1).toUpperCase() + str.substring(1);
            }
            if (!TextUtils.isEmpty(userObj.getLastName())) {
                String str = userObj.getLastName();
                String ln = str.substring(0, 1).toUpperCase() + str.substring(1);
                fullName = fullName + " " + ln;
            }
        }
        return fullName;
    }

    @Target(PARAMETER)
    @Retention(RUNTIME)
    public @interface Chunked {
    }

    public static double getSpecialContestPrice(List<SpecialContestWinnerObj> winnerContestWinnerList) {
        double price = 0.0;
        for (SpecialContestWinnerObj specialContestWinnerObj : winnerContestWinnerList) {
            if (specialContestWinnerObj.getPrice() != null && !TextUtils.isEmpty(specialContestWinnerObj.getPrice())) {
                try {
                    double objPrice = Double.parseDouble(specialContestWinnerObj.getPrice());
                    price = price + objPrice;
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
            }
        }
        return price;
    }

    public static boolean isValidUrl(String urlString) {
        try {
            new URL(urlString);
            return URLUtil.isValidUrl(urlString) && Patterns.WEB_URL.matcher(urlString).matches();
        } catch (MalformedURLException ignored) {
        }
        return false;
    }
}
