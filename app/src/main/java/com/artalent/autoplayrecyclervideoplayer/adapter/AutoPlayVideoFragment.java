package com.artalent.autoplayrecyclervideoplayer.adapter;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextSwitcher;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.artalent.ArtalentApplication;
import com.artalent.R;
import com.artalent.Utils;
import com.artalent.autoplayrecyclervideoplayer.view.CenterLayoutManager;
import com.artalent.ui.activity.ContestRegisterUpdateActivity;
import com.artalent.ui.fragment.BaseFragment;
import com.artalent.ui.model.BaseResponse;
import com.artalent.ui.model.PostContestObj;
import com.artalent.ui.model.PostContestResponse;
import com.artalent.ui.model.VoteResponse;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import butterknife.BindView;
import butterknife.ButterKnife;
import im.ene.toro.CacheManager;
import im.ene.toro.widget.Container;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import com.artalent.exo.NewContestPostAdapter;

public class AutoPlayVideoFragment extends BaseFragment implements NewContestPostAdapter.OnFeedItemClickListener {

    @BindView(R.id.container)
    public Container container;

    @BindView(R.id.pullToRefresh)
    SwipeRefreshLayout pullToRefresh;

    @BindView(R.id.tvNoResult)
    TextView tvNoResult;

    private View view;
    private NewContestPostAdapter adapter;
    private int offset = 0;
    private boolean loading = true;
    boolean isMuted = false;
    private AlertDialog alertDialog;
    private List<PostContestObj> postObjs = new ArrayList<>();

    private static final String TAG = "AutoPlayVideoFragment";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_autoplay_video, container, false);
        ButterKnife.bind(this, view);
        Log.d(TAG, "onCreateView: called");

        initScrollListener();
        refresh();
        pullToRefresh.setOnRefreshListener(this::refresh);
        return view;
    }

    public void refresh() {
        Log.d(TAG, "refresh: called");
        offset = 0;
        postObjs.clear();
        callGetAllPostAPI();
    }

    private void initView() {
        Log.d(TAG, "initView: called");

        int pos = -1;
        for (int i = 0; i < postObjs.size(); i++) {
            if (postObjs.get(i) == null) pos = i;
        }
        if (pos != -1) postObjs.remove(pos);

        adapter = new NewContestPostAdapter(requireContext(), postObjs);
        adapter.setOnFeedItemClickListener(this);

        container.setLayoutManager(new CenterLayoutManager(getActivity()));
        container.setAdapter(adapter);
        container.setCacheManager(CacheManager.DEFAULT);
    }

    private void callGetAllPostAPI() {
        Log.d(TAG, "callGetAllPostAPI: called");
        if (Utils.isInternetAvailable(mActivity)) {
            Call<PostContestResponse> call = ArtalentApplication.apiService.getAllContestPost(ArtalentApplication.prefManager.getUserObj().getId(), offset);
            call.enqueue(new Callback<PostContestResponse>() {
                @Override
                public void onResponse(@NotNull Call<PostContestResponse> call, @NotNull Response<PostContestResponse> response) {
                    int statusCode = response.code();
                    pullToRefresh.setRefreshing(false);
                    loading = true;
                    if (statusCode == HttpsURLConnection.HTTP_OK) {
                        if (response.body() != null && response.body().getObj() != null) {
                            if (response.body().getObj().size() > 0) {
                                container.setVisibility(View.VISIBLE);
                                tvNoResult.setVisibility(View.GONE);
                                postObjs = response.body().getObj();
                                postObjs.removeIf(postContestObj -> postContestObj.getMediaUrls().size() == 0);
                                initView();
                            } else {
                                container.setVisibility(View.GONE);
                                tvNoResult.setVisibility(View.VISIBLE);
                            }
                            offset = response.body().getIndex();
                            Log.d(TAG, "onResponse: offset: " + offset);
                        }
                    } else {
                        container.setVisibility(View.GONE);
                        tvNoResult.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onFailure(Call<PostContestResponse> call, Throwable t) {
                    // Log error here since request failed
                    loading = true;
                    pullToRefresh.setRefreshing(false);
                }
            });
        } else pullToRefresh.setRefreshing(false);
    }

    private void initScrollListener() {
        Log.d(TAG, "initScrollListener: called");

        container.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (!recyclerView.canScrollVertically(1) && newState == RecyclerView.SCROLL_STATE_IDLE) {

                    Log.d(TAG, "onScrollStateChanged: Inside container scrollListener");

                    if (loading) {
                        if (offset != -1) {
                            Log.d(TAG, "onScrollStateChanged: Loading false called");
                            loading = false;
                            callGetAllPostAPI();
                        }
                    }
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onCommentsClick(View v, int position) {
    }

    @Override
    public void onVolumeClick(ImageView btnVolume, int position) {
    }

    @Override
    public void onRegisterClick(int position) {
        Intent intent = new Intent(mActivity, ContestRegisterUpdateActivity.class);
        startActivity(intent);
    }

    @Override
    public void onProfileClick(View v) {
    }

    @Override
    public void onLikeClick(ImageView btnLike, int position, TextSwitcher tsLikesCounter) {
        PostContestObj postObj = postObjs.get(position);
        if (postObj != null && postObj.getContestPostId() > 0)
            showCustomDialog(btnLike, position, postObj, tsLikesCounter);
    }

    private void showCustomDialog(ImageView btnLike, int position, PostContestObj postObj, TextSwitcher tsLikesCounter) {
        ViewGroup viewGroup = view.findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_vote, viewGroup, false);

        Button voteButton = dialogView.findViewById(R.id.rateButton);
        Button laterButton = dialogView.findViewById(R.id.laterButton);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(dialogView);
        alertDialog = builder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();
        View.OnClickListener mVoteButton = new View.OnClickListener() {
            public void onClick(View v) {
                callVoteApi(btnLike, position, postObj, tsLikesCounter);
            }
        };
        View.OnClickListener mLaterButton = v -> {
            alertDialog.dismiss();
        };
        laterButton.setOnClickListener(mLaterButton);
        voteButton.setOnClickListener(mVoteButton);
    }

    private void callVoteApi(ImageView btnLike, int position, PostContestObj postObj, TextSwitcher tsLikesCounter) {
        if (Utils.isInternetAvailable(mActivity)) {
            Call<VoteResponse> call = ArtalentApplication.apiService.callVote(ArtalentApplication.prefManager.getUserObj().getId(), postObj.getContestPostId());
            call.enqueue(new Callback<VoteResponse>() {
                @Override
                public void onResponse(Call<VoteResponse> call, Response<VoteResponse> response) {
                    int statusCode = response.code();
                    if (response.body() != null) {
                        if (postObj.getIsVoted().equals("0")) {
                            postObj.setVoteCount(postObj.getVoteCount() + 1);
                            btnLike.setImageResource(R.drawable.voted_colored);
                        }
                        tsLikesCounter.setCurrentText(getResources().getQuantityString(
                                R.plurals.votes_count, postObj.getVoteCount(), postObj.getVoteCount()
                        ));
                    }
                    alertDialog.dismiss();
                    Utils.showToast(getActivity(), response.body().getMessage());
                }

                @Override
                public void onFailure(Call<VoteResponse> call, Throwable t) {
                    // Log error here since request failed
                    alertDialog.dismiss();
                }
            });
        }
    }

    @Override
    public void onDeleteClick(@NotNull PostContestObj postContestObj, int position) {
        open(postContestObj, position);
    }

    public void open(PostContestObj postObj, int position) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setMessage("Are you sure, You want to delete Contest post?");
        alertDialogBuilder.setPositiveButton("yes",
                (arg0, arg1) -> {
                    arg0.dismiss();
                    deleteInterest(postObj, position);
                });
        alertDialogBuilder.setNegativeButton("No", (dialog, which) -> dialog.dismiss());
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void deleteInterest(PostContestObj postObj, int position) {
        if (Utils.isInternetAvailable(mActivity)) {
            ArtalentApplication.apiService.deleteContestPost(ArtalentApplication.prefManager.getUserObj().getId(), postObj.getContestPostId())
                    .enqueue(new Callback<BaseResponse>() {
                        @Override
                        public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                            if (response.body() != null) {
                                postObjs.remove(position);
                                adapter.notifyDataSetChanged();
                            }
                        }

                        @Override
                        public void onFailure(Call<BaseResponse> call, Throwable t) {
                        }
                    });
        }
    }
}