package com.artalent.autoplayrecyclervideoplayer.mode;

import java.io.Serializable;

/**
 * Created by HoangAnhTuan on 1/21/2018.
 */

public class Feed implements Serializable {

    private Info info;

    public Feed() {
    }

    public Feed(Info info) {
        this.info = info;

    }

    public Info getInfo() {
        return info;
    }

    public void setInfo(Info info) {
        this.info = info;
    }

}
