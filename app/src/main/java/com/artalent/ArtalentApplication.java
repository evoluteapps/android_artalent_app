package com.artalent;

import android.content.res.Resources;
import android.util.Log;

import androidx.appcompat.app.AppCompatDelegate;

import com.artalent.exo.DemoApp;
import com.artalent.ui.rest.ApiClient;
import com.artalent.ui.rest.ApiInterface;
import com.artalent.ui.utils.PrefManager;
import com.tonyodev.fetch2.Fetch;
import com.tonyodev.fetch2.FetchConfiguration;
import com.tonyodev.fetch2.HttpUrlConnectionDownloader;
import com.tonyodev.fetch2core.Downloader;


/**
 * Created by Akshay on 05.09.19.
 */
public class ArtalentApplication extends DemoApp {
    public static ApiInterface apiService;
    public static ApiInterface apiServicePost;
    public static ApiInterface apiServiceOTP;
    public static ApiInterface apiRazorpay;
    //public static ApiInterface apiServiceChunk;
    public static PrefManager prefManager;
    public static int deviceWidth = 0;

    @Override
    public void onCreate() {
        super.onCreate();
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        apiService = ApiClient.getClient().create(ApiInterface.class);
        apiServicePost = ApiClient.getClientPost().create(ApiInterface.class);
        apiServiceOTP = ApiClient.getClientOTP().create(ApiInterface.class);
        apiRazorpay = ApiClient.getRazorpayClient().create(ApiInterface.class);
        //apiServiceChunk = ApiClient.getChunkClient().create(ApiInterface.class);
        prefManager = new PrefManager(this);
        getWidth();
        final FetchConfiguration fetchConfiguration = new FetchConfiguration.Builder(this)
                .enableRetryOnNetworkGain(true)
                .setDownloadConcurrentLimit(3)
                .setHttpDownloader(new HttpUrlConnectionDownloader(Downloader.FileDownloaderType.PARALLEL))
                // OR
                //.setHttpDownloader(getOkHttpDownloader())
                .build();
        Fetch.Impl.setDefaultInstanceConfiguration(fetchConfiguration);
    }


    private void getWidth() {
        deviceWidth = Resources.getSystem().getDisplayMetrics().widthPixels;
        Log.e("widthTop", "width :" + deviceWidth);
    }
}
